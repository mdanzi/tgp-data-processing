version 1.0

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/JointVariantCalling.wdl" as JVC
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

workflow cohortLevelWorkflow {

  meta {
    description: "Joint calls a family."
  }

  input {
    Array[String] sampleGenomicIDs
    Array[String] sampleRunIDs
    Array[String] sampleGUIDs
    String cohortRunID

    File scattered_calling_intervals_list
    Array[File] scattered_calling_intervals = read_lines(scattered_calling_intervals_list)
    File all_calling_intervals

    String gatk_path

    # Runtime attribute overrides
    RuntimeAttr? runtime_attr_override_generateSampleMapFile
    RuntimeAttr? runtime_attr_override_calculateCNNAndROH
    RuntimeAttr? runtime_attr_override_importGVCFs
    RuntimeAttr? runtime_attr_override_genotypeGVCFs
    RuntimeAttr? runtime_attr_override_gatherVCFs
    RuntimeAttr? runtime_attr_override_vcfLevelQC
    RuntimeAttr? runtime_attr_override_createSVBamlets
    RuntimeAttr? runtime_attr_override_parseVCF
        
  }


  if (length(sampleGenomicIDs)==1) {
      # perform CNN filtering on the sample
      scatter (idx in range(length(sampleGenomicIDs))) {
        call JVC.calculateCNNAndROH as calculateCNNAndROHSingleton {
          input:
            sampleGenomicID = sampleGenomicIDs[idx],
            cohortRunID = cohortRunID,
            sampleRunID = sampleRunIDs[idx],
            sampleGUID = sampleGUIDs[idx],
            callingIntervals = all_calling_intervals,
            gatk_path = gatk_path,
            singleton = true,
            runtime_attr_override = runtime_attr_override_calculateCNNAndROH
        }
      }
      
      # perform VCF-level quality checking
      scatter (idx in range(length(sampleGenomicIDs))) {
        call JVC.vcfLevelQC as vcfLevelQCSingleton {
          input:
            sampleGenomicID = sampleGenomicIDs[idx],
            combinedVCF = cohortRunID + ".vcf.gz",
            sampleRunID = sampleRunIDs[idx],
            cohortRunID = cohortRunID,
            gatk_path = gatk_path,
            CNNVCFs = calculateCNNAndROHSingleton.CNNVCFName, # ensure this script waits until calculateCNNAndROH is done
            runtime_attr_override = runtime_attr_override_vcfLevelQC
        }
      }

      # perform SV bamlet creation
      scatter (idx in range(length(sampleGenomicIDs))) {
        call JVC.createSVBamlets as createSVBamletsSingleton {
          input:
            sampleGenomicID = sampleGenomicIDs[idx],
            sampleGenomicIDs = sampleGenomicIDs,
            sampleRunID = sampleRunIDs[idx],
            sampleRunIDs = sampleRunIDs,
            sampleGUID = sampleGUIDs[idx],
            cohortRunID = cohortRunID,
            runtime_attr_override = runtime_attr_override_createSVBamlets
        }
      }

      call JVC.parseVCF as parseVCFSingleton {
        input:
          combinedVCF = cohortRunID + ".vcf.gz",
          CNNVCFs = calculateCNNAndROHSingleton.CNNVCFName,
          sampleGenomicIDs = sampleGenomicIDs,
          sampleRunIDs = sampleRunIDs,
          cohortRunID = cohortRunID,
          runtime_attr_override = runtime_attr_override_parseVCF
      }

  }

  if (length(sampleGenomicIDs)>1) {
      call JVC.generateSampleMapFile as generateSampleMapFile {
        input:
          sample_names = sampleGenomicIDs,
          cohortRunID = cohortRunID,
          outfile = cohortRunID + "_sample_map_file.txt",
          runtime_attr_override = runtime_attr_override_generateSampleMapFile
        }

      # perform CNN filtering on each sample and determine ROHs (parallel across samples)
      scatter (idx in range(length(sampleGenomicIDs))) {
        call JVC.calculateCNNAndROH as calculateCNNAndROH {
          input:
            sampleGenomicID = sampleGenomicIDs[idx],
            cohortRunID = cohortRunID,
            sampleRunID = sampleRunIDs[idx],
            sampleGUID = sampleGUIDs[idx],
            callingIntervals = all_calling_intervals,
            gatk_path = gatk_path,
            runtime_attr_override = runtime_attr_override_calculateCNNAndROH
        }
      }

      # Perform Joint Variant Calling in parallel
      scatter (idx in range(length(scattered_calling_intervals))) {
          # Combine GVCFs by interval into a GenomicsDB
          call JVC.importGVCFs as importGVCFs {
              input:
                  sampleGenomicIDs = sampleGenomicIDs,
                  sampleRunIDs = sampleRunIDs,
                  cohortRunID = cohortRunID,
                  sample_name_map = generateSampleMapFile.sample_map,
                  interval = scattered_calling_intervals[idx],
                  workspace_dir_name = "genomicsdb",
                  gatk_path=gatk_path,
                  runtime_attr_override = runtime_attr_override_importGVCFs
          }

          # Joint genotype by interval
          call JVC.genotypeGVCFs as genotypeGVCFs {
            input:
              workspace_tar = importGVCFs.output_genomicsdb,
              interval = scattered_calling_intervals[idx],
              output_vcf_filename = cohortRunID + "." + idx + "vcf.gz",
              gatk_path=gatk_path,
              runtime_attr_override = runtime_attr_override_genotypeGVCFs
          }
      }

      call JVC.gatherVCFs as gatherVCFs {
        input:
          input_vcfs = genotypeGVCFs.output_vcf,
          output_vcf_name = cohortRunID + ".vcf.gz",
          cohortRunID = cohortRunID,
          gatk_path=gatk_path,
          runtime_attr_override = runtime_attr_override_gatherVCFs
      }
      
      # perform VCF-level quality checking (parallel across samples)
      scatter (idx in range(length(sampleGenomicIDs))) {
        call JVC.vcfLevelQC as vcfLevelQC {
          input:
            sampleGenomicID = sampleGenomicIDs[idx],
            combinedVCF = gatherVCFs.combinedVCFName,
            sampleRunID = sampleRunIDs[idx],
            cohortRunID = cohortRunID,
            gatk_path = gatk_path,
            CNNVCFs = calculateCNNAndROH.CNNVCFName, # ensure this script waits until calculateCNNAndROH is done
            runtime_attr_override = runtime_attr_override_vcfLevelQC
        }
      }

      # perform SV bamlet creation (parallel across samples)
      scatter (idx in range(length(sampleGenomicIDs))) {
        call JVC.createSVBamlets as createSVBamlets {
          input:
            sampleGenomicID = sampleGenomicIDs[idx],
            sampleGenomicIDs = sampleGenomicIDs,
            sampleRunID = sampleRunIDs[idx],
            sampleRunIDs = sampleRunIDs,
            sampleGUID = sampleGUIDs[idx],
            cohortRunID = cohortRunID,
            runtime_attr_override = runtime_attr_override_createSVBamlets
        }
      }

      call JVC.parseVCF as parseVCF {
        input:
          combinedVCF = gatherVCFs.combinedVCFName,
          CNNVCFs = calculateCNNAndROH.CNNVCFName,
          sampleGenomicIDs = sampleGenomicIDs,
          sampleRunIDs = sampleRunIDs,
          cohortRunID = cohortRunID,
          runtime_attr_override = runtime_attr_override_parseVCF
      }

  }
}