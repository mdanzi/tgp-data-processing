// OBSOLETE: This technique did not work out

def set_resource_label(run_id, task) {
    label_map = [:]
    label_value = "${run_id}-${task.process}-${task.index}-${task.attempt}"
    label_map.put("cromwell-task-id", label_value)
    return label_map
}

def init_repo = """
repo=/home/ec2-user/tgp-data-processing
if [[ ! -d \$repo ]]; then
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git \$repo
fi
"""


