def scattered_intervals = [
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part1.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part2.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part3.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part4.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part5.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part6.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part7.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part8.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part9.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part10.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part11.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part12.bed',
    's3://tgp-data-analysis/resources/gatk4hapcaller/scatterFiles/scatterIntervals_part13.bed'
]

def init_repo = """
repo=/home/ec2-user/tgp-data-processing
if [[ ! -d \$repo ]]; then
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git \$repo
fi
"""

def s3_processing_path(key, run_id) {
    return "s3://tgp-sample-processing/${run_id}/${key}"
}

def set_memory_directive(task) {
    increase_memory_flag = "${task}Memory" 
    increase_memory = params[increase_memory_flag]
    if (task == "callSv" && !increase_memory) {
      return '30 GB'
    }
    else if (task == "callSv" && increase_memory) {
      return '61 GB'
    }
}

def set_directive(directive, task) {
    increase_memory_flag = "${task}Memory" 
    increase_time_flag = "${task}Time"
    priority_queue_flag = "${task}PriorityQueue"
    increase_memory = params[increase_memory_flag]
    increase_time = params[increase_time_flag]
    use_priority_queue = params[priority_queue_flag]
    if (task == "callSv" && !increase_memory) {
      if (directive == "memory") { return '30 GB' }
      else if (directive == "queue") { return use_priority_queue ? "$params.large_queue" : "$params.large_spot_queue" }
      else if (directive == "cpus") { return 8 }
    }
    else if (task == "callSv" && increase_memory) {
      if (directive == "memory") { return '61 GB' }
      else if (directive == "queue") { return use_priority_queue ? "$params.xlargemem_queue" : "$params.xlargemem_spot_queue" }
      else if (directive == "cpus") { return 8 }
    }
    else if (task == "mergeVcfs" && !increase_memory) {
        if (directive == "memory") { return '7 GB' }
        else if (directive == "queue") { return use_priority_queue ? "$params.medium_queue" : "$params.medium_spot_queue" }
    }
    else if (task == "mergeVcfs" && increase_memory) {
        if (directive == "memory") { return '14 GB' }
        else if (directive == "queue") { return use_priority_queue ? "$params.large_queue" : "$params.large_spot_queue" }
    }
    else if (task == "fastQc" && !increase_memory) {
      if (directive == "memory") { return '2 GB' }
      else if (directive == "queue") { return use_priority_queue ? "$params.small_queue" : "$params.small_spot_queue" }
      else if (directive == "cpus") { return 2 }
    }
    else if (task == "fastQc" && increase_memory) {
      if (directive == "memory") { return '4 GB' }
      else if (directive == "queue") { return use_priority_queue ? "$params.medium_queue" : "$params.medium_spot_queue" }
      else if (directive == "cpus") { return 2 }
    }
    else if (task == "callTr") {
      if (directive == "memory") { return increase_memory ? '15 GB' : '7 GB' }
      if (directive == "queue") { 
        if (use_priority_queue) {
          return "$params.priority_queue"
        } else {
          return increase_memory ? "$params.medium_large_spot_queue" : "$params.medium_spot_queue"
        }
      }
      if (directive == "cpus") { return increase_memory ? 8 : 2 }
      if (directive == "time") { return increase_time ? '100h' : '36h' }
    }
}

process cramToBam {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '7 GB'
  queue "$params.medium_spot_queue"
  
  input:
  val sample_genomic_id
  val s3_ref_path
  path cram

  output:
  path "${sample_genomic_id}.bam"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline cram_to_bam \
    $sample_genomic_id $s3_ref_path $cram
  """
  
}

process bamToFastq {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '7 GB'
  queue "$params.medium_spot_queue"
  
  input:
  val sample_genomic_id
  val sample_run_id
  path bam

  output:
  tuple val("s3://${params.processing_bucket}/${sample_run_id}/${sample_genomic_id}_R1.fastq.gz"),
    val("s3://${params.processing_bucket}/${sample_run_id}/${sample_genomic_id}_R2.fastq.gz")
  // tuple val(s3_processing_path("${sample_genomic_id}_R1.fastq.gz", sample_run_id)),
  //       val(s3_processing_path("${sample_genomic_id}_R2.fastq.gz", sample_run_id))
  //tuple val("${sample_genomic_id}_R1.fastq.gz"), val("${sample_genomic_id}_R2.fastq.gz")

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline bam_to_fastq \
    $sample_genomic_id $sample_run_id $bam
  """
  
}

process alignPresharded {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 16
  memory '30 GB'
  queue "$params.priority_queue"
  
  input:
  val sample_genomic_id
  val sample_run_id
  tuple val(read1), val(read2)

  output:
  path "${sample_genomic_id}.sorted.bam"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline align_presharded \
    $sample_genomic_id $sample_run_id $read1 $read2
  """
  
}

process mergeBams {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"
  
  input:
  val sample_genomic_id
  val sample_run_id
  path "sorted?.bam"
  //tuple val(read1), val(read2)

  output:
  path "${sample_genomic_id}.sorted.bam"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline merge_bams \
    $sample_genomic_id $sample_run_id sorted*.bam
  """
  
}

process fastQc {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus "${set_directive('cpus', 'fastQc')}"
  memory "${set_directive('memory', 'fastQc')}"
  queue "${set_directive('queue', 'fastQc')}"
  
  input:
  val sample_genomic_id
  val sample_run_id
  val read1_csv
  val read2_csv

  output:
  path "${sample_genomic_id}.fastqLevel.qc"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline fastqc \
    $sample_genomic_id $sample_run_id $read1_csv $read2_csv
  """
}

process gatk4Prep {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"
  
  input:
  val sample_genomic_id
  val sample_run_id
  path bam
  val experiment_type

  output:
  val "${sample_genomic_id}.hg19.sorted.dedup.bam"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline gatk_prep \
    $sample_genomic_id $sample_run_id $bam $experiment_type
  """

}

process baseRecalibrator {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"

  
  input:
  val sample_genomic_id
  val sample_run_id
  val bam
  val recal_report
  each path(interval)

  output:
  path recal_report

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline base_recalibrator \
    $sample_genomic_id $sample_run_id $bam $recal_report $interval
  """
}

process gatherBqsrReports {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"
  
  input:
  path "recal_report?.csv"
  val bqsr_output

  output:
  path bqsr_output

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline gather_bqsr_reports \
    $bqsr_output recal_report*.csv
  """
}

process applyBqsr {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"
  
  input:
  val sample_run_id
  val bam
  path gather_report
  each path(interval)
  val output_bam

  output:
  path output_bam

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline apply_bqsr \
    $sample_run_id $bam $gather_report $interval $output_bam
  """
}

process gatherBams {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"
  
  input:
  val output_bam
  path "recal?.bam"

  output:
  tuple path(output_bam), path("${output_bam}.bai")
  //path output_bam
  //path "${output_bam}.bai"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline gather_bams \
    $output_bam recal*.bam
  """
}

process haplotypeCaller {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 1
  memory '7 GB'
  //queue "$params.priority_queue"
  queue "$params.medium_spot_queue"
  // put on large queue
  
  input:
  tuple path(input_bam), path(input_bam_bai)
  each path(interval)
  val output_gvcf

  output:
  tuple path(output_gvcf), path("${output_gvcf}.tbi")

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline haplotype_caller \
    $input_bam $interval $output_gvcf
  """
}

process mergeVcfs {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 1
  memory "${set_directive('memory', 'mergeVcfs')}"
  queue "${set_directive('queue', 'mergeVcfs')}"
  
  input:
  //tuple path("input?.vcf.gz"), path("input?.vcf.gz.tbi")
  path("input?.vcf.gz")
  path("input?.vcf.gz.tbi")
  val output_gvcf

  output:
  tuple path(output_gvcf), path("${output_gvcf}.idx")

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline merge_vcfs \
    $output_gvcf input*vcf.gz
  """
}

process cleanOutputs {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"
  
  input:
  val sample_run_id
  //tuple path("input?.vcf.gz"), path("input?.vcf.gz.tbi")
  tuple path(input_vcf), path(input_vcf_tbi)

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline clean_outputs \
    $sample_run_id $input_vcf
  """
}

process runPeddy {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '4 GB'
  queue "$params.small_spot_queue"
  
  input: 
  val sample_genomic_id
  val sample_run_id
  tuple path(input_bam), path(input_bam_bai)
  path peddy_sites
  path peddy_sites_idx

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline run_peddy \
    $sample_genomic_id $sample_run_id $input_bam $peddy_sites
  """
}

process customGenotypeCalls {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '2 GB'
  queue "$params.small_spot_queue"
  
  input: 
  val sample_genomic_id
  val sample_run_id
  tuple path(input_bam), path(input_bam_bai)
  path custom_sites
  path custom_sites_idx


  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline custom_genotype_calls \
    $sample_genomic_id $sample_run_id $input_bam $custom_sites
  """
}

process callTr {
  // This process has previously required additional resources
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus "${set_directive('cpus', 'callTr')}"
  memory "${set_directive('memory', 'callTr')}"
  time "${set_directive('time', 'callTr')}"
  queue "${set_directive('queue', 'callTr')}"

  
  input: 
  val sample_genomic_id
  val sample_run_id
  val sample_id
  val input_bam


  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline call_tr \
    $sample_genomic_id $sample_run_id $sample_id $input_bam
  """
}

process callTrWes {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory '7 GB'
  queue "$params.medium_spot_queue"
  maxRetries = 0

  
  input: 
  val sample_genomic_id
  val sample_run_id
  val sample_id
  val input_bam


  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline call_tr_wes \
    $sample_genomic_id $sample_run_id $sample_id $input_bam
  """
}

process callSv {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  //errorStrategy 'finish'
  container "$params.sv_container"
  //cpus "${set_directive('cpus', task.process)}"
  cpus 4
  memory "7 GB"
  queue "$params.mediumcpu_spot_queue"
  //queue "${set_directive('queue', task.process)}"
  
  input: 
  val sample_genomic_id
  val sample_run_id
  val input_bam
  path ref_fasta
  path ref_fasta_idx


  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline call_sv \
    $sample_genomic_id $sample_run_id $input_bam $ref_fasta
  """
}

process mitoPipeline {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '7 GB'
  queue "$params.medium_spot_queue"
  
  input: 
  val sample_genomic_id
  val sample_run_id
  val input_bam  // only needed to establish task dependency

  output:
  path "${sample_genomic_id}.MT.final.split.vcf"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline mito_pipeline \
    $sample_genomic_id $sample_run_id
  """
}


workflow {
  bqsr_output = "${params.sampleGenomicId}.hg19.recal_data.csv"
  bqsr_bam = "${params.sampleGenomicId}.hg19.aligned.duplicates_marked.recalibrated.bam"
  gathered_bam = "${params.sampleGenomicId}.hg19.bam"
  haplotype_output = "${params.sampleGenomicId}.hg19.vcf.gz" 
  if (params?.bam) {
    if (params.bam.endsWith(".cram")) {
      bam_input = cramToBam(params.sampleGenomicId, params.s3_ref_path, params.bam)
    }
    else {
      bam_input = params.bam
    }
    fastqs = bamToFastq(params.sampleGenomicId, params.sampleRunId, bam_input)
    aligned_bam = alignPresharded(params.sampleGenomicId, params.sampleRunId, fastqs)
    // Duplicate outputs from bamToFastq; can't find a way to parse fastqs channel
    // fastq1_csv = "s3://${params.processing_bucket}/${params.sampleRunId}/${params.sampleGenomicId}_R1.fastq.gz"
    // fastq2_csv = "s3://${params.processing_bucket}/${params.sampleRunId}/${params.sampleGenomicId}_R2.fastq.gz"
    // fastq1_csv = fastqs.map { it[0] }.reduce { a, b -> a + "," + b }
    // fastq2_csv = fastqs.map { it[1] }.reduce { a, b -> a + "," + b }
    fastq1_csv = fastqs.map { it[0] }
    fastq2_csv = fastqs.map { it[1] }
    // fastQc(params.sampleGenomicId, params.sampleRunId, fastqs[0], fastqs[1])
  }
  else {
    fastq1_csv = params.fastqs.collect { it[0] }.join(",")
    fastq2_csv = params.fastqs.collect { it[1] }.join(",")
    // fastQc(params.sampleGenomicId, params.sampleRunId, fastq1_csv, fastq2_csv)
    aligned_bams = alignPresharded(params.sampleGenomicId, params.sampleRunId, Channel.fromList(params.fastqs))
    aligned_bam = mergeBams(params.sampleGenomicId, params.sampleRunId, aligned_bams.toList())
  }
  fastQc(params.sampleGenomicId, params.sampleRunId, fastq1_csv, fastq2_csv)
  dedup_bam = gatk4Prep(params.sampleGenomicId, params.sampleRunId, aligned_bam, params.experimentType)
  base_recal_reports = baseRecalibrator(params.sampleGenomicId,
      params.sampleRunId, dedup_bam, bqsr_output, scattered_intervals)
  gather_report = gatherBqsrReports(base_recal_reports.toList(), bqsr_output)
  bqsr_bam_channel = applyBqsr(params.sampleRunId, dedup_bam, gather_report,
      scattered_intervals, bqsr_bam)
  gathered_bam_channel = gatherBams(gathered_bam, bqsr_bam_channel.toList())
  haplotype_channel = haplotypeCaller(gathered_bam_channel, scattered_intervals,
      haplotype_output)
  merged_gvcf_name = "${params.sampleGenomicId}.hg19.g.vcf" 
  haplotype_gvcfs_list = haplotype_channel.map { it[0] }.toList()
  haplotype_indexes_list = haplotype_channel.map { it[1] }.toList()
  merged_gvcf_channel = mergeVcfs(haplotype_gvcfs_list, haplotype_indexes_list, merged_gvcf_name)
  cleanOutputs(params.sampleRunId, merged_gvcf_channel)
  runPeddy(params.sampleGenomicId, params.sampleRunId, gathered_bam_channel, params.peddy_sites, params.peddy_sites_idx)
  customGenotypeCalls(params.sampleGenomicId, params.sampleRunId,
    gathered_bam_channel, params.custom_sites, params.custom_sites_idx)
  if (params.experimentType == 'WGS') {
    callTr(params.sampleGenomicId, params.sampleRunId, params.sampleGuid, dedup_bam)
  }
  else {
    callTrWes(params.sampleGenomicId, params.sampleRunId, params.sampleGuid, dedup_bam)
  }
  if (params.experimentType == 'WGS') {
    ref_fasta = "s3://tgp-data-analysis/ref/hs37d5.fa"
    ref_fasta_idx = "s3://tgp-data-analysis/ref/hs37d5.fa.fai"
    callSv(params.sampleGenomicId, params.sampleRunId, dedup_bam, ref_fasta, ref_fasta_idx)
    mitoVcf = mitoPipeline(params.sampleGenomicId, params.sampleRunId, dedup_bam)
  }
}
