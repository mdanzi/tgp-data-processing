process deep_variant_make_examples {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 1
  memory "3GB"
  queue "$params.small_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-deepvariant:1.8.0"

  input:
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  val model_type
  each chrom
  each task_num
  val num_tasks

  output:
  tuple(
    val(chrom),
    path("make_examples.tfrecord-0000${task_num}-of-0000${num_tasks}.gz"),
    path("make_examples.tfrecord-0000${task_num}-of-0000${num_tasks}.gz.example_info.json"),
    path("make_examples_call_variant_outputs.tfrecord-0000${task_num}-of-0000${num_tasks}.gz")
  )

  """
  $params.init_repo_cmd
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom $task_num $num_tasks
  """
}

// This runs eight instances in parallel, reducing idle time spent transferring
// the bam file to each instance and taking advantage of unused CPUs.
// Unfortunately, it appears there may be complications with running multiple
// instances of this in the same directory and on the same instance. Also, it
// appears that the runtime is longer when all CPUs are saturated. On a two
// core machine running a single task, it runs for two hours. On an 8-core
// machine running 8 tasks it takes three and a half hours and produces some
// warnings that don't seem to occur in the other case.
process deep_variant_make_examples_parallel {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 8
  memory "15GB"
  maxForks 1
  queue "$params.medium_large_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-deepvariant:1.8.0"

  input:
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  val model_type
  each chrom

  output:
  tuple(
    val(chrom),
    path("make_examples.tfrecord-00000-of-00008.gz"),
    path("make_examples.tfrecord-00001-of-00008.gz"),
    path("make_examples.tfrecord-00002-of-00008.gz"),
    path("make_examples.tfrecord-00003-of-00008.gz"),
    path("make_examples.tfrecord-00004-of-00008.gz"),
    path("make_examples.tfrecord-00005-of-00008.gz"),
    path("make_examples.tfrecord-00006-of-00008.gz"),
    path("make_examples.tfrecord-00007-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00000-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00001-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00002-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00003-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00004-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00005-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00006-of-00008.gz"),
    path("make_examples_call_variant_outputs.tfrecord-00007-of-00008.gz")
  )

  """
  $params.init_repo_cmd
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 0 8 &
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 1 8 &
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 2 8 &
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 3 8 &
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 4 8 &
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 5 8 &
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 6 8 &
  $params.pipeline_cli deep_variant_make_examples $bam $ref_fasta $model_type $chrom 7 8
  """
}

process deep_variant_call_variants {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 8
  memory "30GB"
  queue "$params.large_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-deepvariant:1.8.0"

  input:
  val model_type
  val num_tasks
  tuple(val(chrom), path(make_examples), path(make_examples_json), val(call_variant_outputs))

  output:
  tuple(
    val(chrom),
    val(call_variant_outputs),
    path("call_variants_output-00000-of-00001.tfrecord.gz")
  )
  
  """
  $params.init_repo_cmd
  $params.pipeline_cli deep_variant_call_variants $model_type $num_tasks
  """
}

process deep_variant_postprocess_variants {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 8
  memory "60GB"
  time "2h"
  queue "$params.xlargemem_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-deepvariant:1.8.0"

  input:
  val sample_genomic_id
  path(ref_fasta)
  path(ref_fasta_fai)
  val model_type
  val num_tasks
  tuple(val(chrom), path(make_examples), path(call_variant))

  output:
  path("${sample_genomic_id}.${chrom}.DV.vcf.gz")

  """
  $params.init_repo_cmd
  $params.pipeline_cli deep_variant_postprocess_variants $sample_genomic_id $ref_fasta $model_type $num_tasks $chrom
  """
}

process deep_variant_merge_vcfs {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 8
  memory "30GB"
  queue "$params.large_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-deepvariant:1.8.0"
  
  input:
  val sample_genomic_id
  val sample_run_id
  path vcfs

  output:
  path "${sample_genomic_id}.DV.vcf.gz"

  """
  $params.init_repo_cmd
  $params.pipeline_cli deep_variant_merge_vcfs $sample_genomic_id $sample_run_id ${vcfs.join(" ")}
  """
}
