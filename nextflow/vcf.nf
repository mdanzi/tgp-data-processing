process vcfInputQc {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '3.5 GB'
  queue "$params.small_spot_queue"
  
  input: 
  path vcf
  val sample_genomic_id
  val cohort_run_id
  val sample_id

  output:
  path "${cohort_run_id}.vcf.gz"

  """
  $params.init_repo_cmd
  $params.pipeline_cli vcf_input_qc $vcf $sample_genomic_id $cohort_run_id $sample_id
  """
}


process parseVcfInput {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '7 GB'
  queue "$params.medium_spot_queue"
  
  input: 
  path vcf
  val sample_genomic_id
  val cohort_run_id

  output:
  path "${sample_genomic_id}.parsed.txt"

  """
  $params.init_repo_cmd
  $params.pipeline_cli parse_vcf_input $vcf $sample_genomic_id $cohort_run_id
  """
}


workflow {
  vcf_qc = parseVcfInput(params.vcf, params.sampleGenomicId, params.cohortRunId)
  input_qc = vcfInputQc(params.vcf, params.sampleGenomicId, params.cohortRunId, params.sampleId)
}
