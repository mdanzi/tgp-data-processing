call_sv() {
  local sampleGenomicID=$1
  local sampleRunID=$2
  local input_bam=$3
  local ref_fasta=$4

  aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${input_bam} /home/dnanexus/in/ --region us-east-1 --only-show-errors
  aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${input_bam}.bai /home/dnanexus/in/  --region us-east-1 --only-show-errors

  cp ${ref_fasta} /home/dnanexus/in/
  cp ${ref_fasta}.fai /home/dnanexus/in/

  # Must be in /home/dnanexus to find parliament2.sh
  cd /home/dnanexus

  set +euo pipefail

  /home/dnanexus/parliament2.py \
    --bam ${input_bam} \
    --bai ${input_bam}.bai \
    --r hs37d5.fa \
    --fai hs37d5.fa.fai \
    --prefix ${sampleGenomicID} \
    --filter_short_contigs --manta --genotype || true

  set -euo pipefail 

  # fix chromosome naming in output vcf
  sed 's/^chr//' /home/dnanexus/out/${sampleGenomicID}.combined.genotyped.vcf > ${sampleGenomicID}.combined.genotyped.vcf

  # upload output to S3
  aws s3 cp ${sampleGenomicID}.combined.genotyped.vcf s3://tgp-sample-processing/${sampleRunID}/ --region us-east-1 --only-show-errors
}
