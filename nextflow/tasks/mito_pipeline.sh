mito_pipeline() {
  local sampleGenomicID=$1
  local sampleRunID=$2
  local input_bam=${sampleGenomicID}.hg19.sorted.dedup.bam

  s3_cp_from_processing $input_bam $sampleRunID
  s3_cp_from_processing $input_bam.bai $sampleRunID

  # get just the mitochondrial genome
  $GATK PrintReads \
    -R $REF/hs37d5.fa \
    -L MT \
    --read-filter MateOnSameContigOrNoMappedMateReadFilter \
    --read-filter MateUnmappedAndUnmappedReadFilter \
    -I $input_bam \
    --read-index $input_bam.bai \
    -O ${sampleGenomicID}.bam

  rm ${sampleGenomicID}.hg19.*

  # remove alignment info
  $GATK --java-options "-Xmx1000m" RevertSam \
    --INPUT ${sampleGenomicID}.bam \
    --OUTPUT_BY_READGROUP false \
    --OUTPUT ${sampleGenomicID}.reverted.bam \
    --VALIDATION_STRINGENCY LENIENT \
    --ATTRIBUTE_TO_CLEAR FT \
    --ATTRIBUTE_TO_CLEAR CO \
    --SORT_ORDER queryname \
    --RESTORE_ORIGINAL_QUALITIES false \
    --SANITIZE true \
    --KEEP_FIRST_DUPLICATE true

  mv ${sampleGenomicID}.reverted.bam ${sampleGenomicID}.bam

  # align and mark dups
  bwa_version="0.7.17-r1188"
  # bwa_version=$(bwa 2>&1 | grep -e '^Version' | sed 's/Version: //')
  $GATK --java-options "-Xms5000m" SamToFastq \
    --INPUT ${sampleGenomicID}.bam \
    --FASTQ /dev/stdout \
    --INTERLEAVE true \
    -NON_PF true | \
  bwa mem -K 100000000 -p -v 3 -t 2 -Y $REF/mitochondria/hs37d5.MT.fa /dev/stdin - 2> >(tee ${sampleGenomicID}.bwa.stderr.log >&2) | \
  $GATK --java-options "-Xms3000m" MergeBamAlignment \
    --VALIDATION_STRINGENCY SILENT \
    --EXPECTED_ORIENTATIONS FR \
    --ATTRIBUTES_TO_RETAIN X0 \
    --ATTRIBUTES_TO_REMOVE NM \
    --ATTRIBUTES_TO_REMOVE MD \
    --ALIGNED_BAM /dev/stdin \
    --UNMAPPED_BAM ${sampleGenomicID}.bam \
    --OUTPUT mba.bam \
    --REFERENCE_SEQUENCE $REF/mitochondria/hs37d5.MT.fa \
    --PAIRED_RUN true \
    --SORT_ORDER "unsorted" \
    --IS_BISULFITE_SEQUENCE false \
    --ALIGNED_READS_ONLY false \
    --CLIP_ADAPTERS false \
    --MAX_RECORDS_IN_RAM 2000000 \
    --ADD_MATE_CIGAR true \
    --MAX_INSERTIONS_OR_DELETIONS -1 \
    --PRIMARY_ALIGNMENT_STRATEGY MostDistant \
    --PROGRAM_RECORD_ID "bwamem" \
    --PROGRAM_GROUP_VERSION "${bwa_version}" \
    --PROGRAM_GROUP_COMMAND_LINE "bwa mem -K 100000000 -p -v 3 -t 2 -Y $REF/mitochondria/hs37d5.MT.fa" \
    --PROGRAM_GROUP_NAME "bwamem" \
    --UNMAPPED_READ_STRATEGY COPY_TO_TAG \
    --ALIGNER_PROPER_PAIR_FLAGS true \
    --UNMAP_CONTAMINANT_READS true \
    --ADD_PG_TAG_TO_READS false

  $GATK --java-options "-Xms4000m" MarkDuplicates \
    --INPUT mba.bam \
    --OUTPUT md.bam \
    --METRICS_FILE ${sampleGenomicID}.MT.dupsMetrics.txt \
    --VALIDATION_STRINGENCY SILENT \
    --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
    --ASSUME_SORT_ORDER "queryname" \
    --CLEAR_DT "false" \
    --ADD_PG_TAG_TO_READS false

  $GATK --java-options "-Xms4000m" SortSam \
    --INPUT md.bam \
    --OUTPUT ${sampleGenomicID}.MT.sorted.bam \
    --SORT_ORDER "coordinate" \
    --CREATE_INDEX true \
    --MAX_RECORDS_IN_RAM 300000

  # align and mark dups shifted
  $GATK --java-options "-Xms5000m" SamToFastq \
    --INPUT ${sampleGenomicID}.bam \
    --FASTQ /dev/stdout \
    --INTERLEAVE true \
    -NON_PF true | \
  bwa mem -K 100000000 -p -v 3 -t 2 -Y $REF/mitochondria/hs37d5.MT.shifted_by_8000_bases.fa /dev/stdin - 2> >(tee ${sampleGenomicID}.bwa.stderr.log >&2) | \
  $GATK --java-options "-Xms3000m" MergeBamAlignment \
    --VALIDATION_STRINGENCY SILENT \
    --EXPECTED_ORIENTATIONS FR \
    --ATTRIBUTES_TO_RETAIN X0 \
    --ATTRIBUTES_TO_REMOVE NM \
    --ATTRIBUTES_TO_REMOVE MD \
    --ALIGNED_BAM /dev/stdin \
    --UNMAPPED_BAM ${sampleGenomicID}.bam \
    --OUTPUT mba.bam \
    --REFERENCE_SEQUENCE $REF/mitochondria/hs37d5.MT.shifted_by_8000_bases.fa \
    --PAIRED_RUN true \
    --SORT_ORDER "unsorted" \
    --IS_BISULFITE_SEQUENCE false \
    --ALIGNED_READS_ONLY false \
    --CLIP_ADAPTERS false \
    --MAX_RECORDS_IN_RAM 2000000 \
    --ADD_MATE_CIGAR true \
    --MAX_INSERTIONS_OR_DELETIONS -1 \
    --PRIMARY_ALIGNMENT_STRATEGY MostDistant \
    --PROGRAM_RECORD_ID "bwamem" \
    --PROGRAM_GROUP_VERSION "${bwa_version}" \
    --PROGRAM_GROUP_COMMAND_LINE "bwa mem -K 100000000 -p -v 3 -t 2 -Y $REF/mitochondria/hs37d5.MT.shifted_by_8000_bases.fa" \
    --PROGRAM_GROUP_NAME "bwamem" \
    --UNMAPPED_READ_STRATEGY COPY_TO_TAG \
    --ALIGNER_PROPER_PAIR_FLAGS true \
    --UNMAP_CONTAMINANT_READS true \
    --ADD_PG_TAG_TO_READS false

  $GATK --java-options "-Xms4000m" MarkDuplicates \
    --INPUT mba.bam \
    --OUTPUT md.bam \
    --METRICS_FILE ${sampleGenomicID}.MT.dupsMetrics.shifted.txt \
    --VALIDATION_STRINGENCY SILENT \
    --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
    --ASSUME_SORT_ORDER "queryname" \
    --CLEAR_DT "false" \
    --ADD_PG_TAG_TO_READS false

  $GATK --java-options "-Xms4000m" SortSam \
    --INPUT md.bam \
    --OUTPUT ${sampleGenomicID}.MT.shifted.sorted.bam \
    --SORT_ORDER "coordinate" \
    --CREATE_INDEX true \
    --MAX_RECORDS_IN_RAM 300000

  # call variants
  $GATK --java-options "-Xmx3000m" Mutect2 \
    -R $REF/mitochondria/hs37d5.MT.fa \
    -I ${sampleGenomicID}.MT.sorted.bam \
    --read-filter MateOnSameContigOrNoMappedMateReadFilter \
    --read-filter MateUnmappedAndUnmappedReadFilter \
    -O ${sampleGenomicID}.MT.unshifted.vcf \
    -L MT:576-16024 \
    --annotation StrandBiasBySample \
    --mitochondria-mode \
    --max-reads-per-alignment-start 75 \
    --max-mnp-distance 0

  # call variants shifted
  $GATK --java-options "-Xmx3000m" Mutect2 \
    -R $REF/mitochondria/hs37d5.MT.shifted_by_8000_bases.fa \
    -I ${sampleGenomicID}.MT.shifted.sorted.bam \
    --read-filter MateOnSameContigOrNoMappedMateReadFilter \
    --read-filter MateUnmappedAndUnmappedReadFilter \
    -O ${sampleGenomicID}.MT.shifted.vcf \
    -L MT:8025-9144 \
    --annotation StrandBiasBySample \
    --mitochondria-mode \
    --max-reads-per-alignment-start 75 \
    --max-mnp-distance 0

  # liftover shfited calls
  $GATK LiftoverVcf \
    -I ${sampleGenomicID}.MT.shifted.vcf \
    -O ${sampleGenomicID}.shifted_back.vcf \
    -R $REF/mitochondria/hs37d5.MT.fa \
    --CHAIN $REF/mitochondria/ShiftBack.chain \
    --REJECT /home/ec2-user/${sampleGenomicID}.rejected.vcf

  # merge vcfs
  $GATK MergeVcfs \
    -I ${sampleGenomicID}.shifted_back.vcf \
    -I ${sampleGenomicID}.MT.unshifted.vcf \
    -O ${sampleGenomicID}.merged.vcf

  $GATK MergeMutectStats --stats ${sampleGenomicID}.MT.shifted.vcf.stats --stats ${sampleGenomicID}.MT.unshifted.vcf.stats -O ${sampleGenomicID}.rawVCFStats.txt

  # initial filter
  $GATK  --java-options "-Xmx2500m" FilterMutectCalls -V ${sampleGenomicID}.merged.vcf \
      -R $REF/mitochondria/hs37d5.MT.fa \
      -O filtered.vcf \
      --stats ${sampleGenomicID}.rawVCFStats.txt \
      --max-alt-allele-count 4 \
      --mitochondria-mode \
      --min-allele-fraction 0 \
      --contamination-estimate 0.0

  $GATK VariantFiltration -V filtered.vcf \
      -O ${sampleGenomicID}.merged.filtered.vcf \
      --apply-allele-specific-filters \
      --mask $REF/mitochondria/blacklist_sites.hs37d5.MT.bed \
      --mask-name "blacklisted_site"

  # split multi-allelic sites
  $GATK  LeftAlignAndTrimVariants \
    -R $REF/mitochondria/hs37d5.MT.fa \
    -V ${sampleGenomicID}.merged.filtered.vcf \
    -O split.vcf \
    --split-multi-allelics \
    --dont-trim-alleles \
    --keep-original-ac

  $GATK  SelectVariants \
      -V split.vcf \
      -O ${sampleGenomicID}.merged.splitAndPassOnly.vcf \
      --exclude-filtered

  # get contamination
  mkdir thisSample
  cp ${sampleGenomicID}.merged.splitAndPassOnly.vcf thisSample/
  PARENT_DIR="$(dirname "thisSample/${sampleGenomicID}.merged.splitAndPassOnly.vcf")"
  java -jar $TGP_HOME/haplocheckCLI/haplocheckCLI.jar "${PARENT_DIR}"

  sed 's/\"//g' output > output-noquotes

  grep "SampleID" output-noquotes > headers
  FORMAT_ERROR="Bad contamination file format"
  if [ `awk '{print $2}' headers` != "Contamination" ]; then
    echo $FORMAT_ERROR; exit 1
  fi
  if [ `awk '{print $6}' headers` != "HgMajor" ]; then
    echo $FORMAT_ERROR; exit 1
  fi
  if [ `awk '{print $8}' headers` != "HgMinor" ]; then
    echo $FORMAT_ERROR; exit 1
  fi
  if [ `awk '{print $14}' headers` != "MeanHetLevelMajor" ]; then
    echo $FORMAT_ERROR; exit 1
  fi
  if [ `awk '{print $15}' headers` != "MeanHetLevelMinor" ]; then
    echo $FORMAT_ERROR; exit 1
  fi

  grep -v "SampleID" output-noquotes > output-data
  awk -F "\t" '{print $2}' output-data > contamination.txt
  awk -F "\t" '{print $6}' output-data > major_hg.txt
  awk -F "\t" '{print $8}' output-data > minor_hg.txt
  awk -F "\t" '{print $14}' output-data > mean_het_major.txt
  awk -F "\t" '{print $15}' output-data > mean_het_minor.txt

  # filter contamination
  hcContamination=$(awk -F'\t' -v OFS='\t' '{if($2=="YES"){if($14==0){print $15}else{print 1-$14}}else{print "0"}}' output-data)

  $GATK  --java-options "-Xmx2500m" FilterMutectCalls -V ${sampleGenomicID}.merged.splitAndPassOnly.vcf \
      -R $REF/mitochondria/hs37d5.MT.fa \
      -O filtered.vcf \
      --stats ${sampleGenomicID}.rawVCFStats.txt \
      --max-alt-allele-count 4 \
      --mitochondria-mode \
      --min-allele-fraction 0 \
      --contamination-estimate ${hcContamination}

  $GATK VariantFiltration -V filtered.vcf \
      -O ${sampleGenomicID}.merged.filtered.vcf \
      --apply-allele-specific-filters \
      --mask $REF/mitochondria/blacklist_sites.hs37d5.MT.bed \
      --mask-name "blacklisted_site"

  # get coverage at every base
  $GATK CollectHsMetrics \
    -I ${sampleGenomicID}.MT.sorted.bam \
    -R $REF/mitochondria/hs37d5.MT.fa \
    --PER_BASE_COVERAGE non_control_region.tsv \
    -O non_control_region.metrics \
    -TI $REF/mitochondria/non_control_region.MT.interval_list \
    -BI $REF/mitochondria/non_control_region.MT.interval_list \
    -covMax 20000 \
    --SAMPLE_SIZE 1

  $GATK CollectHsMetrics \
    -I ${sampleGenomicID}.MT.shifted.sorted.bam \
    -R $REF/mitochondria/hs37d5.MT.shifted_by_8000_bases.fa \
    --PER_BASE_COVERAGE control_region_shifted.tsv \
    -O control_region_shifted.metrics \
    -TI $REF/mitochondria/control_region_shifted.MT.interval_list \
    -BI $REF/mitochondria/control_region_shifted.MT.interval_list \
    -covMax 20000 \
    --SAMPLE_SIZE 1

  # Toggle set -u to avoid the below error:
  # activate-binutils_linux-64.sh: line 65: ADDR2LINE: unbound variable
  set +u
  conda activate gatk
  set -u

  R --vanilla <<CODE
    shift_back = function(x) {
      if (x < 8570) {
        return(x + 8000)
      } else {
        return (x - 8569)
      }
    }

    control_region_shifted = read.table("control_region_shifted.tsv", header=T)
    shifted_back = sapply(control_region_shifted[,"pos"], shift_back)
    control_region_shifted[,"pos"] = shifted_back

    beginning = subset(control_region_shifted, control_region_shifted[,'pos']<8000)
    end = subset(control_region_shifted, control_region_shifted[,'pos']>8000)

    non_control_region = read.table("non_control_region.tsv", header=T)
    combined_table = rbind(beginning, non_control_region, end)
    write.table(combined_table, "per_base_coverage.tsv", row.names=F, col.names=T, quote=F, sep="\t")

CODE

  mv per_base_coverage.tsv ${sampleGenomicID}_MT_per_base_coverage.tsv
  aws s3 cp ${sampleGenomicID}_MT_per_base_coverage.tsv s3://tgp-sample-processing/${sampleRunID}/ --only-show-errors

  # split multi-allelic sites
  $GATK LeftAlignAndTrimVariants \
    -R $REF/hs37d5.fa \
    -V ${sampleGenomicID}.merged.filtered.vcf  \
    -O ${sampleGenomicID}.MT.final.split.vcf \
    --split-multi-allelics \
    --dont-trim-alleles \
    --keep-original-ac

    aws s3 cp ${sampleGenomicID}.MT.final.split.vcf s3://tgp-sample-processing/${sampleRunID}/ --only-show-errors
    aws s3 cp ${sampleGenomicID}.MT.final.split.vcf.idx s3://tgp-sample-processing/${sampleRunID}/ --only-show-errors
}
