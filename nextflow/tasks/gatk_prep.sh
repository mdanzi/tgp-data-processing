gatk_prep() {
  local sampleGenomicID=$1
  local sampleRunID=$2
  local input_bam=$3
  local experimentType=${4:-"Unknown"}

  # establish variables
  local MinCov="6"
  local RefExons="exome_calling_regions.bed"

  # dedup the BAM file
  if [ "${experimentType}" != "Panel" ]; then
    { # try to mark duplicates
        $TGP_HOME/gatk-4.2.6.1/gatk --java-options -Xmx3G MarkDuplicates -I $input_bam \
          -O ${sampleGenomicID}.hg19.sorted.dedup.bam -M ${sampleGenomicID}.metrics.txt
    } || { # catch exception (this deals with the 'value put into pairInfoMap more than once' error)
        samtools view -b -f 0x2 ${sampleGenomicID}.sorted.bam > ${sampleGenomicID}.sorted.bam.tmp
        mv ${sampleGenomicID}.sorted.bam.tmp ${sampleGenomicID}.sorted.bam
        $TGP_HOME/gatk-4.2.6.1/gatk --java-options -Xmx3G MarkDuplicates -I ${sampleGenomicID}.sorted.bam -O ${sampleGenomicID}.hg19.sorted.dedup.bam -M ${sampleGenomicID}.metrics.txt
    }
  fi
  samtools index ${sampleGenomicID}.hg19.sorted.dedup.bam

  # BAM-level QC
  local DedupMetrics="${sampleGenomicID}.metrics.txt"
  samtools flagstat ${sampleGenomicID}.hg19.sorted.dedup.bam > ${sampleGenomicID}.hg19.sorted.dedup.bam.flagstat 
  echo -e "sampleName\tpercentReadsAligned\tpercentProperlyPaired\tpercentDuplicates\testimatedLibrarySize\tpercentReferenceCovered\tpercentReadsOnExons" > ${sampleGenomicID}.hg19.bamLevel.qc
  bedtools genomecov -ibam ${sampleGenomicID}.hg19.sorted.dedup.bam > ${sampleGenomicID}.hg19.coverage.txt
  PercentReadsAligned=$(grep "mapped" ${sampleGenomicID}.hg19.sorted.dedup.bam.flagstat | sed '1q;d' | awk -F'(' '{print $2}' | sed 's/%.*//') 
  PercentReadsPaired=$(grep "properly paired" ${sampleGenomicID}.hg19.sorted.dedup.bam.flagstat | sed '1q;d' | awk -F'(' '{print $2}' | sed 's/%.*//') 
  PercentDups=$(grep -v "#" ${DedupMetrics} | sed '3q;d' | awk -F'\t' '{print 100*$9}')
  EstLibSize=$(grep -v "#" ${DedupMetrics} | sed '3q;d' | awk -F'\t' '{print $10}')
  PercentRefCov=$((grep "genome" ${sampleGenomicID}.hg19.coverage.txt || :) | head -n ${MinCov} | awk -F'\t' '{n1+=$5} END {print 1-n1}')
  PercentRefCov=$(awk -F'\t' '{print 100*$1}' <<< ${PercentRefCov})
  ReadsOnExons=$(bedtools intersect -u -bed -abam ${sampleGenomicID}.hg19.sorted.dedup.bam -b $TGP_HOME/${RefExons} | wc -l) 
  TotalReads=$(grep "mapped" ${sampleGenomicID}.hg19.sorted.dedup.bam.flagstat | sed '1q;d' | awk -F' ' '{print $1}') 
  PercentReadsOnExons=$(expr 100 \* ${ReadsOnExons} / ${TotalReads} )
  echo -e "${sampleGenomicID}\t${PercentReadsAligned}\t${PercentReadsPaired}\t${PercentDups}\t${EstLibSize}\t${PercentRefCov}\t${PercentReadsOnExons}" >> ${sampleGenomicID}.hg19.bamLevel.qc 

  # upload output to S3
  s3_cp_to_processing ${sampleGenomicID}.hg19.bamLevel.qc ${sampleRunID}
  s3_cp_to_processing ${sampleGenomicID}.hg19.sorted.dedup.bam ${sampleRunID}
  s3_cp_to_processing ${sampleGenomicID}.hg19.sorted.dedup.bam.bai ${sampleRunID}
}

