clean_outputs() {
  local sample_run_id=$1 input_vcf=$2
  vcf_gz=${input_vcf}.gz
  bgzip -c ${input_vcf} > $vcf_gz
  tabix $vcf_gz
  s3_cp_to_processing $vcf_gz $sample_run_id
  s3_cp_to_processing ${vcf_gz}.tbi $sample_run_id
  s3_cp_to_processing $input_vcf.idx $sample_run_id
}

run_peddy() {
  local sample_genomic_id=$1 sample_run_id=$2 input_bam=$3 peddy_sites=$4
  $GATK HaplotypeCaller \
    -I ${input_bam} \
    -R $TGP_HOME/ref/hs37d5.fa \
    -O $sample_genomic_id.peddySites.vcf \
    --alleles $peddy_sites \
    -L $peddy_sites
  s3_cp_to_processing $sample_genomic_id.peddySites.vcf $sample_run_id
}

custom_genotype_calls() {
  local sample_genomic_id=$1 sample_run_id=$2 input_bam=$3 custom_sites=$4
  local output_vcf=$sample_genomic_id.customSites.vcf
  $GATK HaplotypeCaller \
    -I ${input_bam} \
    -R $TGP_HOME/ref/hs37d5.fa \
    -O $output_vcf \
    --alleles ${custom_sites} \
    -L ${custom_sites}
  s3_cp_to_processing $output_vcf $sample_run_id
}
