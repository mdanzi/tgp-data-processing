# Copies the given file to the specified bucket.
# file: file to finalize
# keyfile: [optional] File used to key this finaliation. Finalization will not take place until the KeyFile exists. This can be used to force the finaliation to wait until a certain point in a workflow.
# outdir: directory to which files should be uploaded
# name: name to set for uploaded file
finalize_to_file() {
    local file=$1
    local outdir=$2
    local name=$3
    local keyfile=$4

    local s3_output_dir=$(echo $outdir | sed 's/\/\+$//')
    local s3_output_file="$s3_output_dir/${name:-$(basename $file)}"

    aws s3 cp "$file" "$s3_output_file"
}

# Copies the given files to the specified bucket.
# files: files to finalize
# keyfile: [optional] File used to key this finaliation. Finalization will not take place until the KeyFile exists. This can be used to force the finaliation to wait until a certain point in a workflow.
# outdir: directory to which files should be uploaded
finalize_to_dir() {
    local files=(${1//,/ })
    local outdir=$2
    local keyfile=$3

    local s3_output_dir=$(echo $outdir | sed 's/\/\+$//')

    for file in "${files[@]}"; do
        aws s3 cp "$file" "$s3_output_dir"
    done
}

# Copies the contents of the given tar.gz file to the specified bucket.
# tar_gz_file: Gzipped tar file whose contents we'll copy.
# outdir: Google cloud path to the destination folder.
# keyfile: [optional] File used to key this finaliation. Finalization will not take place until the KeyFile exists. This can be used to force the finaliation to wait until a certain point in a workflow.
finalize_tar_gz_contents() {
    local tar_gz_file=$1
    local outdir=$2
    local keyfile=$3

    local s3_output_dir=$(echo $outdir | sed 's/\/\+$//')

    mkdir tmp
    cd tmp
    tar -zxf "$tar_gz_file"

    aws s3 cp ./* "$s3_output_dir" --recursive
}

# Write a file to the given directory indicating the run has completed.
# outdir: Google cloud path to the destination folder.
# keyfile: [optional] File used to key this finaliation. Finalization will not take place until the KeyFile exists. This can be used to force the finaliation to wait until a certain point in a workflow.
write_completion_file() {
    local outdir=$1
    local keyfile=$2

    local completion_file="COMPLETED_AT_$(date +%Y%m%dT%H%M%S).txt"
    touch "$completion_file"

    aws s3 cp "$completion_file" "$outdir"
}

# Write a file to the given directory with the given name.
# name: Name of the file to write.
# outdir: Google cloud path to the destination folder.
# keyfile: [optional] File used to key this finaliation. Finalization will not take place until the KeyFile exists. This can be used to force the finaliation to wait until a certain point in a workflow.
write_named_file() {
    local name=$1
    local outdir=$2
    local keyfile=$3

    touch "$name"

    aws s3 cp "$name" "$outdir"
}

# Gzip a file and finalize
# file: File to compress and finalize.
# outdir: Google cloud path to the destination folder.
# name: [optional] Name of the file to write. If not specified, the name of the input file will be used.
compress_and_finalize() {
    local file=$1
    local outdir=$2
    local name=$3

    local base=$(basename "$file")
    local out="${name:-$base}"
    out="${out%.gz}.gz"
    local s3_output_file=$(echo $outdir | sed 's/\/\+$//')"/$out"

    gzip -vkc "$file" > "$base.gz"
    aws s3 cp "$base.gz" "$s3_output_file"
}

# Gzip a bunch of files and finalize to the same 'folder'
# files: Files to compress and finalize.
# outdir: Google cloud path to the destination folder.
# prefix: [optional] Prefix to add to the output files.
finalize_and_compress() {
    local files=(${1//,/ })
    local outdir=$2
    local prefix=$3

    local s3_output_file=$(echo $outdir | sed 's/\/\+$//')"/$prefix/"

    for ff in "${files[@]}"; do
        local base=$(basename -- "$ff")
        mv "$ff" "$base" && gzip -vk "$base"
    done

    aws s3 cp /cromwell_root/ "$s3_output_file" --recursive --exclude "*" --include "*.gz"
}
