merge_and_sort_vcfs() {
  local prefix=$1
  local sample_run_id=$2
  local ref_fasta_fai=$3
  shift 3 # Remaining parameters are VCF files to be merged
  local vcfs=("$@")
  local num_cores
  num_cores=$(get_num_cores)

  echo "${vcfs[@]}" | sed 's/ /\n/g' > all_raw_vcfs.txt
  bcftools concat --naive --threads $((num_cores-1)) -f all_raw_vcfs.txt \
    --output-type v -o tmp.wgs.vcf.gz   # fast, at the expense of disk space
  rm "${vcfs[@]}"
  bcftools reheader --fai $ref_fasta_fai -o wgs_raw.vcf.gz tmp.wgs.vcf.gz
  rm tmp.wgs.vcf.gz
  output_vcf="$prefix.vcf.gz"
  bcftools sort --temp-dir tm_sort --output-type z -o $output_vcf wgs_raw.vcf.gz
  bcftools index --tbi --force $output_vcf
  s3_cp_to_processing $output_vcf $sample_run_id
  s3_cp_to_processing $output_vcf.tbi $sample_run_id
}

