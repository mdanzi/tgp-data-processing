# For subsetting a high-coverage BAM stored in GCS, without localizing (more resilient to auth. expiration).
resilient_subset_bam() {
  local bam=$1
  local interval_list_file=$2

  samtools view \
    -bhX \
    -M \
    -@ 1 \
    --verbosity=8 \
    --write-index \
    -o "subset.bam##idx##subset.bam.bai" \
    ${bam} ${bam}.bai $(cat "${interval_list_file}")
}
