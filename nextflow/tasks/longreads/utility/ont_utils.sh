# Find sequencing summary files in an ONT basecall directory.
# gcs_input_dir: GCS directory containing sequencing summary files.
# runtime_attr_override: Override default runtime attributes.
find_sequencing_summary_files() {
    local gcs_input_dir=$1
    local indir=$(echo $gcs_input_dir | sed 's:/$::')
    local summary_file DIR FASTQ_COUNT FAST5_COUNT
    touch summaries.txt
    for summary_file in $(gsutil ls "$indir/**sequencing_summary*.txt*")
    do
        DIR=$(dirname $summary_file)
        echo ${DIR}

        if aws s3 ls "${DIR}" | grep fastq_pass && gsutil ls "${DIR}" | grep fast5_pass; then
            FASTQ_COUNT=$(gsutil ls "${DIR}/fastq_pass/*.fastq*" | wc -l)
            FAST5_COUNT=$(gsutil ls "${DIR}/fast5_pass/*.fast5*" | wc -l)

            echo "${FASTQ_COUNT} ${FAST5_COUNT}"

            if [ ${FASTQ_COUNT} -eq ${FAST5_COUNT} ]; then
                echo $summary_file >> summaries.txt
            else
                echo "# fastq != # fast5.  Skipped."
            fi
        else
            echo "No passing fastq and fast5 files.  Skipped."
        fi

        echo ""
    done
    local summary_files=($(cat summaries.txt))
    echo ${summary_files[@]}
}

# Get ONT run info from a final summary file.
# final_summary: Sequencing summary file.
# runtime_attr_override: Override default runtime attributes.
get_run_info() {
    local final_summary=$1
    aws s3 cp "$final_summary" - | sed 's/=[[:space:]]*$/=unknown/' | sed 's/=/\t/g' > run_info.txt
    local run_info=($(cat run_info.txt))
    declare -A run_info_map
    for info in "${run_info[@]}"; do
        key=$(echo $info | cut -f1)
        value=$(echo $info | cut -f2)
        run_info_map[$key]=$value
    done
    echo ${run_info_map[@]}
}

# List files in a GCS directory.
# sequencing_summary: Sequencing summary file.
# suffix: Suffix of files to list.
# runtime_attr_override: Override default runtime attributes.
list_files() {
    local sequencing_summary=$1
    local suffix=$2
    local indir=$(echo $sequencing_summary | sed 's:/[^/]*$::' | sed 's:/$::')
    aws s3 ls "$indir/" | grep ".$suffix" | grep -v fail > files.txt
    local count=$(cat files.txt | wc -l)
    echo $count
    echo files.txt
}

# Partition a manifest into chunks.
# manifest: Manifest to partition.
# N: Number of chunks to partition into.
# runtime_attr_override: Override default runtime attributes.
partition_manifest() {
    local manifest=$1
    local N=$2
    split -a 5 -d --additional-suffix=.txt -e -n l/$N $manifest manifest_chunk_
    local manifest_chunks=($(ls manifest_chunk_*.txt))
    echo ${manifest_chunks[@]}
}
