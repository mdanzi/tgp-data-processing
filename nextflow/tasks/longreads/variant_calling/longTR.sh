#!/bin/bash

# Runs LongTR for tandem repeat calling
run_longtr() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local haplotaggedBam=$3
  local ref_fasta=$4
  local repeat_catalog=$5
  local sex=${6-"F"}

  karyotype=""
  if [[ $sex == "M" ]]; then karyotype="--haploid-chrs 'X,Y'"; fi
  LongTR --bams ${haplotaggedBam} --fasta ${ref_fasta} --regions ${repeat_catalog} --tr-vcf ${sample_genomic_id}.longTR.vcf.gz --phased --min-reads 2 --max-tr-len 100000 ${karyotype}
  
  s3_cp_to_processing ${sample_genomic_id}.longTR.vcf.gz $sample_run_id
}
