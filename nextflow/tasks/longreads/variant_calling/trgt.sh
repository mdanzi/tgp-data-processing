#!/bin/bash

# Runs TRGT for tandem repeat calling
run_trgt() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3
  local ref_fasta=$4
  local repeat_catalog=$5
  local sex=${6-"F"}
  local num_cores
  num_cores=$(get_num_cores)
  karyotype="XX"
  if [[ $sex == "M" ]]; then karyotype="XY"; fi
  trgt genotype \
    --genome $ref_fasta \
    --repeats $repeat_catalog \
    --reads $bam \
    --threads $num_cores \
    --output-prefix ${sample_genomic_id}.trgt \
    --sample-name $sample_genomic_id\
    --karyotype $karyotype
  s3_cp_to_processing ${sample_genomic_id}.trgt.vcf.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.trgt.spanning.bam $sample_run_id
}
