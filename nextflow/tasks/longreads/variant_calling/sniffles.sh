#!/bin/bash

# Runs Sniffles for structural variant calling (using pre-phased reads as input)
run_sniffles() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3
  local num_cores
  num_cores=$(get_num_cores)
  
  sniffles -t $num_cores \
    -i $bam \
    --sample-id $sample_genomic_id \
    --tandem-repeats $TGP_HOME/tgp-data-processing/resources/human_hs37d5.trf.bed \
    --vcf ${sample_genomic_id}.Sniffles.vcf.gz \
    --snf ${sample_genomic_id}.Sniffles.snf \
    --phase

  s3_cp_to_processing ${sample_genomic_id}.Sniffles.vcf.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.Sniffles.vcf.gz.tbi $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.Sniffles.snf $sample_run_id
}
