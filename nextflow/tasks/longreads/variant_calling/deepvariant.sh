
# Runs DeepVariant for variant calling
deep_variant() {
  local bam=$1
  local ref_fasta=$2
  local sample_run_id=$3
  local model_type=${4:-"PACBIO"}
  local prefix num_cores
  prefix=$(basename $bam ".bam")
  num_cores=$(get_num_cores)

  samtools index $bam

  /opt/deepvariant/bin/run_deepvariant \
    --model_type=$model_type \
    --ref=$ref_fasta \
    --reads=$bam \
    --output_vcf=$prefix.DV.vcf.gz \
    --num_shards=$num_cores 

  s3_cp_to_processing $prefix.DV.vcf.gz $sample_run_id 
}

deep_variant_make_examples() {
  local bam=$1
  local ref_fasta=$2
  local model_type=${3:-"PACBIO"}
  local regions=${4:-"ALL"}
  local taskNum=${5:-"ALL"}
  local numTasks=${6:-"8"}
  prefix=$(basename $bam ".bam")
  local regionsVar=""
  if [[ ${regions} != "ALL" ]]; then regionsVar="--regions ${regions}"; fi
  local taskVar=""
  if [[ ${taskNum} != "ALL" ]]; then taskVar="--task ${taskNum}"; fi
  local minMappingQuality="1"
  if [[ ${model_type} == 'ONT_R104' ]]; then minMappingQuality="5"; fi
  local pileupImageWidth="147"
  if [[ ${model_type} == 'ONT_R104' ]]; then pileupImageWidth="99"; fi
  local indelGQThreshold="30"
  if [[ ${model_type} == 'ONT_R104' ]]; then indelGQThreshold="25"; fi
  local snpGQThreshold="25"
  if [[ ${model_type} == 'ONT_R104' ]]; then snpGQThreshold="20"; fi
  local extraParams=""
  if [[ ${model_type} == 'ONT_R104' ]]; then extraParams='--vsc_min_fraction_snps 0.08'; fi

  /opt/deepvariant/bin/make_examples \
    --mode calling \
    --ref $ref_fasta \
    --reads $bam \
    --examples "make_examples.tfrecord@${numTasks}.gz" \
    --checkpoint "/opt/models/${model_type,,}" \
    --alt_aligned_pileup "diff_channels" \
    --call_small_model_examples \
    --max_reads_per_partition "600" \
    --min_mapping_quality "${minMappingQuality}" \
    --parse_sam_aux_fields \
    --partition_size "25000" \
    --phase_reads \
    --pileup_image_width "${pileupImageWidth}" \
    --norealign_reads \
    ${regionsVar} \
    --small_model_indel_gq_threshold "${indelGQThreshold}" \
    --small_model_snp_gq_threshold "${snpGQThreshold}" \
    --small_model_vaf_context_window_size "51" \
    --sort_by_haplotypes \
    --track_ref_reads \
    --trained_small_model_path "/opt/smallmodels/${model_type,,}" \
    --trim_reads_for_pileup \
    --vsc_min_fraction_indels "0.12" \
    ${taskVar} \
    ${extraParams}

  # need to save the files: ${output_dir}/make_examples.tfrecord-00000-of-00008.gz 
  # need to save the files: ${output_dir}/make_examples_call_variant_outputs.tfrecord-00000-of-00008.gz 

}

deep_variant_call_variants() {
  local model_type=${1:-"PACBIO"}
  local numTasks=${2:-"8"}

  # will look for the -00000 through -00007 files made above
  /opt/deepvariant/bin/call_variants \
    --outfile "call_variants_output.tfrecord.gz" \
    --examples "make_examples.tfrecord@${numTasks}.gz" \
    --checkpoint "/opt/models/${model_type,,}"
}

deep_variant_postprocess_variants() {
  local sample_genomic_id=$1
  local ref_fasta=$2
  local model_type=${3:-"PACBIO"}
  local numTasks=${4:-"8"}
  local regions=${5:-"ALL"}
  local prefix num_cores
  local regionsVar=""
  if [[ ${regions} != "ALL" ]]; then regionsVar="--regions ${regions}"; fi
  num_cores=$(get_num_cores)
  prefix="${sample_genomic_id}.${regions}"
  /opt/deepvariant/bin/postprocess_variants \
    --ref $ref_fasta --infile "call_variants_output@1.tfrecord.gz" \
    --outfile "${prefix}.DV.vcf.gz" \
    --cpus ${num_cores} \
    --small_model_cvo_records "make_examples_call_variant_outputs.tfrecord@${numTasks}.gz" \
    ${regionsVar}

}

deep_variant_merge_vcfs() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  shift 2 # remaining args are vcf files

  bcftools concat -o $sample_genomic_id.unsorted.DV.vcf.gz -O z "$@"
  bcftools sort -o $sample_genomic_id.unfiltered.DV.vcf.gz -O z $sample_genomic_id.unsorted.DV.vcf.gz 
  bcftools view -c 1 -O z -o $sample_genomic_id.DV.vcf.gz $sample_genomic_id.unfiltered.DV.vcf.gz
  s3_cp_to_processing $sample_genomic_id.DV.vcf.gz $sample_run_id 
}
