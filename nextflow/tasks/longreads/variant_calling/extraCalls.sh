#!/bin/bash

run_peddySites() {
    local phased_bam=$1
    local sample_genomic_id=$2
    local sample_run_id=$3
    local ref_fasta=$4
    local peddy_sites=$5
    grep -v '^#' ${peddy_sites} | awk -F'\t' -v OFS='\t' '{print $1,$2,$4","$5}' > peddySites.txt
    bcftools mpileup -Ou -f ${ref_fasta} -T ${peddy_sites} ${phased_bam} | bcftools call -m -O v -o ${sample_genomic_id}.peddySites.vcf -T peddySites.txt -C alleles 
    s3_cp_to_processing ${sample_genomic_id}.peddySites.vcf $sample_run_id 
}

run_customSites() {
    local phased_bam=$1
    local sample_genomic_id=$2
    local sample_run_id=$3
    local ref_fasta=$4
    local custom_sites=$5
    grep -v '^#' ${custom_sites} | awk -F'\t' -v OFS='\t' '{print $1,$2,$4","$5}' > customSites.txt
    bcftools mpileup -Ou -f ${ref_fasta} -T ${custom_sites} ${phased_bam} | bcftools call -m -O v -o ${sample_genomic_id}.customSites.vcf -T customSites.txt -C alleles 
    s3_cp_to_processing ${sample_genomic_id}.customSites.vcf $sample_run_id 
}

run_lr_roh() {
    local vcf_gz=$1
    local sample_genomic_id=$2
    local sample_id=$3
    local run_id=$4
    local roh_regions=${sample_genomic_id}.RohRegions.txt
    local roh_txt=${sample_genomic_id}.RegionsOfHomozygosity.txt
    local roh_bed=${sample_genomic_id}.RegionsOfHomozygosity.bed
    # prepare 1000Genomes data on allele frequencies and genetic mappings for ROH mapping
    aws s3 cp s3://tgp-data-analysis/StructuralVariation/genetic-map.tgz . --only-show-errors
    aws s3 cp s3://tgp-data-analysis/StructuralVariation/1000GP-AFs.tgz . --only-show-errors
    tar -zxvf 1000GP-AFs.tgz
    tar -zxvf genetic-map.tgz
    # filter DV VCF file
    bcftools view -O z -o tmp.vcf.gz --apply-filters 'PASS' ${vcf_gz}
    # run ROH
    bcftools annotate -c CHROM,POS,REF,ALT,AF1KG -h 1000GP-AFs/AFs.tab.gz.hdr -a 1000GP-AFs/AFs.tab.gz tmp.vcf.gz |
    bcftools roh --AF-tag AF1KG -M 100 -m genetic-map/genetic_map_chr{CHROM}_combined_b37.txt -o roh.txt
    if grep '^RG' roh.txt > $roh_regions; then
        # filter the output to only include ROHs at least 5kb in size
        echo -e "chr\tstart\tend\tlength\tnumberOfMarkers\tquality" > $roh_txt
        awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,$6,$7,$8}}' $roh_regions >> $roh_txt
        # create bed file for IGV visualization
        echo "#gffTags" > $roh_bed
        awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,"Length="$6";NumberOfMarkers="$7";Quality="$8}}' $roh_regions >> $roh_bed
        # upload the roh's to s3
        aws s3 cp $roh_txt s3://tgp-sample-processing/${run_id}/ --only-show-errors
        aws s3 cp $roh_bed s3://tgp-sample-assets/${sample_id}/ROHs/ --only-show-errors
    fi


}
