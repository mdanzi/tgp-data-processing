#!/bin/bash

# Runs PBSV for structural variant calling
run_pbsv() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3
  local ref_fasta=$4
  local num_cores
  num_cores=$(get_num_cores)
  pbsv discover $bam ${sample_genomic_id}.svsig.gz
  pbsv call $ref_fasta ${sample_genomic_id}.svsig.gz ${sample_genomic_id}.pbsv.vcf
  s3_cp_to_processing ${sample_genomic_id}.pbsv.vcf $sample_run_id
}
