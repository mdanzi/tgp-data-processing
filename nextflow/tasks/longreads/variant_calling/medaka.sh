#!/bin/bash

# Runs Medaka for tandem repeat calling
run_medaka() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3
  local ref_fasta=$4
  local repeat_catalog=$5
  local sex=${6-"F"}
  local num_cores
  num_cores=$(get_num_cores)
  karyotype="female"
  if [[ $sex == "M" ]]; then karyotype="male"; fi
  medaka tandem --threads $num_cores $bam $ref_fasta $repeat_catalog $karyotype $sample_genomic_id
  mv ${sample_genomic_id}/medaka_to_ref.TR.vcf ${sample_genomic_id}.medaka.vcf
  gzip ${sample_genomic_id}.medaka.vcf
  mv ${sample_genomic_id}/consensus.fasta ${sample_genomic_id}.medaka.consensus.fasta
  
  s3_cp_to_processing ${sample_genomic_id}.medaka.vcf.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.medaka.consensus.fasta $sample_run_id
}
