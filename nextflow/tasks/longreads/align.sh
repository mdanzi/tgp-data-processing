#!/bin/bash

align_pb() {
    #local fastqs=(${1//,/ })
    local inputFiles=(${1//,/ })
    local sample_genomic_id=$2
    local sample_run_id=$3
    local ref_fasta=$4
    local map_preset=${5:-"CCS"}
    local drop_per_base_n_pulse_tags=${6:-true}
    local median_filter=""
    local extra_options=""

    if [ "$map_preset" == "SUBREAD" ]; then
        median_filter="--median-filter"
    fi
    if [ "$drop_per_base_n_pulse_tags" == true ]; then
        extra_options=" --strip "
    fi

    #  # re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
    #first_file=${fastqs[0]}
    #extension="${first_file##*.}"
    #fastq_extension=""
    #if [[ $extension == "bz2" || $extension == "gz" ]]; then
    #    fastq_extension=".${extension}"
    #fi

    #reads="${sample_genomic_id}.fq${fastq_extension}"

    ## re-implementation of the s3_read_concat function -- can be moved later if that gets updated to cromwell-friendly version
    ## Outputs a concatenated stream of all files listed in the input array
    #s3_read_concat_cromwell() {
    #    for s3file in "$@"; do
    #        aws s3 cp --only-show-errors "$s3file" -
    #    done
    #}
    #s3_read_concat_cromwell "${fastqs[@]}" > "${reads}"

    s3_cp_and_make_fofn() {
        for s3file in "$@"; do 
            aws s3 cp --only-show-errors "$s3file" .
            sed 's#^.*/##' <<< $s3file >> inputFiles.fofn
        done
    }

    s3_cp_and_make_fofn "${inputFiles[@]}"
 

    # Unused variable
    # note, the Read Group is written to match the style and defaults used by Picard AddOrReplaceReadGroups
    # read_group="@RG\\tID:1\\tSM:${sample_genomic_id}\\tPL:PBCCS\\tLB:lib1\\tPU:unit1"

    # align reads
    #pbmm2 align $ref_fasta ${reads} ${sample_genomic_id}.pre.bam --preset $map_preset $median_filter \
    #  --sample $sample_genomic_id $extra_options --sort --unmapped
    pbmm2 align $ref_fasta inputFiles.fofn ${sample_genomic_id}.pre.bam --preset $map_preset $median_filter \
        --sample $sample_genomic_id $extra_options --sort --unmapped
    samtools calmd -b --no-PG ${sample_genomic_id}.pre.bam $ref_fasta > ${sample_genomic_id}.bam
    # index reads
    samtools index ${sample_genomic_id}.bam
    #pbindex ${sample_genomic_id}.bam

    s3_cp_to_processing $sample_genomic_id.bam $sample_run_id/alignments 
    s3_cp_to_processing $sample_genomic_id.bam.bai $sample_run_id/alignments 
    #s3_cp_to_processing $sample_genomic_id.bam.pbi $sample_run_id/alignments 
}

# A wrapper to minimap2 for mapping & aligning (groups of) sequences to a reference
run_minimap2() {
    local s3reads=(${1//,/ })
    local ref_fasta=$2
    local sample_genomic_id=$3
    local sample_run_id=$4
    local map_preset=${5:-"map-ont"}
    # Split tags on commas. Yield empty array if parameter is not supplied
    local tags_to_preserve=(${6:+${6//,/ }})
    local prefix=${7:-$sample_genomic_id}
    local do_preserve_tags=false

    if [ ${#tags_to_preserve[@]} -ne 0 ]; then
        do_preserve_tags=true
    fi

    reads=()
    for s3file in "${s3reads[@]}"; do 
      aws s3 cp --only-show-errors "$s3file" .
      readfilename=$(basename "$s3file")
      reads+=("$readfilename")
    done

    num_cpus=$(grep -c ^processor /proc/cpuinfo)
    ram_in_gb=$(free -g | awk '/^Mem:/{print $2}')
    mem_for_sort=$((ram_in_gb / num_cpus))
    mem_for_sort=$((mem_for_sort / 2))

    local rg="@RG\\tID:1\\tSM:${sample_genomic_id}\\tPL:ONT\\tLB:Library1\\tPU:unknown\\tDT:2021-01-01T12:00:00.000000-05:00"
    local rg_len
    rg_len=$(echo -n '${rg}' | wc -c | awk '{print $NF}')
    if [ $rg_len -ne 0 ]; then
        map_params="-aYL --MD --eqx -x $map_preset -R $rg -t $num_cpus $ref_fasta"
    else
        map_params="-aYL --MD --eqx -x $map_preset -t $num_cpus $ref_fasta"
    fi

    sort_params="-@${num_cpus} -m${mem_for_sort}G --no-PG -o ${prefix}.pre.bam"
    file="${reads[0]}"

    # We write to a SAM file before sorting and indexing because rarely, doing everything
    # in a single one-liner leads to a truncated file error and segmentation fault of unknown
    # origin.  Separating these commands requires more resources, but is more reliable overall.

    if [[ "$file" =~ \.fastq$ ]] || [[ "$file" =~ \.fq$ ]]; then
        cat "${reads[@]}" | minimap2 $map_params - > tmp.sam
    elif [[ "$file" =~ \.fastq.gz$ ]] || [[ "$file" =~ \.fq.gz$ ]]; then
        zcat "${reads[@]}" | minimap2 $map_params - > tmp.sam
    elif [[ "$file" =~ \.fasta$ ]] || [[ "$file" =~ \.fa$ ]]; then
        cat "${reads[@]}" | python3 /usr/local/bin/cat_as_fastq.py | minimap2 $map_params - > tmp.sam
    elif [[ "$file" =~ \.fasta.gz$ ]] || [[ "$file" =~ \.fa.gz$ ]]; then
        zcat "${reads[@]}" | python3 /usr/local/bin/cat_as_fastq.py | minimap2 $map_params - > tmp.sam
    elif [[ "$file" =~ \.bam$ ]]; then
        # samtools fastq takes only 1 file at a time so we need to merge them together:
        for f in "${reads[@]}"; do
            if $do_preserve_tags; then
                samtools fastq -T "${tags_to_preserve[*]}" "$f"
            else
                samtools fastq "$f"
            fi
        done > tmp.fastq

        echo "Memory info:"
        cat /proc/meminfo
        echo ""

        if $do_preserve_tags; then
            minimap2 $map_params -y tmp.fastq > tmp.sam
        else
            minimap2 $map_params tmp.fastq > tmp.sam
        fi
    else
        echo "Did not understand file format for '$file'"
        exit 1
    fi

    samtools view -@${num_cpus} -b tmp.sam | samtools sort $sort_params -
    samtools calmd -b --no-PG ${prefix}.pre.bam $ref_fasta > ${prefix}.bam
    samtools index -@${num_cpus} ${prefix}.bam
}
