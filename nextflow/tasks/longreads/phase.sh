#!/bin/bash

# Runs HiPhase to jointly phase DeepVariant, PBSV, and TRGT results and create haplotagged bam file
run_hiphase() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3
  local ref_fasta=$4
  local dv_vcf=$5
  local pbsv_vcf=$6
  local trgt_vcf=$7
  local num_cores
  num_cores=$(get_num_cores)

  # bgzip and tabix vcf files
  tabix ${dv_vcf}
  bgzip $pbsv_vcf
  tabix ${pbsv_vcf}.gz
  bcftools sort -O z $trgt_vcf > ${trgt_vcf}.gz
  tabix ${trgt_vcf}.gz

  hiphase \
    --threads $num_cores \
    --reference $ref_fasta \
    --bam $bam \
    --output-bam ${sample_genomic_id}.haplotagged.bam \
    --vcf ${dv_vcf} \
    --output-vcf ${sample_genomic_id}.DV.hiphase.vcf.gz \
    --vcf ${pbsv_vcf}.gz \
    --output-vcf ${sample_genomic_id}.pbsv.hiphase.vcf.gz \
    --vcf ${trgt_vcf}.gz \
    --output-vcf ${sample_genomic_id}.trgt.hiphase.vcf.gz \
    --stats-file ${sample_genomic_id}.hiphase.stats.txt \
    --blocks-file ${sample_genomic_id}.hiphase.blocks.txt \
    --summary-file ${sample_genomic_id}.hiphase.summary.txt 

  s3_cp_to_processing ${sample_genomic_id}.haplotagged.bam $sample_run_id/alignments
  s3_cp_to_processing ${sample_genomic_id}.haplotagged.bam.bai $sample_run_id/alignments
  s3_cp_to_processing ${sample_genomic_id}.DV.hiphase.vcf.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.pbsv.hiphase.vcf.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.trgt.hiphase.vcf.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.hiphase.stats.txt $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.hiphase.blocks.txt $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.hiphase.summary.txt $sample_run_id
}

# Runs HiPhase to jointly phase DeepVariant, PBSV, and TRGT results and create haplotagged bam file
run_whatshap() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3
  local ref_fasta=$4
  local dv_vcf=$5
  local num_cores
  num_cores=$(get_num_cores)

  # phase variants using reads
  source activate whatshap
  tabix ${dv_vcf}
  whatshap phase -o ${sample_genomic_id}.DV.whatshap.vcf --reference $ref_fasta $dv_vcf $bam
  bgzip ${sample_genomic_id}.DV.whatshap.vcf
  tabix ${sample_genomic_id}.DV.whatshap.vcf.gz

  # haplotag bam file using phased vcf
  whatshap haplotag -o ${sample_genomic_id}.haplotagged.bam --reference $ref_fasta ${sample_genomic_id}.DV.whatshap.vcf.gz $bam
  
  # need to index bam file
  samtools index ${sample_genomic_id}.haplotagged.bam

  # upload results
  s3_cp_to_processing ${sample_genomic_id}.DV.whatshap.vcf.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}.haplotagged.bam $sample_run_id/alignments
  s3_cp_to_processing ${sample_genomic_id}.haplotagged.bam.bai $sample_run_id/alignments
}