#!/bin/bash

# Function to create a custom metrics summary file from attributes and values
custom_metrics_summary_to_file() {
    local attributes=("$@")
    local values=("${@:$((${#attributes[@]} + 1))}")
    local output_file="alignment.metrics.tsv"

    # Combine attributes and values into a TSV format
    paste <(printf "%s\n" "${attributes[@]}") <(printf "%s\n" "${values[@]}") > "$output_file"

    echo "$output_file"
}

# Function to summarize depth over a whole bed file from MosDepth output
summarize_depth_over_whole_bed() {
    local mosdepth_output=$1
    local prefix=$(basename "$mosdepth_output" ".regions.bed.gz")
    local summary_file="${prefix}.summary.txt"

    # Create a summary file with headers
    echo -e 'chr\tstart\tstop\tgene\tcov_mean' > "$summary_file"
    # Append the MosDepth output to the summary file
    zcat "$mosdepth_output" >> "$summary_file"

    echo "$summary_file"
}
