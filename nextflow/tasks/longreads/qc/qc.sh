run_qc_bam() {
  local sampleGenomicID=$1
  local sampleRunID=$2
  local input_bam=$3

  # establish variables
  local MinCov="6"
  local RefExons="exome_calling_regions.bed"
  samtools flagstat ${input_bam} > ${input_bam}.flagstat 
  echo -e "sampleName\tpercentReadsAligned\tpercentProperlyPaired\tpercentDuplicates\testimatedLibrarySize\tpercentReferenceCovered\tpercentReadsOnExons" > ${sampleGenomicID}.hg19.bamLevel.qc
  bedtools genomecov -ibam ${input_bam} > ${sampleGenomicID}.hg19.coverage.txt
  PercentReadsAligned=$(grep "mapped" ${input_bam}.flagstat | sed '1q;d' | awk -F'(' '{print $2}' | sed 's/%.*//') 
  PercentReadsPaired=$(grep "properly paired" ${input_bam}.flagstat | sed '1q;d' | awk -F'(' '{print $2}' | sed 's/%.*//') 
  PercentDups=""
  EstLibSize=""
  PercentRefCov=$((grep "genome" ${sampleGenomicID}.hg19.coverage.txt || :) | head -n ${MinCov} | awk -F'\t' '{n1+=$5} END {print 1-n1}')
  PercentRefCov=$(awk -F'\t' '{print 100*$1}' <<< ${PercentRefCov})
  ReadsOnExons=$(bedtools intersect -u -bed -abam ${input_bam} -b $TGP_HOME/${RefExons} | wc -l) 
  TotalReads=$(grep "mapped" ${input_bam}.flagstat | sed '1q;d' | awk -F' ' '{print $1}') 
  PercentReadsOnExons=$(expr 100 \* ${ReadsOnExons} / ${TotalReads} )
  echo -e "${sampleGenomicID}\t${PercentReadsAligned}\t${PercentReadsPaired}\t${PercentDups}\t${EstLibSize}\t${PercentRefCov}\t${PercentReadsOnExons}" >> ${sampleGenomicID}.hg19.bamLevel.qc 
  
  s3_cp_to_processing ${sampleGenomicID}.hg19.bamLevel.qc ${sampleRunID}
}

run_qc_vcf() {
  set -xeuo pipefail
  local sampleGenomicID=$1
  local sampleRunID=$2
  local peddyResults=$3

  source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports s3_path_exists

  inputVCF="${sampleGenomicID}.DV.hiphase.vcf.gz"
  # if this sample is ONT, then switch to different nomenclature
  if s3_path_exists s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.DV.whatshap.vcf.gz; then
      inputVCF="${sampleGenomicID}.DV.whatshap.vcf.gz"
  fi
  aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${inputVCF} . --only-show-errors
  # filter DV VCF file
  bcftools view -O v -o ${inputVCF::-3} --apply-filters 'PASS' ${inputVCF}
  inputVCF=${inputVCF::-3}

  echo -e "fileName\tsampleName\tChr\tTSTV\tnumSNPs\tnumINDELs\tavgDepth\tavgQuality\tHeterozygousHomozygousRatio\tmeanGQ\tGender" > ${sampleGenomicID}.vcfLevel.byChr.qc
  TSTV=$(cat ${inputVCF} | vcf-tstv | awk '{print $1}')
  vcf-stats ${inputVCF} > tmpStats.txt 
  SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
  INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
  DEPTH=$(vcftools --vcf ${inputVCF} --depth --stdout | sed '2q;d' | awk '{print $3}')
  QUAL=$(vcftools --vcf ${inputVCF} --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
  HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
  HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
  HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
  if [ "${SNPs}" == "" ]; then SNPs="0"; fi
  if [ "${INDELs}" == "" ]; then INDELs="0"; fi
  if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
  if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
  if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
  HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
  GQ=$(vcftools --vcf ${inputVCF} --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')

  ## new, peddy-based sex check
  echo -e "${sampleGenomicID}\t${sampleGenomicID}\t0\t0\t0\t0" > ${sampleGenomicID}.ped
  bgzip -c ${inputVCF} > ${sampleGenomicID}.vcf.gz
  tabix ${sampleGenomicID}.vcf.gz
  set +u
  source activate py2
  set -u
  python -m peddy --prefix ${sampleGenomicID} ${sampleGenomicID}.vcf.gz ${sampleGenomicID}.ped
  set +u
  conda deactivate
  set -u
  SEX=$(sed -n '2p' ${sampleGenomicID}.sex_check.csv | cut -f 7 -d, )
  if [ ${SEX} == "male" ]; then
    SEX='M'
  elif [ ${SEX} == "female" ]; then
    SEX='F'
  else 
    SEX='U'
  fi 

  echo -e "${inputVCF}\t${sampleGenomicID}\tGenome\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
  for j in {1..22} X Y; do 
    vcftools --vcf ${inputVCF} --chr ${j} --stdout --recode > thisChr.vcf 
    numVariants=$(grep -v ^'#' thisChr.vcf | wc -l)
    if [ "${numVariants}" -gt "0" ]; then 
      TSTV=$(cat thisChr.vcf | vcf-tstv | awk '{print $1}')
      vcf-stats thisChr.vcf > tmpStats.txt 
      SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
      INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
      DEPTH=$(vcftools --vcf thisChr.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
      QUAL=$(vcftools --vcf thisChr.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
      HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
      HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
      HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
      if [ "${SNPs}" == "" ]; then SNPs="0"; fi
      if [ "${INDELs}" == "" ]; then INDELs="0"; fi
      if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
      if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
      if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
      HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
      GQ=$(vcftools --vcf thisChr.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')
      echo -e "${inputVCF}\t${sampleGenomicID}\t${j}\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
      fi
    done 
    ##ancestry clustering
    vcftools --vcf ${peddyResults} --012 --out ${sampleGenomicID}
    set +u
    source activate py2
    set -u
    python $TGP_HOME/tgp-data-processing/scripts/vcfLevelQC/samplePCA.py -i ${sampleGenomicID}
    set +u
    conda deactivate
    set -u

  # upload results 
  s3_cp_to_processing ${sampleGenomicID}.AncestryClustering.png ${sampleRunID}
  s3_cp_to_processing ${sampleGenomicID}.vcfLevel.byChr.qc ${sampleRunID}
}