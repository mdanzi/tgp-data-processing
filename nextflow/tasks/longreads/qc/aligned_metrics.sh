# Workflow to generate metrics for aligned BAMs
# aligned_bam: "Aligned BAM file"
# aligned_bai: "Index for aligned BAM file"
# ref_fasta: "Reference FASTA file"
# ref_dict: "Reference dictionary file"
# gcs_output_dir: "GCS output directory"
make_chr_interval_list() {
    local ref_dict=$1
    grep '^@SQ' $ref_dict | awk '{ print $2 "\t" 1 "\t" $3 }' | sed 's/[SL]N://g' |
      grep -v -e random -e chrUn -e decoy -e alt -e HLA -e EBV > chrs.txt
}

# Description: Runs mosdepth on the specified chromosome
# window_size: "Optional window size for coverage calculation"
mosdepth() {
    local bam=$1
    local bai=$2
    local chr=$3
    local window_size=${4:-500}
    local bam_basename
    bam_basename=$(basename $bam .bam)
    local prefix="${bam_basename}.coverage.${chr}"

    mosdepth -t 4 -c "$chr" -n -x -Q 1 "${prefix}.full" $bam
    mosdepth -t 4 -c "$chr" -n -x -Q 1 -b $window_size "${prefix}" $bam

    export MOSDEPTH_Q0=NO_COVERAGE   # 0 -- defined by the arguments to --quantize
    export MOSDEPTH_Q1=LOW_COVERAGE  # 1..4
    export MOSDEPTH_Q2=CALLABLE      # 5..149
    export MOSDEPTH_Q3=HIGH_COVERAGE # 150 ...

    mosdepth -t 4 -c "$chr" -n -x -Q 1 --quantize 0:1:5:150: "${prefix}.quantized" $bam

}

# Description: Summarizes coverage depth from mosdepth output
# regions: "Mosdepth regions output file"
summarize_depth() {
    local regions=$1
    local chr_name=$(basename $regions .regions.bed.gz | sed 's/out.coverage.//')

    {
        echo 'chr start stop cov_mean cov_sd cov_q1 cov_median cov_q3 cov_iqr'
        zcat $regions | datamash first 1 first 2 last 3 mean 4 sstdev 4 q1 4 median 4 q3 4 iqr 4
    } | column -t > "${chr_name}.summary.txt"
}

# Description: Computes flag statistics for the aligned BAM file
# bam: "Input BAM file"
flag_stats() {
    local bam=$1
    local basename=$(basename $bam .bam)

    samtools flagstat $bam > "${basename}.flag_stats.txt"
}
# Convert BAM to BED format
# bam: "Input BAM file"
# bai: "Index for input BAM file"
bam_to_bed() {
    local bam=$1
    local bed=${bam%.bam}.bed

    bedtools bamtobed -i $bam > $bed
    echo $bed
}

# Compute metrics for long reads
# bam: "Input BAM file"
read_metrics() {
    local bam=$1
    local basename=${bam%.bam}

    gatk ComputeLongReadMetrics -I $bam -O $basename.read_metrics -DF WellformedReadFilter
    echo $basename.read_metrics.np_hist.txt
    echo $basename.read_metrics.range_gap_hist.txt
    echo $basename.read_metrics.zmw_hist.txt
    echo $basename.read_metrics.prl_counts.txt
    echo $basename.read_metrics.prl_hist.txt
    echo $basename.read_metrics.prl_nx.txt
    echo $basename.read_metrics.prl_yield_hist.txt
    echo $basename.read_metrics.rl_counts.txt
    echo $basename.read_metrics.rl_hist.txt
    echo $basename.read_metrics.rl_nx.txt
    echo $basename.read_metrics.rl_yield_hist.txt
}

# Compute coverage for regions specified in a BED file
# bam: "Input BAM file"
# bai: "Index for input BAM file"
# bed: "BED file with regions"
# prefix: "Output prefix for coverage files"
compute_bed_coverage() {
    local bam=$1
    local bed=$2
    local prefix=$3

    bedtools coverage -b $bed -a $bam -nobuf | gzip > $prefix.txt.gz
    zcat $prefix.txt.gz | awk '{ sum += sprintf("%f", $15*$16) } END { printf("%f\n", sum) }' > $prefix.count.txt
    echo $prefix.txt.gz
    echo $prefix.count.txt
}

# Filter out reads with mapping quality 0
# bam: "Input BAM file"
filter_mq0_reads() {
    local bam=$1
    local prefix=${bam%.bam}

    samtools view -q 1 -b $bam > $prefix.no_mq0.bam
    samtools index $prefix.no_mq0.bam
    echo $prefix.no_mq0.bam
    echo $prefix.no_mq0.bam.bai
}

# Extract read names and lengths
# bam: "Input BAM file"
read_names_and_lengths() {
    local bam=$1
    local basename=${bam%.bam}

    samtools view $bam | awk '{ print $1, length($10) }' | gzip -1 > $basename.read_names_and_lengths.txt.gz
    echo $basename.read_names_and_lengths.txt.gz
}

# Generate coverage track for a specific region
# bam: "Input BAM file"
# bai: "Index for input BAM file"
# chr: "Chromosome"
# start: "Start position"
# end: "End position"
coverage_track() {
    local bam=$1
    local chr=$2
    local start=$3
    local end=$4
    local basename=${bam%.bam}

    samtools depth -a $bam -r $chr:$start-$end | bgzip > $basename.coverage.$chr_$start_$end.txt.gz
    tabix -p bed $basename.coverage.$chr_$start_$end.txt.gz
    echo $basename.coverage.$chr_$start_$end.txt.gz
    echo $basename.coverage.$chr_$start_$end.txt.gz.tbi
}

# Run mosdepth on regions specified in a BED file
# bam: "Input BAM file"
# bai: "Index for input BAM file"
# bed: "BED file with regions"
mosdepth_over_bed() {
    local bam=$1
    local bed=$2
    local basename=${bam%.bam}
    local bedname=${bed%.bed}
    local prefix="$basename.coverage_over_bed.$bedname"

    mosdepth -t 4 -b $bed -n -x -Q 1 $prefix $bam
    echo $prefix.mosdepth.global.dist.txt
    echo $prefix.mosdepth.region.dist.txt
    echo $prefix.regions.bed.gz
    echo $prefix.regions.bed.gz.csi
}
