#!/bin/bash

parse_DV_vcf() {
    set -xeuo pipefail
    local sampleGenomicID=$1
    local sampleRunID=$2

    source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports s3_path_exists

    inputVCF="${sampleGenomicID}.DV.hiphase.vcf.gz"
    # if this sample is ONT, then switch to different nomenclature
    if s3_path_exists s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.DV.whatshap.vcf.gz; then
        inputVCF="${sampleGenomicID}.DV.whatshap.vcf.gz"
    fi
    aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${inputVCF} . --only-show-errors
    # filter DV VCF file
    bcftools view -O v -o ${inputVCF::-3} --apply-filters 'PASS' ${inputVCF}
    inputVCF=${inputVCF::-3}

    # parse the vcf into a text file
    timeCount=$(date +%s)
    timeCount2=$(echo "scale=0; ${timeCount}/3600" | bc)
    set +u
    source activate py2
    set -u
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser_withPhasing.py -r "${timeCount2}${sampleGenomicID}" -s $sampleGenomicID < ${inputVCF} > ${sampleGenomicID}.parsed.txt


    # add in custom variants
    if s3_path_exists s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.customSites.vcf; then
        aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.customSites.vcf . --only-show-errors
        # there is no phasing info in the customSites VCF, so use the original parser for it (the merging script deals with that difference)
        python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleGenomicID}" -s ${sampleGenomicID} < ${sampleGenomicID}.customSites.vcf > ${sampleGenomicID}.customSites.parsed.txt
        python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnCustomGenotypeCalls_withPhasing.py --input=${sampleGenomicID}.parsed.txt --customSites=${sampleGenomicID}.customSites.parsed.txt --output=${sampleGenomicID}.parsed.2.txt
    else
        cp ${sampleGenomicID}.parsed.txt ${sampleGenomicID}.parsed.2.txt
    fi
    # download the ROH files to this workspace
    if s3_path_exists s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.RegionsOfHomozygosity.txt; then
        aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.RegionsOfHomozygosity.txt . --only-show-errors
        # add the ROH info as another column
        tail -n +2 ${sampleGenomicID}.RegionsOfHomozygosity.txt | cut -f 1,2,3,4 > ${sampleGenomicID}.RegionsOfHomozygosity.2.txt
        numFields=$(grep -v '^#' ${inputVCF} | awk -F'\t' '{print NF; exit}' || true)
        numFields=$((numFields + 4))
        bedtools intersect -wo -a ${inputVCF} -b ${sampleGenomicID}.RegionsOfHomozygosity.2.txt | cut -f 1,2,${numFields} | sort -u -k1n,1n -k2n,2n > variantsInROHs.txt
    else
        touch variantsInROHs.txt
    fi
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnROH_withPhasing.py --input=${sampleGenomicID}.parsed.2.txt --roh=variantsInROHs.txt --output=${sampleGenomicID}.parsed.txt
    # upload the results to S3
    aws s3 cp ${sampleGenomicID}.parsed.txt s3://tgp-sample-processing/${sampleRunID}/ --only-show-errors
}
