#!/bin/bash

# vcfInputQC - Quality control for VCF input
vcf_input_qc() {
  local input_vcf=$1
  local all_sample_genomics_ids=$2
  local cohort_run_id=$3
  local all_sample_guids=$4

  # Convert comma-separated strings to arrays
  local sample_genomic_ids_array=(${all_sample_genomics_ids//,/ })
  local sample_guids_array=(${all_sample_guids//,/ })

  local input_vcf_filename=$(basename $input_vcf)
  local extension="${input_vcf_filename##*.}"

  # Decompress if necessary
  if [[ $extension == "gz" || $extension == "bgz" ]]; then
    gunzip -c $input_vcf > input.vcf
  else
    cp $input_vcf input.vcf
  fi

  # Convert chromosome names to GRCh37 style
  sed -i 's/##contig=<ID=chrM/##contig=<ID=MT/g' input.vcf
  sed -i 's/^chrM/MT/g' input.vcf
  sed -i 's/##contig=<ID=chr/##contig=<ID=/g' input.vcf
  sed -i 's/^chr//g' input.vcf

  bgzip input.vcf
  tabix input.vcf.gz

  # Apply filter and reheader the VCF
  bcftools filter -i "(FORMAT/DP)>=6" input.vcf.gz > input.vcf || bcftools filter -i "DP>=6" input.vcf.gz > input.vcf
  rm input.vcf.gz
  rm input.vcf.gz.tbi

  # reheader the vcf so that it uses the sampleGenomicIDs
  echo ${sample_genomic_ids_array[@]} > sample_ids.txt
  bcftools reheader -s sample_ids.txt input.vcf -o $cohort_run_id.vcf

  # Upload the variant calls
  bgzip -c $cohort_run_id.vcf > $cohort_run_id.vcf.gz
  tabix $cohort_run_id.vcf.gz
  s3_cp_to_processing $cohort_run_id.vcf.gz $cohort_run_id 
  s3_cp_to_processing $cohort_run_id.vcf.gz.tbi $cohort_run_id 

  # Prepare 1000Genomes data on allele frequencies and genetic mappings for ROH mapping
  aws s3 cp s3://tgp-data-analysis/StructuralVariation/genetic-map.tgz . --only-show-errors
  aws s3 cp s3://tgp-data-analysis/StructuralVariation/1000GP-AFs.tgz . --only-show-errors
  tar -zxvf 1000GP-AFs.tgz
  tar -zxvf genetic-map.tgz

  # QC operations
  # Turn off errexit since this part is error prone
  set +eo pipefail
  trap "" ERR

  for idx in "${!sample_genomic_ids_array[@]}"; do
    sample_genomic_id=${sample_genomic_ids_array[$idx]}
    sample_guid=${sample_guids_array[$idx]}
    vcf-subset -c ${sample_genomic_id} $cohort_run_id.vcf > ${sample_genomic_id}.vcf
    echo -e "fileName\tsampleName\tChr\tTSTV\tnumSNPs\tnumINDELs\tavgDepth\tavgQuality\tHeterozygousHomozygousRatio\tmeanGQ\tGender" > ${sample_genomic_id}.vcfLevel.byChr.qc
    TSTV=$(cat ${sample_genomic_id}.vcf | vcf-tstv | awk '{print $1}')
    vcf-stats ${sample_genomic_id}.vcf > tmpStats.txt 
    SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
    INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
    DEPTH=$(vcftools --vcf ${sample_genomic_id}.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
    QUAL=$(vcftools --vcf ${sample_genomic_id}.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
    HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
    HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
    HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
    if [ "${SNPs}" == "" ]; then SNPs="0"; fi
    if [ "${INDELs}" == "" ]; then INDELs="0"; fi
    if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
    if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
    if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
    HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
    GQ=$(vcftools --vcf ${sample_genomic_id}.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')

    ## peddy-based sex check
    echo -e "${sample_genomic_id}\t${sample_genomic_id}\t0\t0\t0\t0" > ${sample_genomic_id}.ped
    bgzip -c ${sample_genomic_id}.vcf > ${sample_genomic_id}.vcf.gz
    tabix ${sample_genomic_id}.vcf.gz
    set +u
    source activate py2
    set -u
    python -m peddy --prefix ${sample_genomic_id} ${sample_genomic_id}.vcf.gz ${sample_genomic_id}.ped
    set +u
    conda deactivate
    set -u
    SEX=$(sed -n '2p' ${sample_genomic_id}.sex_check.csv | cut -f 7 -d, )
    if [ ${SEX} == "male" ]; then
        SEX='M'
    elif [ ${SEX} == "female" ]; then
        SEX='F'
    else 
        SEX='U'
    fi 

    echo -e "~{cohortRunID}.vcf\t${sample_genomic_id}\tGenome\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sample_genomic_id}.vcfLevel.byChr.qc 
    for j in {1..22} X Y; do 
        vcftools --vcf ${sample_genomic_id}.vcf --chr ${j} --stdout --recode > thisChr.vcf 
        numVariants=$(grep -v ^'#' thisChr.vcf | wc -l)
        if [ "${numVariants}" -gt "0" ]; then 
            TSTV=$(cat thisChr.vcf | vcf-tstv | awk '{print $1}')
            vcf-stats thisChr.vcf > tmpStats.txt 
            SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
            INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
            DEPTH=$(vcftools --vcf thisChr.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
            QUAL=$(vcftools --vcf thisChr.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
            HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
            HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
            HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
            if [ "${SNPs}" == "" ]; then SNPs="0"; fi
            if [ "${INDELs}" == "" ]; then INDELs="0"; fi
            if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
            if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
            if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
            HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
            GQ=$(vcftools --vcf thisChr.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')
            echo -e "~{cohortRunID}.vcf\t${sample_genomic_id}\t${j}\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sample_genomic_id}.vcfLevel.byChr.qc 
        fi
    done 

    # Upload QC results
    s3_cp_to_processing $sample_genomic_id.vcfLevel.byChr.qc $cohort_run_id 

    # Runs of Homozygosity mapping
    # Run bcftools roh on the sample
    bcftools annotate -c CHROM,POS,REF,ALT,AF1KG \
      -h 1000GP-AFs.v2023-11-02/AFs.tab.gz.hdr -a 1000GP-AFs.v2023-11-02/AFs.tab.gz $sample_genomic_id.vcf.gz |
      bcftools roh --AF-tag AF1KG -M 100 -m genetic-map/genetic_map_chr{CHROM}_combined_b37.txt -o roh.txt
    grep '^RG' roh.txt > $sample_genomic_id.RohRegions.txt
    # Filter the output to only include ROHs at least 5kb in size
    echo -e "chr\tstart\tend\tlength\tnumberOfMarkers\tquality" > header.txt
    awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,$6,$7,$8}}' $sample_genomic_id.RohRegions.txt | cat header.txt - > $sample_genomic_id.RegionsOfHomozygosity.txt
    # Create bed file for IGV visualization
    echo "#gffTags" > header.txt
    awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,"Length="$6";NumberOfMarkers="$7";Quality="$8}}' $sample_genomic_id.RohRegions.txt |
      cat header.txt - > $sample_genomic_id.RegionsOfHomozygosity.bed
    # Upload the ROHs to S3
    s3_cp_to_processing $sample_genomic_id.RegionsOfHomozygosity.txt $cohort_run_id 
    aws s3 cp $sample_genomic_id.RegionsOfHomozygosity.bed s3://tgp-sample-assets/$sample_guid/ROHs/ --only-show-errors
  done
}
