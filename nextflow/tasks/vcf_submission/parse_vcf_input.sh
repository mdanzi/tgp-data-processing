#!/bin/bash

# parse_vcf_input - Parse the VCF into a text file and handle ROH
parse_vcf_input() {
  local combined_vcf=$1
  local sample_genomic_ids_csv=$2
  local sample_genomic_ids_array=("${2//,/ }")
  local cohort_run_id=$3

  local input_vcf_filename=$(basename "$combined_vcf")
  local extension="${input_vcf_filename##*.}"

  # Decompress if necessary
  if [[ $extension == "gz" || $extension == "bgz" ]]; then
    gunzip -c "$combined_vcf" > "${cohort_run_id}.vcf"
  else
    cp "$combined_vcf" "${cohort_run_id}.vcf"
  fi

  # Parse the VCF into a text file
  local time_count time_count2
  time_count=$(date +%s)
  time_count2=$(echo "scale=0; ${time_count}/3600" | bc)
  set +x
  source activate py2
  set -x
  python $REPO/scripts/vcfParser/vcfFileParser.py \
    -r "${time_count2}${sample_genomic_ids_array[0]}" -s $sample_genomic_ids_csv \
    < "${cohort_run_id}.vcf" > "${cohort_run_id}.parsed.txt"
  # conda deactivate

  for sample_genomic_id in "${sample_genomic_ids_array[@]}"; do
    # Separate the parsed text file out by sample
    grep -w "^${sample_genomic_id}" "${cohort_run_id}.parsed.txt" > "${sample_genomic_id}.parsed.txt"
    # Skip setting the filter column values to use the CNN filter status
    # Skip swapping in the mitochondrial variant calls
    # Skip adding in custom variants
    # Download the ROH files to this workspace
    local num_fields
    local roh=${sample_genomic_id}.RegionsOfHomozygosity.txt
    local s3_roh=s3://tgp-sample-processing/${cohort_run_id}/$roh
    if s3_path_exists $s3_roh; then
      aws s3 cp $s3_roh . --only-show-errors
      # Add the ROH info as another column
      tail -n +2 $roh | cut -f 1,2,3,4 > "${sample_genomic_id}.RegionsOfHomozygosity.2.txt"
      num_fields=$(grep -v '^#' "${cohort_run_id}.vcf" | awk -F'\t' '{print NF; exit}' || true)
      num_fields=$((num_fields + 4))
      bedtools intersect -wo -a "${cohort_run_id}.vcf" -b "${sample_genomic_id}.RegionsOfHomozygosity.2.txt" |
        cut -f 1,2,${num_fields} | sort -u -k1n,1n -k2n,2n > variants_in_rohs.txt
    else
      touch variants_in_rohs.txt
    fi
    python $REPO/scripts/vcfParser/mergeOnROH.py --input="${sample_genomic_id}.parsed.txt" \
      --roh=variants_in_rohs.txt --output="${sample_genomic_id}.parsed.txt"
    # Upload the results to S3
    s3_cp_to_processing "${sample_genomic_id}.parsed.txt" $cohort_run_id 
  done
}
