cram_to_bam() {
  local sample_genomic_id=$1
  local s3_ref_path=$2
  local cram_file=$3

  # Get all files in s3 path that end in .fa
  references=$(aws s3 ls $s3_ref_path/ | awk '$4 ~ /.fa$/ {print $4}')

  # Check if cram has chr prefix
  chr_prefix=$(samtools view -H $cram_file | grep -w 'SN:chr22' | wc -l)

  num_cpus=$(grep -c ^processor /proc/cpuinfo)

  # The hs37d5.fa reference is the only one without chr prefix
  for reference in $references; do
    if [[ $chr_prefix -eq 0 && $reference != "hs37d5.fa" ]]; then
      continue
    fi

    # Try to convert cram to bam with the current reference
    reference_index=$reference.fai
    aws s3 cp --only-show-errors $s3_ref_path/$reference $reference
    aws s3 cp --only-show-errors $s3_ref_path/$reference_index $reference_index
    if samtools view -b -T $reference -@ $num_cpus -o $sample_genomic_id.bam $cram_file; then
      echo "Conversion successful with reference $reference"
      return 0
    fi

    rm $reference $reference_index

  done

  echo "Conversion failed with all references"
  return 1
}
