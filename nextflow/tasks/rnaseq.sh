#!/bin/bash

run_star() {
    local sample_genomic_id=$1
    local sample_run_id=$2
    local read1_csv=$3
    local read2_csv=$4

    local num_cores
    num_cores=$(get_num_cores)

    local read1_array=(${read1_csv//,/ })
    local read2_array=(${read2_csv//,/ })

    local firstFile=${read1_array[0]}
    local extension=${firstFile##*.}
    local fastqExtension=""
    if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=".${extension}"; fi
    local readCommand="cat"
    if [[ $extension = "bz2" ]]; then readCommand="bzcat"; fi
    if [[ $extension = "gz" ]]; then readCommand="zcat"; fi

    read1="${sample_genomic_id}_read1.fq${fastqExtension}"
    read2="${sample_genomic_id}_read2.fq${fastqExtension}"
    s3_stream_output "${read1_array[@]}" > $read1 &
    s3_stream_output "${read2_array[@]}" > $read2
    wait

    # align reads
    STAR --runThreadN $num_cores --runMode alignReads --genomeDir /STAR_index/ --readFilesIn $read1 $read2 --quantMode TranscriptomeSAM GeneCounts \
        --readFilesCommand $readCommand --outSAMtype BAM SortedByCoordinate --outFileNamePrefix $sample_genomic_id

    s3_cp_to_processing ${sample_genomic_id}Aligned.sortedByCoord.out.bam $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}Aligned.toTranscriptome.out.bam $sample_run_id 
}

run_salmon() {
    local sample_genomic_id=$1
    local sample_run_id=$2
    local transcriptomeBam=$3

    source activate salmon
    salmon quant -t /hs37d5_transcriptome.fa -l A -a $transcriptomeBam -o ${sample_genomic_id}_quant
    mv ${sample_genomic_id}_quant/quant.sf ${sample_genomic_id}_quant.sf
    s3_cp_to_processing ${sample_genomic_id}_quant.sf $sample_run_id 
}

run_rmats() {
    local sample_genomic_id=$1
    local sample_run_id=$2
    local genomeBam=$3
    local tissueType=$4
    local readPaired=${5:-"paired"}
    local readLength=${6:-"76"}
    local detectNovelSpliceSites=${7:-"false"}

    if [[ ! $tissueType =~ ^(Blood|Fibroblasts|Lymphocytes)$ ]]; then
        echo "ERROR: Invalid value for tissueType: $tissueType"
        exit 1
    fi

    spliceSites=""
    if [[ ${detectNovelSpliceSites} = "true" ]]; then spliceSites="--novelSS"; fi

    local num_cores
    num_cores=$(get_num_cores)

    aws s3 cp s3://tgp-data-analysis/RNASeq/Controls/Pooled/${tissueType}/ ./${tissueType}/ --recursive --only-show-errors
    echo "${genomeBam##*/}" > thisSample.txt
    ls ${tissueType}/*.bam > pooledControls.txt

    python /rmats/rmats.py --b1 thisSample.txt --b2 pooledControls.txt --gtf /rmats/Homo_sapiens.GRCh37.75.gtf -t $readPaired --readLength $readLength --nthread $num_cores \
        --od $sample_genomic_id --tmp ${sample_genomic_id}_tmp --variable-read-length --darts-model $spliceSites
    
    mv ${sample_genomic_id}/A3SS.MATS.JC.txt ${sample_genomic_id}_A3SS.MATS.JC.txt
    mv ${sample_genomic_id}/A5SS.MATS.JC.txt ${sample_genomic_id}_A5SS.MATS.JC.txt
    mv ${sample_genomic_id}/MXE.MATS.JC.txt ${sample_genomic_id}_MXE.MATS.JC.txt
    mv ${sample_genomic_id}/RI.MATS.JC.txt ${sample_genomic_id}_RI.MATS.JC.txt
    mv ${sample_genomic_id}/SE.MATS.JC.txt ${sample_genomic_id}_SE.MATS.JC.txt
    mv ${sample_genomic_id}/A3SS.MATS.JCEC.txt ${sample_genomic_id}_A3SS.MATS.JCEC.txt
    mv ${sample_genomic_id}/A5SS.MATS.JCEC.txt ${sample_genomic_id}_A5SS.MATS.JCEC.txt
    mv ${sample_genomic_id}/MXE.MATS.JCEC.txt ${sample_genomic_id}_MXE.MATS.JCEC.txt
    mv ${sample_genomic_id}/RI.MATS.JCEC.txt ${sample_genomic_id}_RI.MATS.JCEC.txt
    mv ${sample_genomic_id}/SE.MATS.JCEC.txt ${sample_genomic_id}_SE.MATS.JCEC.txt
    
    s3_cp_to_processing ${sample_genomic_id}_A3SS.MATS.JC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_A5SS.MATS.JC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_MXE.MATS.JC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_RI.MATS.JC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_SE.MATS.JC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_A3SS.MATS.JCEC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_A5SS.MATS.JCEC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_MXE.MATS.JCEC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_RI.MATS.JCEC.txt $sample_run_id 
    s3_cp_to_processing ${sample_genomic_id}_SE.MATS.JCEC.txt $sample_run_id 

}

parse_rnaseq_results() {
    local sample_genomic_id=$1
    local sample_run_id=$2

    aws s3 cp s3://tgp-sample-processing/${sample_run_id}/${sample_genomic_id}_A3SS.MATS.JC.txt . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/${sample_run_id}/${sample_genomic_id}_A5SS.MATS.JC.txt . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/${sample_run_id}/${sample_genomic_id}_MXE.MATS.JC.txt . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/${sample_run_id}/${sample_genomic_id}_RI.MATS.JC.txt . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/${sample_run_id}/${sample_genomic_id}_SE.MATS.JC.txt . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/${sample_run_id}/${sample_genomic_id}_quant.sf . --only-show-errors

    python $TGP_HOME/tgp-data-processing/scripts/rnaSeq/parseSplicing_A3SS.py --input=${sample_genomic_id}_A3SS.MATS.JC.txt --output=${sample_genomic_id}_A3SS.parsed.txt
    python $TGP_HOME/tgp-data-processing/scripts/rnaSeq/parseSplicing_A5SS.py --input=${sample_genomic_id}_A5SS.MATS.JC.txt --output=${sample_genomic_id}_A5SS.parsed.txt
    python $TGP_HOME/tgp-data-processing/scripts/rnaSeq/parseSplicing_MXE.py --input=${sample_genomic_id}_MXE.MATS.JC.txt --output=${sample_genomic_id}_MXE.parsed.txt
    python $TGP_HOME/tgp-data-processing/scripts/rnaSeq/parseSplicing_RI.py --input=${sample_genomic_id}_RI.MATS.JC.txt --output=${sample_genomic_id}_RI.parsed.txt
    python $TGP_HOME/tgp-data-processing/scripts/rnaSeq/parseSplicing_SE.py --input=${sample_genomic_id}_SE.MATS.JC.txt --output=${sample_genomic_id}_SE.parsed.txt

    # construct conversion table
    grep -v '^#' /Homo_sapiens.GRCh37.75.gtf | awk -F'\t' -v OFS='\t' '{if($3=="transcript"){print $9}}' | cut -d'"' -f 2,4 | tr '"' '\t' | sort -k1,1 -k2,2 | uniq > transcriptToGeneConversionTable.txt

    python $TGP_HOME/tgp-data-processing/scripts/rnaSeq/mergeRNASeqResults.py --input=${sample_genomic_id}
    s3_cp_to_processing ${sample_genomic_id}_RNASeq_parsed.txt $sample_run_id 
    
}
