generate_sample_map_file() {
  cohortRunID=$1
  allSampleGenomicIDs=$2

  for sampleGenomicID in ${allSampleGenomicIDs//,/ }; do
    printf "%s\t%s.hg19.g.vcf.gz\n" $sampleGenomicID $sampleGenomicID >> sample_map_file.txt
  done

  aws s3 cp sample_map_file.txt s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
}

cnn_and_roh() {
  cohortRunID=$1
  sampleGenomicID=$2
  sampleRunID=$3
  sampleGUID=$4
  callingIntervals=$5
  gatk_path=$6
  singleton=$7


  aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.hg19.g.vcf.gz . --only-show-errors
  aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.hg19.g.vcf.idx . --only-show-errors
  gunzip ${sampleGenomicID}.hg19.g.vcf.gz


  # prepare 1000Genomes data on allele frequencies and genetic mappings for ROH mapping
  aws s3 cp s3://tgp-data-analysis/StructuralVariation/genetic-map.tgz . --only-show-errors
  aws s3 cp s3://tgp-data-analysis/StructuralVariation/1000GP-AFs.tgz . --only-show-errors
  tar -zxvf 1000GP-AFs.tgz
  tar -zxvf genetic-map.tgz

  ${gatk_path} --java-options "-Xmx5g" GenotypeGVCFs -V ${sampleGenomicID}.hg19.g.vcf -R $TGP_HOME/ref/hs37d5.fa -O ${sampleGenomicID}.hg19.genotyped.vcf --allow-old-rms-mapping-quality-annotation-data --use-new-qual-calculator --only-output-calls-starting-in-intervals -L ${callingIntervals} --merge-input-intervals
  bcftools filter -i "(FORMAT/DP)>=6" ${sampleGenomicID}.hg19.genotyped.vcf > ${sampleGenomicID}.hg19.genotyped.filtered.vcf
  if [[ ${singleton} -eq 1 ]]; then
    bgzip -c ${sampleGenomicID}.hg19.genotyped.filtered.vcf > ${cohortRunID}.vcf.gz
    tabix ${cohortRunID}.vcf.gz
    aws s3 cp ${cohortRunID}.vcf.gz s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
    aws s3 cp ${cohortRunID}.vcf.gz.tbi s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
  fi
  set +ux
  source activate gatk
  set -ux
  ${gatk_path} CNNScoreVariants -V ${sampleGenomicID}.hg19.genotyped.filtered.vcf -R $TGP_HOME/ref/hs37d5.fa -O ${sampleGenomicID}.CNN.vcf
  ${gatk_path} FilterVariantTranches -V ${sampleGenomicID}.CNN.vcf -O ${sampleGenomicID}.CNN.filtered.vcf --resource $TGP_HOME/ref/hapmap_3.3.b37.vcf --resource $TGP_HOME/ref/Mills_and_1000G_gold_standard.indels.b37.vcf --info-key CNN_1D --snp-tranche 99.95 --snp-tranche 99.9 --snp-tranche 99.5 --snp-tranche 99 --snp-tranche 98 --snp-tranche 95 --indel-tranche 99.4 --indel-tranche 99.2 --indel-tranche 99 --indel-tranche 98 --indel-tranche 95
  set +ux
  conda deactivate
  set -ux
  # upload the CNN-filtered VCF to the cohortRun folder on S3
  aws s3 cp ${sampleGenomicID}.CNN.filtered.vcf s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
  ## Runs of Homozygosity mapping
  # remove the CNN-failed variants
  grep -wv 'CNN_1D_SNP_Tranche_99.95_100.00' ${sampleGenomicID}.CNN.filtered.vcf | grep -wv 'CNN_1D_INDEL_Tranche_99.40_100.00' > ${sampleGenomicID}.filtered.vcf
  bgzip ${sampleGenomicID}.filtered.vcf
  tabix ${sampleGenomicID}.filtered.vcf.gz

  # run bcftools roh on the sample
  run_roh ${sampleGenomicID}.filtered.vcf.gz $sampleGenomicID $sampleGUID $sampleRunID

}

run_roh() {
  local vcf_gz=$1
  local sample_genomic_id=$2
  local sample_id=$3
  local run_id=$4
  local roh_regions=${sample_genomic_id}.RohRegions.txt
  local roh_txt=${sample_genomic_id}.RegionsOfHomozygosity.txt
  local roh_bed=${sample_genomic_id}.RegionsOfHomozygosity.bed
  bcftools annotate -c CHROM,POS,REF,ALT,AF1KG -h 1000GP-AFs/AFs.tab.gz.hdr -a 1000GP-AFs/AFs.tab.gz "${vcf_gz}" |
    bcftools roh --AF-tag AF1KG -M 100 -m genetic-map/genetic_map_chr{CHROM}_combined_b37.txt -o roh.txt
  if grep '^RG' roh.txt > $roh_regions; then
    # filter the output to only include ROHs at least 5kb in size
    echo -e "chr\tstart\tend\tlength\tnumberOfMarkers\tquality" > $roh_txt
    awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,$6,$7,$8}}' $roh_regions >> $roh_txt
    # create bed file for IGV visualization
    echo "#gffTags" > $roh_bed
    awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,"Length="$6";NumberOfMarkers="$7";Quality="$8}}' $roh_regions >> $roh_bed
    # upload the roh's to s3
    aws s3 cp $roh_txt s3://tgp-sample-processing/${run_id}/ --only-show-errors
    aws s3 cp $roh_bed s3://tgp-sample-assets/${sample_id}/ROHs/ --only-show-errors
  fi
}

import_gvcfs() {

  WORKSPACE=genomicsdb

  cohortRunID=$1
  all_sample_genomics_ids=$2
  all_sample_run_ids=$3
  map_file=$4
  interval=$5

  rm -rf $WORKSPACE

  aws s3 cp s3://tgp-sample-processing/$cohortRunID/$map_file . --only-show-errors

  sampleGenomicIDsArray=(${all_sample_genomics_ids//,/ })
  sampleRunIDsArray=(${all_sample_run_ids//,/ })

  sample_count=${#sampleGenomicIDsArray[@]}
  for ((idx=0; idx < $sample_count; idx++)); do
    gvcf="s3://tgp-sample-processing/${sampleRunIDsArray[${idx}]}/${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz"
    aws s3 cp $gvcf . --only-show-errors
    if s3_path_exists ${gvcf}.tbi; then
      aws s3 cp ${gvcf}.tbi . --only-show-errors
    else
      gunzip ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz
      bgzip ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf
      tabix ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz
    fi
  done

  # We've seen some GenomicsDB performance regressions related to intervals, so we're going to pretend we only have a single interval
  # using the --merge-input-intervals arg
  # There's no data in between since we didn't run HaplotypeCaller over those loci so we're not wasting any compute

  # The memory setting here is very important and must be several GiB lower
  # than the total memory allocated to the VM because this tool uses
  # a significant amount of non-heap memory for native libraries.
  # Also, testing has shown that the multithreaded reader initialization
  # does not scale well beyond 5 threads, so don't increase beyond that.
  $GATK --java-options "-Xms8000m -Xmx25000m" \
    GenomicsDBImport \
    --genomicsdb-workspace-path $WORKSPACE \
    -L ${interval} \
    --sample-name-map ${map_file} \
    --reader-threads 5 \
    --merge-input-intervals 

  tar -cf $WORKSPACE.tar $WORKSPACE 
}

genotype_gvcfs() {
  workspace_tar=$1
  interval=$2
  outfile=$3

  tar -xf ${workspace_tar}
  WORKSPACE=$(basename ${workspace_tar} .tar)

  $GATK --java-options "-Xms8000m -Xmx25000m" \
    GenotypeGVCFs \
    -R $TGP_HOME/ref/hs37d5.fa \
    -O ${outfile} \
    --only-output-calls-starting-in-intervals \
    --use-new-qual-calculator \
    -V gendb://$WORKSPACE \
    -L ${interval} \
    --allow-old-rms-mapping-quality-annotation-data \
    --merge-input-intervals

#mv ~{output_vcf_filename} /tmp/scratch/
#mv ~{output_vcf_filename}.tbi /tmp/scratch/

}

gather_vcfs() {
  outfile=$1
  cohort_run_id=$2
  shift
  shift # remaining args are *.vcf.gz files


  # --ignore-safety-checks makes a big performance difference so we include it in our invocation.
  # This argument disables expensive checks that the file headers contain the same set of
  # genotyped samples and that files are in order by position of first record.
  $GATK --java-options "-Xms6000m -Xmx6500m" \
    GatherVcfsCloud \
    --ignore-safety-checks \
    --gather-type BLOCK \
    $(printf -- "--input %s " "$@") \
    --output $outfile

  # output should be bgzipped
  mv $outfile tmp.gz
  tabix tmp.gz

  bcftools filter -i "MAX(FORMAT/DP)>=6" -o $outfile -O b tmp.gz 
  tabix $outfile
  s3_cp_to_processing $outfile $cohort_run_id
  s3_cp_to_processing $outfile.tbi $cohort_run_id

}

vcf_level_qc() {
  cohortRunID=$1
  sampleGenomicID=$2
  sampleRunID=$3
  sampleGUID=$4

  combined_vcf=${cohortRunID}.vcf.gz
  s3_combined_vcf=$(get_s3_path $cohortRunID $combined_vcf)

  aws s3 cp $s3_combined_vcf . --only-show-errors
  gunzip $combined_vcf

  vcf-subset -c ${sampleGenomicID} ${cohortRunID}.vcf > ${sampleGenomicID}.vcf
  echo -e "fileName\tsampleName\tChr\tTSTV\tnumSNPs\tnumINDELs\tavgDepth\tavgQuality\tHeterozygousHomozygousRatio\tmeanGQ\tGender" > ${sampleGenomicID}.vcfLevel.byChr.qc
  TSTV=$(cat ${sampleGenomicID}.vcf | vcf-tstv | awk '{print $1}')
  vcf-stats ${sampleGenomicID}.vcf > tmpStats.txt 
  SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
  INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
  DEPTH=$(vcftools --vcf ${sampleGenomicID}.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
  QUAL=$(vcftools --vcf ${sampleGenomicID}.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
  HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
  HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
  HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
  if [ "${SNPs}" == "" ]; then SNPs="0"; fi
  if [ "${INDELs}" == "" ]; then INDELs="0"; fi
  if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
  if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
  if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
  HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
  GQ=$(vcftools --vcf ${sampleGenomicID}.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')

  ## new, peddy-based sex check
  echo -e "${sampleGenomicID}\t${sampleGenomicID}\t0\t0\t0\t0" > ${sampleGenomicID}.ped
  bgzip -c ${sampleGenomicID}.vcf > ${sampleGenomicID}.vcf.gz
  tabix ${sampleGenomicID}.vcf.gz
  set +u
  source activate py2
  set -u
  python -m peddy --prefix ${sampleGenomicID} ${sampleGenomicID}.vcf.gz ${sampleGenomicID}.ped
  set +u
  conda deactivate
  set -u
  SEX=$(sed -n '2p' ${sampleGenomicID}.sex_check.csv | cut -f 7 -d, )
  if [ ${SEX} == "male" ]; then
    SEX='M'
  elif [ ${SEX} == "female" ]; then
    SEX='F'
  else 
    SEX='U'
  fi 

  echo -e "${cohortRunID}.vcf\t${sampleGenomicID}\tGenome\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
  for j in {1..22} X Y; do 
    vcftools --vcf ${sampleGenomicID}.vcf --chr ${j} --stdout --recode > thisChr.vcf 
    numVariants=$(grep -v ^'#' thisChr.vcf | wc -l)
    if [ "${numVariants}" -gt "0" ]; then 
      TSTV=$(cat thisChr.vcf | vcf-tstv | awk '{print $1}')
      vcf-stats thisChr.vcf > tmpStats.txt 
      SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
      INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
      DEPTH=$(vcftools --vcf thisChr.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
      QUAL=$(vcftools --vcf thisChr.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
      HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
      HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
      HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
      if [ "${SNPs}" == "" ]; then SNPs="0"; fi
      if [ "${INDELs}" == "" ]; then INDELs="0"; fi
      if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
      if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
      if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
      HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
      GQ=$(vcftools --vcf thisChr.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')
      echo -e "${cohortRunID}.vcf\t${sampleGenomicID}\t${j}\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
      fi
    done 
    ##ancestry clustering
    aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.peddySites.vcf . --only-show-errors
    vcftools --vcf ${sampleGenomicID}.peddySites.vcf --012 --out ${sampleGenomicID}
    set +u
    source activate py2
    set -u
    python $TGP_HOME/tgp-data-processing/scripts/vcfLevelQC/samplePCA.py -i ${sampleGenomicID}
    set +u
    conda deactivate
    set -u

  # upload results 
  aws s3 cp ${sampleGenomicID}.AncestryClustering.png s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
  aws s3 cp ${sampleGenomicID}.vcfLevel.byChr.qc s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors

}

sv_bamlets() {
  sampleGenomicID=$1
  sampleRunID=$2
  sampleGUID=$3
  allSampleRunIDs=$4

  source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport & s3_path_exists
  trap "errorReport" ERR


  s3_sv_vcf=$(get_s3_path $sampleRunID $sampleGenomicID.combined.genotyped.vcf)
  s3_bam=$(get_s3_path $sampleRunID $sampleGenomicID.hg19.sorted.dedup.bam)
  if ! s3_path_exists $s3_sv_vcf || ! s3_path_exists $s3_bam; then
    echo "[TGP Warning]: Exiting without creating SV bamlets because $s3_sv_vcf and $s3_bam were not found" >&2
    exit 0
  fi

  # Download all SV vcfs
  # NB: Old implementation used below but its not compatible with glacier restored objects:
  # cp --recursive --exclude "*" --include "*.combined.genotyped.vcf"
  for runID in ${allSampleRunIDs//,/ }; do
    # Find the .combined.genotyped.vcf file in the S3 bucket directory
    file=$(aws s3 ls s3://tgp-sample-processing/${runID}/ | awk '/.combined.genotyped.vcf/ {print $4}' | head -n 1)
    if [ -n "$file" ]; then
      s3_cp_from_processing $file $runID
    fi
  done

  # Don't delete existing bamlets from previous cohort runs for safety in case
  # new bamlets can't be created for some reason
  # aws s3 rm s3://tgp-sample-assets/${sampleGUID}/SVs/ --recursive --only-show-errors

  aws s3 cp ${s3_bam} . --only-show-errors
  aws s3 cp ${s3_bam}.bai . --only-show-errors
  set +u
  source activate py37
  set -u
  for VCF in *.combined.genotyped.vcf; do
    python $TGP_HOME/tgp-data-processing/scripts/structuralVariation/printBamlets.py --vcf=${VCF} --bam=${sampleGenomicID}.hg19.sorted.dedup.bam --out=${sampleGUID}/SVs/
  done
  set +u
  conda deactivate
  set -u            
  cd ${sampleGUID}/SVs/
  if ! ls *.bam &>/dev/null; then
    echo "[TGP Warning]: No *.bam files were found. Exiting without creating SV bamlets" >&2
    exit 0
  fi
  for FILE in *.bam; do samtools index ${FILE}; done
  cd ../../
  aws s3 cp ${sampleGUID}/SVs/ s3://tgp-sample-assets/${sampleGUID}/SVs/ --recursive --only-show-errors

}

parse_vcf() {
  cohortRunID=$1
  allSampleGenomicIDs=$2
  allSampleRunIDs=$3

  combinedVCF="${cohortRunID}.vcf.gz"
  aws s3 cp s3://tgp-sample-processing/${cohortRunID}/${combinedVCF} . --only-show-errors
  gunzip ${combinedVCF}

  sampleGenomicIDsArray=(${allSampleGenomicIDs//,/ })
  sampleRunIDsArray=(${allSampleRunIDs//,/ })
  firstSampleGenomicID=${sampleGenomicIDsArray[0]}


  # parse the vcf into a text file
  timeCount=$(date +%s)
  timeCount2=$(echo "scale=0; ${timeCount}/3600" | bc)
  set +u
  source activate py2
  set -u
  python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${firstSampleGenomicID}" -s $allSampleGenomicIDs < ${cohortRunID}.vcf > ${cohortRunID}.parsed.txt

  # max_idx=$((${#sampleGenomicIDsArray[@]}-1))
  # idx_list=$(seq 0 $max_idx)
  # for idx in {0..~{length(sampleGenomicIDs)-1}}; do 
  sample_count=${#sampleGenomicIDsArray[@]}
  for ((idx=0; idx < $sample_count; idx++)); do
    sampleID=${sampleGenomicIDsArray[${idx}]}
    runID=${sampleRunIDsArray[${idx}]}
    # separate the parsed text file out by sample
    grep -w "^${sampleID}" ${cohortRunID}.parsed.txt > ${sampleID}.parsed.txt
    # download the CNN-filtered vcf file
    aws s3 cp s3://tgp-sample-processing/${cohortRunID}/${sampleID}.CNN.filtered.vcf . --only-show-errors
    # parse the CNN-filtered vcf into a text file
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.CNN.filtered.vcf > ${sampleID}.CNN.parsed.txt
    # set the filter column values to use the CNN filter status
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnCNNFilters.py --input=${sampleID}.parsed.txt --cnn=${sampleID}.CNN.parsed.txt --output=${sampleID}.parsed.2.txt
    # swap the mitochondrial variant calls
    if s3_path_exists s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf; then
      aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf . --only-show-errors
      aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf.idx . --only-show-errors
      python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.MT.final.split.vcf > ${sampleID}.MT.parsed.txt
      awk -F'\t' -v OFS='\t' '{if($2<25){print $0}}' ${sampleID}.parsed.2.txt | cat - ${sampleID}.MT.parsed.txt > ${sampleID}.parsed.3.txt
    else
      cp ${sampleID}.parsed.2.txt ${sampleID}.parsed.3.txt
    fi
    # add in custom variants
    if s3_path_exists s3://tgp-sample-processing/${runID}/${sampleID}.customSites.vcf; then
      aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.customSites.vcf . --only-show-errors
      python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.customSites.vcf > ${sampleID}.customSites.parsed.txt
      python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnCustomGenotypeCalls.py --input=${sampleID}.parsed.3.txt --customSites=${sampleID}.customSites.parsed.txt --output=${sampleID}.parsed.4.txt
    else
      cp ${sampleID}.parsed.3.txt ${sampleID}.parsed.4.txt
    fi
    # download the ROH files to this workspace
    if s3_path_exists s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt; then
      aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt . --only-show-errors
      # add the ROH info as another column
      tail -n +2 ${sampleID}.RegionsOfHomozygosity.txt | cut -f 1,2,3,4 > ${sampleID}.RegionsOfHomozygosity.2.txt
      numFields=$(grep -v '^#' ${cohortRunID}.vcf | awk -F'\t' '{print NF; exit}' || true)
      numFields=$((numFields + 4))
      bedtools intersect -wo -a ${cohortRunID}.vcf -b ${sampleID}.RegionsOfHomozygosity.2.txt | cut -f 1,2,${numFields} | sort -u -k1n,1n -k2n,2n > variantsInROHs.txt
    else
      touch variantsInROHs.txt
    fi
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnROH.py --input=${sampleID}.parsed.4.txt --roh=variantsInROHs.txt --output=${sampleID}.parsed.txt
    # upload the results to S3
    aws s3 cp ${sampleID}.parsed.txt s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
  done

}


