merge_bams() {
  sampleGenomicID=$1
  sampleRunID=$2
  shift
  shift # remaining args are bam files
  samtools merge -c -p -o ${sampleGenomicID}.sorted.bam "$@"
  s3_cp_to_processing ${sampleGenomicID}.sorted.bam $sampleRunID
}

fastqc() {

  local sample_genomic_id=$1
  local sample_run_id=$2
  local read1_csv=$3
  local read2_csv=$4
  local output="${sample_genomic_id}.fastqLevel.qc"

  local read1_array=(${read1_csv//,/ })
  local read2_array=(${read2_csv//,/ })

  # re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
  local firstFile=${read1_array[0]}
  local extension=${firstFile##*.}
  local fastqExtension=""
  if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=$(echo ".${extension}"); fi

  # make output file for this file 
  echo -e "fileName\tsampleName\tnumberOfReads\tsizeInGB\tQ30" > $output

  # QC read1
  read1="${sample_genomic_id}_read1.fq${fastqExtension}"
  read2="${sample_genomic_id}_read2.fq${fastqExtension}"
  s3_stream_output "${read1_array[@]}" > $read1 &
  s3_stream_output "${read2_array[@]}" > $read2
  wait

  $TGP_HOME/FastQC/fastqc ${read1} 
  unzip ${sample_genomic_id}_read1_fastqc.zip
  numReads=$(grep "Total Sequences" ${sample_genomic_id}_read1_fastqc/fastqc_data.txt | awk '{print $3}')
  fileSize=$(du -h ${read1} | awk '{print $1}')
  numQ30=$(grep -A42 -m1 "Per sequence quality" ${sample_genomic_id}_read1_fastqc/fastqc_data.txt | sed -n '/^29/{:a;n;p;/^>>END/!ba}' | head -n -1 | awk '{n1+=$2} END {print n1}')
  percentQ30=$(expr 100 \* ${numQ30} / ${numReads} )
  echo -e "${read1}\t${sample_genomic_id}\t${numReads}\t${fileSize}\t${percentQ30}" >> $output

  # QC read2
  $TGP_HOME/FastQC/fastqc ${read2} 
  unzip ${sample_genomic_id}_read2_fastqc.zip
  numReads=$(grep "Total Sequences" ${sample_genomic_id}_read2_fastqc/fastqc_data.txt | awk '{print $3}')
  fileSize=$(du -h ${read2} | awk '{print $1}')
  numQ30=$(grep -A42 -m1 "Per sequence quality" ${sample_genomic_id}_read2_fastqc/fastqc_data.txt | sed -n '/^29/{:a;n;p;/^>>END/!ba}' | head -n -1 | awk '{n1+=$2} END {print n1}')
  percentQ30=$(expr 100 \* ${numQ30} / ${numReads} )
  echo -e "${read2}\t${sample_genomic_id}\t${numReads}\t${fileSize}\t${percentQ30}" >> $output

  # upload output to S3
  s3_cp_to_processing $output $sample_run_id
}

align_presharded() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local s3_read1=$3
  local s3_read2=$4
  local read1=${s3_read1##*/}
  local read2=${s3_read2##*/}

  aws s3 cp $s3_read1 $read1 --only-show-errors
  aws s3 cp $s3_read2 $read2 --only-show-errors

  # note, the Read Group is written to match the style and defaults used by Picard AddOrReplaceReadGroups
  readGroup="@RG\\tID:1\\tSM:${sample_genomic_id}\\tPL:illumina\\tLB:lib1\\tPU:unit1"

  # align reads
  bwa mem -M -t 16 -R "${readGroup}" $TGP_HOME/ref/hs37d5.fa $read1 $read2 |
  sambamba view -f bam -l 0 -S -t 16 /dev/stdin |
  sambamba sort -t 16 -o $sample_genomic_id.sorted.bam /dev/stdin
}

base_recalibrator() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3
  local recalibration_report_filename=$4
  local sequence_group_interval_file=$5

  s3_cp_from_processing $bam $sample_run_id
  s3_cp_from_processing $bam.bai $sample_run_id

  $GATK --java-options -Xmx4g \
    BaseRecalibrator \
    -R $TGP_HOME/ref/hs37d5.fa \
    -I $bam \
    --use-original-qualities \
    -O ${recalibration_report_filename} \
    --known-sites $TGP_HOME/ref/dbsnp_138.b37.vcf \
    --known-sites $TGP_HOME/ref/Mills_and_1000G_gold_standard.indels.b37.vcf \
    --known-sites $TGP_HOME/ref/1000G_phase1.indels.b37.vcf \
    -L ${sequence_group_interval_file}
}

gather_bqsr_reports() {
  local outfile=$1
  shift # remaining args are bqsr reports

  $GATK --java-options -Xmx4g \
    GatherBQSRReports \
    $(printf -- "-I %s " "$@") \
    -O $outfile
}

apply_bqsr() {
  local sample_run_id=$1
  local bam=$2
  local recalibration_report=$3
  local sequence_group_interval_file=$4
  local output_bam=$5

  s3_cp_from_processing $bam $sample_run_id
  s3_cp_from_processing $bam.bai $sample_run_id

  $GATK --java-options -Xmx4g \
    ApplyBQSR \
    -R $TGP_HOME/ref/hs37d5.fa \
    -I ${bam} \
    -O ${output_bam} \
    -L ${sequence_group_interval_file} \
    -bqsr ${recalibration_report} \
    --static-quantized-quals 10 --static-quantized-quals 20 --static-quantized-quals 30 \
    --add-output-sam-program-record \
    --create-output-bam-md5 \
    --use-original-qualities
}

gather_bams() {
  local output_bam=$1
  shift # remaining args are bam files

  $GATK --java-options -Xmx4g \
    GatherBamFiles \
    $(printf -- "-I %s " "$@") \
    -O $output_bam
  samtools index $output_bam
}

haplotype_caller() {
  local input_bam=$1
  local sequence_group_interval_file=$2
  local output_gvcf=$3

  $GATK --java-options -Xmx4g \
    HaplotypeCaller \
    -R $TGP_HOME/ref/hs37d5.fa \
    -I ${input_bam} \
    -ERC GVCF \
    -L ${sequence_group_interval_file} \
    -ip 100 \
    -O $output_gvcf
}

merge_vcfs() {
  local output=$1
  shift # remaining args are vcf files

  $GATK MergeVcfs $(printf -- "-I %s " "$@") -O $output
}

bam_to_fastq() {
  local sample_genomic_id=$1
  local sample_run_id=$2
  local bam=$3

  samtools collate -f --output-fmt BAM -r 100000 -o ${sample_genomic_id}_collated.bam ${bam}
  samtools fastq -c 4 -1 ${sample_genomic_id}_R1.fastq.gz -2 ${sample_genomic_id}_R2.fastq.gz \
    -0 /dev/null -s /dev/null ${sample_genomic_id}_collated.bam
  s3_cp_to_processing ${sample_genomic_id}_R1.fastq.gz $sample_run_id
  s3_cp_to_processing ${sample_genomic_id}_R2.fastq.gz $sample_run_id
}
