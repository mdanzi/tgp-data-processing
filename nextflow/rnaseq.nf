
process run_star {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 8
  memory "60GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/rna-star:2.7.10b"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(read1_csv)
  val(read2_csv)

  output:
  path("${sample_genomic_id}Aligned.sortedByCoord.out.bam")
  path("${sample_genomic_id}Aligned.toTranscriptome.out.bam")


  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_star $sample_genomic_id $sample_run_id $read1_csv $read2_csv
  """
}


process run_salmon {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/rna-salmon:1.10.1"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  path(transcriptome_bam)

  output:
  val "salmonMarker"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_salmon $sample_genomic_id $sample_run_id $transcriptome_bam
  """
}

process run_rmats {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 4
  memory "15GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/rna-rmats:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  path(genome_bam)
  val(tissue_type)

  output:
  val "rmatsMarker"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_rmats $sample_genomic_id $sample_run_id $genome_bam $tissue_type 
  """
}

process parse_rnaseq_results {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/rna-import:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(salmon)
  val(rmats)

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli parse_rnaseq_results $sample_genomic_id $sample_run_id 
  """
}

workflow {
    fastq1_csv = params.fastqs.collect { it[0] }.join(",")
    fastq2_csv = params.fastqs.collect { it[1] }.join(",")
    (genome_bam, transcriptome_bam) = run_star(params.sampleGenomicId, params.sampleRunId, fastq1_csv, fastq2_csv)
    salmon = run_salmon(params.sampleGenomicId, params.sampleRunId, transcriptome_bam)
    rmats = run_rmats(params.sampleGenomicId, params.sampleRunId, genome_bam, params.tissueType)
    parse_results = parse_rnaseq_results(params.sampleGenomicId, params.sampleRunId, salmon, rmats)
}
