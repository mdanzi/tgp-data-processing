// include { set_resource_label } from './util'

def gatk_path = "/home/ec2-user/gatk-4.2.6.1/gatk"

def init_repo = """
repo=/home/ec2-user/tgp-data-processing
if [[ ! -d \$repo ]]; then
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git \$repo
fi
"""


process calculateCNNAndROH {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '7 GB'
  queue "$params.priority_queue"
  input:
  val sample
  val cohortRunID
  path callingIntervals
  val gatkPath
  val singleton

  output:
  val "${sample.sampleGenomicId}.CNN.filtered.vcf"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline cnn_and_roh \
    $cohortRunID $sample.sampleGenomicId $sample.sampleRunId $sample.sampleGuid \
    $callingIntervals $gatkPath $singleton
  """
}

process vcfLevelQC {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '3.5 GB'
  //memory '7 GB'
  queue "$params.small_spot_queue"
  input:
  val sample
  val cohortRunID
  val combined_vcf  // Only needed for workflow dependency
  val cnn_vcf_list  // Only needed for workflow dependency


  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline vcf_level_qc \
    $cohortRunID $sample.sampleGenomicId $sample.sampleRunId $sample.sampleGuid
  """
}

process svBamlets {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '3.5 GB'
  queue "$params.small_spot_queue"
  input:
  val sample
  val sample_run_csv


  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline sv_bamlets \
    $sample.sampleGenomicId $sample.sampleRunId $sample.sampleGuid $sample_run_csv
  """
}

process parseVCF {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  //memory '4 GB'
  memory '3.5 GB'
  queue "$params.small_spot_queue"
  input:
  val cohort_run_id
  val sample_genomic_csv
  val sample_run_csv
  val combined_vcf  // Only needed for workflow dependency
  val cnn_vcf_list  // Only needed for workflow dependency

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline parse_vcf \
    $cohort_run_id $sample_genomic_csv $sample_run_csv
  """
}

process generateSampleMapFile {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '0.75 GB'
  queue "$params.xsmall_spot_queue"
  input:
  val cohort_run_id
  val sample_genomic_csv

  output:
  val "sample_map_file.txt"
  
  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline generate_sample_map_file \
    $cohort_run_id $sample_genomic_csv 
  """
}

process importGVCFs {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  //maxForks 1
  cpus 4
  memory '30 GB'
  queue "$params.large_spot_queue"

  input:
  val cohort_run_id
  val sample_genomic_csv
  val sample_run_csv
  val map_file
  tuple val(interval_index), path(interval)

  output:
  tuple path("genomicsdb.tar"), val(interval_index), path(interval)

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline import_gvcfs \
    $cohort_run_id $sample_genomic_csv $sample_run_csv $map_file $interval
  """
}

process genotypeGVCFs {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  //maxForks 1
  cpus 2
  memory '30 GB'
  queue "$params.large_spot_queue"

  input:
  tuple path(workspace_tar), val(interval_idx), path(interval)
  //tuple path workspace_tar
  //path interval
  //val interval_idx
  val cohort_run_id

  output:
  path "${cohort_run_id}.${interval_idx}.vcf.gz"

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline genotype_gvcfs \
    $workspace_tar $interval ${cohort_run_id}.${interval_idx}.vcf.gz
  """
}

process gatherVCFs {
  resourceLabels "cromwell-task-id": "${params.cohortRunId}-${task.process}-${task.index}-${task.attempt}"
  memory '3.5 GB'
  queue "$params.small_spot_queue"

  input:
  val output_file
  val cohort_run_id
  path input_vcfs

  output:
  val output_file

  """
  $init_repo
  /home/ec2-user/tgp-data-processing/nextflow/tgp-pipeline gather_vcfs \
    $output_file $cohort_run_id $input_vcfs
  """
}

workflow {
  def samples = Channel.fromList(params.samples)
  sample_genomic_csv = params.samples.sampleGenomicId.join(",")
  def index_counter = 1
  scattered_intervals_channel = Channel.fromList(params.scattered_intervals).map { item -> [index_counter++, item] }
  sample_run_csv = params.samples.sampleRunId.join(",")
  combined_vcf_name = "${params.cohortRunId}.vcf.gz"
  sample_count = params.samples.size()
  is_singleton = sample_count == 1 ? 1 : 0
  cnn_vcfs = calculateCNNAndROH(samples, params.cohortRunId, params.all_calling_intervals, gatk_path, is_singleton)
  cnn_vcf_list = cnn_vcfs.toList()
  //svBamlets(samples, sample_run_csv)
  if ( is_singleton == 1 ) {
    vcfLevelQC(samples, params.cohortRunId, combined_vcf_name, cnn_vcf_list)
    parseVCF(params.cohortRunId, sample_genomic_csv, sample_run_csv, combined_vcf_name, cnn_vcf_list)
  }
  else {
    map_file = generateSampleMapFile(params.cohortRunId, sample_genomic_csv)
    genomic_dbs = importGVCFs(params.cohortRunId, sample_genomic_csv, sample_run_csv, map_file, scattered_intervals_channel)
    genotype_gvcfs = genotypeGVCFs(genomic_dbs, params.cohortRunId)
    gathered_vcf = gatherVCFs(combined_vcf_name, params.cohortRunId, genotype_gvcfs.toList()) 
    // gathered_vcf output is equivalent to combined_vcf_name. Former used here for job dependency
    vcfLevelQC(samples, params.cohortRunId, gathered_vcf, cnn_vcf_list)
    parseVCF(params.cohortRunId, sample_genomic_csv, sample_run_csv, gathered_vcf, cnn_vcf_list)
  }
}


