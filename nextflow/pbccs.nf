
process alignPb {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 16
  memory "60GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-pb:0.1.40"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(fastqs)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)

  output:
  tuple path("${sample_genomic_id}.bam"), path("${sample_genomic_id}.bam.bai")

  script:
  // def fastq_list = fastqs.join(",")
  // def ref_fasta = params.ref_fasta
  // def map_preset = params.map_preset ?: "CCS"
  // def drop_per_base_n_pulse_tags = params.drop_per_base_n_pulse_tags ?: true
  """
  $params.init_repo_cmd
  $params.pipeline_cli align_pb $fastqs $sample_genomic_id $sample_run_id $ref_fasta 
  """
}


process deepVariant {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 16
  memory "120GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-deepvariant:1.8.0"

  input:
  tuple path(bam), path(bai)
  val(sample_genomic_id)
  val(sample_run_id)
  path(ref_fasta)
  path(ref_fasta_fai)
  val(model_type) // optional -- should be "PACBIO" for PacBio samples

  output:
  path("${sample_genomic_id}.DV.vcf.gz")

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli deep_variant $bam $ref_fasta $sample_run_id $model_type $sample_genomic_id
  """
}

process run_trgt {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 8
  memory "30GB"
  queue "$params.large_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-trgt:1.1.1"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(repeat_catalog)
  val(sex) // values of either "M" or "F"

  output:
  path("${sample_genomic_id}.trgt.vcf.gz")

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_trgt $sample_genomic_id $sample_run_id $bam \
    $ref_fasta $repeat_catalog $sex
  """
}

process run_pbsv {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 4
  memory "60GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-pbsv:2.9"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)

  output:
  path("${sample_genomic_id}.pbsv.vcf")

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_pbsv $sample_genomic_id $sample_run_id $bam \
    $ref_fasta 
  """
}

process run_hiphase {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 4
  memory "30GB"
  queue "$params.large_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-hiphase:1.4.2"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(dv_vcf)
  path(pbsv_vcf)
  path(trgt_vcf)

  output:
  tuple path("${sample_genomic_id}.haplotagged.bam"), path("${sample_genomic_id}.haplotagged.bam.bai")
  path "${sample_genomic_id}.DV.hiphase.vcf.gz"
  path "${sample_genomic_id}.pbsv.hiphase.vcf.gz"
  path "${sample_genomic_id}.trgt.hiphase.vcf.gz"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_hiphase $sample_genomic_id $sample_run_id $bam \
    $ref_fasta $dv_vcf $pbsv_vcf $trgt_vcf
  """
}

process run_customSites {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-hiphase:1.4.2"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(custom_sites)

  output:
  val "customSitesMarker"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_customSites $bam $sample_genomic_id $sample_run_id  \
    $ref_fasta $custom_sites
  """
}

process run_peddySites {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-hiphase:1.4.2"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(peddy_sites)

  output:
  path("${sample_genomic_id}.peddySites.vcf")

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_peddySites $bam $sample_genomic_id $sample_run_id  \
    $ref_fasta $peddy_sites
  """
}

process run_roh {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-hiphase:1.4.2"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(sample_id)
  path(dv_phased_vcf)

  output:
  val "rohMarker"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_lr_roh $dv_phased_vcf $sample_genomic_id $sample_id $sample_run_id
  """
}

process run_qc_bam {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_qc_bam $sample_genomic_id $sample_run_id $bam 
  """
}

process run_qc_vcf {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  path(peddyResults)

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_qc_vcf $sample_genomic_id $sample_run_id $peddyResults 
  """
}

process parse_DV {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(hiphase_dv)
  val(custom_sites)
  val(roh)

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli parse_DV_vcf $sample_genomic_id $sample_run_id
  """
}


workflow {
    aligned_bam = alignPb(params.sampleGenomicId, params.sampleRunId,
        params.fastqs.join(","),
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict
        )
    deep_variant = deepVariant(aligned_bam, params.sampleGenomicId, params.sampleRunId,
        params.ref_fasta, params.ref_fasta_fai, "PACBIO")
    trgt = run_trgt(params.sampleGenomicId,  params.sampleRunId, aligned_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict, 
        params.trgt_repeat_catalog, params.sex)
    pbsv = run_pbsv(params.sampleGenomicId,  params.sampleRunId, aligned_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict)
    (hiphase_bam, hiphase_dv, hiphase_sv, hiphase_tr) = run_hiphase(
        params.sampleGenomicId, params.sampleRunId, aligned_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict,
        deep_variant, pbsv, trgt)
    customSites = run_customSites(params.sampleGenomicId, params.sampleRunId, hiphase_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict, params.custom_sites)
    peddyResults = run_peddySites(params.sampleGenomicId, params.sampleRunId, hiphase_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict, params.peddy_sites)
    roh = run_roh(params.sampleGenomicId, params.sampleRunId, params.sampleGuid, hiphase_dv)
    run_qc_bam(params.sampleGenomicId, params.sampleRunId, hiphase_bam)
    run_qc_vcf(params.sampleGenomicId, params.sampleRunId, peddyResults)
    parse_DV(params.sampleGenomicId, params.sampleRunId, hiphase_dv, customSites, roh)
}
