include { deep_variant_make_examples; deep_variant_call_variants; deep_variant_postprocess_variants; deep_variant_merge_vcfs } from "./deep_variant_shard.nf"

process alignONT {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 16
  memory "60GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-align:0.1.28"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(inputFiles)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)

  output:
  tuple path("${sample_genomic_id}.bam"), path("${sample_genomic_id}.bam.bai")


  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_minimap2 $inputFiles $ref_fasta $sample_genomic_id $sample_run_id  
  """
}


process deepVariant {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 16
  memory "240GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-deepvariant:1.8.0"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  val(model_type) // optional -- should be "ONT_R104" for samples for ONT samples

  output:
  path("${sample_genomic_id}.DV.vcf.gz")

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli deep_variant $bam $ref_fasta $sample_run_id $model_type
  """
}

process run_whatshap {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 4
  memory "15GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-whatshap:2.3.0"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(bam), path(bai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(dv_vcf)

  output: 
  tuple path("${sample_genomic_id}.haplotagged.bam"), path("${sample_genomic_id}.haplotagged.bam.bai")
  path ("${sample_genomic_id}.DV.whatshap.vcf.gz")

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_whatshap $sample_genomic_id $sample_run_id $bam \
    $ref_fasta $dv_vcf
  """
}

process run_longtr {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "15GB"
  queue "priority-gwfcore4"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-longtr:1.0"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(haplotaggedBam), path(haplotaggedBai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(repeat_catalog)
  val(sex) // values of either "M" or "F"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_longtr $sample_genomic_id $sample_run_id $haplotaggedBam \
    $ref_fasta $repeat_catalog $sex
  """
}

process run_sniffles {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 8
  memory "15GB"
  queue "$params.medium_large_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-sniffles2:2.0.7"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(haplotaggedBam), path(haplotaggedBai)

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_sniffles $sample_genomic_id $sample_run_id $haplotaggedBam
  """
}

process run_customSites {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-hiphase:1.4.2"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(haplotaggedBam), path(haplotaggedBai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(custom_sites)
  path(custom_sites_idx)

  output:
  val "customSitesMarker"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_customSites $haplotaggedBam $sample_genomic_id $sample_run_id  \
    $ref_fasta $custom_sites
  """
}

process run_peddySites {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-hiphase:1.4.2"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(haplotaggedBam), path(haplotaggedBai)
  path(ref_fasta)
  path(ref_fasta_fai)
  path(ref_fasta_dict)
  path(peddy_sites)

  output:
  path("${sample_genomic_id}.peddySites.vcf")

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_peddySites $haplotaggedBam $sample_genomic_id $sample_run_id  \
    $ref_fasta $peddy_sites
  """
}

process run_roh {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-hiphase:1.4.2"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(sample_id)
  path(dv_phased_vcf)

  output:
  val "rohMarker"

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_lr_roh $dv_phased_vcf $sample_genomic_id $sample_id $sample_run_id
  """
}

process run_qc_bam {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  tuple path(haplotaggedBam), path(haplotaggedBai)

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_qc_bam $sample_genomic_id $sample_run_id $haplotaggedBam 
  """
}

process run_qc_vcf {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  path(peddyResults)

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli run_qc_vcf $sample_genomic_id $sample_run_id $peddyResults 
  """
}

process parse_DV {
  resourceLabels "cromwell-task-id": "${params.sampleRunId}-${task.process}-${task.index}-${task.attempt}"
  cpus 2
  memory "7GB"
  queue "$params.medium_spot_queue"
  container "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest"

  input:
  val(sample_genomic_id)
  val(sample_run_id)
  val(phased_dv)
  // Markers used to define dependencies without any outputs
  val customSitesMarker
  val rohMarker

  script:
  """
  $params.init_repo_cmd
  $params.pipeline_cli parse_DV_vcf $sample_genomic_id $sample_run_id
  """
}



workflow {
    def model_type = "ONT_R104"
    def chroms = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y", "MT"]
    // def task_groups = [["0", "1", "2", "3"], ["4", "5", "6", "7"]]
    def tasks = ["0", "1", "2", "3", "4", "5", "6", "7"]
    def num_tasks = 8
    aligned_bam = alignONT(params.sampleGenomicId, params.sampleRunId,
        params.fastqs.join(","),
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict
        )
    // deep_variant = deepVariant(params.sampleGenomicId, params.sampleRunId, aligned_bam,
    //     params.ref_fasta, params.ref_fasta_fai, "ONT_R104")
    // make_examples = deep_variant_make_examples_parallel(
    //     aligned_bam, params.ref_fasta, params.ref_fasta_fai, model_type, chroms)
    // make_examples_group = make_examples.map {
    //     it -> [
    //         it[0],
    //         [it[1], it[2], it[3], it[4], it[5], it[6], it[7], it[8]],
    //         [it[9], it[10], it[11], it[12], it[13], it[14], it[15], it[16]]
    //     ] }
    make_examples = deep_variant_make_examples(
        aligned_bam, params.ref_fasta, params.ref_fasta_fai, model_type, chroms, tasks, num_tasks)
    make_examples_group = make_examples.groupTuple()
    call_variants = deep_variant_call_variants(
        model_type, num_tasks, make_examples_group)
    postprocess_variants = deep_variant_postprocess_variants(
        params.sampleGenomicId, params.ref_fasta, params.ref_fasta_fai, model_type, num_tasks, call_variants)
    merge_vcfs = deep_variant_merge_vcfs(
        params.sampleGenomicId, params.sampleRunId, postprocess_variants.toList())
    (phased_bam, phased_dv) = run_whatshap(params.sampleGenomicId, params.sampleRunId, aligned_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict,
        merge_vcfs)
    long_tr = run_longtr(params.sampleGenomicId,  params.sampleRunId, phased_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict, 
        params.longtr_repeat_catalog, params.sex)
    sniffles = run_sniffles(params.sampleGenomicId,  params.sampleRunId, phased_bam)
    customSites = run_customSites(params.sampleGenomicId, params.sampleRunId, phased_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict, params.custom_sites,
        params.custom_sites_idx)
    peddyResults = run_peddySites(params.sampleGenomicId, params.sampleRunId, phased_bam, 
        params.ref_fasta, params.ref_fasta_fai, params.ref_fasta_dict, params.peddy_sites)
    roh = run_roh(params.sampleGenomicId, params.sampleRunId, params.sampleGuid, phased_dv)
    run_qc_bam(params.sampleGenomicId, params.sampleRunId, phased_bam)
    run_qc_vcf(params.sampleGenomicId, params.sampleRunId, peddyResults)
    parse_DV(params.sampleGenomicId, params.sampleRunId, phased_dv, customSites, roh)
}
