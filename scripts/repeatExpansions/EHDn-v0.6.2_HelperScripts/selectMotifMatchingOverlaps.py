import sys, getopt, os

def main ( argv ):
	sampleFile = ''
	refFile = ''
	outFileName='output.txt'
	try:
		opts, args = getopt.getopt(argv,"h",["input=","ref=","output=","help"])
	except getopt.GetoptError:
		print('selectMotifMatchingOverlaps.py --input=<inputBedFile> --ref=<reference1KGBedFile> --output=<outputFileName>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--input'):
			sampleFile=arg
		elif opt in ('--ref'):
			refFile=arg
		elif opt in ('--output'):
			outFileName=arg
		elif opt in ('-h','--help'):
			print('selectMotifMatchingOverlaps.py --input=<inputBedFile> --ref=<reference1KGBedFile> --output=<outputFileName>')
			sys.exit()
		else:
			print('selectMotifMatchingOverlaps.py --input=<inputBedFile> --ref=<reference1KGBedFile> --output=<outputFileName>')
			sys.exit()
	import pandas
	import numpy
	import pybedtools
	from Bio.Seq import Seq
	sampleInput=pybedtools.BedTool(sampleFile)
	refInput=pybedtools.BedTool(refFile)
	matchingPairs=sampleInput.intersect(refInput,wo=True,f=0.01,F=0.01,e=True,sorted=True).to_dataframe(names=['sampleChr','sampleStart','sampleEnd','sampleMotif','sampleCounts','refChr','refStart','refEnd','refMotif','bpOverlap'],index_col=False,low_memory=False)
	indices=numpy.zeros(len(matchingPairs))
	for i in range(0,len(matchingPairs)):
		indices[i]=compareMotifs(matchingPairs.iloc[i,3],matchingPairs.iloc[i,8])
	matchingPairs2=matchingPairs[indices.astype(bool)]
	matchingPairs2.to_csv(outFileName,sep='\t',index=False)
	return

def compareMotifs(motifA, motifB):
	from Bio.Seq import Seq
	seqA = Seq(motifA)
	seqB = Seq(motifB)
	lengthA=len(seqA)
	lengthB=len(seqB)
	if (lengthA == lengthB):
		doubleSeqA=seqA + seqA
		if (seqB in doubleSeqA):
			return 1
		else:
			revCompA=seqA.reverse_complement()
			doubleRevCompA=revCompA + revCompA
			if (seqB in doubleRevCompA):
				return 1
			else:
				return 0
	else:
		return 0

if __name__ == "__main__":
	main(sys.argv[1:])
