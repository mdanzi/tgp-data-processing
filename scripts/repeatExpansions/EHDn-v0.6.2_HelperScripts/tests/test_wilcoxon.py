#!/usr/bin/env python3

#
# Expansion Hunter Denovo
# Copyright (c) 2017 Illumina, Inc.
#
# Author: Egor Dolzhenko <edolzhenko@illumina.com>,
#         Sai Chen <schen6@illumina.com>
# Concept: Michael Eberle <meberle@illumina.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import pytest

from core.wilcoxontest import *


class TestPermutationTest(object):
    def setup_method(self, test_method):
        self.cases = [8.50, 9.48, 8.65, 8.16, 8.83, 7.76, 8.63]
        self.controls = [8.27, 8.20, 8.25, 8.14, 9.00, 8.10, 7.20, 8.32, 7.70]
        self.expected_case_rank_sum = 75
        self.expected_pvalue = 0.057

    def test_permutation_pvalue(self):
        pvalue = calculate_permutation_pvalue(self.cases, self.controls,
                                              num_permutations=100000)

        assert abs(pvalue - self.expected_pvalue) < 0.01


class TestPermutationTestWithTies(object):
    def setup_method(self, test_method):
        self.cases = [0.45, 0.50, 0.61, 0.63, 0.75, 0.85, 0.93]
        self.controls = [0.44, 0.45, 0.52, 0.53, 0.56, 0.58, 0.58, 0.65, 0.79]
        self.expected_case_rank_sum = 71.5
        self.expected_pvalue = 0.105

    def test_permutation_pvalue(self):
        pvalue = calculate_permutation_pvalue(self.cases, self.controls,
                                              num_permutations=100000)

        assert abs(pvalue - self.expected_pvalue) < 0.01

    def test_approximate_pvalue(self):
        pvalue = calculate_approximate_pvalue(self.cases, self.controls)
        assert abs(pvalue - self.expected_pvalue) < 0.01
