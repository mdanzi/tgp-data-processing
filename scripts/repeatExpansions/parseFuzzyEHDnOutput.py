import sys, getopt, os

def main ( argv ):
	sampleFile = ''
	phenoCode = ''
	phenoName = ''
	try:
		opts, args = getopt.getopt(argv,"h",["sample=","phenoCode=","phenoName=","help"])
	except getopt.GetoptError:
		print('parseFuzzyEHDnOutput.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--sample'):
			sampleFile=arg
		elif opt in ('--phenoCode'):
			phenoCode=arg
		elif opt in ('--phenoName'):
			phenoName=arg
		elif opt in ('-h','--help'):
			print('parseFuzzyEHDnOutput.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
			sys.exit()
		else:
			print('parseFuzzyEHDnOutput.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
			sys.exit()
	import numpy as np
	import pandas
	import pybedtools
	TRDataFile = 'fuzzyTRVariantIndex.txt'
	GenoDataFile = 'fuzzyTRGenoData.txt'
	sampleID=sampleFile[:-23]
	# load annotations
	geneLocations=pybedtools.BedTool('GRCh37.75.genes.bed')
	genePaddedLocations=pybedtools.BedTool('GRCh37.75.genes2kbBuffer.bed')
	exonLocations=pybedtools.BedTool('GRCh37.75.exons.gtf')
	exonPaddedLocations=pybedtools.BedTool('GRCh37.75.exons2kbBuffer.gtf')
	cdsLocations=pybedtools.BedTool('GRCh37.75.CDSs.gtf')
	tssLocations=pybedtools.BedTool('GRCh37.75.TSSs2kbBuffer.gtf')
	canonicalTranscriptLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts.bed')
	canonicalTranscriptPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts2kbBuffer.bed')
	canonicalExonLocations=pybedtools.BedTool('GRCh37.75.canonical.exons.gtf')
	canonicalExonPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.exons2kbBuffer.gtf')
	canonicalCDSLocations=pybedtools.BedTool('GRCh37.75.canonical.CDSs.gtf')
	canonicalTSSLocations=pybedtools.BedTool('GRCh37.75.canonical.TSSs2kbBuffer.gtf')
	enhancerLocations=pybedtools.BedTool('EncodeEnhancerSites.bed')
	promoterLocations=pybedtools.BedTool('EncodePromoterSites.bed')
	ctcfOnlyLocations=pybedtools.BedTool('EncodeCTCFOnlySites.bed')
	gwasGeneData=pandas.read_csv('gwas_gene.txt',sep='\t',low_memory=False,quoting=3)
	gwasGenesUnique=gwasGeneData.drop_duplicates(subset='geneName',keep='first')
	gwasGenesUnique.loc[:,'gene_data']=gwasGenesUnique.apply(lambda x: '||'.join(gwasGeneData.loc[gwasGeneData['geneName']==x.geneName,'gene_data']) , axis=1).values
	gwasGeneData=gwasGenesUnique
	omimGeneData=pandas.read_csv('omim_data.txt',sep='\t',low_memory=False,quoting=3)
	omimGeneData=omimGeneData.astype({'inheritance':str})
	gnomadConstraintGeneData=pandas.read_csv('gnomad_constraint_data.txt',sep='\t',low_memory=False,quoting=3,na_values="\\N")
	gnomadConstraintGeneData=gnomadConstraintGeneData.astype({'misZ':str,'pLI':str})
	# load the data
	TRDataColNames=['chrom','start','end','fuzzyTRID','repeatUnit','sampleCount','1kg_irr_max','1kg_irr_af_5','1kg_irr_af_10','1kg_irr_af_15','1kg_irr_af_20','1kg_irr_af_25','1kg_irr_af_30','1kg_irr_af_35','genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped','gwasGeneData','omimGeneData','omim_inheritance','gnomadConstraintGeneData','gnomad_constraint_max_pli','gnomad_constraint_max_misz','phenocounts']
	TRData=pandas.read_csv(TRDataFile,sep='\t',header=None,low_memory=False,index_col=False,names=TRDataColNames,na_values="\\N",dtype={'start':'Int64','end':'Int64','sampleCount':'Int64','omim_inheritance':'Int64','phenocounts':'str'})
	GenoDataColNames=['fuzzyTRID','sampleID','irrCount']
	GenoData=pandas.read_csv(GenoDataFile,sep='\t',header=None,low_memory=False,names=GenoDataColNames,na_values="\\N",dtype={'sampleID':'Int64'})
	# identify novel and previously-seen sites
	sampleInput=pandas.read_csv(sampleFile,sep='\t',low_memory=False,header=None,names=['chrom','start','end','repeatUnit','irrCount'],skiprows=1)
	sampleInput['sampleID']=sampleID
	sampleInput['fuzzyTRID']=sampleInput.loc[:,'chrom'].astype(str)  + ':' + sampleInput.loc[:,'start'].astype(str)  + '-' + sampleInput.loc[:,'end'].astype(str) + '_' + sampleInput.loc[:,'repeatUnit'].astype(str)
	sampleInputBT=pybedtools.BedTool.from_dataframe(sampleInput)
	matchingPairs=sampleInputBT.intersect(pybedtools.BedTool.from_dataframe(TRData),wo=True,f=0.5,F=0.5,e=True).to_dataframe(names=['sampleChrom','sampleStart','sampleEnd','sampleRepeatUnit','sampleIRRCount','sampleID','sampleFuzzyTRID'] + TRDataColNames + ['bpOverlap'],index_col=False,low_memory=False,na_values="\\N")
	matchingPairs2=matchingPairs.loc[matchingPairs['sampleRepeatUnit']==matchingPairs['repeatUnit'],:].reset_index(drop=True)
	novelLoci=sampleInput.loc[~(sampleInput['fuzzyTRID'].isin(matchingPairs2['sampleFuzzyTRID'].values)),:].reset_index(drop=True)
	# remove any duplicates
	matchesWithDuplicateTRDatafuzzyTRIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='fuzzyTRID',keep=False),:]
	duplicateTRDatafuzzyTRIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='fuzzyTRID',keep='first'),'fuzzyTRID'].unique()
	blackListIndices=[]
	for k in range(0,len(duplicateTRDatafuzzyTRIDs)):
		tmp=matchesWithDuplicateTRDatafuzzyTRIDs.loc[matchesWithDuplicateTRDatafuzzyTRIDs.fuzzyTRID==duplicateTRDatafuzzyTRIDs[k],:]
		# select the match with the greatest bpOverlap
		blackListIndices.append(tmp.index[tmp['bpOverlap']!= np.max(tmp.bpOverlap)])
		# manage possible ties for most overlap
		tmp2=tmp.loc[tmp['bpOverlap'] == np.max(tmp.bpOverlap),:]
		if (len(tmp2)>1):
			blackListIndices.append(tmp2.iloc[1:].index)
	if (len(blackListIndices)>0):
		matchingPairs2=matchingPairs2.drop(np.concatenate(blackListIndices))
	blackListIndices=[]
	matchesWithDuplicateSamplefuzzyTRIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='sampleFuzzyTRID',keep=False),:]
	duplicateSamplefuzzyTRIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='sampleFuzzyTRID',keep='first'),'sampleFuzzyTRID'].unique()
	# go through sites where one TR from this sample matched multiple TRs in TRData
	for k in range(0,len(duplicateSamplefuzzyTRIDs)):
		tmp=matchesWithDuplicateSamplefuzzyTRIDs.loc[matchesWithDuplicateSamplefuzzyTRIDs.sampleFuzzyTRID==duplicateSamplefuzzyTRIDs[k],:]
		# check if any of the TRData entries matching this sample TR are seen in 1KG
		thousandGenomesSites=tmp.loc[tmp['1kg_irr_max'].notnull(),:]
		if (len(thousandGenomesSites)==1):
			blackListIndices.append(tmp.index[tmp['1kg_irr_max'].isnull()])
		elif (len(thousandGenomesSites)>1):
			# pick the site with the largest overlap, add the rest to the blacklist
			idx = thousandGenomesSites.index[thousandGenomesSites['bpOverlap']==np.max(thousandGenomesSites.bpOverlap)][0]
			blackListIndices.append(tmp.index[tmp.index!=thousandGenomesSites.index[idx]])
		else:
			blackListIndices.append(tmp.index[tmp['bpOverlap'] != np.max(tmp.bpOverlap)])
			# manage possible ties for most overlap
			tmp2=tmp.loc[tmp['bpOverlap'] == np.max(tmp.bpOverlap),:]
			if (len(tmp2)>1):
				blackListIndices.append(tmp2.iloc[1:].index)
	if (len(blackListIndices)>0):
		matchingPairs2=matchingPairs2.drop(np.concatenate(blackListIndices))
	# update fuzzyTRID, start/end coordinates, and sampleCount values in TRData for non-1KG sites
	tmp1=sampleInput.loc[sampleInput['fuzzyTRID'].isin(matchingPairs2['sampleFuzzyTRID'].values),:]
	tmp1=tmp1.assign(sort_cat=pandas.Categorical(tmp1['fuzzyTRID'],categories=matchingPairs2.sampleFuzzyTRID,ordered=True))
	i=tmp1.sort_values('sort_cat').index
	tmp2=TRData.loc[TRData.fuzzyTRID.isin(matchingPairs2.fuzzyTRID)]
	tmp2=tmp2.assign(sort_cat=pandas.Categorical(tmp2['fuzzyTRID'],categories=matchingPairs2.fuzzyTRID,ordered=True))
	j=tmp2.sort_values('sort_cat').index
	sampleInput.loc[i,'fuzzyTRID']=matchingPairs2.fuzzyTRID.values
	TRData.loc[j,'sampleCount']+=1
	# only change the following values for the non-1KG sites
	j=tmp2.loc[tmp2['1kg_irr_max'].isnull(),:].sort_values('sort_cat').index
	if (len(j)>0):
		TRData.loc[j,'start']=np.round((((TRData.loc[j,'sampleCount']-1).values*matchingPairs2.loc[matchingPairs2['1kg_irr_max'].isnull(),'start'].values)+matchingPairs2.loc[matchingPairs2['1kg_irr_max'].isnull(),'sampleStart'].values)/TRData.loc[j,'sampleCount'].values).astype('int')
		TRData.loc[j,'end']=np.round((((TRData.loc[j,'sampleCount']-1).values*matchingPairs2.loc[matchingPairs2['1kg_irr_max'].isnull(),'end'].values)+matchingPairs2.loc[matchingPairs2['1kg_irr_max'].isnull(),'sampleEnd'].values)/TRData.loc[j,'sampleCount'].values).astype('int')
	# remove any rows from sampleInput that are in neither novelLoci nor matchingPairs2
	sampleInput=sampleInput.loc[((sampleInput['fuzzyTRID'].isin(matchingPairs2.fuzzyTRID.values)) | (sampleInput['fuzzyTRID'].isin(novelLoci.fuzzyTRID.values))),:]
	# remove any duplicates from sampleInput
	sampleInput=sampleInput.drop_duplicates(subset='fuzzyTRID',keep='first').reset_index(drop=True)


	# annotate the novel loci
	if len(novelLoci)>0:
		stagingTRData=pandas.DataFrame(index=range(0,len(novelLoci)),columns=TRData.columns.values)
		stagingTRData=stagingTRData.astype({'omim_inheritance':'Int64'})
		stagingTRData.loc[:,['chrom','start','end','fuzzyTRID','repeatUnit']]=novelLoci[['chrom','start','end','fuzzyTRID','repeatUnit']].values
		stagingTRData.loc[:,'sampleCount']=1
		stagingTRData.loc[:,['1kg_irr_max','1kg_irr_af_5','1kg_irr_af_10','1kg_irr_af_15','1kg_irr_af_20','1kg_irr_af_25','1kg_irr_af_30','1kg_irr_af_35']]=0
		stagingTRData.loc[:,['isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon']]=['FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE']
		stagingTRData.loc[:,['genesOverlapped','genesOverlappedPadded','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped']]=[numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan]
		## add annotation data
		stagingTRDataBT=pybedtools.BedTool.from_dataframe(stagingTRData)
		intersectionCounts=stagingTRDataBT.intersect(geneLocations,c=True).intersect(genePaddedLocations,c=True).intersect(tssLocations,c=True).intersect(exonLocations,c=True).intersect(cdsLocations,c=True).intersect(exonPaddedLocations,c=True).intersect(canonicalTranscriptLocations,c=True).intersect(canonicalTranscriptPaddedLocations,c=True).intersect(canonicalTSSLocations,c=True).intersect(canonicalExonLocations,c=True).intersect(canonicalCDSLocations,c=True).intersect(canonicalExonPaddedLocations,c=True).to_dataframe(header=None,low_memory=False,index_col=False,names=TRDataColNames + ['genes','genesPadded','tss','exons','cds','exonsPadded','canonicalTranscripts','canonicalTranscriptsPadded','canonicalTSS','canonicalExons','canonicalCDS','canonicalExonsPadded'])
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['tss']>0],'isNearTSS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['exons']>0],'overlapsExon']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['cds']>0],'overlapsCDS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0)],'isIntronic']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0) & (intersectionCounts['exonsPadded']>0)],'isNearExon']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['canonicalTSS']>0],'isNearCanonicalTSS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['canonicalExons']>0],'overlapsCanonicalExon']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['canonicalCDS']>0],'overlapsCanonicalCDS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0)],'isCanonicalIntronic']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0) & (intersectionCounts['canonicalExonsPadded']>0)],'isNearCanonicalExon']="TRUE"
		## get list of overlapping genes
		# get the closest gene for each fuzzyTRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(geneLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate fuzzyTRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		# get the list of fuzzyTRID-geneName combinations
		fuzzyTRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['fuzzyTRID','geneName']),:]
		# get the list of fuzzyTRIDs that I will group by
		fuzzyTRIDsWithMultipleGenes=fuzzyTRIDGeneNameCombinations.loc[~fuzzyTRIDGeneNameCombinations.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return ','.join(fuzzyTRIDGeneNameCombinations.loc[(fuzzyTRIDGeneNameCombinations['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations['start']==start) & (fuzzyTRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'genesOverlapped']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched fuzzyTRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		index1=uniqueMatches.index[uniqueMatches.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches.fuzzyTRID)]
		stagingTRData.loc[index2,'genesOverlapped']=uniqueMatches.loc[index1,'geneName'].values
		## get list of gwas results for overlapped genes
		fuzzyTRIDGeneNameCombinations2=fuzzyTRIDGeneNameCombinations.merge(gwasGeneData,how='left',on='geneName')
		fuzzyTRIDGeneNameCombinations2=fuzzyTRIDGeneNameCombinations2.loc[fuzzyTRIDGeneNameCombinations2['gene_data'].notnull(),:]
		fuzzyTRIDsWithMultipleGenes=fuzzyTRIDGeneNameCombinations2.loc[~fuzzyTRIDGeneNameCombinations2.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return '||'.join(fuzzyTRIDGeneNameCombinations2.loc[(fuzzyTRIDGeneNameCombinations2['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations2['start']==start) & (fuzzyTRIDGeneNameCombinations2['end']==end),'gene_data'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'gwasGeneData']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(gwasGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches2.fuzzyTRID)]
		stagingTRData.loc[index2,'gwasGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
		# replace entries with more than 10 gwas associations with 'True'
		stagingTRData.loc[stagingTRData.gwasGeneData.str.count('\|\|')>=10,'gwasGeneData']="TRUE"
		## get list of omim results for overlapped genes
		fuzzyTRIDGeneNameCombinations2=fuzzyTRIDGeneNameCombinations.merge(omimGeneData,how='left',on='geneName')
		fuzzyTRIDGeneNameCombinations2=fuzzyTRIDGeneNameCombinations2.loc[fuzzyTRIDGeneNameCombinations2['gene_data'].notnull(),:]
		fuzzyTRIDsWithMultipleGenes=fuzzyTRIDGeneNameCombinations2.loc[~fuzzyTRIDGeneNameCombinations2.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return '||'.join(fuzzyTRIDGeneNameCombinations2.loc[(fuzzyTRIDGeneNameCombinations2['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations2['start']==start) & (fuzzyTRIDGeneNameCombinations2['end']==end),'gene_data'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'omimGeneData']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches2.fuzzyTRID)]
		stagingTRData.loc[index2,'omimGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
		# replace entries with more than 50 overlapping omim genes with 'True'
		stagingTRData.loc[stagingTRData.omimGeneData.str.count('\|\|')>=50,'omimGeneData']="TRUE"
		## get omim inheritance results for overlapped genes
		def f(chrom, start, end):
			tmp='||'.join(fuzzyTRIDGeneNameCombinations2.loc[(fuzzyTRIDGeneNameCombinations2['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations2['start']==start) & (fuzzyTRIDGeneNameCombinations2['end']==end),'inheritance'])
			if '5' in tmp:
				return 5
			elif (('1' in tmp) and ('2' in tmp)):
				return 5
			elif (('3' in tmp) or ('6' in tmp) or ('7' in tmp) or ('8' in tmp)):
				return 3
			elif '4' in tmp:
				return 4
			else:
				return int(max([int(s) for s in tmp.split('||')]))
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'omim_inheritance']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['inheritance'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches2.fuzzyTRID)]
		stagingTRData.loc[index2,'omim_inheritance']=uniqueMatches2.loc[index1,'inheritance'].astype('int').values
		## get list of gnomad constraint results for overlapped genes
		fuzzyTRIDGeneNameCombinations2=fuzzyTRIDGeneNameCombinations.merge(gnomadConstraintGeneData,how='left',on='geneName')
		fuzzyTRIDGeneNameCombinations2=fuzzyTRIDGeneNameCombinations2.loc[fuzzyTRIDGeneNameCombinations2['gene_data'].notnull(),:]
		fuzzyTRIDsWithMultipleGenes=fuzzyTRIDGeneNameCombinations2.loc[~fuzzyTRIDGeneNameCombinations2.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return '||'.join(fuzzyTRIDGeneNameCombinations2.loc[(fuzzyTRIDGeneNameCombinations2['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations2['start']==start) & (fuzzyTRIDGeneNameCombinations2['end']==end),'gene_data'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'gnomadConstraintGeneData']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches2.fuzzyTRID)]
		stagingTRData.loc[index2,'gnomadConstraintGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
		# replace entries with more than 50 overlapping genes with 'True'
		stagingTRData.loc[stagingTRData.gnomadConstraintGeneData.str.count('\|\|')>=50,'gnomadConstraintGeneData']="TRUE"
		## get gnomad max pLI and misZ results for overlapped genes
		def f1(chrom, start, end):
			tmp='||'.join(fuzzyTRIDGeneNameCombinations2.loc[(fuzzyTRIDGeneNameCombinations2['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations2['start']==start) & (fuzzyTRIDGeneNameCombinations2['end']==end),'pLI'])
			return max([float(s) for s in tmp.split('||')])
		def f2(chrom, start, end):
			tmp='||'.join(fuzzyTRIDGeneNameCombinations2.loc[(fuzzyTRIDGeneNameCombinations2['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations2['start']==start) & (fuzzyTRIDGeneNameCombinations2['end']==end),'misZ'])
			return max([float(s) for s in tmp.split('||')])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'gnomad_constraint_max_pli']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f1(x.chrom,x.start,x.end), axis=1).values
			stagingTRData.loc[index1,'gnomad_constraint_max_misz']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f2(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['pLI'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches2.fuzzyTRID)]
		stagingTRData.loc[index2,'gnomad_constraint_max_pli']=uniqueMatches2.loc[index1,'pLI'].values
		stagingTRData.loc[index2,'gnomad_constraint_max_misz']=uniqueMatches2.loc[index1,'misZ'].values
		## get list of overlapping genes (padded)
		# get the closest gene for each fuzzyTRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(genePaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate fuzzyTRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		# get the list of fuzzyTRID-geneName combinations
		fuzzyTRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['fuzzyTRID','geneName']),:]
		# get the list of fuzzyTRIDs that I will group by
		fuzzyTRIDsWithMultipleGenes=fuzzyTRIDGeneNameCombinations.loc[~fuzzyTRIDGeneNameCombinations.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return ','.join(fuzzyTRIDGeneNameCombinations.loc[(fuzzyTRIDGeneNameCombinations['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations['start']==start) & (fuzzyTRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'genesOverlappedPadded']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched fuzzyTRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		index1=uniqueMatches.index[uniqueMatches.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches.fuzzyTRID)]
		stagingTRData.loc[index2,'genesOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping canonical transcripts
		# get the closest gene for each fuzzyTRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(canonicalTranscriptLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate fuzzyTRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		# get the list of fuzzyTRID-geneName combinations
		fuzzyTRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['fuzzyTRID','geneName']),:]
		# get the list of fuzzyTRIDs that I will group by
		fuzzyTRIDsWithMultipleGenes=fuzzyTRIDGeneNameCombinations.loc[~fuzzyTRIDGeneNameCombinations.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return ','.join(fuzzyTRIDGeneNameCombinations.loc[(fuzzyTRIDGeneNameCombinations['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations['start']==start) & (fuzzyTRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'canonicalTranscriptsOverlapped']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched fuzzyTRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		index1=uniqueMatches.index[uniqueMatches.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches.fuzzyTRID)]
		stagingTRData.loc[index2,'canonicalTranscriptsOverlapped']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping canonical transcripts (padded)
		# get the closest gene for each fuzzyTRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(canonicalTranscriptPaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate fuzzyTRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		# get the list of fuzzyTRID-geneName combinations
		fuzzyTRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['fuzzyTRID','geneName']),:]
		# get the list of fuzzyTRIDs that I will group by
		fuzzyTRIDsWithMultipleGenes=fuzzyTRIDGeneNameCombinations.loc[~fuzzyTRIDGeneNameCombinations.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return ','.join(fuzzyTRIDGeneNameCombinations.loc[(fuzzyTRIDGeneNameCombinations['chrom']==chrom) & (fuzzyTRIDGeneNameCombinations['start']==start) & (fuzzyTRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleGenes.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'canonicalTranscriptsOverlappedPadded']=fuzzyTRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched fuzzyTRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		index1=uniqueMatches.index[uniqueMatches.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches.fuzzyTRID)]
		stagingTRData.loc[index2,'canonicalTranscriptsOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping encode enhancers
		# get the closest enhancer for each fuzzyTRID 
		enhancersIntersectingTRs=stagingTRDataBT.intersect(enhancerLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['enhancerChr','enhancerStart','enhancerEnd','enhancerID'])
		# get the subset of rows which have duplicate fuzzyTRIDs 
		multipleEntries=enhancersIntersectingTRs.loc[enhancersIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		# get the list of fuzzyTRID-enhancer combinations
		fuzzyTRIDEnhancerCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['fuzzyTRID','enhancerID']),:]
		# get the list of fuzzyTRIDs that I will group by
		fuzzyTRIDsWithMultipleEnhancers=fuzzyTRIDEnhancerCombinations.loc[~fuzzyTRIDEnhancerCombinations.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return ','.join(fuzzyTRIDEnhancerCombinations.loc[(fuzzyTRIDEnhancerCombinations['chrom']==chrom) & (fuzzyTRIDEnhancerCombinations['start']==start) & (fuzzyTRIDEnhancerCombinations['end']==end),'enhancerID'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleEnhancers.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleEnhancers)>0:
			stagingTRData.loc[index1,'encodeEnhancersOverlapped']=fuzzyTRIDsWithMultipleEnhancers.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched fuzzyTRID-enhancer pairs
		uniqueMatches=enhancersIntersectingTRs.loc[~enhancersIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		index1=uniqueMatches.index[uniqueMatches.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches.fuzzyTRID)]
		stagingTRData.loc[index2,'encodeEnhancersOverlapped']=uniqueMatches.loc[index1,'enhancerID'].values
		## get list of overlapping encode promoters
		# get the closest enhancer for each fuzzyTRID 
		promotersIntersectingTRs=stagingTRDataBT.intersect(promoterLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['promoterChr','promoterStart','promoterEnd','promoterID'])
		# get the subset of rows which have duplicate fuzzyTRIDs 
		multipleEntries=promotersIntersectingTRs.loc[promotersIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		# get the list of fuzzyTRID-promoter combinations
		fuzzyTRIDPromoterCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['fuzzyTRID','promoterID']),:]
		# get the list of fuzzyTRIDs that I will group by
		fuzzyTRIDsWithMultiplePromoters=fuzzyTRIDPromoterCombinations.loc[~fuzzyTRIDPromoterCombinations.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return ','.join(fuzzyTRIDPromoterCombinations.loc[(fuzzyTRIDPromoterCombinations['chrom']==chrom) & (fuzzyTRIDPromoterCombinations['start']==start) & (fuzzyTRIDPromoterCombinations['end']==end),'promoterID'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultiplePromoters.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultiplePromoters)>0:
			stagingTRData.loc[index1,'encodePromotersOverlapped']=fuzzyTRIDsWithMultiplePromoters.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched fuzzyTRID-promoter pairs
		uniqueMatches=promotersIntersectingTRs.loc[~promotersIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		index1=uniqueMatches.index[uniqueMatches.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches.fuzzyTRID)]
		stagingTRData.loc[index2,'encodePromotersOverlapped']=uniqueMatches.loc[index1,'promoterID'].values
		## get list of overlapping encode ctcfOnly elements
		# get the closest enhancer for each fuzzyTRID 
		ctcfOnlyIntersectingTRs=stagingTRDataBT.intersect(ctcfOnlyLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['ctcfOnlyChr','ctcfOnlyStart','ctcfOnlyEnd','ctcfOnlyID'])
		# get the subset of rows which have duplicate fuzzyTRIDs 
		multipleEntries=ctcfOnlyIntersectingTRs.loc[ctcfOnlyIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		# get the list of fuzzyTRID-ctcfOnly combinations
		fuzzyTRIDCtcfOnlyCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['fuzzyTRID','ctcfOnlyID']),:]
		# get the list of fuzzyTRIDs that I will group by
		fuzzyTRIDsWithMultipleCTCFOnlySites=fuzzyTRIDCtcfOnlyCombinations.loc[~fuzzyTRIDCtcfOnlyCombinations.duplicated(keep='first',subset='fuzzyTRID'),:]
		def f(chrom, start, end):
			return ','.join(fuzzyTRIDCtcfOnlyCombinations.loc[(fuzzyTRIDCtcfOnlyCombinations['chrom']==chrom) & (fuzzyTRIDCtcfOnlyCombinations['start']==start) & (fuzzyTRIDCtcfOnlyCombinations['end']==end),'ctcfOnlyID'])
		index1=stagingTRData.index[stagingTRData.fuzzyTRID.isin(fuzzyTRIDsWithMultipleCTCFOnlySites.fuzzyTRID)]
		if len(fuzzyTRIDsWithMultipleCTCFOnlySites)>0:
			stagingTRData.loc[index1,'encodeCTCFSitesOverlapped']=fuzzyTRIDsWithMultipleCTCFOnlySites.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched fuzzyTRID-gene pairs
		uniqueMatches=ctcfOnlyIntersectingTRs.loc[~ctcfOnlyIntersectingTRs.duplicated(keep=False,subset='fuzzyTRID'),:]
		index1=uniqueMatches.index[uniqueMatches.fuzzyTRID.isin(stagingTRData.fuzzyTRID)]
		index2=stagingTRData.index[stagingTRData.fuzzyTRID.isin(uniqueMatches.fuzzyTRID)]
		stagingTRData.loc[index2,'encodeCTCFSitesOverlapped']=uniqueMatches.loc[index1,'ctcfOnlyID'].values
		## combine staging with the rest of TRData
		TRData=pandas.concat([TRData,stagingTRData],axis=0,ignore_index=True)
	TRData['chrom']=TRData['chrom'].astype(str)
	TRData['start']=TRData['start'].astype(int)
	TRData=TRData.sort_values(by=['chrom','start'])
	## increment values in phenocounts column
	# phenocounts part 1 (new sites)
	phenoCounts=TRData.loc[TRData['fuzzyTRID'].isin(sampleInput.loc[:,'fuzzyTRID'].values),['fuzzyTRID','phenocounts']]
	noPhenoCounts=phenoCounts.loc[phenoCounts['phenocounts'].isna(),:]
	noPhenoCounts2=noPhenoCounts.merge(sampleInput.loc[:,['fuzzyTRID','irrCount']],how='left',on='fuzzyTRID')
	if len(noPhenoCounts2)>0:
		noPhenoCounts2['phenocounts']=phenoCode + "@" + phenoName + "@" + "1"
		TRData=TRData.merge(noPhenoCounts2.loc[:,['fuzzyTRID','phenocounts']],how='left',on='fuzzyTRID')
		TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_y']=TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_x']
		TRData=TRData.drop(columns='phenocounts_x')
		TRData=TRData.rename(columns={'phenocounts_y':'phenocounts'})
	# phenocounts part 2 (new phenotype for existing sites)
	inPhenoCounts=phenoCounts.loc[(~(phenoCounts['phenocounts'].isna()) & (phenoCounts['phenocounts'].str.contains(phenoCode))),:]
	notInPhenoCounts=phenoCounts.loc[~((phenoCounts['phenocounts'].isna()) | (phenoCounts.index.isin(inPhenoCounts.index.values))),:]
	notInPhenoCounts2=notInPhenoCounts.merge(sampleInput.loc[:,['fuzzyTRID','irrCount']],how='left',on='fuzzyTRID')
	if len(notInPhenoCounts2)>0:
		notInPhenoCounts2['phenocounts']=notInPhenoCounts2.loc[:,'phenocounts'] + '<>' + phenoCode + "@" + phenoName + "@" + "1"
		TRData=TRData.merge(notInPhenoCounts2.loc[:,['fuzzyTRID','phenocounts']],how='left',on='fuzzyTRID')
		TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_y']=TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_x']
		TRData=TRData.drop(columns='phenocounts_x')
		TRData=TRData.rename(columns={'phenocounts_y':'phenocounts'})
	# phenocounts part 3 (update phenotype in existing sites)
	inPhenoCounts=inPhenoCounts.reset_index(drop=True)
	if len(inPhenoCounts)>0:
		inPhenoCounts['phenoCountsRows']=inPhenoCounts.loc[:,'phenocounts'].str.split('<>').tolist()
		inPhenoCounts2=inPhenoCounts.loc[:,['fuzzyTRID','phenoCountsRows']].explode('phenoCountsRows').reset_index(drop=True)
		inPhenoCounts2[['phenoCode','phenoName','numObserved']]=inPhenoCounts2.loc[:,'phenoCountsRows'].str.split('@',expand=True)
		inPhenoCounts2=inPhenoCounts2.merge(sampleInput.loc[:,['fuzzyTRID','irrCount']],how='left',on='fuzzyTRID')
		inPhenoCounts2.loc[inPhenoCounts2['phenoCode']==phenoCode,'numObserved']=inPhenoCounts2.loc[inPhenoCounts2['phenoCode']==phenoCode,'numObserved'].values.astype(int)+1
		inPhenoCounts2['phenocounts']=inPhenoCounts2.loc[:,['phenoCode','phenoName','numObserved']].astype(str).agg('@'.join,axis=1)
		inPhenoCounts3=inPhenoCounts2.loc[:,['fuzzyTRID','phenocounts']].groupby('fuzzyTRID').agg('<>'.join).reset_index(drop=False)
		TRData=TRData.merge(inPhenoCounts3.loc[:,['fuzzyTRID','phenocounts']],how='left',on='fuzzyTRID')
		TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_y']=TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_x']
		TRData=TRData.drop(columns='phenocounts_x')
		TRData=TRData.rename(columns={'phenocounts_y':'phenocounts'})
	############ deal with GenoData
	stagingGenoData=pandas.DataFrame(index=range(0,len(sampleInput)),columns=GenoDataColNames)
	stagingGenoData.loc[:,['fuzzyTRID','sampleID','irrCount']]=sampleInput.loc[:,['fuzzyTRID','sampleID','irrCount']].values
	GenoData=pandas.concat([GenoData,stagingGenoData])
	TRData.to_csv(TRDataFile,sep='\t',header=False,index=False,na_rep="\\N")
	GenoData.to_csv(GenoDataFile,sep='\t',header=False,index=False,na_rep="\\N")
	############ create IGV track 
	trackData=TRData.loc[~(TRData['phenocounts'].isna()),['chrom','start','end','repeatUnit','phenocounts']]
	# get max size for each phenotype
	trackData['phenoCountsRows']=trackData.loc[:,'phenocounts'].str.split('<>').tolist()
	trackData2=trackData.loc[:,['chrom','start','end','repeatUnit','phenoCountsRows']].explode('phenoCountsRows').reset_index(drop=True)
	trackData2['phenoCode']=''
	trackData2['phenoName']=''
	trackData2['numObserved']=0
	trackData2.loc[:,['phenoCode','phenoName','numObserved']]=trackData2.loc[:,'phenoCountsRows'].str.split('@',expand=True).values
	trackData2['text']='Name=(' + trackData2.loc[:,'repeatUnit'] + ')n;Phenotype=' + trackData2.loc[:,'phenoName'] + ';NumberObserved=' + trackData2.loc[:,'numObserved'].astype(str)
	trackData2.loc[:,['chrom','start','end','text']].to_csv('GenesisFuzzyTRsTrack.bed',sep='\t',header=False,index=False)
	return

if __name__ == "__main__":
	main(sys.argv[1:])
