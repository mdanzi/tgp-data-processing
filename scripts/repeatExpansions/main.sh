parseSRTRs() {

    local inputFileS3=$1 # table with SampleGenomicId, LatestSampleRunIdText, DiagnosisCodeText, DiagnosisName for each sample to import

    cd $TGP_HOME

    aws s3 cp $inputFileS3 . --only-show-errors

    inputFile=$(basename $inputFileS3)

    # download the database tables
    aws s3 cp s3://tgp-annotation/trvariantindex/trvariantindex.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/tr_genomic_data/TRGenoData.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/fuzzytrvariantindex/fuzzyTRVariantIndex.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/fuzzy_tr_genomic_data/fuzzyTRGenoData.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/omim/omim.tsv . --only-show-errors
    aws s3 cp s3://tgp-annotation/gwas/gwas.tsv . --only-show-errors
    aws s3 cp s3://tgp-annotation/gnomad_constraint/gnomad_constraint.txt . --only-show-errors

    # convert chromosome numbers back into names
    sed -i 's/^23/X/' trvariantindex.txt
    sed -i 's/^24/Y/' trvariantindex.txt
    sed -i 's/^25/MT/' trvariantindex.txt
    sed -i 's/^23/X/' fuzzyTRVariantIndex.txt
    sed -i 's/^24/Y/' fuzzyTRVariantIndex.txt
    sed -i 's/^25/MT/' fuzzyTRVariantIndex.txt

    # transform updateable annotation tables into their aggregate formats
    # omim
    echo -e "geneName\tgene_data\tinheritance" > tmp_header
    cut -f 3 omim.tsv > tmp1
    cut -f 1,2,3,4,5,6,7 omim.tsv | sed 's/\t/<>/g' > tmp2
    cut -f 7 omim.tsv > tmp3
    paste tmp1 tmp2 tmp3 | cat tmp_header - > omim_data.txt
    rm tmp1 tmp2 tmp_header
    # gwas
    echo -e "geneName\tgene_data" > tmp_header
    cut -f 8 gwas.tsv > tmp1
    cut -f 3,4,5,6,7,8,9 gwas.tsv | sed 's/\t/<>/g' > tmp2
    paste tmp1 tmp2 | cat tmp_header - > gwas_gene.txt
    rm tmp1 tmp2 tmp_header
    # gnomad constraint (canonical transcripts only)
    echo -e "geneName\tgene_data\tmisZ\tpLI" > tmp_header
    # filter down to only canonical transcripts and keep first if there are multiple
    awk -F'\t' -v OFS='\t' '{if($3=="true"){print $0}}' gnomad_constraint.txt | sort -k1,1 -u > gnomad_constraint_filt.txt 
    cut -f 1 gnomad_constraint_filt.txt > tmp1
    awk -F'\t' -v OFS='\t' '{print $1,$22,$20,$4,$5,$9,$10,$14,$15,$21}' gnomad_constraint_filt.txt | sed 's/\t/<>/g' > tmp2
    cut -f 20,22 gnomad_constraint_filt.txt > tmp3
    paste tmp1 tmp2 tmp3 | cat tmp_header - > gnomad_constraint_data.txt
    rm tmp1 tmp2 tmp_header

    mkdir -p toUpload
    COUNT=0

    source activate SVParser

    while read BASE sampleRunID diagnosisCodeText diagnosisName; do
        COUNT=$((COUNT+1))
        fileName="${BASE}_EH.vcf"
        fileFullName="s3://tgp-sample-processing/${sampleRunID}/${fileName}"
        EXISTS=$(aws s3 ls ${fileFullName} | grep ${BASE} | wc -l || true)
        if [[ ${EXISTS} -ne 0 ]]; then
            SEEN=$(grep -wFc "${BASE}" TRGenoData.txt || true)
            if [[ ${SEEN} -eq 0 ]]; then
                aws s3 cp ${fileFullName} . --only-show-errors
                gatk-4.2.6.1/gatk VariantsToTable -V ${BASE}_EH.vcf -O ${BASE}.gatkParsed.txt -F CHROM -F POS -F END -F FILTER -F RU -F VARID -GF SO -GF REPCN -GF REPCI -GF ADSP -GF ADFL -GF ADIR -GF LC
                tail -n +2 ${BASE}.gatkParsed.txt | uniq > tmp; mv tmp ${BASE}.gatkParsed.txt
                if [[ $(cat ${BASE}.gatkParsed.txt | wc -l) -gt 0 ]]; then
                    python $TGP_HOME/tgp-data-processing/scripts/repeatExpansions/parseEHOutput.py --sample=${BASE}.gatkParsed.txt --phenoCode="${diagnosisCodeText}" --phenoName="${diagnosisName}"
                    # prepend the header line to the GenesisTRsTrack.bed file
                    echo "#gffTags" | cat - GenesisTRsTrack.bed > tmp; mv tmp GenesisTRsTrack.bed
                    # every other sample, upload the data tables just in case of a crash later
                    if [[ $((COUNT % 2)) -eq 0 ]]; then
                        sed 's/^X/23/' trvariantindex.txt | sed 's/^Y/24/' | sed 's/^MT/25/' > toUpload/trvariantindex.txt
                        aws s3 cp toUpload/trvariantindex.txt s3://tgp-annotation/trvariantindex/ --only-show-errors
                        aws s3 cp TRGenoData.txt s3://tgp-annotation/tr_genomic_data/ --only-show-errors
                        aws s3 cp GenesisTRsTrack.bed s3://tgp-public-assets/ --only-show-errors 
                    fi
                fi
                rm ${fileName}
                rm ${BASE}.gatkParsed.txt
            fi
        fi
        # repeat for non-resolvable "fuzzy" EHDn calls
        fileName="${BASE}_unresolvedOutliers.txt"
        fileFullName="s3://tgp-sample-processing/${sampleRunID}/${fileName}"
        EXISTS=$(aws s3 ls ${fileFullName} | grep ${BASE} | wc -l || true)
        if [[ ${EXISTS} -ne 0 ]]; then
            SEEN=$(grep -wFc "${BASE}" fuzzyTRGenoData.txt || true)
            if [[ ${SEEN} -eq 0 ]]; then
                aws s3 cp ${fileFullName} . --only-show-errors
                if [[ $(cat ${fileName} | wc -l) -gt 1 ]]; then
                    python $TGP_HOME/tgp-data-processing/scripts/repeatExpansions/parseFuzzyEHDnOutput.py --sample=${fileName} --phenoCode="${diagnosisCodeText}" --phenoName="${diagnosisName}"
                    # prepend the header line to the GenesisFuzzyTRsTrack.bed file
                    echo "#gffTags" | cat - GenesisFuzzyTRsTrack.bed > tmp; mv tmp GenesisFuzzyTRsTrack.bed
                    # every other sample, upload the data tables just in case of a crash later
                    if [[ $((COUNT % 2)) -eq 0 ]]; then
                        sed 's/^X/23/' fuzzyTRVariantIndex.txt | sed 's/^Y/24/' | sed 's/^MT/25/' > toUpload/fuzzyTRVariantIndex.txt
                        aws s3 cp toUpload/fuzzytrvariantindex.txt s3://tgp-annotation/fuzzytrvariantindex/ --only-show-errors
                        aws s3 cp fuzzyTRGenoData.txt s3://tgp-annotation/fuzzy_tr_genomic_data/ --only-show-errors
                        aws s3 cp GenesisFuzzyTRsTrack.bed s3://tgp-public-assets/ --only-show-errors 
                    fi
                    rm ${fileName}
                fi
            fi
        fi
    done < ${inputFile}

    # convert chromosome names back into numbers
    sed -i 's/^X/23/' trvariantindex.txt
    sed -i 's/^Y/24/' trvariantindex.txt
    sed -i 's/^MT/25/' trvariantindex.txt
    sed -i 's/^X/23/' fuzzyTRVariantIndex.txt
    sed -i 's/^Y/24/' fuzzyTRVariantIndex.txt
    sed -i 's/^MT/25/' fuzzyTRVariantIndex.txt


    # upload the database tables
    aws s3 cp trvariantindex.txt s3://tgp-annotation/trvariantindex/ --only-show-errors
    aws s3 cp TRGenoData.txt s3://tgp-annotation/tr_genomic_data/ --only-show-errors
    aws s3 cp GenesisTRsTrack.bed s3://tgp-public-assets/ --only-show-errors
    aws s3 cp fuzzyTRVariantIndex.txt s3://tgp-annotation/fuzzytrvariantindex/ --only-show-errors
    aws s3 cp fuzzyTRGenoData.txt s3://tgp-annotation/fuzzy_tr_genomic_data/ --only-show-errors
    aws s3 cp GenesisFuzzyTRsTrack.bed s3://tgp-public-assets/ --only-show-errors

}