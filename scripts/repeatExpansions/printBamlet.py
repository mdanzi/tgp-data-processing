
import sys, getopt, os
from collections import defaultdict
import pysam, csv

def main ( argv ):
	bedFile = ''
	bamFile = ''
	sampleName = ''
	outDir = '.'
	try:
		opts, args = getopt.getopt(argv,"h",["bed=","bam=","name=","out=","help"])
	except getopt.GetoptError:
		print('printBamlet.py --bed=<BedFile> --bam=<BamFile> --name=<sampleName> --out=<OutputDir>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--bed'):
			bedFile=arg
		elif opt in ('--bam'):
			bamFile=arg
		elif opt in ('--name'):
			sampleName=arg
		elif opt in ('--out'):
			outDir=arg
			try: 
				os.makedirs(outDir)
			except OSError:
				if not os.path.isdir(outDir):
					raise
		elif opt in ('-h','--help'):
			print('printBamlet.py --bed=<BedFile> --bam=<BamFile> --name=<sampleName> --out=<OutputDir>')
			sys.exit()
		else:
			print('printBamlet.py --bed=<BedFile> --bam=<BamFile> --name=<sampleName> --out=<OutputDir>')
			sys.exit()
	if sampleName=='':
		sampleName=bamFile
	bedIn=csv.reader(open(bedFile),delimiter='\t')
	bamIn=pysam.AlignmentFile(bamFile,'rb')
	bamletOut=pysam.AlignmentFile(outDir + "/" + sampleName + "_repeatRegionsBamlet.bam",'wb',template=bamIn)
	read_dict = defaultdict(lambda: [None])
	unmappedMate_dict = defaultdict(lambda: [None])
	improper_dict = defaultdict(lambda: [None])
	for record in bedIn:
		chrom=record[0]
		start=int(record[1])
		stop=int(record[2])
		for read in bamIn.fetch(chrom, start, stop):
			qname=read.query_name
			if read.is_secondary or read.is_supplementary:
				continue
			elif read.mate_is_unmapped:
				unmappedMate_dict[qname]=read
			elif not read.is_proper_pair:
				if qname not in improper_dict:
					improper_dict[qname]=read
				else:
					bamletOut.write(read)
					bamletOut.write(improper_dict[qname])
					del improper_dict[qname]
			elif qname not in read_dict:
				read_dict[qname]=read
			else:
				bamletOut.write(read)
				bamletOut.write(read_dict[qname])
				del read_dict[qname]
	for read in bamIn.fetch(region="*"):
		if read.query_name in unmappedMate_dict:
			bamletOut.write(read)
			bamletOut.write(unmappedMate_dict[read.query_name])
			del unmappedMate_dict[read.query_name]
	for qname in improper_dict:
		read=improper_dict[qname]
		mate=bamIn.mate(read)
		bamletOut.write(read)
		bamletOut.write(mate)
	for qname in read_dict:
		read=read_dict[qname]
		mate=bamIn.mate(read)
		bamletOut.write(read)
		bamletOut.write(mate)
	bamletOut.close()
	return

if __name__ == "__main__":
	main(sys.argv[1:])