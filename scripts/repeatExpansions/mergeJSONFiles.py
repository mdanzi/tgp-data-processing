
import sys, getopt, os

def main ( argv ):
	name = ''
	fileName1 = ''
	fileName2 = ''
	try:
		opts, args = getopt.getopt(argv,"h",["name=","json1=","json2=","help"])
	except getopt.GetoptError:
		print('mergeJSONFiles.py --name=<OutputBaseName> --json1=<JSONFile1> --json2=<JSONFile2>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--name'):
			name=arg
		elif opt in ('--json1'):
			fileName1=arg
		elif opt in ('--json2'):
			fileName2=arg
		elif opt in ('-h','--help'):
			print('mergeJSONFiles.py --name=<OutputBaseName> --json1=<JSONFile1> --json2=<JSONFile2>')
			sys.exit()
		else:
			print('mergeJSONFiles.py --name=<OutputBaseName> --json1=<JSONFile1> --json2=<JSONFile2>')
			sys.exit()
	import json
	json1=json.load(open(fileName1,'r'))
	json2=json.load(open(fileName2,'r'))
	for i in range(len(json1)):
		json2.append(json1[i])
	json.dump(json2,open('variant_catalog_merged_' + name + '.json','w'))
	return

if __name__ == "__main__":
	main(sys.argv[1:])