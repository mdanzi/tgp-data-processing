import sys, getopt, os

def main ( argv ):
	sampleFile = ''
	phenoCode = ''
	phenoName = ''
	try:
		opts, args = getopt.getopt(argv,"h",["sample=","phenoCode=","phenoName=","help"])
	except getopt.GetoptError:
		print('parseEHOutput.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--sample'):
			sampleFile=arg
		elif opt in ('--phenoCode'):
			phenoCode=arg
		elif opt in ('--phenoName'):
			phenoName=arg
		elif opt in ('-h','--help'):
			print('parseEHOutput.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
			sys.exit()
		else:
			print('parseEHOutput.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
			sys.exit()
	import numpy as np
	import pandas
	import pybedtools
	TRDataFile = 'trvariantindex.txt'
	GenoDataFile = 'TRGenoData.txt'
	sampleID=sampleFile[:-15]
	# load annotations
	geneLocations=pybedtools.BedTool('GRCh37.75.genes.bed')
	genePaddedLocations=pybedtools.BedTool('GRCh37.75.genes2kbBuffer.bed')
	exonLocations=pybedtools.BedTool('GRCh37.75.exons.gtf')
	exonPaddedLocations=pybedtools.BedTool('GRCh37.75.exons2kbBuffer.gtf')
	cdsLocations=pybedtools.BedTool('GRCh37.75.CDSs.gtf')
	tssLocations=pybedtools.BedTool('GRCh37.75.TSSs2kbBuffer.gtf')
	canonicalTranscriptLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts.bed')
	canonicalTranscriptPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts2kbBuffer.bed')
	canonicalExonLocations=pybedtools.BedTool('GRCh37.75.canonical.exons.gtf')
	canonicalExonPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.exons2kbBuffer.gtf')
	canonicalCDSLocations=pybedtools.BedTool('GRCh37.75.canonical.CDSs.gtf')
	canonicalTSSLocations=pybedtools.BedTool('GRCh37.75.canonical.TSSs2kbBuffer.gtf')
	enhancerLocations=pybedtools.BedTool('EncodeEnhancerSites.bed')
	promoterLocations=pybedtools.BedTool('EncodePromoterSites.bed')
	ctcfOnlyLocations=pybedtools.BedTool('EncodeCTCFOnlySites.bed')
	gwasGeneData=pandas.read_csv('gwas_gene.txt',sep='\t',low_memory=False,quoting=3)
	gwasGenesUnique=gwasGeneData.drop_duplicates(subset='geneName',keep='first')
	gwasGenesUnique.loc[:,'gene_data']=gwasGenesUnique.apply(lambda x: '||'.join(gwasGeneData.loc[gwasGeneData['geneName']==x.geneName,'gene_data']) , axis=1).values
	gwasGeneData=gwasGenesUnique
	omimGeneData=pandas.read_csv('omim_data.txt',sep='\t',low_memory=False,quoting=3)
	omimGeneData=omimGeneData.astype({'inheritance':str})
	gnomadConstraintGeneData=pandas.read_csv('gnomad_constraint_data.txt',sep='\t',low_memory=False,quoting=3,na_values="\\N")
	gnomadConstraintGeneData=gnomadConstraintGeneData.astype({'misZ':str,'pLI':str})
	thousandGenomes=pybedtools.BedTool('aws-pipeline/repeatExpansions/1000Genomes_hg19_anchoredIRRStats.bed')
	# load the data
	sampleInput=pandas.read_csv(sampleFile,sep='\t',header=None,low_memory=False,names=['chrom','start','end','filter','repeatUnit','varID','supportTypes','repcn','repci','adsp','adfl','adir','dp'])
	TRDataColNames=['chrom','start','end','TRID','ref_len','repeatUnit','gnomad3_small_5','gnomad3_small_10','gnomad3_small_15','gnomad3_small_20','gnomad3_small_25','gnomad3_small_30','gnomad3_small_35','gnomad3_small_40','gnomad3_small_45','gnomad3_small_50','gnomad3_small_55','gnomad3_small_60','gnomad3_small_65','gnomad3_small_70','gnomad3_small_75','gnomad3_small_80','gnomad3_small_85','gnomad3_small_90','gnomad3_small_95','gnomad3_small_100','gnomad3_large_5','gnomad3_large_10','gnomad3_large_15','gnomad3_large_20','gnomad3_large_25','gnomad3_large_30','gnomad3_large_35','gnomad3_large_40','gnomad3_large_45','gnomad3_large_50','gnomad3_large_55','gnomad3_large_60','gnomad3_large_65','gnomad3_large_70','gnomad3_large_75','gnomad3_large_80','gnomad3_large_85','gnomad3_large_90','gnomad3_large_95','gnomad3_large_100','1kg_irr_max','1kg_irr_af_5','1kg_irr_af_10','1kg_irr_af_15','1kg_irr_af_20','1kg_irr_af_25','1kg_irr_af_30','1kg_irr_af_35','genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped','gwasGeneData','omimGeneData','omim_inheritance','gnomadConstraintGeneData','gnomad_constraint_max_pli','gnomad_constraint_max_misz','phenocounts']
	TRData=pandas.read_csv(TRDataFile,sep='\t',header=None,low_memory=False,index_col=False,names=TRDataColNames,na_values="\\N",dtype={'ref_len':'Int64','omim_inheritance':'Int64'})
	GenoDataColNames=['TRID','SampleID','allele1_ru','allele2_ru','allele1_ci','allele2_ci','allele1_support_type','allele2_support_type','allele1_spanning_ad','allele2_spanning_ad','allele1_flanking_ad','allele2_flanking_ad','allele1_irr_ad','allele2_irr_ad','dp']
	GenoData=pandas.read_csv(GenoDataFile,sep='\t',header=None,low_memory=False,names=GenoDataColNames,na_values="\\N",dtype={'SampleID':'Int64','allele1_ru':'Int64','allele2_ru':'Int64','allele1_spanning_ad':'Int64','allele2_spanning_ad':'Int64','allele1_flanking_ad':'Int64','allele2_flanking_ad':'Int64','allele1_irr_ad':'Int64','allele2_irr_ad':'Int64'})
	# add identifier columns
	sampleInput['SampleID']=sampleID
	sampleInput['TRID']=sampleInput.loc[:,'varID']
	sampleInput.loc[~(sampleInput['TRID'].str.contains('_[A-Z]',regex=True)),'TRID']=sampleInput.loc[~(sampleInput['TRID'].str.contains('_[A-Z]',regex=True)),'varID'] + '_' + sampleInput.loc[~(sampleInput['TRID'].str.contains('_[A-Z]',regex=True)),'repeatUnit']
	# annotate the novel loci
	novelLoci=sampleInput.loc[~(sampleInput['TRID'].isin(TRData['TRID'].values)),:].reset_index(drop=True)
	if len(novelLoci)>0:
		stagingTRData=pandas.DataFrame(index=range(0,len(novelLoci)),columns=TRData.columns.values)
		stagingTRData=stagingTRData.astype({'omim_inheritance':'Int64'})
		stagingTRData.loc[:,['chrom','start','end','TRID','repeatUnit']]=novelLoci[['chrom','start','end','TRID','repeatUnit']].values
		stagingTRData.loc[:,'ref_len']=stagingTRData.loc[:,'end']-stagingTRData.loc[:,'start']
		stagingTRData.loc[:,['gnomad3_small_5','gnomad3_small_10','gnomad3_small_15','gnomad3_small_20','gnomad3_small_25','gnomad3_small_30','gnomad3_small_35','gnomad3_small_40','gnomad3_small_45','gnomad3_small_50','gnomad3_small_55','gnomad3_small_60','gnomad3_small_65','gnomad3_small_70','gnomad3_small_75','gnomad3_small_80','gnomad3_small_85','gnomad3_small_90','gnomad3_small_95','gnomad3_small_100','gnomad3_large_5','gnomad3_large_10','gnomad3_large_15','gnomad3_large_20','gnomad3_large_25','gnomad3_large_30','gnomad3_large_35','gnomad3_large_40','gnomad3_large_45','gnomad3_large_50','gnomad3_large_55','gnomad3_large_60','gnomad3_large_65','gnomad3_large_70','gnomad3_large_75','gnomad3_large_80','gnomad3_large_85','gnomad3_large_90','gnomad3_large_95','gnomad3_large_100']]=np.nan
		stagingTRData.loc[:,['1kg_irr_max','1kg_irr_af_5','1kg_irr_af_10','1kg_irr_af_15','1kg_irr_af_20','1kg_irr_af_25','1kg_irr_af_30','1kg_irr_af_35']]=0
		stagingTRData.loc[:,['isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon']]=['FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE']
		stagingTRData.loc[:,['genesOverlapped','genesOverlappedPadded','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped']]=[numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan]
		## add annotation data
		stagingTRDataBT=pybedtools.BedTool.from_dataframe(stagingTRData)
		intersectionCounts=stagingTRDataBT.intersect(geneLocations,c=True).intersect(genePaddedLocations,c=True).intersect(tssLocations,c=True).intersect(exonLocations,c=True).intersect(cdsLocations,c=True).intersect(exonPaddedLocations,c=True).intersect(canonicalTranscriptLocations,c=True).intersect(canonicalTranscriptPaddedLocations,c=True).intersect(canonicalTSSLocations,c=True).intersect(canonicalExonLocations,c=True).intersect(canonicalCDSLocations,c=True).intersect(canonicalExonPaddedLocations,c=True).to_dataframe(header=None,low_memory=False,index_col=False,names=TRDataColNames + ['genes','genesPadded','tss','exons','cds','exonsPadded','canonicalTranscripts','canonicalTranscriptsPadded','canonicalTSS','canonicalExons','canonicalCDS','canonicalExonsPadded'])
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['tss']>0],'isNearTSS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['exons']>0],'overlapsExon']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['cds']>0],'overlapsCDS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0)],'isIntronic']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0) & (intersectionCounts['exonsPadded']>0)],'isNearExon']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['canonicalTSS']>0],'isNearCanonicalTSS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['canonicalExons']>0],'overlapsCanonicalExon']="TRUE"
		stagingTRData.loc[intersectionCounts.index[intersectionCounts['canonicalCDS']>0],'overlapsCanonicalCDS']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0)],'isCanonicalIntronic']="TRUE"
		stagingTRData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0) & (intersectionCounts['canonicalExonsPadded']>0)],'isNearCanonicalExon']="TRUE"
		## get list of overlapping genes
		# get the closest gene for each TRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(geneLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate TRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		# get the list of TRID-geneName combinations
		TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
		# get the list of TRIDs that I will group by
		TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chrom']==chrom) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'genesOverlapped']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched TRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches.TRID)]
		stagingTRData.loc[index2,'genesOverlapped']=uniqueMatches.loc[index1,'geneName'].values
		## get list of gwas results for overlapped genes
		TRIDGeneNameCombinations2=TRIDGeneNameCombinations.merge(gwasGeneData,how='left',on='geneName')
		TRIDGeneNameCombinations2=TRIDGeneNameCombinations2.loc[TRIDGeneNameCombinations2['gene_data'].notnull(),:]
		TRIDsWithMultipleGenes=TRIDGeneNameCombinations2.loc[~TRIDGeneNameCombinations2.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return '||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chrom']==chrom) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'gene_data'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'gwasGeneData']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(gwasGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches2.TRID)]
		stagingTRData.loc[index2,'gwasGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
		# replace entries with more than 10 gwas associations with 'True'
		stagingTRData.loc[stagingTRData.gwasGeneData.str.count('\|\|')>=10,'gwasGeneData']="TRUE"
		## get list of omim results for overlapped genes
		TRIDGeneNameCombinations2=TRIDGeneNameCombinations.merge(omimGeneData,how='left',on='geneName')
		TRIDGeneNameCombinations2=TRIDGeneNameCombinations2.loc[TRIDGeneNameCombinations2['gene_data'].notnull(),:]
		TRIDsWithMultipleGenes=TRIDGeneNameCombinations2.loc[~TRIDGeneNameCombinations2.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return '||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chrom']==chrom) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'gene_data'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'omimGeneData']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches2.TRID)]
		stagingTRData.loc[index2,'omimGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
		# replace entries with more than 50 overlapping omim genes with 'True'
		stagingTRData.loc[stagingTRData.omimGeneData.str.count('\|\|')>=50,'omimGeneData']="TRUE"
		## get omim inheritance results for overlapped genes
		def f(chrom, start, end):
			tmp='||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chrom']==chrom) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'inheritance'])
			if '5' in tmp:
				return 5
			elif (('1' in tmp) and ('2' in tmp)):
				return 5
			elif (('3' in tmp) or ('6' in tmp) or ('7' in tmp) or ('8' in tmp)):
				return 3
			elif '4' in tmp:
				return 4
			else:
				return int(max([int(s) for s in tmp.split('||')]))
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'omim_inheritance']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['inheritance'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches2.TRID)]
		stagingTRData.loc[index2,'omim_inheritance']=uniqueMatches2.loc[index1,'inheritance'].astype('int').values
		## get list of gnomad constraint results for overlapped genes
		TRIDGeneNameCombinations2=TRIDGeneNameCombinations.merge(gnomadConstraintGeneData,how='left',on='geneName')
		TRIDGeneNameCombinations2=TRIDGeneNameCombinations2.loc[TRIDGeneNameCombinations2['gene_data'].notnull(),:]
		TRIDsWithMultipleGenes=TRIDGeneNameCombinations2.loc[~TRIDGeneNameCombinations2.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return '||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chrom']==chrom) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'gene_data'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'gnomadConstraintGeneData']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches2.TRID)]
		stagingTRData.loc[index2,'gnomadConstraintGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
		# replace entries with more than 50 overlapping genes with 'True'
		stagingTRData.loc[stagingTRData.gnomadConstraintGeneData.str.count('\|\|')>=50,'gnomadConstraintGeneData']="TRUE"
		## get gnomad max pLI and misZ results for overlapped genes
		def f1(chrom, start, end):
			tmp='||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chrom']==chrom) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'pLI'])
			return max([float(s) for s in tmp.split('||')])
		def f2(chrom, start, end):
			tmp='||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chrom']==chrom) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'misZ'])
			return max([float(s) for s in tmp.split('||')])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'gnomad_constraint_max_pli']=TRIDsWithMultipleGenes.apply(lambda x: f1(x.chrom,x.start,x.end), axis=1).values
			stagingTRData.loc[index1,'gnomad_constraint_max_misz']=TRIDsWithMultipleGenes.apply(lambda x: f2(x.chrom,x.start,x.end), axis=1).values
		uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
		uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['pLI'].notnull(),:]
		index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches2.TRID)]
		stagingTRData.loc[index2,'gnomad_constraint_max_pli']=uniqueMatches2.loc[index1,'pLI'].values
		stagingTRData.loc[index2,'gnomad_constraint_max_misz']=uniqueMatches2.loc[index1,'misZ'].values
		## get list of overlapping genes (padded)
		# get the closest gene for each TRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(genePaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate TRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		# get the list of TRID-geneName combinations
		TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
		# get the list of TRIDs that I will group by
		TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chrom']==chrom) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'genesOverlappedPadded']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched TRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches.TRID)]
		stagingTRData.loc[index2,'genesOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping canonical transcripts
		# get the closest gene for each TRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(canonicalTranscriptLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate TRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		# get the list of TRID-geneName combinations
		TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
		# get the list of TRIDs that I will group by
		TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chrom']==chrom) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'canonicalTranscriptsOverlapped']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched TRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches.TRID)]
		stagingTRData.loc[index2,'canonicalTranscriptsOverlapped']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping canonical transcripts (padded)
		# get the closest gene for each TRID 
		genesIntersectingTRs=stagingTRDataBT.intersect(canonicalTranscriptPaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate TRIDs 
		multipleEntries=genesIntersectingTRs.loc[genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		# get the list of TRID-geneName combinations
		TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
		# get the list of TRIDs that I will group by
		TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chrom']==chrom) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
		if len(TRIDsWithMultipleGenes)>0:
			stagingTRData.loc[index1,'canonicalTranscriptsOverlappedPadded']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched TRID-gene pairs
		uniqueMatches=genesIntersectingTRs.loc[~genesIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches.TRID)]
		stagingTRData.loc[index2,'canonicalTranscriptsOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping encode enhancers
		# get the closest enhancer for each TRID 
		enhancersIntersectingTRs=stagingTRDataBT.intersect(enhancerLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['enhancerChr','enhancerStart','enhancerEnd','enhancerID'])
		# get the subset of rows which have duplicate TRIDs 
		multipleEntries=enhancersIntersectingTRs.loc[enhancersIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		# get the list of TRID-enhancer combinations
		TRIDEnhancerCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','enhancerID']),:]
		# get the list of TRIDs that I will group by
		TRIDsWithMultipleEnhancers=TRIDEnhancerCombinations.loc[~TRIDEnhancerCombinations.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return ','.join(TRIDEnhancerCombinations.loc[(TRIDEnhancerCombinations['chrom']==chrom) & (TRIDEnhancerCombinations['start']==start) & (TRIDEnhancerCombinations['end']==end),'enhancerID'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleEnhancers.TRID)]
		if len(TRIDsWithMultipleEnhancers)>0:
			stagingTRData.loc[index1,'encodeEnhancersOverlapped']=TRIDsWithMultipleEnhancers.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched TRID-enhancer pairs
		uniqueMatches=enhancersIntersectingTRs.loc[~enhancersIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches.TRID)]
		stagingTRData.loc[index2,'encodeEnhancersOverlapped']=uniqueMatches.loc[index1,'enhancerID'].values
		## get list of overlapping encode promoters
		# get the closest enhancer for each TRID 
		promotersIntersectingTRs=stagingTRDataBT.intersect(promoterLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['promoterChr','promoterStart','promoterEnd','promoterID'])
		# get the subset of rows which have duplicate TRIDs 
		multipleEntries=promotersIntersectingTRs.loc[promotersIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		# get the list of TRID-promoter combinations
		TRIDPromoterCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','promoterID']),:]
		# get the list of TRIDs that I will group by
		TRIDsWithMultiplePromoters=TRIDPromoterCombinations.loc[~TRIDPromoterCombinations.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return ','.join(TRIDPromoterCombinations.loc[(TRIDPromoterCombinations['chrom']==chrom) & (TRIDPromoterCombinations['start']==start) & (TRIDPromoterCombinations['end']==end),'promoterID'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultiplePromoters.TRID)]
		if len(TRIDsWithMultiplePromoters)>0:
			stagingTRData.loc[index1,'encodePromotersOverlapped']=TRIDsWithMultiplePromoters.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched TRID-promoter pairs
		uniqueMatches=promotersIntersectingTRs.loc[~promotersIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches.TRID)]
		stagingTRData.loc[index2,'encodePromotersOverlapped']=uniqueMatches.loc[index1,'promoterID'].values
		## get list of overlapping encode ctcfOnly elements
		# get the closest enhancer for each TRID 
		ctcfOnlyIntersectingTRs=stagingTRDataBT.intersect(ctcfOnlyLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRDataColNames + ['ctcfOnlyChr','ctcfOnlyStart','ctcfOnlyEnd','ctcfOnlyID'])
		# get the subset of rows which have duplicate TRIDs 
		multipleEntries=ctcfOnlyIntersectingTRs.loc[ctcfOnlyIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		# get the list of TRID-ctcfOnly combinations
		TRIDCtcfOnlyCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','ctcfOnlyID']),:]
		# get the list of TRIDs that I will group by
		TRIDsWithMultipleCTCFOnlySites=TRIDCtcfOnlyCombinations.loc[~TRIDCtcfOnlyCombinations.duplicated(keep='first',subset='TRID'),:]
		def f(chrom, start, end):
			return ','.join(TRIDCtcfOnlyCombinations.loc[(TRIDCtcfOnlyCombinations['chrom']==chrom) & (TRIDCtcfOnlyCombinations['start']==start) & (TRIDCtcfOnlyCombinations['end']==end),'ctcfOnlyID'])
		index1=stagingTRData.index[stagingTRData.TRID.isin(TRIDsWithMultipleCTCFOnlySites.TRID)]
		if len(TRIDsWithMultipleCTCFOnlySites)>0:
			stagingTRData.loc[index1,'encodeCTCFSitesOverlapped']=TRIDsWithMultipleCTCFOnlySites.apply(lambda x: f(x.chrom,x.start,x.end), axis=1).values
		# deal with the uniquely matched TRID-gene pairs
		uniqueMatches=ctcfOnlyIntersectingTRs.loc[~ctcfOnlyIntersectingTRs.duplicated(keep=False,subset='TRID'),:]
		index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingTRData.TRID)]
		index2=stagingTRData.index[stagingTRData.TRID.isin(uniqueMatches.TRID)]
		stagingTRData.loc[index2,'encodeCTCFSitesOverlapped']=uniqueMatches.loc[index1,'ctcfOnlyID'].values
		## get allele frequency of anchored IRRs in 1000Genomes dataset
		stagingTRDataBT=pybedtools.BedTool.from_dataframe(stagingTRData.loc[:,['chrom','start','end','TRID','repeatUnit']])
		matchingPairs=stagingTRDataBT.intersect(thousandGenomes,wo=True,f=1).to_dataframe(names=['sampleChr','sampleStart','sampleEnd','TRID','sampleMotif','1kg_chr','1kg_start','1kg_end','1kg_motif','1kg_irr_max','1kg_irr_af_5','1kg_irr_af_10','1kg_irr_af_15','1kg_irr_af_20','1kg_irr_af_25','1kg_irr_af_30','1kg_irr_af_35','bpOverlap'],index_col=False,low_memory=False,na_values="\\N")
		matchingPairs2=matchingPairs.loc[matchingPairs['sampleMotif']==matchingPairs['1kg_motif'],:].reset_index(drop=True)
		for i in range(len(matchingPairs2)):
			stagingTRData.loc[stagingTRData['TRID']==matchingPairs2.loc[i,'TRID'],['1kg_irr_max','1kg_irr_af_5','1kg_irr_af_10','1kg_irr_af_15','1kg_irr_af_20','1kg_irr_af_25','1kg_irr_af_30','1kg_irr_af_35']]=matchingPairs2.loc[i,['1kg_irr_max','1kg_irr_af_5','1kg_irr_af_10','1kg_irr_af_15','1kg_irr_af_20','1kg_irr_af_25','1kg_irr_af_30','1kg_irr_af_35']].values
		## combine staging with the rest of TRData
		TRData=pandas.concat([TRData,stagingTRData],axis=0,ignore_index=True)
	TRData=TRData.sort_values(by=['chrom','start'])
	## increment values in phenocounts column
	# phenocounts part 1 (new sites)
	phenoCounts=TRData.loc[TRData['TRID'].isin(sampleInput.loc[:,'TRID'].values),['TRID','phenocounts']]
	noPhenoCounts=phenoCounts.loc[phenoCounts['phenocounts'].isna(),:]
	noPhenoCounts2=noPhenoCounts.merge(sampleInput.loc[:,['TRID','repcn']],how='left',on='TRID')
	if len(noPhenoCounts2)>0:
		noPhenoCounts2['thisAlleleSmallSize']=noPhenoCounts2.loc[:,'repcn'].str.split('/',expand=True).loc[:,0].astype(int)
		noPhenoCounts2['thisAlleleLargeSize']=noPhenoCounts2.loc[:,'repcn'].str.split('/',expand=True).loc[:,1].astype(int)
		noPhenoCounts2=noPhenoCounts2.assign(small5=0,small10=0,small15=0,small20=0,small25=0,small30=0,small35=0,small40=0,small45=0,small50=0,small55=0,small60=0,small65=0,small70=0,small75=0,small80=0,small85=0,small90=0,small95=0,small100=0)
		noPhenoCounts2=noPhenoCounts2.assign(large5=0,large10=0,large15=0,large20=0,large25=0,large30=0,large35=0,large40=0,large45=0,large50=0,large55=0,large60=0,large65=0,large70=0,large75=0,large80=0,large85=0,large90=0,large95=0,large100=0)
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=5,'small5']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=10,'small10']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=15,'small15']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=20,'small20']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=25,'small25']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=30,'small30']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=35,'small35']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=40,'small40']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=45,'small45']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=50,'small50']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=55,'small55']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=60,'small60']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=65,'small65']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=70,'small70']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=75,'small75']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=80,'small80']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=85,'small85']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=90,'small90']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=95,'small95']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleSmallSize']>=100,'small100']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=5,'large5']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=10,'large10']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=15,'large15']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=20,'large20']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=25,'large25']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=30,'large30']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=35,'large35']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=40,'large40']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=45,'large45']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=50,'large50']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=55,'large55']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=60,'large60']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=65,'large65']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=70,'large70']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=75,'large75']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=80,'large80']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=85,'large85']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=90,'large90']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=95,'large95']=1
		noPhenoCounts2.loc[noPhenoCounts2['thisAlleleLargeSize']>=100,'large100']=1
		noPhenoCounts2['phenoCode']=phenoCode
		noPhenoCounts2['phenoName']=phenoName
		noPhenoCounts2['numObserved']=1
		noPhenoCounts2['phenocounts']=noPhenoCounts2.loc[:,['phenoCode','phenoName','numObserved','small5','small10','small15','small20','small25','small30','small35','small40','small45','small50','small55','small60','small65','small70','small75','small80','small85','small90','small95','small100','large5','large10','large15','large20','large25','large30','large35','large40','large45','large50','large55','large60','large65','large70','large75','large80','large85','large90','large95','large100']].astype(str).agg('@'.join,axis=1)
		TRData=TRData.merge(noPhenoCounts2.loc[:,['TRID','phenocounts']],how='left',on='TRID')
		TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_y']=TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_x']
		TRData=TRData.drop(columns='phenocounts_x')
		TRData=TRData.rename(columns={'phenocounts_y':'phenocounts'})
	# phenocounts part 2 (new phenotype for existing sites)
	inPhenoCounts=phenoCounts.loc[(~(phenoCounts['phenocounts'].isna()) & (phenoCounts['phenocounts'].str.contains(phenoCode))),:]
	notInPhenoCounts=phenoCounts.loc[~((phenoCounts['phenocounts'].isna()) | (phenoCounts.index.isin(inPhenoCounts.index.values))),:]
	notInPhenoCounts2=notInPhenoCounts.merge(sampleInput.loc[:,['TRID','repcn']],how='left',on='TRID')
	if len(notInPhenoCounts2)>0:
		notInPhenoCounts2['thisAlleleSmallSize']=notInPhenoCounts2.loc[:,'repcn'].str.split('/',expand=True).loc[:,0].astype(int)
		notInPhenoCounts2['thisAlleleLargeSize']=notInPhenoCounts2.loc[:,'repcn'].str.split('/',expand=True).loc[:,1].astype(int)
		notInPhenoCounts2=notInPhenoCounts2.assign(small5=0,small10=0,small15=0,small20=0,small25=0,small30=0,small35=0,small40=0,small45=0,small50=0,small55=0,small60=0,small65=0,small70=0,small75=0,small80=0,small85=0,small90=0,small95=0,small100=0)
		notInPhenoCounts2=notInPhenoCounts2.assign(large5=0,large10=0,large15=0,large20=0,large25=0,large30=0,large35=0,large40=0,large45=0,large50=0,large55=0,large60=0,large65=0,large70=0,large75=0,large80=0,large85=0,large90=0,large95=0,large100=0)
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=5,'small5']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=10,'small10']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=15,'small15']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=20,'small20']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=25,'small25']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=30,'small30']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=35,'small35']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=40,'small40']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=45,'small45']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=50,'small50']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=55,'small55']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=60,'small60']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=65,'small65']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=70,'small70']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=75,'small75']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=80,'small80']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=85,'small85']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=90,'small90']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=95,'small95']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleSmallSize']>=100,'small100']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=5,'large5']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=10,'large10']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=15,'large15']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=20,'large20']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=25,'large25']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=30,'large30']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=35,'large35']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=40,'large40']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=45,'large45']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=50,'large50']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=55,'large55']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=60,'large60']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=65,'large65']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=70,'large70']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=75,'large75']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=80,'large80']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=85,'large85']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=90,'large90']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=95,'large95']=1
		notInPhenoCounts2.loc[notInPhenoCounts2['thisAlleleLargeSize']>=100,'large100']=1
		notInPhenoCounts2['phenoCode']=phenoCode
		notInPhenoCounts2['phenoName']=phenoName
		notInPhenoCounts2['numObserved']=1
		notInPhenoCounts2['phenocounts']=notInPhenoCounts2.loc[:,'phenocounts'] + '<>' + notInPhenoCounts2.loc[:,['phenoCode','phenoName','numObserved','small5','small10','small15','small20','small25','small30','small35','small40','small45','small50','small55','small60','small65','small70','small75','small80','small85','small90','small95','small100','large5','large10','large15','large20','large25','large30','large35','large40','large45','large50','large55','large60','large65','large70','large75','large80','large85','large90','large95','large100']].astype(str).agg('@'.join,axis=1)
		TRData=TRData.merge(notInPhenoCounts2.loc[:,['TRID','phenocounts']],how='left',on='TRID')
		TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_y']=TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_x']
		TRData=TRData.drop(columns='phenocounts_x')
		TRData=TRData.rename(columns={'phenocounts_y':'phenocounts'})
	# phenocounts part 3 (update phenotype in existing sites)
	inPhenoCounts=inPhenoCounts.reset_index(drop=True)
	if len(inPhenoCounts)>0:
		inPhenoCounts['phenoCountsRows']=inPhenoCounts.loc[:,'phenocounts'].str.split('<>').tolist()
		inPhenoCounts2=inPhenoCounts.loc[:,['TRID','phenoCountsRows']].explode('phenoCountsRows').reset_index(drop=True)
		inPhenoCounts2[['phenoCode','phenoName','numObserved','small5','small10','small15','small20','small25','small30','small35','small40','small45','small50','small55','small60','small65','small70','small75','small80','small85','small90','small95','small100','large5','large10','large15','large20','large25','large30','large35','large40','large45','large50','large55','large60','large65','large70','large75','large80','large85','large90','large95','large100']]=inPhenoCounts2.loc[:,'phenoCountsRows'].str.split('@',expand=True)
		inPhenoCounts2=inPhenoCounts2.merge(sampleInput.loc[:,['TRID','repcn']],how='left',on='TRID')
		inPhenoCounts2['thisAlleleSmallSize']=inPhenoCounts2.loc[:,'repcn'].str.split('/',expand=True).loc[:,0].astype(int)
		inPhenoCounts2['thisAlleleLargeSize']=inPhenoCounts2.loc[:,'repcn'].str.split('/',expand=True).loc[:,1].astype(int)
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=5)   & (inPhenoCounts2['phenoCode']==phenoCode)),'small5']=  inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=5)   & (inPhenoCounts2['phenoCode']==phenoCode)),'small5'].values.astype(int)  + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=10)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small10']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=10)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small10'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=15)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small15']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=15)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small15'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=20)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small20']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=20)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small20'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=25)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small25']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=25)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small25'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=30)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small30']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=30)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small30'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=35)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small35']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=35)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small35'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=40)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small40']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=40)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small40'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=45)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small45']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=45)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small45'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=50)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small50']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=50)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small50'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=55)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small55']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=55)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small55'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=60)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small60']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=60)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small60'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=65)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small65']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=65)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small65'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=70)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small70']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=70)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small70'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=75)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small75']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=75)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small75'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=80)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small80']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=80)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small80'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=85)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small85']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=85)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small85'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=90)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small90']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=90)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small90'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=95)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small95']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=95)  & (inPhenoCounts2['phenoCode']==phenoCode)),'small95'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=100) & (inPhenoCounts2['phenoCode']==phenoCode)),'small100']=inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleSmallSize']>=100) & (inPhenoCounts2['phenoCode']==phenoCode)),'small100'].values.astype(int)+ 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=5)   & (inPhenoCounts2['phenoCode']==phenoCode)),'large5']=  inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=5)   & (inPhenoCounts2['phenoCode']==phenoCode)),'large5'].values.astype(int)  + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=10)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large10']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=10)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large10'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=15)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large15']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=15)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large15'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=20)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large20']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=20)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large20'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=25)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large25']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=25)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large25'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=30)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large30']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=30)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large30'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=35)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large35']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=35)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large35'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=40)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large40']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=40)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large40'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=45)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large45']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=45)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large45'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=50)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large50']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=50)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large50'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=55)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large55']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=55)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large55'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=60)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large60']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=60)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large60'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=65)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large65']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=65)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large65'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=70)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large70']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=70)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large70'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=75)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large75']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=75)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large75'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=80)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large80']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=80)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large80'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=85)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large85']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=85)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large85'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=90)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large90']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=90)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large90'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=95)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large95']= inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=95)  & (inPhenoCounts2['phenoCode']==phenoCode)),'large95'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=100) & (inPhenoCounts2['phenoCode']==phenoCode)),'large100']=inPhenoCounts2.loc[((inPhenoCounts2['thisAlleleLargeSize']>=100) & (inPhenoCounts2['phenoCode']==phenoCode)),'large100'].values.astype(int)+ 1
		inPhenoCounts2.loc[inPhenoCounts2['phenoCode']==phenoCode,'numObserved']=inPhenoCounts2.loc[inPhenoCounts2['phenoCode']==phenoCode,'numObserved'].values.astype(int)+1
		inPhenoCounts2['phenocounts']=inPhenoCounts2.loc[:,['phenoCode','phenoName','numObserved','small5','small10','small15','small20','small25','small30','small35','small40','small45','small50','small55','small60','small65','small70','small75','small80','small85','small90','small95','small100','large5','large10','large15','large20','large25','large30','large35','large40','large45','large50','large55','large60','large65','large70','large75','large80','large85','large90','large95','large100']].astype(str).agg('@'.join,axis=1)
		inPhenoCounts3=inPhenoCounts2.loc[:,['TRID','phenocounts']].groupby('TRID').agg('<>'.join).reset_index(drop=False)
		TRData=TRData.merge(inPhenoCounts3.loc[:,['TRID','phenocounts']],how='left',on='TRID')
		TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_y']=TRData.loc[TRData['phenocounts_y'].isna(),'phenocounts_x']
		TRData=TRData.drop(columns='phenocounts_x')
		TRData=TRData.rename(columns={'phenocounts_y':'phenocounts'})
	############ deal with GenoData
	stagingGenoData=pandas.DataFrame(index=range(0,len(sampleInput)),columns=GenoDataColNames)
	stagingGenoData.loc[:,['TRID','SampleID','dp',]]=sampleInput[['TRID','SampleID','dp']].values
	stagingGenoData.loc[:,['allele1_ru','allele2_ru']]=sampleInput.loc[:,'repcn'].str.split('/',expand=True).values
	stagingGenoData.loc[:,['allele1_ci','allele2_ci']]=sampleInput.loc[:,'repci'].str.split('/',expand=True).values
	stagingGenoData.loc[:,['allele1_support_type','allele2_support_type']]=sampleInput.loc[:,'supportTypes'].str.split('/',expand=True).values
	stagingGenoData.loc[:,['allele1_spanning_ad','allele2_spanning_ad']]=sampleInput.loc[:,'adsp'].str.split('/',expand=True).values
	stagingGenoData.loc[:,['allele1_flanking_ad','allele2_flanking_ad']]=sampleInput.loc[:,'adfl'].str.split('/',expand=True).values
	stagingGenoData.loc[:,['allele1_irr_ad','allele2_irr_ad']]=sampleInput.loc[:,'adir'].str.split('/',expand=True).values
	GenoData=pandas.concat([GenoData,stagingGenoData])
	TRData.drop_duplicates(keep='first').to_csv(TRDataFile,sep='\t',header=False,index=False,na_rep="\\N")
	GenoData.to_csv(GenoDataFile,sep='\t',header=False,index=False,na_rep="\\N")
	############ create IGV track 
	trackData=TRData.loc[~(TRData['phenocounts'].isna()),['chrom','start','end','repeatUnit','phenocounts']]
	# get max size for each phenotype
	trackData['phenoCountsRows']=trackData.loc[:,'phenocounts'].str.split('<>').tolist()
	trackData2=trackData.loc[:,['chrom','start','end','repeatUnit','phenoCountsRows']].explode('phenoCountsRows').reset_index(drop=True)
	trackData2['phenoCode']=''
	trackData2['phenoName']=''
	trackData2['numObserved']=0
	trackData2=trackData2.assign(small5=0,small10=0,small15=0,small20=0,small25=0,small30=0,small35=0,small40=0,small45=0,small50=0,small55=0,small60=0,small65=0,small70=0,small75=0,small80=0,small85=0,small90=0,small95=0,small100=0)
	trackData2=trackData2.assign(large5=0,large10=0,large15=0,large20=0,large25=0,large30=0,large35=0,large40=0,large45=0,large50=0,large55=0,large60=0,large65=0,large70=0,large75=0,large80=0,large85=0,large90=0,large95=0,large100=0)
	trackData2.loc[:,['phenoCode','phenoName','numObserved','small5','small10','small15','small20','small25','small30','small35','small40','small45','small50','small55','small60','small65','small70','small75','small80','small85','small90','small95','small100','large5','large10','large15','large20','large25','large30','large35','large40','large45','large50','large55','large60','large65','large70','large75','large80','large85','large90','large95','large100']]=trackData2.loc[:,'phenoCountsRows'].str.split('@',expand=True).values
	percentileKey={'0':0,'1':5,'2':10,'3':15,'4':20,'5':25,'6':30,'7':35,'8':40,'9':45,'10':50,'11':55,'12':60,'13':65,'14':70,'15':75,'16':80,'17':85,'18':90,'19':95,'20':100}
	trackData2['maxSizeSmallAllele']=[percentileKey[x] for x in (trackData2.iloc[:,8:28].astype(int)>0).sum(axis=1).astype(str)]
	trackData2['maxSizeLargeAllele']=[percentileKey[x] for x in (trackData2.iloc[:,28:48].astype(int)>0).sum(axis=1).astype(str)]
	trackData2['text']='Name=(' + trackData2.loc[:,'repeatUnit'] + ')n;Phenotype=' + trackData2.loc[:,'phenoName'] + ';NumberObserved=' + trackData2.loc[:,'numObserved'].astype(str) + ';MaxSizeSmallAllele=' + trackData2.loc[:,'maxSizeSmallAllele'].astype(str) + ';MaxSizeLargeAllele=' + trackData2.loc[:,'maxSizeLargeAllele'].astype(str)
	trackData2.loc[:,['chrom','start','end','text']].to_csv('GenesisTRsTrack.bed',sep='\t',header=False,index=False)
	return

if __name__ == "__main__":
	main(sys.argv[1:])
