import csv
from typing import List, Dict
import json
from shared_datatypes import Variant


def read_ehdn_variants(filename: str) -> List[Variant]:
    variant_list = []
    with open(filename) as f:
        data = csv.reader(f, delimiter="\t")
        for d in data:

            contig, start, end, motif, count = d[0], d[1], d[2], d[3], d[4]

            # Excluding the header row
            if contig == 'chr':
                continue

            # Excluding the sex and decoy chromosomes from our analysis
            #if any(c in contig for c in ['Un', 'KI', 'EBV', 'random', 'X', 'Y']):
            if any(c in contig for c in ['Un', 'KI', 'EBV', 'random']):
                continue

            try:
                start = int(start)
                end = int(end)
            except ValueError:
                print("Something went wrong converting coordinates to numbers.")

            variant_list.append(Variant({
                'contig': contig,
                'start': start,
                'end': end,
                'motif': motif,
                'count': count
            }))
    return variant_list


def json_to_dict(filename: str) -> List[Dict]:
    res = []

    with open(filename) as f:
        data = json.load(f)
        for d in data:
            contig = d['ehdn']['contig']
            start = d['ehdn']['start']
            end = d['ehdn']['end']
            motif = d['ehdn']['motif']
            if isinstance(motif, list):
                motif = motif[0]

            res.append({
                'contig': contig,
                'start': start,
                'end': end,
                'motif': motif,
            })

    return res


def read_in_varcat(filename: str):
    res = []
    with open(filename) as f:
        data = json.load(f)
        for d in data:
            res.append({
                'LocusId': d['LocusId'],
                'LocusStructure': d['LocusStructure'],
                'ReferenceRegion': d['ReferenceRegion'],
                'VariantType': d['VariantType'],
            })
    return res


def read_gff_file(filename: str):
    with open(filename) as f:
        data = f.readlines()
        for d in data[500:505]:
            print(d)

