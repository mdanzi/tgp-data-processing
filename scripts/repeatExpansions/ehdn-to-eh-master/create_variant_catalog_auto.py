import pysam
import transform as t
import read as r
import write as w
from shared_datatypes import RefGenome
import sys

# Version for EHdn output in format for Sarah
def transform_format(ehdn_file: str, refgen_file: str, varcat_out_file: str, unmatched_out_file: str, excluded_out_file: str):
    ehdn_variants = r.read_ehdn_variants(ehdn_file)
    ref_genome = RefGenome(pysam.FastaFile(refgen_file))

    # If a variant has no basis in the reference genome, it gets excluded.
    transformed_variants, unmatched_variants = t.transform_variants(ehdn_variants, ref_genome)
    print("Excluded: {}, transformed: {} variants with no basis in the ref.".format(len(unmatched_variants), len(transformed_variants)))

    # If a variant has too many Ns in the surrounding region, it gives an error when running EH
    # The default value is a margin of 1000, corresponding to --region-extension-length arg (=1000) optional argument
    # for EH, to define how far from on/off-target regions to search for informative reads
    retained_variants, excluded_variants = t.filter_out_too_many_Ns(transformed_variants, ref_genome, margin=1000)
    print("Excluded: {}, retained: {} variants after filtering out too many Ns.".format(len(excluded_variants), len(retained_variants)))

    # Output the actual variant catalog to file
    w.write_variant_catalog(varcat_out_file, retained_variants)
    # Output the excluded regions that have no basis in the ref to file
    w.write_variants_to_csv_format(unmatched_out_file, unmatched_variants)
    # Output the regions excluded due to too many Ns to file
    w.write_variants_to_csv_format(excluded_out_file, excluded_variants)

    print("Successfully created a variant catalog with {} entries from {} inputted regions.".format(len(retained_variants), len(ehdn_variants)))
    return retained_variants


refgenfile = r'/home/ec2-user/ref/hs37d5.fa'
name = sys.argv[1]
ehdnfile = r'tgp-data-processing/scripts/repeatExpansions/ehdn-to-eh-master/input/input_%s.txt' % name
varcat_output = r'variant_catalog_%s.json' % name
unmatched_output = r'unmatched_regions_%s.txt' % name
excluded_output = r'excluded_%s.txt' % name

transform_format(ehdnfile, refgenfile, varcat_output, unmatched_output, excluded_output)
