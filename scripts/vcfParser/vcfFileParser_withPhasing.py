#!/usr/bin/env python



import argparse, sys, re, os, subprocess, math, collections, time, datetime, uuid, json, logging
import parser_utilities


#### Class to parse VCF files
class VCF_Parsing(object):


	#### Initialize class
	def __init__(self, args):

		self.args = args
		self.sampleList = self.args['sampleList'].strip().split(',')

		## Set logging instance
		logger = logging.getLogger("GEDI VCFParser")

		if self.args['logging_mode'] == 'debug': logger.setLevel(logging.DEBUG)
		elif self.args['logging_mode'] == 'verbose': logger.setLevel(logging.INFO)
		elif self.args['logging_mode'] == 'warning': logger.setLevel(logging.WARNING)
		elif self.args['logging_mode'] == 'quiet': logger.setLevel(logging.ERROR)

		# Format for our loglines
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

		# Setup console logging
		ch = logging.StreamHandler(sys.stderr)

		if self.args['logging_mode'] == 'debug': logger.setLevel(logging.DEBUG)
		elif self.args['logging_mode'] == 'verbose': logger.setLevel(logging.INFO)
		elif self.args['logging_mode'] == 'warning': logger.setLevel(logging.WARNING)
		elif self.args['logging_mode'] == 'quiet': logger.setLevel(logging.ERROR)

		ch.setFormatter(formatter)
		logger.addHandler(ch)

	#### Start reading VCF file
	def parse_vcf(self):

		logger = logging.getLogger("GEDI VCFParser")
		logger.info("Starting to parse VCF file")

		## read VCF line by line
		for line in sys.stdin:

			## Skip header lines and parse other lines
			if line.strip().startswith('#'):pass
			else: self.parse_vcf_data(line.strip())

		logger.info("Completed parsing of VCF file")


	#### parse the sample data/genotypes from VCF file
	def parse_vcf_data(self, p_line):

		logger = logging.getLogger("GEDI VCFParser")
		logger.debug("Starting to parse line: {0}".format(p_line.strip()))

		#### Define the columns
		data = p_line.split('\t')
		chromosome = parser_utilities.reformat_Chromosome(data[0].lower())
		position = data[1]
		dbSNP_id = data[2]
		reference = data[3]
		alt_allele = data[4]
		alternate = data[4].strip().split(',')
		quality = parser_utilities.replace_dot(data[5])
		filter = parser_utilities.reformat_filter_data(data[6])
		info = data[7].strip().split(';')
		format = data[8]
		sample_data = data[9:]

		#### Global variables
		lines = []
		sample_data_results = []

		#### Verify that the number of samples in the VCF matches what we expect
		if len(sample_data) != len(self.sampleList):
			logger.error("Number of samples is not what we expect! Expected # of samples in VCF: {0}. Actual # of samples in VCF: {1}".format(len(sample_data), len(self.sampleList)))
			sys.exit(1)

		#### At the moment we only want to store variant data in our Genomic Variant Database, so we skip non variant sites. Also we skip non SNV/indel sites
		if alt_allele != '.' and not alt_allele.startswith('<') and chromosome != '':

			## Parse out genomic data for each sample listed in VCF
			sample_data_results = self.extract_sample_data(chromosome, position, reference, alternate, quality, filter, info, format, sample_data)

		if len(sample_data_results) > 0:
			logger.debug("Starting Input: {0}".format(p_line))

		else:
			logger.warning("{0} was skipped!".format(p_line))

		for item in sample_data_results:
			sys.stdout.write("{0}\t{1}\n".format(item, self.args['run_id']))
		sys.stdout.flush()

		logger.debug("Parsing of line {0} completed".format(p_line))


	#### Extract genotype data from sample data
	def extract_sample_data(self, chromosome, position, reference, alternate, quality, filter, info, format, sample_data):

		logger = logging.getLogger("GEDI VCFParser")

		#### Extract specific fields (Depth, genotype quality, allelic depth)
		data_keys = format.strip().split(':')

		#### Common fields
		try:
			GT_idx = data_keys.index('GT')
		except ValueError:
			logger.warning("GT info not available!")
			GT_idx = -9

		info_depth = False
		try:
			DP_idx = data_keys.index('DP')

		except ValueError:

			## if not in the sample genotype column, try the info field
			for info_item in info:
				elements = info_item.split("=")
				if elements[0] == "DP":
					info_depth = True
					DP_idx = elements[1]

			if info_depth == False:
				logger.warning("DP info not available!")
				DP_idx = -9

		try:
			GQ_idx = data_keys.index('GQ')
		except ValueError:
			logger.warning("GQ info not available!")
			GQ_idx = -9


		#### GATK specific fields
		try:
			AD_idx = data_keys.index('AD')
		except ValueError:
			logger.warning("AD info not available!")
			AD_idx = -9


		#### FreeBayes specific fields
		try:
			RO_idx = data_keys.index('RO')
		except ValueError:
			logger.warning("RO info not available!")
			RO_idx = -9

		try:
			AO_idx = data_keys.index('AO')
		except ValueError:
			logger.warning("AO info not available!")
			AO_idx = -9

		#### Variant call type for FreeBayes. This helps us adjust complex variant calls ####
		type_elements=[]
		for info_item in info:
			elements = info_item.split("=")
			if elements[0] == "TYPE":
				type_elements.append(info_item)

		if len(type_elements) > 1:
			logger.warning("More than one TYPE attribute for locus chr{0}:{1}. We only take the first occurence! TYPE list: {2}".format(chromosome, position, type_elements))

		try:
			variant_type_array = type_elements[0].strip().split('=')[1].split(',')
		except IndexError:
			logger.warning("There is not TYPE attribute")
			variant_type_array = []

		#### Loop through sample data
		sample_array = []
		sample_idx = 0
		for sample in sample_data:

			## Initialize all the variables we will need
			sample_genotype_data = sample.strip().split(':')
			if '|' in sample_genotype_data[GT_idx]: sample_alleles = sample_genotype_data[GT_idx].strip().split('|')
			else: sample_alleles = sample_genotype_data[GT_idx].strip().split('/')
			phasedVariant = False
			if '|' in sample_genotype_data[GT_idx]: phasedVariant = True
			sample_id = self.sampleList[sample_idx]

			## Determine the genotype for this sample
			if GT_idx != -9:
				sample_genotype = parser_utilities.reformat_sample_genotype(sample_genotype_data[GT_idx])
			else:
				logger.warning("{0} genotype is not available!".format(sample_id))
				sample_genotype = ''

			## We only parse out sample data if there is genotype information
			if sample_genotype != '':

				sample_depth = ''
				sample_genotype_quality = ''
				sample_ref_reads = 0
				sample_alt_reads = []

				#### Common fields for the variant sites (not allele-specific)
				if DP_idx != -9:
					if info_depth == False:
						sample_depth = sample_genotype_data[DP_idx]
					else:
						sample_depth = DP_idx

				try:
					if GQ_idx != -9:
						if sample_genotype_data[GQ_idx] != '.':
							sample_genotype_quality = sample_genotype_data[GQ_idx]
				except IndexError:
					logger.warning("{0} genotype quality is not available!".format(sample_id))
					sample_genotype_quality = ''

				#### GATK specific fields. We prefer the FreeBayes quality scores/depths, but we will take GATK scores as well. Scores will be overwritten by RO and AO if available
				#### These scores are for each variant site (not allele)
				if AD_idx == -9:

					for allele in alternate: sample_alt_reads.append(0)
					if sample_genotype == 0: sample_ref_reads = sample_depth
					logger.warning("{0} AD is not available! Defaults were set".format(sample_id))

				else:

					try:
						if sample_genotype_data[AD_idx] == '.':

							for allele in alternate: sample_alt_reads.append(0)
							if sample_genotype == 0: sample_ref_reads = sample_depth
							logger.warning("{0} AD is not available! Defaults were set".format(sample_id))

						else:
							sample_ref_reads = sample_genotype_data[AD_idx].strip().split(',')[0]
							alt_read_depth = sum(map(int, sample_genotype_data[AD_idx].strip().split(',')[1:]))
							for allele in alternate: sample_alt_reads.append(alt_read_depth)

					except IndexError:

						for allele in alternate: sample_alt_reads.append(0)
						if sample_genotype == 0: sample_ref_reads = sample_depth
						logger.warning("{0} AD is not available! Defaults were set".format(sample_id))

				#### FreeBayes specific Reference observations
				if RO_idx != -9: sample_ref_reads = sample_genotype_data[RO_idx]
				if AO_idx != -9: sample_alt_reads = sample_genotype_data[AO_idx].strip().split(',')

				## Reference wildtype homozygous variant calls.
				## We print a row for each variant allele observed
				if sample_genotype == 0:
					logger.debug("{0} has a homozygous reference genotype {1} at locus chr{2}:{3}".format(sample_id, sample_genotype, chromosome, position))
					phaseGroup=float('nan')
					## Add an entry for each alternate allele
					## Alternate alleles start at 1, thus we initialize the counter at 1. This will correctly match allele idx with type idx
					allele_idx=1
					for alternate_allele in alternate:

						# Discard asterisk alleles ("*"), GATK-specific
						if alternate_allele == '*': continue

						## If we have a complex site or indel, we might need to adjust the alleles
						if len(variant_type_array) > 0:

							if variant_type_array[int(allele_idx)-1] in ['snp'] and (len(reference) > 1 or len(alternate_allele) > 1):
								logger.warning("{0} has a snp in a complex variant locus at chr{1}:{2}".format(sample_id, chromosome, position))
								new_allele_results = parser_utilities.fix_alleles(position, reference, alternate_allele)
								items = range(0, len(new_allele_results['position']))
								for item in items: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, new_allele_results['position'][item], new_allele_results['reference'][item], new_allele_results['alternate'][item], sample_genotype, parser_utilities.mutation_class_type(variant_type_array[int(allele_idx)-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(allele_idx)-1], filter, phaseGroup))

							else: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, position, reference, alternate_allele, sample_genotype, parser_utilities.mutation_class_type(variant_type_array[int(allele_idx)-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(allele_idx)-1], filter, phaseGroup))

						else: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, position, reference, alternate_allele, sample_genotype, parser_utilities.variant_class_type(reference, alternate_allele), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(allele_idx)-1], filter, phaseGroup))
						allele_idx+=1

				## Heterozygous genotypes.
				## We support multihet genotypes as well
				elif sample_genotype == 1:
					logger.debug("{0} has a heterozygous genotype {1} at locus chr{2}:{3}".format(sample_id, sample_genotype, chromosome, position))

					ref_alt_combos = []
					for phaseGroup, sample_allele in enumerate(sample_alleles):
						phaseGroupText=float('nan')
						if phasedVariant:
							phaseGroupText=phaseGroup
						## Only add non reference alleles
						if int(sample_allele) != 0:

							# Discard asterisk alleles ("*"), GATK-specific
							if alternate[int(sample_allele)-1] == '*': continue

							## If we have a complex site or indel, we might need to adjust the alleles
							if len(variant_type_array) > 0:

								if variant_type_array[int(sample_allele)-1] in ['snp'] and (len(reference) > 1 or len(alternate[int(sample_allele)-1]) > 1):
									logger.warning("{0} has a snp in a complex variant locus at chr{1}:{2}".format(sample_id, chromosome, position))
									new_allele_results = parser_utilities.fix_alleles(position, reference, alternate[int(sample_allele)-1])
									items = range(0, len(new_allele_results['position']))

									for item in items:
										ref_alt_combo = "{0}-{1}-{2}".format(new_allele_results['position'][item], new_allele_results['reference'][item], new_allele_results['alternate'][item])
										if ref_alt_combo not in ref_alt_combos: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, new_allele_results['position'][item], new_allele_results['reference'][item], new_allele_results['alternate'][item], sample_genotype, parser_utilities.mutation_class_type(variant_type_array[int(sample_allele)-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(sample_allele)-1], filter, phaseGroupText))
										ref_alt_combos.append(ref_alt_combo)

								else: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, position, reference, alternate[int(sample_allele)-1], sample_genotype, parser_utilities.mutation_class_type(variant_type_array[int(sample_allele)-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(sample_allele)-1], filter, phaseGroupText))

							else: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, position, reference, alternate[int(sample_allele)-1], sample_genotype, parser_utilities.variant_class_type(reference, alternate[int(sample_allele)-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(sample_allele)-1], filter, phaseGroupText))

				elif sample_genotype == 2:
					logger.debug("{0} has a homozygous alternate genotype {1} at locus chr{2}:{3}".format(sample_id, sample_genotype, chromosome, position))
					phaseGroup=float('nan')
					# Discard asterisk alleles ("*"), GATK-specific
					if alternate[int(sample_alleles[0])-1] == '*': continue

					## If we have a complex site or indel, we might need to adjust the alleles
					if len(variant_type_array) > 0:

						if variant_type_array[int(sample_alleles[0])-1] in ['snp'] and (len(reference) > 1 or len(alternate[int(sample_alleles[0])-1]) > 1):
							logger.warning("{0} has a snp in a complex variant locus at chr{1}:{2}".format(sample_id, chromosome, position))
							new_allele_results = parser_utilities.fix_alleles(position, reference, alternate[int(sample_alleles[0])-1])
							items = range(0, len(new_allele_results['position']))
							for item in items: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, new_allele_results['position'][item], new_allele_results['reference'][item], new_allele_results['alternate'][item], sample_genotype, parser_utilities.mutation_class_type(variant_type_array[int(sample_alleles[0])-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(sample_alleles[0])-1], filter, phaseGroup))

						else: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, position, reference, alternate[int(sample_alleles[0])-1], sample_genotype, parser_utilities.mutation_class_type(variant_type_array[int(sample_alleles[0])-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(sample_alleles[0])-1], filter, phaseGroup))

					else: sample_array.append(parser_utilities.create_sample_output_phased(sample_id, chromosome, position, reference, alternate[int(sample_alleles[0])-1], sample_genotype, parser_utilities.variant_class_type(reference, alternate[int(sample_alleles[0])-1]), sample_depth, quality, sample_genotype_quality, sample_ref_reads, sample_alt_reads[int(sample_alleles[0])-1], filter, phaseGroup))

			else:
				logger.warning("Error parsing genotype '{geno}'.  Discarding data for sample {samp} at locus {chrom} {pos} {ref} {alt}".format(
					geno=sample_genotype_data[GT_idx], samp=sample_id, chrom=chromosome, pos=position, ref=reference, alt=alternate))

			## Move on to the next sample
			sample_idx+=1

		return sample_array


if __name__ == "__main__":

	# create the top-level parser
	parser = argparse.ArgumentParser()
	parser.add_argument('-l', '--logging-mode', action="store", default='quiet')
	parser.add_argument('-r', '--run-id', action="store", default=None)
	parser.add_argument('-s', '--sample-list', action="store", default=None,
						dest='sampleList')

	args = parser.parse_args(sys.argv[1:]).__dict__
	VCF_Parsing(args).parse_vcf()

