#!/usr/bin/env python

import sys, re, os, subprocess, math, collections, time, datetime, uuid, json, logging


## Set logging instance
logger = logging.getLogger("GEDI Utilities")
logger.setLevel(logging.ERROR)

# Format for our loglines
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# Setup console logging
ch = logging.StreamHandler()
logger.setLevel(logging.ERROR)

ch.setFormatter(formatter)
logger.addHandler(ch)


#######################################################################################
###### This function will help us use the zookeeper service Updated to use Kazoo ######
#######################################################################################
def zookeeper(p_function, optional_parameters, p_mode='strict', p_socket='2181'):

	zk = KazooClient(hosts='{0}:{1}'.format(optional_parameters['zookeeper_dns'], p_socket))
	zk.start()

	data = ""
	sysCode = 0

	## Build zookeeper command and execute
	if p_function == 'create':

		if zk.exists("/{0}".format(optional_parameters['zookeeper_node'])):

			## If node already exists, verify that it has the same value. If so, don't raise an error
			data, stat = zk.get("{0}".format(optional_parameters['zookeeper_node']))
			data = data.decode("utf-8")
			if data != optional_parameters['zk_value']:
				error_reporting(p_mode, "Node {0} already exists".format(optional_parameters['zookeeper_node']))
				sysCode = 1

		else: zk.create("/{0}".format(optional_parameters['zookeeper_node']), value="{0}".format(optional_parameters['zk_value']))

	## Get info from a znode
	elif p_function == 'get':

		## First check if the node exists
		if zk.exists("{0}".format(optional_parameters['zookeeper_node'])):
			data, stat = zk.get("{0}".format(optional_parameters['zookeeper_node']))
			data = data.decode("utf-8")

		else:
			error_reporting(p_mode, "Node {0} does not exist!".format(optional_parameters['zookeeper_node']))
			sysCode = 1

	## Delete info from a znode
	elif p_function == 'delete':

		if zk.exists("{0}".format(optional_parameters['zookeeper_node'])): zk.delete("{0}".format(optional_parameters['zookeeper_node']))
		else:
			error_reporting(p_mode, "Node {0} does not exist!".format(optional_parameters['zookeeper_node']))
			sysCode = 1

	return {"data": data, "sysCode": sysCode}


###########################
### Load JSON from file ###
###########################
def load_json_from_file(p_file):
    object = {}
    with open(p_file, 'r') as object_data:
        object = json.load(object_data)
    return object



########################
### Get current time ###
########################
def get_time():
    now = datetime.datetime.now()
    time = '_'.join([str(now.year),str(now.month),str(now.day),str(now.hour),str(now.minute),str(now.second),str(now.microsecond)])
    return time


########################
### Replace Dots VCF ###
########################
def replace_dot(p_value):
	if str(p_value) == '.':
		return ''
	else:
		return p_value



#########################
### Will variant type ###
#########################
def variant_class_type(p_reference, p_alternate):

    #snp
	if len(p_reference) == 1 and len(p_alternate) == 1:
		return 0
	#ins
	elif len(p_reference) == 1 and len(p_alternate) > 1:
		return 1
	#del
	elif len(p_reference) > 1 and len(p_alternate) == 1:
		return 2
	#mnp
	elif len(p_reference) > 1 and len(p_alternate) > 1 and len(p_reference) == len(p_alternate):
		return 3
	#complex
	else:
		return 4



######################
### Mutation Class ###
######################
def mutation_class_type(p_value):
	if p_value == 'snp':
		return 0
	elif p_value == 'ins':
		return 1
	elif p_value == 'del':
		return 2
	elif p_value == 'mnp':
		return 3
	elif p_value == 'complex':
		return 4
	else:
		logger.error('Unrecognized mutation type: {0}\n'.format(p_value))
		sys.exit(1)


###########################################################################
### Reformat sample genotype data to fit with GENESIS genomic datastore ###
###########################################################################
def reformat_sample_genotype(sample_genotype):

	logger = logging.getLogger("GEDI Utilities")

	## Determine sample genotype format
	# EP - Match genotypes containing any dots instead of just only dots
	# if sample_genotype in ['.', './.']: return ''
	if '.' in sample_genotype: return ''
	# Exclude genotypes that are a single digit eg '1'
	elif re.match('[0-9]$', sample_genotype): return ''
	elif '|' in sample_genotype: sample_alleles = sample_genotype.strip().split('|')
	elif '/' in sample_genotype: sample_alleles = sample_genotype.strip().split('/')
	else:
		logger.error('Unrecognized genotype format: {0}\n'.format(sample_genotype))
		sys.exit(1)

	## Homozygous genotypes (either reference or alternate)
	if int(sample_alleles[0]) == int(sample_alleles[1]):
		if int(sample_alleles[0]) == 0: return 0
		else: return 2

	## Heterozygous genotypes
	else: return 1


# Will check the return code of an multithreaded process and report the results
def check_multithreaded_syscode(v_hash, v_stage):

	for k, v in v_hash["processes-details"].items():

		#if v["out"] != "": print "out: {0}".format(v["out"])
		if v["error"] != "": print "err: {0}".format(v["error"])

	if v_hash["sysCode"] == 0: print "\n\n{0} completed!\n\n".format(v_stage)
	else:
		print "\n\n{0} failed!\n\n".format(v_stage)
		sys.exit(1)


# Will check the return code of an execution process and report the results
def check_execution_syscode(v_hash, v_stage):

	#if v_hash["out"] != "": print "out: {0}".format(v_hash["out"])
	if v_hash["err"] != "": print "err: {0}".format(v_hash["err"])

	if v_hash["sysCode"] == 0: print "\n\n{0} complete...\n\n".format(v_stage)
	else:
		print "\n\n{0} failed: {1}\n\n".format(v_stage, v_hash["err"])
		sys.exit(1)



# Will launch processes using multithreading and wait for all processes to finish before moving on. Will add the ability to catch errors
#modes #0=Strict #1=Lenient #2=Silent
def multithreading(commands, threads=2, mode= 0, delay=1):
	processes_hash = {}
	processes = set()
	for  command_ in commands:
		while len(processes) >= threads:
			time.sleep(0.5)
			tmpProcessesPoll = []
			for p in processes:
				if p.poll() is not None:
					vv_out, vv_err = p.communicate()
					processes_hash["{0}".format(p.pid)] = {"command": processes_hash["{0}".format(p.pid)]["command"], "out": vv_out.strip(), "error": vv_err.strip(), "sysCode": p.returncode }
					tmpProcessesPoll.append(p)

			processes.difference_update(tmpProcessesPoll)
		time.sleep(delay)
		p_aux = subprocess.Popen(command_, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		processes_hash["{0}".format(p_aux.pid)] = {"command": command_}
		processes.add(p_aux)

	#Wait for last bulk
	tmpProcessesPoll = []
	for p in processes:
		if p.poll() is None:
			p.wait();
			tmpProcessesPoll.append(p)
		vv_out, vv_err = p.communicate()
		processes_hash["{0}".format(p.pid)] = {"command": processes_hash["{0}".format(p.pid)]["command"], "out": vv_out.strip(), "error": vv_err.strip(), "sysCode": p.returncode }

	#Clean processes poll
	processes.difference_update(tmpProcessesPoll)

	#Verify all processes
	v_ret = 0
	for k, v in processes_hash.items():
		if v["sysCode"] != 0 and mode == 0:
			v_ret = v["sysCode"]
			return {"sysCode": v_ret, "processes-details" : processes_hash}

	#verify all sysCodes are 0
	return {"sysCode": v_ret, "processes-details" : processes_hash}



# Will execute command and capture out and err messages into a variable
def execute_command(v_cmd):

	p = subprocess.Popen(v_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out,err = p.communicate()

	return {"sysCode": p.returncode, "out": out.strip(), "err": err.strip() }



# Will execute command and print out and err onto the screen. Good for debugging or real time monitoring
def execute_command_debug(v_cmd):

	p = subprocess.Popen(v_cmd, shell=True)
	p.wait()

	return {"sysCode": p.returncode, "out": "", "err": "" }



###########################################################################
### Reformat sample genotype data to fit with GENESIS genomic datastore ###
###########################################################################
def fix_alleles(position, reference, alternate):

	## Break ref/alt sequences into nucleotide arrays
	reference_sequence = list(reference)
	alternate_sequence = list(alternate)

	new_reference=[]
	new_alternate=[]
	new_position=[]
	bp_changes=0

	## Identify changes in reference/alternate sequences only if the two sequences are of equal size and not a single nucleotide
	if len(reference_sequence) == len(alternate_sequence) and len(reference_sequence) > 1:

		idx=0
		ref_alt_combos =[]
		for nucelotide in reference_sequence:

			if nucelotide != alternate_sequence[idx]:

				ref_alt_combo = "{0}-{1}-{2}".format(nucelotide,alternate_sequence[idx], str(int(position)+idx))

				if ref_alt_combo not in ref_alt_combos:
					new_reference.append(nucelotide)
					new_alternate.append(alternate_sequence[idx])
					new_position.append(str(int(position)+idx))

				ref_alt_combos.append(ref_alt_combo)

			idx+=1

		return {"position": new_position, "reference": new_reference, "alternate": new_alternate}

	else:

		return {"position": [position], "reference": [reference], "alternate": [alternate]}


##############################
### Reformat FILTER column ###
##############################
def reformat_filter_data(filter):

	if (filter=='PASS'):
		return '0'
	elif (filter=='CNN_1D_SNP_Tranche_95.00_98.00') or (filter=='CNN_1D_INDEL_Tranche_95.00_98.00'):
		return '1'
	elif (filter=='CNN_1D_SNP_Tranche_98.00_99.00') or (filter=='CNN_1D_INDEL_Tranche_98.00_99.00'):
		return '2'
	elif (filter=='CNN_1D_SNP_Tranche_99.00_99.50') or (filter=='CNN_1D_INDEL_Tranche_99.00_99.20'):
		return '3'
	elif (filter=='CNN_1D_SNP_Tranche_99.50_99.90') or (filter=='CNN_1D_INDEL_Tranche_99.20_99.40'):
		return '4'
	elif (filter=='CNN_1D_SNP_Tranche_99.90_99.95'):
		return '5'
	elif (filter=='CNN_1D_SNP_Tranche_99.95_100.00') or (filter=='CNN_1D_INDEL_Tranche_99.40_100.00'):
		return '6'
	else:
		return ''


#############################################
### Create extract sample output from VCF ###
#############################################
def create_sample_output(sample_id, chromosome, position, ref, alt, genotype, mutation_class_type, depth, quality, genotype_quality, ref_reads, alt_reads, filter):

	## If we ever decide to display alt/ref reads we may need to reprocess the VCFs because there was a typo here {11} was {1} instead - so it printed the chromosome instead of alt_reads
	return "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}".format(sample_id, chromosome, position, ref, alt, genotype, mutation_class_type, depth, quality, genotype_quality, ref_reads, alt_reads, filter)

def create_sample_output_phased(sample_id, chromosome, position, ref, alt, genotype, mutation_class_type, depth, quality, genotype_quality, ref_reads, alt_reads, filter, phaseGroup):

	## If we ever decide to display alt/ref reads we may need to reprocess the VCFs because there was a typo here {11} was {1} instead - so it printed the chromosome instead of alt_reads
	return "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}".format(sample_id, chromosome, position, ref, alt, genotype, mutation_class_type, depth, quality, genotype_quality, ref_reads, alt_reads, filter, phaseGroup)



#######################################
### Will parse EXaC VCF INFO column ###
#######################################
def reformat_EXaC_info(p_column, p_column_separator, allele_idx):

	logger = logging.getLogger("GEDI Utilities")

	EXaC_allele_columns = ['AC','AC_AFR','AC_AMR','AC_Adj','AC_EAS','AC_FIN','AC_Het','AC_Hom','AC_NFE','AC_OTH','AC_SAS','AF','AN','AN_AFR','AN_AMR','AN_Adj','AN_EAS','AN_FIN','AN_NFE','AN_OTH','AN_SAS','Het_AFR','Het_AMR','Het_EAS','Het_FIN','Het_NFE','Het_OTH','Het_SAS','Hom_AFR','Hom_AMR','Hom_EAS','Hom_FIN','Hom_NFE','Hom_OTH','Hom_SAS']

	new_values = []
	values = p_column.strip().split(p_column_separator)

	for value in values:
		if value.startswith('AC=') or value.startswith('AC_') or value.startswith('AF=') or value.startswith('Hom_') or value.startswith('Het_'):
			info_data = value.strip().split('=')
			value_values_array = info_data[1].strip().split(',')
			if info_data[0] in EXaC_allele_columns: new_values.append(str(value_values_array[allele_idx]))

		elif value.startswith('AN=') or value.startswith('AN_'):
			info_data = value.strip().split('=')
			if info_data[0] in EXaC_allele_columns: new_values.append(str(info_data[1]))


	if len(new_values) != len(EXaC_allele_columns):
	   logger.error('Not all EXaC values were parsed! Only have {0} values, expected {1}\n'.format(len(new_values),len(EXaC_allele_columns)))
	   sys.exit(1)

	return new_values



#################################
### Will reformat strand info ###
#################################
def reformat_strand(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in [-1, 1]: return str(p_column)
	else:
		if p_column == '-': return '-1'
		elif p_column == '+': return '1'
		else:
			logger.error('Unknown strand: {0}\n'.format(p_column))
			sys.exit(1)



#######################################
### Will reformat chromosome column ###
#######################################
def reformat_Chromosome(p_column):

	chromosomes = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22']
	## Remove any leading values for chromosome
	try: chromosome = str(p_column.replace("chr", ""))
	except: chromosome = str(p_column)

	## Transform X, Y, MT
	if chromosome == 'x': return '23'
	elif chromosome == 'y': return '24'
	elif chromosome in ['mt','m']: return '25'
	elif chromosome in chromosomes: return chromosome
	elif chromosome == '': return ''

	## We will skip non standard chromosomes
	else: return ''



########################################
### Will select the alternate allele ###
########################################
def reformat_seattleSeq_alleles(p_column, p_column_separator):

	## We always submit homozygous alleles to SeattleSeq so we can expect that the first allele is the ALT
	alleles = p_column.strip().split(p_column_separator)
	return alleles[0]



#########################################
### Will reformat ClinVar Type column ###
#########################################
def reformat_ClinVar_type(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'copy number gain': return '0'
	elif p_column == 'copy number loss': return '1'
	elif p_column == 'deletion': return '2'
	elif p_column == 'duplication': return '3'
	elif p_column == 'fusion': return '4'
	elif p_column == 'indel': return '5'
	elif p_column == 'insertion': return '6'
	elif p_column == 'inversion': return '7'
	elif p_column == 'nt expansion': return '8'
	elif p_column == 'protein only': return '9'
	elif p_column == 'short repeat': return '10'
	elif p_column == 'single nucleotide variant': return '11'
	elif p_column == 'structural variant': return '12'
	elif p_column == 'undetermined variant': return '13'
	else:
		logger.error('Unknown ClinVar Type: {0}'.format(p_column))
		sys.exit(1)



#######################################
### Will reformat LOF LOFTEE column ###
#######################################
def reformat_lof(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'HC': return '1'
	elif p_column == 'LC': return '2'
	elif p_column == '': return ''
	else:
		logger.error('Unknown LOF: {0}'.format(p_column))
		sys.exit(1)



#######################################
### Will reformat VEP IMPACT column ###
#######################################
def reformat_impact(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'HIGH': return '0'
	elif p_column == 'MODIFIER': return '1'
	elif p_column == 'MODERATE': return '2'
	elif p_column == 'LOW': return '3'
	elif p_column == '': return ''
	else:
		logger.error('Unknown VEP IMPACT: {0}'.format(p_column))
		sys.exit(1)



###################################################
### Will pick the largest float value in a list ###
###################################################
def pick_largest_float_score(p_column, p_column_separator):

	column_values = p_column.strip().split(p_column_separator)
	largest_value = -999999999999.9

	for value in column_values:

		try:
			if float(value) > float(largest_value):
				largest_value = float(value)
		except ValueError: pass

	return largest_value



#####################################################
### Will pick the largest integer value in a list ###
#####################################################
def pick_largest_int_score(p_column, p_column_separator):

	column_values = p_column.strip().split(p_column_separator)
	largest_value = -999999999999

	for value in column_values:

		try:
			if int(value) > int(largest_value):
				largest_value = int(value)
		except ValueError: pass

	return largest_value



###################################################
### Will pick the largest float value in a list ###
###################################################
def pick_smallest_float_score(p_column, p_column_separator):

	column_values = p_column.strip().split(p_column_separator)
	smallest_value = 999999999999.9

	for value in column_values:

		try:
			if float(value) < float(smallest_value):
				smallest_value = float(value)
		except ValueError: pass

	return smallest_value



#####################################################
### Will pick the largest integer value in a list ###
#####################################################
def pick_smallest_int_score(p_column, p_column_separator):

	column_values = p_column.strip().split(p_column_separator)
	smallest_value = 999999999999

	for value in column_values:

		try:
			if int(value) < int(smallest_value):
				smallest_value = int(value)
		except ValueError: pass

	return smallest_value



#############################################################
### Will reformat dbscSNV refseq Sequence Ontology column ###
#############################################################
def reformat_dbscSNV_SO(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'stopgain snv': return '0'
	elif p_column == 'stoploss snv': return '1'
	elif p_column == 'nonsynonymous snv': return '2'
	elif p_column == 'synonymous snv': return '3'
	elif p_column == 'unknown': return '4'
	else:
		logger.error('Uknown RefSeq Sequence Ontology: {0}'.format(p_column))
		sys.exit(1)



###############################################
### Will reformat FILTER column in VCF file ###
###############################################
def reformat_VCF_Filter(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'pass': return 0
	elif p_column == 'lowqual': return 1
	else:
		logger.error('Uknown FILTER status {0}'.format(p_column))
		sys.exit(1)



##################################################
### Will reformat SeattleSeq Sequence Ontology ###
##################################################
def reformat_seattleSeq_SO(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'frameshift': return '1'
	elif p_column == 'frameshift-near-splice': return '2'
	elif p_column == 'coding': return '3'
	elif p_column == 'coding-near-splice': return '4'
	elif p_column == 'codingcomplex': return '5'
	elif p_column == 'codingcomplex-near-splice': return '6'
	elif p_column in ['coding-unknown', 'coding-notmod3']: return '7'
	elif p_column in ['coding-unknown-near-splice', 'coding-notmod3-near-splice']: return '8'
	elif p_column == 'stop-gained': return '9'
	elif p_column == 'stop-gained-near-splice': return '10'
	elif p_column == 'missense': return '11'
	elif p_column == 'missense-near-splice': return '12'
	elif p_column == 'stop-lost': return '13'
	elif p_column == 'stop-lost-near-splice': return '14'
	elif p_column in ['splice-5', 'splice-donor']: return '15'
	elif p_column in ['splice-3', 'splice-acceptor', '']: return '16'
	elif p_column in ['synonymous', 'coding-synonymous']: return '17'
	elif p_column in ['synonymous-near-splice', 'coding-synonymous-near-splice']: return '18'
	elif p_column in ['intron-near-splice','intron-variant-near-splice']: return '19'
	elif p_column in ['intron','intron-variant']: return '20'
	elif p_column == 'non-coding-exon-near-splice': return '21'
	elif p_column == 'non-coding-exon': return '22'
	elif p_column in ['5-prime-utr', 'utr-5']: return '23'
	elif p_column in ['3-prime-utr', 'utr-3']: return '24'
	elif p_column in ['upstream-gene', 'near-gene-5']: return '25'
	elif p_column in ['downstream-gene', 'near-gene-3']: return '26'
	elif p_column == 'intergenic': return '27'
	else:
		logger.error('Uknown SeattleSeq Sequence Ontology Term: {0}\n'.format(p_column))
		sys.exit(1)



###################################################
### Will reformat Ensembl VEP Sequence Ontology ###
###################################################
def reformat_ensembl_SO(p_column):

    ## Based on ENSEMBL VEP documentation and sequence ontology
    ## Can be found here: http://www.ensembl.org/info/genome/variation/predicted_data.html#consequences

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'transcript_ablation': return '1'
	elif p_column == 'splice_acceptor_variant': return '2'
	elif p_column == 'splice_donor_variant': return '3'
	elif p_column == 'stop_gained': return '4'
	elif p_column == 'frameshift_variant': return '5'
	elif p_column == 'stop_lost': return '6'
	#by racosta
	#fixed by mike, they replaced initiator_codon_variant with start_lost
	elif p_column == 'start_lost': return '7'
	elif p_column == 'initiator_codon_variant': return '7'
	elif p_column == 'transcript_amplification': return '8'
	elif p_column == 'inframe_insertion': return '9'
	elif p_column == 'inframe_deletion': return '10'
	elif p_column == 'missense_variant': return '11'
	elif p_column == 'splice_region_variant': return '12'
	elif p_column == 'incomplete_terminal_codon_variant': return '13'
	elif p_column == 'stop_retained_variant': return '14'
	elif p_column == 'protein_altering_variant': return '15'
	elif p_column == 'synonymous_variant': return '16'
	elif p_column == 'coding_sequence_variant': return '17'
	elif p_column == 'mature_mirna_variant': return '18'
	elif p_column == '5_prime_utr_variant': return '19'
	elif p_column == '3_prime_utr_variant': return '20'
	elif p_column == 'non_coding_transcript_exon_variant': return '21'
	elif p_column == 'intron_variant': return '22'
	elif p_column == 'nmd_transcript_variant': return '23'
	elif p_column == 'non_coding_transcript_variant': return '24'
	elif p_column == 'upstream_gene_variant': return '25'
	elif p_column == 'downstream_gene_variant': return '26'
	elif p_column == 'tfbs_ablation': return '27'
	elif p_column == 'tfbs_amplification': return '28'
	elif p_column == 'tf_binding_site_variant': return '29'
	elif p_column == 'regulatory_region_ablation': return '30'
	elif p_column == 'regulatory_region_amplification': return '31'
	elif p_column == 'regulatory_region_variant': return '32'
	elif p_column == 'feature_elongation': return '33'
	elif p_column == 'feature_truncation': return '34'
	elif p_column == 'intergenic_variant': return '35'
	else:
		logger.error('Uknown Ensembl Sequence Ontology Term: {0}\n'.format(p_column))
		sys.exit(1)



######################################################
### Convert our encoding back to Sequence ontology ###
######################################################
def convert_genesis_to_so(p_value):

	try:
		p_value = int(p_value)
	except ValueError:
		logger.error('Sequence ontology encoding must be an integer: {0}\n'.format(p_value))
		sys.exit(1)

	if p_value == 1: return 'SO:0001893'
	elif p_value == 2: return 'SO:0001574'
	elif p_value == 3: return 'SO:0001575'
	elif p_value == 4: return 'SO:0001587'
	elif p_value == 5: return 'SO:0001589'
	elif p_value == 6: return 'SO:0001578'
	elif p_value == 7: return 'SO:0002012'
	elif p_value == 8: return 'SO:0001889'
	elif p_value == 9: return 'SO:0001821'
	elif p_value == 10: return 'SO:0001822'
	elif p_value == 11: return 'SO:0001583'
	elif p_value == 12: return 'SO:0001630'
	elif p_value == 13: return 'SO:0001626'
	elif p_value == 14: return 'SO:0001567'
	elif p_value == 15: return 'SO:0001818'
	elif p_value == 16: return 'SO:0001819'
	elif p_value == 17: return 'SO:0001580'
	elif p_value == 18: return 'SO:0001620'
	elif p_value == 19: return 'SO:0001623'
	elif p_value == 20: return 'SO:0001624'
	elif p_value == 21: return 'SO:0001792'
	elif p_value == 22: return 'SO:0001627'
	elif p_value == 23: return 'SO:0001621'
	elif p_value == 24: return 'SO:0001619'
	elif p_value == 25: return 'SO:0001631'
	elif p_value == 26: return 'SO:0001632'
	elif p_value == 27: return 'SO:0001895'
	elif p_value == 28: return 'SO:0001892'
	elif p_value == 29: return 'SO:0001782'
	elif p_value == 30: return 'SO:0001894'
	elif p_value == 31: return 'SO:0001891'
	elif p_value == 32: return 'SO:0001566'
	elif p_value == 33: return 'SO:0001907'
	elif p_value == 34: return 'SO:0001906'
	elif p_value == 35: return 'SO:0001628'
	else:
		logger.error('Unrecognized sequence ontology encoding: {0}'.format(p_value))
		sys.exit(1)



##########################################################
### Will reformat ClinVar Clinical Significance column ###
##########################################################
def reformat_ClinVar_ClinicalSignificance(p_column):

	logger = logging.getLogger("GEDI Utilities")

	values = p_column.strip().split(';')
	new_values = []

	for value in values:
		if value == 'association': new_values.append('0')
		elif value == 'confers sensitivity': new_values.append('1')
		elif value == 'benign': new_values.append('2')
		elif value == 'likely benign': new_values.append('3')
		elif value == 'likely pathogenic': new_values.append('4')
		elif value == 'pathogenic': new_values.append('5')
		elif value == 'drug response': new_values.append('6')
		elif value == 'histocompatibility': new_values.append('7')
		elif value == 'not provided': new_values.append('8')
		elif value == 'other': new_values.append('9')
		elif value == 'protective': new_values.append('10')
		elif value == 'risk factor': new_values.append('11')
		elif value == 'uncertain significance': new_values.append('12')
		elif value == 'conflicting data from submitters': new_values.append('13')
		else:
			logger.error('Unknown ClinVar Clinical Significance: {0}'.format(value))
			sys.exit(1)

	return ';'.join(new_values)



################################################
### Will reformat ClinVar TestedInGTR column ###
################################################
def reformat_yes_no(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in ['y','yes']: return '0'
	elif p_column in ['n','no']: return '1'
	else:
		logger.error('Unknown Clinvar TestedInGTR: {0}'.format(p_column))
		sys.exit(1)



################################################
### Will reformat ClinVar TestedInGTR column ###
################################################
def reformat_cosmicID(p_column):

	## Remove any leading values for chromosome
	if p_column.startswith('cosm'): return p_column.replace('cosm', '')
	else: return p_column



################################################
### Will reformat ClinVar TestedInGTR column ###
################################################
def reformat_rsID(p_column):

	## Remove any leading values for chromosome
	if p_column.startswith('~rs'): return_value = p_column.replace('~rs', '')
	elif p_column.startswith('rs'): return_value = p_column.replace('rs', '')
	else: return_value = p_column

	try:
		int(return_value)
		return return_value
	except ValueError: return ''



###########################################
### Will reformat ClinVar Origin column ###
###########################################
def reformat_ClinVar_Origin(p_column):

	logger = logging.getLogger("GEDI Utilities")

	values = p_column.strip().split(';')
	new_values = []

	for value in values:
		if value == 'unknown': new_values.append('0')
		elif value == 'uniparental': new_values.append('1')
		elif value == 'tested-inconclusive': new_values.append('2')
		elif value == 'somatic': new_values.append('3')
		elif value == 'paternal': new_values.append('4')
		elif value == 'origin': new_values.append('5')
		elif value == 'not provided': new_values.append('6')
		elif value == 'maternal': new_values.append('7')
		elif value == 'inherited': new_values.append('8')
		elif value == 'germline': new_values.append('9')
		elif value == 'de novo': new_values.append('10')
		elif value == 'biparental': new_values.append('11')
		else:
			logger.error('Unknown ClinVar Clinical Origin: {0}'.format(value))
			sys.exit(1)

	return ';'.join(new_values)



##############################
### Will reformat Assembly ###
##############################
def reformat_Assembly(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'grch37': return '0'
	elif p_column == 'grch38': return '1'
	elif p_column == 'ncbi36': return '2'
	else:
		logger.error('Unknown Assembly: {0}'.format(p_column))
		sys.exit(1)



###########################
### Will reformat dbVAR ###
###########################
def reformat_dbVAR(p_column):

	## Remove any leading values for chromosome
	if p_column.startswith('nsv'): return p_column.replace('nsv', '')
	else: return p_column



############################
### Will reformat RCV ID ###
############################
def reformat_rcv(p_column):

	## Remove any leading values for chromosome
	if p_column.startswith('rcv'): return p_column.replace('rcv', '')
	else: return p_column



####################################
### Will reformat weird VEP MAFs ###
####################################
def reformat_maf(p_column, p_allele):

	logger = logging.getLogger("GEDI Utilities")
	expected_allele = p_allele.split('/')[1]
	allele_freqs = str(p_column).split(',')
	output = ''

	for allele_freq in allele_freqs:

		## Lovely VEP has different formats....
		if ':' in str(allele_freq):

			try:
				(allele, maf) = allele_freq.strip().split(':')
				if allele == expected_allele: output = str(maf)

			except ValueError:
				logger.error('Not standard format of allele:maf! Input observed: {0}'.format(allele_freq) )
				sys.exit(1)

		else: output = str(allele_freq)

	return output


###########################################################
### Will split a column of keys into two for the mapper ###
###########################################################
def split_keys(p_column, p_column_separator):

	values = p_column.strip().split(p_column_separator)
	return '_'.join(values)



#############################################################
### Will split a column of values into two for the mapper ###
#############################################################
def split_values(p_column, p_column_separator, p_elements):

	values = []
	if p_column != '': values = p_column.strip().split(p_column_separator)
	else:
		for x in range(0, int(p_elements)): values.append('')

	return '<>'.join(values)



##################################################
### Will reformat ClinVar Review Status column ###
##################################################
def reformat_ClinVar_ReviewStatus(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'reviewed by professional society': return '0'
	elif p_column == 'reviewed by expert panel': return '1'
	elif p_column == 'not classified by submitter': return '2'
	elif p_column == 'classified by single submitter': return '3'
	elif p_column == 'classified by multiple submitters': return '4'
	else:
		logger.error('Unknown ClinVar Review Status: {0}'.format(p_column))
		sys.exit(1)



##################################################
### Will reformat ClinVar Guidelines column ###
##################################################
def reformat_ClinVar_Guidelines(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'acmg': return '0'
	else:
		logger.error('Unknown ClinVar Guidelines: {0}'.format(p_column))
		sys.exit(1)



#################################################
### Will reformat genomic coordinates chr:pos ###
#################################################
def reformat_genomic_coordinate(p_column):

	if p_column not in ['-1', -1]:

		values = p_column.strip().split(':')
		chromosome = reformat_Chromosome(values[0])
		return '{0}<>{1}'.format(chromosome, values[1])

	else: return '<>'


######################################
### Will reformat SIFT predictions ###
######################################
def DorT_pred(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in ['d', 'damaging']: return '0'
	elif p_column in ['t', 'tolerated']: return '1'
	elif p_column in ['.']: return ''
	else:
		logger.error('Unknown prediction: {0}'.format(p_column))
		sys.exit(1)



#########################################
### Will reformat PROVEAN predictions ###
#########################################
def DorN_pred(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in ['d', 'damaging']: return '0'
	elif p_column in ['n', 'neutral']: return '1'
	elif p_column in ['.']: return ''
	else:
		logger.error('Unknown prediction: {0}'.format(p_column))
		sys.exit(1)



###########################################
### Will reformat PolyPhen2 predictions ###
###########################################
def Polyphen2_pred(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in ['d', 'probably damaging', 'probably-damaging']: return '0'
	elif p_column in ['p', 'possibly damaging', 'possibly-damaging']: return '1'
	elif p_column in ['b', 'benign']: return '2'
	elif p_column in ['.']: return ''
	else:
		logger.error('Unknown PolyPhen2 prediction: {0}'.format(p_column))
		sys.exit(1)



#####################################
### Will reformat LRT predictions ###
#####################################
def convert_LRT_pred(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in ['d', 'deleterious']: return '0'
	elif p_column in ['n', 'neutral']: return '1'
	elif p_column in ['u', 'unknown']: return '2'
	elif p_column in ['.']: return ''
	else:
		logger.error('Unknown LRT prediction: {0}'.format(p_column))
		sys.exit(1)



################################################
### Will reformat MutationTaster predictions ###
################################################
def convert_MutationTaster_pred(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in ['a', 'disease_causing_automatic']: return '0'
	elif p_column in ['d', 'disease_causing']: return '1'
	elif p_column in ['n', 'polymorphism']: return '2'
	elif p_column in ['p', 'polymorphism_automatic']: return '3'
	elif p_column in ['.']: return ''
	else:
		logger.error('Unknown MutationTaster prediction: {0}'.format(p_column))
		sys.exit(1)



################################################
### Will reformat MutationTaster predictions ###
################################################
def convert_MutationAssessor_pred(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column in ['h', 'high']: return '0'
	elif p_column in ['m', 'medium']: return '1'
	elif p_column in ['l', 'low']: return '2'
	elif p_column in ['n', 'neutral']: return '3'
	elif p_column in ['.']: return ''
	else:
		logger.error('Unknown MutationAssessor prediction: {0}'.format(p_column))
		sys.exit(1)



##############################################
### Will reformat the EVS PolyPhen2 column ###
##############################################
def reformat_EVS_PolyPhen2(p_column, p_column_separator, p_elements):

	values = []
	if p_column != '':
		values = p_column.strip().split(p_column_separator)
		pred = Polyphen2_pred(values[0])
		return '<>'.join([pred, values[1]])
	else:
		for x in range(0, int(p_elements)): values.append('')
		return '<>'.join(values)


#######################################
### Will reformat wgRNA type column ###
#######################################
def reformat_wgRNA_type(p_column):

	logger = logging.getLogger("GEDI Utilities")

	if p_column == 'cdbox': return '0'
	elif p_column == 'hacabox': return '1'
	elif p_column == 'mirna': return '2'
	elif p_column == 'scarna': return '3'
	else:
		logger.error('Unknown wgRNA type: {0}'.format(p_column))
		sys.exit(1)



##############################################################
### Will find the transcript with the worst SO consequence ###
##############################################################
def find_most_severe_SO(p_idx, p_value_column, p_table_columns, p_dict, p_key):

	logger = logging.getLogger("GEDI Utilities")

	for value_element in p_value_column.strip().split('<>'):

		## Find the column that has to do with Sequence Ontology
		if p_table_columns[p_idx]['field_name'] == 'GeneAccession_SequenceOntology':
			try:
				if int(value_element) < int(p_dict['extra_dict']['worst_transcript_score']):
					p_dict['extra_dict']['worst_transcript_score'] = int(value_element)
					p_dict['extra_dict']['worst_transcript'] = str(p_value_column)
			except:
				logger.error('Wrong Value for Sequence ontology: {0}. Current key: {1} and Current Value: {2} and Current Dict: {3}'.format(value_element, p_key, p_value_column, p_dict))
				exit(1)

		p_idx=p_idx+1

	return {'new_dict': p_dict}



###################################################
### Will parse out sample data from VCF format  ###
###################################################
def extract_sample_data_from_vcf(p_sample_data_key, p_sample_data, p_keys_to_extract):

	logger = logging.getLogger("GEDI Utilities")

	column_values = {}
	if p_sample_data not in ['.', './.']:

		## Split data and data keys
		data_keys = p_sample_data_key.strip().split(':')
		sample_data = p_sample_data.strip().split(':')

		## Try to extract the values we need
		for column in p_keys_to_extract:

			try:
				column_idx = data_keys.index("{0}".format(column))

				try:
					if sample_data[column_idx] == '.': column_values[column] = ''
					else: column_values[column] = sample_data[column_idx]
				except IndexError: column_values[column] = ''

				## We need to transform the GT column
				if column == 'GT':

					if '/' in sample_data[column_idx]: alleles = sample_data[column_idx].strip().split('/')
					elif '|' in sample_data[column_idx]: alleles = sample_data[column_idx].strip().split('|')
					elif sample_data[column_idx] in ['.', './.']: alleles = sample_data[column_idx]
					else:
						logger.error('Extract Error: Unrecogized GT: {0}'.format(sample_data[column_idx]))
						sys.exit(1)

					if alleles in ['.','./.']: column_values['GTR'] = -1
					else:
						if int(alleles[0]) == int(alleles[1]):
							if int(alleles[0]) == 0: column_values['GTR'] = 0
							else: column_values['GTR'] = 2
						else: column_values['GTR'] = 1

			## If that key does not exist, we need to keep it as null
			except ValueError: column_values[column] = ''

	return column_values



###################################################
### Will parse out sample data from VCF format  ###
###################################################
def convert_genotype(p_genotype, p_ref, p_alt):

	logger = logging.getLogger("GEDI Utilities")

	if int(p_genotype) == 0: return "{0}/{0}".format(p_ref)
	elif int(p_genotype) == 1: return "{0}/{1}".format(p_ref,p_alt)
	elif int(p_genotype) == 2: return "{0}/{0}".format(p_alt)
	else:
		logger.error('Uknown genotype: {0}'.format(p_genotype))
		sys.exit(1)



### Build Impala commands
def build_impala_sql(p_node, p_database, p_sql):

	return ' impala-shell -i {0} -d {1} -B -q "{2}"; '.format(p_node, p_database, p_sql)

