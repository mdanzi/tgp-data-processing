
import sys, getopt, os

def main ( argv ):
	inFile = ''
	rohFile = 'False'
	outFile = ''
	try:
		opts, args = getopt.getopt(argv,"h",["input=","output=","roh=","help"])
	except getopt.GetoptError:
		print('mergeOnROH.py --input=<parsedVcfFile> --roh=<variantsInROHs> --output=<OutFile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--input'):
			inFile=arg
		elif opt in ('--roh'):
			rohFile=arg
		elif opt in ('--output'):
			outFile=arg
		elif opt in ('-h','--help'):
			print('mergeOnROH.py --input=<parsedVcfFile> --roh=<variantsInROHs> --output=<OutFile>')
			sys.exit()
		else:
			print('mergeOnROH.py --input=<parsedVcfFile> --roh=<variantsInROHs> --output=<OutFile>')
			sys.exit()
	import pandas
	input=pandas.read_csv(inFile,sep='\t',low_memory=False,header=None,names=['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','cohortImportRunID'],dtype={'filter':pandas.Int64Dtype()})
	if rohFile=='False':
		input['rohLen']=''
		input=input[['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','rohLen','cohortImportRunID']]
		input.to_csv(outFile,sep='\t',index=False,header=False,na_rep="\\N")
	else:
		roh=pandas.read_csv(rohFile,sep='\t',low_memory=False,header=None,names=['chr','pos','rohLen'],dtype={'rohLen':pandas.Int64Dtype()})
		merged=input.merge(roh,how='left',on=['chr','pos'])
		merged=merged[['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','rohLen','cohortImportRunID']]
		merged.to_csv(outFile,sep='\t',index=False,header=False,na_rep="\\N")
	return

if __name__ == "__main__":
	main(sys.argv[1:])
