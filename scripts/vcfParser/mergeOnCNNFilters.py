
import sys, getopt, os

def main ( argv ):
	inFile = ''
	cnnFile = 'False'
	outFile = ''
	try:
		opts, args = getopt.getopt(argv,"h",["input=","output=","cnn=","help"])
	except getopt.GetoptError:
		print('mergeOnCNNFilters.py --input=<parsedVcfFile> --cnn=<parsedCnnVcfFile> --output=<OutFile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--input'):
			inFile=arg
		elif opt in ('--cnn'):
			cnnFile=arg
		elif opt in ('--output'):
			outFile=arg
		elif opt in ('-h','--help'):
			print('mergeOnCNNFilters.py --input=<parsedVcfFile> --cnn=<parsedCnnVcfFile> --output=<OutFile>')
			sys.exit()
		else:
			print('mergeOnCNNFilters.py --input=<parsedVcfFile> --cnn=<parsedCnnVcfFile> --output=<OutFile>')
			sys.exit()
	import pandas
	input=pandas.read_csv(inFile,sep='\t',low_memory=False,header=None,names=['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','cohortImportRunID'],dtype={'filter':pandas.Int64Dtype()})
	if cnnFile=='False':
		input['filter']=''
		input.to_csv(outFile,sep='\t',index=False,header=False)
	else:
		input=input.drop(columns='filter')
		cnn=pandas.read_csv(cnnFile,sep='\t',low_memory=False,header=None,names=['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','cohortImportRunID'],dtype={'filter':pandas.Int64Dtype()})
		cnn=cnn[['chr','pos','ref','alt','filter']]
		merged=input.merge(cnn,how='left',on=['chr','pos','ref','alt'])
		merged=merged[['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','cohortImportRunID']]
		merged.to_csv(outFile,sep='\t',index=False,header=False)
	return

if __name__ == "__main__":
	main(sys.argv[1:])
