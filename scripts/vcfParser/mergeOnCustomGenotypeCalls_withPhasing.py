
import sys, getopt, os

def main ( argv ):
	inFile = ''
	customSitesFile = 'False'
	outFile = ''
	try:
		opts, args = getopt.getopt(argv,"h",["input=","output=","customSites=","help"])
	except getopt.GetoptError:
		print('mergeOnCustomGenotypeCalls.py --input=<parsedVcfFile> --customSites=<parsedCustomGenotypesVcfFile> --output=<OutFile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--input'):
			inFile=arg
		elif opt in ('--customSites'):
			customSitesFile=arg
		elif opt in ('--output'):
			outFile=arg
		elif opt in ('-h','--help'):
			print('mergeOnCustomGenotypeCalls.py --input=<parsedVcfFile> --customSites=<parsedCustomGenotypesVcfFile> --output=<OutFile>')
			sys.exit()
		else:
			print('mergeOnCustomGenotypeCalls.py --input=<parsedVcfFile> --customSites=<parsedCustomGenotypesVcfFile> --output=<OutFile>')
			sys.exit()
	import pandas
	import numpy as np
	inputData=pandas.read_csv(inFile,sep='\t',low_memory=False,header=None,names=['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','phaseGroup','cohortImportRunID'],dtype={'filter':pandas.Int64Dtype(),'phaseGroup':pandas.Int64Dtype()})
	if customSitesFile=='False':
		inputData.to_csv(outFile,sep='\t',index=False,header=False)
	else:
		customSites=pandas.read_csv(customSitesFile,sep='\t',low_memory=False,header=None,names=['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','cohortImportRunID'],dtype={'filter':pandas.Int64Dtype(),'phaseGroup':pandas.Int64Dtype()})
		# customSites do not have phasing info, so add in a column for that and re-order columns
		customSites['phaseGroup']=np.nan
		customSites=customSites.loc[:,['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','phaseGroup','cohortImportRunID']]
		customSites2=customSites.merge(inputData.loc[:,['chr','pos','ref','alt']],how='left',on=['chr','pos','ref','alt'],indicator=True)
		customSites2=customSites2.loc[customSites2['_merge']=='left_only',:].drop(columns='_merge').reset_index(drop=True)
		combined=pandas.concat([inputData,customSites2],sort=False)
		combined=combined.sort_values(by=['chr','pos'],ascending=[True,True]).reset_index(drop=True).loc[:,['sampleID','chr','pos','ref','alt','genotype','mutationClassType','depth','quality','genotypeQuality','refReads','altReads','filter','phaseGroup','cohortImportRunID']]
		combined.to_csv(outFile,sep='\t',index=False,header=False)
	return

if __name__ == "__main__":
	main(sys.argv[1:])
