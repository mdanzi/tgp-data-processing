import sys, getopt, os

def simplest_motif(motif):
    if motif=='':
        return motif
    i = (motif+motif).find(motif, 1, -1)
    if i != -1:
        motif = motif[:i]
    motifs = []
    b = len(motif)
    for i in range(b):
        c = motif[i:]+motif[:i]
        motifs.append(c)
    motifs = [x.upper() for x in motifs]
    return(min(motifs))

def get_lps_len_motif(row, col1, col2):
    import regex as re
    approvedMotifs={}
    approvedMotifCounts={}
    lpsLength=0
    lpsMotif=''
    spanCovered=0
    done=0
    topLPSLength=0
    topLPSMotif=''
    allele=row[col1]
    refMotifs=row[col2]
    for k in refMotifs:
        # if any one of the reference motifs has an LPS greater than half the repeat span, that is the LPS
        matches=re.findall('(?:' + k + ')+',allele)
        if len(matches)>0 and len(max(matches))>0.5*len(allele):
            lpsLength = len(max(matches))
            lpsMotif = simplest_motif(k)
            done=1
            break
        else:
            # if not, track the spans covered by this motif
            spanCovered+=sum([len(x) for x in matches])
            if len(matches)>0 and len(max(matches))>topLPSLength:
                topLPSLength=len(max(matches))
                topLPSMotif=k
    if done==0:
        # is there enough space left in the repeat for a sequence to be the LPS that isn't already accounted for?
        if (len(allele)-spanCovered)>topLPSLength:
            # do a k-mer search for novel motifs
            maxKmerLen=20
            if len(allele)<3*maxKmerLen:
                maxKmerLen=int(len(allele)/3)
            for k in range(1,maxKmerLen+1):
                if done==1:
                    break
                words=re.findall('.'*k,allele)
                words=' '.join(words)
                matches=set(re.findall(r"\b(\w+)\s+\1\b\s\1\b",words)) # only keep k-mers with at least 3 consecutive matches
                for motif in matches:
                    matches=re.findall('(?:' + motif + ')+',allele)
                    spanCovered+=sum([len(x) for x in matches])
                    if len(max(matches))>0.5*len(allele):
                        topLPSLength=len(max(matches))
                        topLPSMotif=motif
                        done=1
                        break
                    elif len(max(matches))>topLPSLength:
                        topLPSLength=len(max(matches))
                        topLPSMotif=motif
        # pick the best LPS
        lpsLength = topLPSLength
        lpsMotif = simplest_motif(topLPSMotif)

    return lpsLength, lpsMotif

def getRefMotifs(IDs):
    namedTRs={'VWA1': 'GGCGCGGAGC',
    'DAB1': 'AAAAT',
    'ABCD3': 'GCC',
    'NOTCH2NLA': 'GCC',
    'FRA10AC1_CCA': 'CCA',
    'FRA10AC1': 'CCG',
    'C11ORF80': 'GGC',
    'CBL': 'CGG',
    'ATN1': 'CAG',
    'DIP2B': 'GGC',
    'ATXN2': 'GCT',
    'RILPL1': 'GGC',
    'ATXN8OS_CTA': 'CTA',
    'ATXN8OS': 'CTG',
    'ZIC2': 'GCN',
    'PABPN1': 'GCG',
    'ATXN3': 'CTG',
    'NIPA1': 'GCG',
    'XYLT1': 'GCC',
    'TNRC6A': 'TTTTA',
    'BEAN1': 'TAAAA',
    'THAP11': 'CAG',
    'ZFHX3': 'GCC',
    'JPH3': 'CTG',
    'TCF4': 'CAG',
    'CACNA1A': 'CTG',
    'GIPC1': 'CCG',
    'COMP': 'GTC',
    'DMPK': 'CAG',
    'STARD7': 'AAAAT',
    'AFF3': 'GCC',
    'HOXD13': 'GCG',
    'GLS': 'GCA',
    'NOP56': 'GGCCTG',
    'NOP56_CGCCTG': 'CGCCTG',
    'PRNP_CCTCAGGGCGGTGGTGGCTGGGGGCAG': 'CCTCAGGGCGGTGGTGGCTGGGGGCAG',
    'PRNP': 'CCTCATGGTGGTGGCTGGGGGCAG',
    'CSTB': 'CGCGGGGCGGGG',
    'TBX1': 'GCN',
    'ATXN10': 'ATTCT',
    'ATXN7': 'GCA',
    'ATXN7_GCC': 'GCC',
    'FOXL2': 'NGC',
    'YEATS2': 'TTTTA',
    'RFC1': 'AAAAG',
    'PHOX2B': 'GCN',
    'RAPGEF2': 'TTTTA',
    'MARCHF6': 'ATTTT',
    'PPP2R2B': 'GCT',
    'ATXN1': 'TGC',
    'RUNX2': 'GCN',
    'TBP': 'GCA',
    'HOXA13_3': 'NGC',
    'HOXA13_2': 'NGC',
    'HOXA13_1': 'NGC',
    'SAMD12': 'AAAAT',
    'C9ORF72': 'GGCCCC',
    'FXN_A': 'A',
    'FXN': 'GAA',
    'PRDM12': 'GCC',
    'BCLAF3': 'CCG',
    'ARX_2': 'NGC',
    'ARX_1': 'NGC',
    'DMD': 'TTC',
    'AR': 'GCA',
    'ZIC3': 'GCC',
    'SOX3': 'NGC',
    'FMR1': 'CGG',
    'AFF2': 'GCC'}
    IDsList=IDs.split(',')
    motifs=[]
    for ID in IDsList:
        if ID in namedTRs.keys():
            motifs.append(namedTRs[ID])
        else:
            motifs.append(ID.split('-')[-1])
    return list(set(motifs))

def updateLPSPhenocounts(row, phenoCode, phenoName):
    import pandas
    import numpy as np
    TRID=row.TRID
    allele1_lps_motif=row.allele1_lps_motif
    allele2_lps_motif=row.allele2_lps_motif
    allele1_lps_len=-1
    allele2_lps_len=-1
    if not np.isnan(row.allele1_lps_len):
        allele1_lps_len=int(row.allele1_lps_len)
    if not np.isnan(row.allele2_lps_len):
        allele2_lps_len=int(row.allele2_lps_len)
    allele1_lps_percentile=0
    allele2_lps_percentile=0

    if not isinstance(row.hprcLPSQuantilesData,str):
        popAF=pandas.DataFrame(columns=['lps_motif','n_motif','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev'])
    else:
        popAFRows=row.hprcLPSQuantilesData.split('<>')
        popAF=pandas.DataFrame([x.split('@') for x in popAFRows], columns=['lps_motif','n_motif','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev'])
        popAF[['n_motif','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev']]=popAF.loc[:,['n_motif','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev']].astype(float)

    # calculate allele1_lps_percentile
    if allele1_lps_motif in popAF.lps_motif.values:
        idxs=np.where(popAF.loc[popAF['lps_motif']==allele1_lps_motif,:].iloc[0,2:26].astype(float).values<allele1_lps_len)[0]
        if len(idxs)>0:
            idx=max(idxs)
            allele1_lps_percentile=float(popAF.columns.values[2+idx])
    else:
        allele1_lps_percentile=100
    # calculate allele2_lps_percentile
    if allele2_lps_motif in popAF.lps_motif.values:
        idxs=np.where(popAF.loc[popAF['lps_motif']==allele2_lps_motif,:].iloc[0,2:26].astype(float).values<allele2_lps_len)[0]
        if len(idxs)>0:
            idx=max(idxs)
            allele2_lps_percentile=float(popAF.columns.values[2+idx])
    else:
        allele2_lps_percentile=100

    return allele1_lps_percentile, allele2_lps_percentile

def updateLenPhenocounts(row, phenoCode, phenoName):
    import pandas
    import numpy as np
    if not isinstance(row.hprcLenQuantilesData,str):
        popAF=pandas.DataFrame(columns=['n','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev'])
    else:
        popAF=pandas.DataFrame([row.hprcLenQuantilesData.split('@')], columns=['n','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev'])
        popAF[['n','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev']]=popAF.loc[:,['n','0','1','5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100','MAD','Mean','Stdev']].astype(float)
    TRID=row.TRID
    allele1_len=-1
    allele2_len=-1
    allele1_len_percentile=0
    allele2_len_percentile=0
    if not np.isnan(row.allele1_len):
        allele1_len=int(row.allele1_len)
    if not np.isnan(row.allele2_len):
        allele2_len=int(row.allele2_len)

    # calculate allele1_len_percentile and allele2_len_percentile
    if len(popAF)>0:
        idxs=np.where(popAF.iloc[0,1:25].astype(float).values<allele1_len)[0]
        if len(idxs)>0:
            idx=max(idxs)
            allele1_len_percentile=float(popAF.columns.values[1+idx])
        # calculate allele2_len_percentile
        idxs=np.where(popAF.iloc[0,1:25].astype(float).values<allele2_len)[0]
        if len(idxs)>0:
            idx=max(idxs)
            allele2_len_percentile=float(popAF.columns.values[1+idx])
    else:
        allele1_len_percentile=100
        allele2_len_percentile=100
    
    return allele1_len_percentile, allele2_len_percentile

def main ( argv ):
    sampleFile = ''
    phenoCode = ''
    phenoName = ''
    try:
        opts, args = getopt.getopt(argv,"h",["sample=","phenoCode=","phenoName=","help"])
    except getopt.GetoptError:
        print('parseLRTRs.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('--sample'):
            sampleFile=arg
        elif opt in ('--phenoCode'):
            phenoCode=arg
        elif opt in ('--phenoName'):
            phenoName=arg
        elif opt in ('-h','--help'):
            print('parseLRTRs.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
            sys.exit()
        else:
            print('parseLRTRs.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
            sys.exit()
    import numpy as np
    import pandas
    TRVDataFile = 'lr_TRVData.txt'
    GenoDataFile = 'lr_tr_GenoData.txt'
    maxSize=50000000
    # load the data
    GenoDataColNames=['TRID','SampleID','allele1','allele2','phased','reads_allele1','reads_allele2','allele1_len','allele2_len','allele1_lps_motif','allele1_lps_len','allele2_lps_motif','allele2_lps_len',
    'allele1_len_percentile','allele2_len_percentile','allele1_lps_percentile','allele2_lps_percentile']
    GenoData=pandas.read_csv(GenoDataFile,sep='\t',header=None,low_memory=False,names=GenoDataColNames,na_values="\\N",dtype={'SVID':str,'SampleID':'Int64','Genotype':'Int64','phaseGroup':'Int64','ref_reads':'Int64','alt_reads':'Int64','isImprecise':'Int64','Filter':str})
    TRVDataColNames=['chr','start','end','TRID_text','TRID','sampleCount', 'hprcLPSQuantilesData','hprcLenQuantilesData',
    'genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded',
    'isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped','gwasGeneData','omimGeneData','omim_inheritance','gnomadConstraintGeneData','gnomad_constraint_max_pli','gnomad_constraint_max_misz','lps_phenocounts','len_phenocounts']
    sampleInput=pandas.read_csv(sampleFile,sep='\t',header=None,low_memory=False,names=['TRID_text','alleles','reads','phased','SampleID'])
    TRData=pandas.read_csv(TRVDataFile,sep='\t',header=None,low_memory=False,index_col=False,names=TRVDataColNames,na_values="\\N",dtype={'omim_inheritance':'Int64'})

    # get longest pure segment for each allele
    sampleInput[['allele1','allele2']]=sampleInput.loc[:,'alleles'].str.split(',',expand=True)
    sampleInput[['reads_allele1','reads_allele2']]=sampleInput.loc[:,'reads'].str.split(',',expand=True)
    sampleInput['motifs']=sampleInput.loc[:,'TRID_text'].apply(getRefMotifs)
    sampleInput['allele1_lps_motif']=''
    sampleInput['allele1_lps_len']=np.nan
    sampleInput['allele2_lps_motif']=''
    sampleInput['allele2_lps_len']=np.nan
    sampleInput.loc[~sampleInput['allele1'].isna(),'allele1_lps_len'], sampleInput.loc[~sampleInput['allele1'].isna(),'allele1_lps_motif']=zip(*sampleInput.loc[~sampleInput['allele1'].isna(),:].apply(get_lps_len_motif, args=('allele1','motifs'),axis=1))
    sampleInput.loc[~sampleInput['allele2'].isna(),'allele2_lps_len'], sampleInput.loc[~sampleInput['allele2'].isna(),'allele2_lps_motif']=zip(*sampleInput.loc[~sampleInput['allele2'].isna(),:].apply(get_lps_len_motif, args=('allele2','motifs'),axis=1))
    sampleInput[['allele1_len','allele2_len']]=np.nan
    sampleInput.loc[~sampleInput['allele1'].isna(),'allele1_len']=sampleInput.loc[~sampleInput['allele1'].isna(),'allele1'].str.len()
    sampleInput.loc[~sampleInput['allele2'].isna(),'allele2_len']=sampleInput.loc[~sampleInput['allele2'].isna(),'allele2'].str.len()

    sampleInput2=sampleInput.merge(TRData,how='outer',on='TRID_text',indicator=True)
    sampleInput2=sampleInput2.loc[sampleInput2['_merge']!='left_only',:].reset_index(drop=True)

    ## set percentiles columns 
    # load popAF data
    popAFLen=pandas.read_csv('HPRC_100_alleleLengthQuantiles.txt.gz',sep='\t',low_memory=False)
    popAFLPS=pandas.read_csv('HPRC_100_LongestPureSegmentQuantiles.txt.gz',sep='\t',low_memory=False)
    # use only simple motif for LPS
    popAFLPS['simpleLPSMotif']=popAFLPS.loc[:,'longestPureSegmentMotif'].apply(simplest_motif)
    popAFLPS=popAFLPS.sort_values(by='N_motif',ascending=False).drop_duplicates(subset=['TRID','simpleLPSMotif'],keep='first')
    # set len percentile
    lenData=sampleInput2.loc[:,['TRID_text','allele1_len','allele2_len']].rename(columns={'TRID_text':'TRID'}).merge(popAFLen,how='left',on='TRID')
    lenData=lenData.fillna(0)
    sampleInput2[['allele1_len_percentile','allele2_len_percentile','allele1_lps_percentile','allele2_lps_percentile']]=0
    for i in ['5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100']:
        sampleInput2.loc[lenData['allele1_len']>lenData[i + 'thPercentile'],'allele1_len_percentile']=float(i)
        sampleInput2.loc[lenData['allele2_len']>lenData[i + 'thPercentile'],'allele2_len_percentile']=float(i)
    # set lps percentile
    lpsData1=sampleInput2.loc[:,['TRID_text','allele1_lps_len','allele1_lps_motif']].rename(columns={'TRID_text':'TRID','allele1_lps_motif':'simpleLPSMotif'}).merge(popAFLPS,how='left',on=['TRID','simpleLPSMotif'])
    lpsData1=lpsData1.fillna(0)
    lpsData2=sampleInput2.loc[:,['TRID_text','allele2_lps_len','allele2_lps_motif']].rename(columns={'TRID_text':'TRID','allele2_lps_motif':'simpleLPSMotif'}).merge(popAFLPS,how='left',on=['TRID','simpleLPSMotif'])
    lpsData2=lpsData2.fillna(0)
    for i in ['5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','99','99.9','100']:
        sampleInput2.loc[lpsData1['allele1_lps_len']>lpsData1[i + 'thPercentile'],'allele1_lps_percentile']=float(i)
        sampleInput2.loc[lpsData2['allele2_lps_len']>lpsData1[i + 'thPercentile'],'allele2_lps_percentile']=float(i)


    # update sampleCount values in TRData for sites observed before
    sampleInput2.loc[((sampleInput2['allele1_len']=='100') | (sampleInput2['allele2_len']=='100')),'sampleCount']=sampleInput2.loc[((sampleInput2['allele1_len']=='100') | (sampleInput2['allele2_len']=='100')),'sampleCount']+1

    ## increment values in phenocounts columns -- work in progress
    sampleInput2[['is_het','is_hom']]=0
    sampleInput2['len_min']=sampleInput2.loc[:,['allele1_len','allele2_len']].values.astype(float).min(axis=1)
    sampleInput2['len_max']=sampleInput2.loc[:,['allele1_len','allele2_len']].values.astype(float).max(axis=1)
    sampleInput2['len_min']=sampleInput2.loc[:,'len_min'].fillna(0).astype(float)
    sampleInput2['len_max']=sampleInput2.loc[:,'len_max'].fillna(0).astype(float)

    # len_phenocounts part 1 (update existing sites with the phenotype) -- to do 
    #lenPhenoCounts=sampleInput2.loc[(~(sampleInput2['len_phenocounts'].isna()) and ((sampleInput2['is_het']==1) | (sampleInput2['is_hom']==1))),:]
    #inLenPhenoCounts=lenPhenoCounts.loc[lenPhenoCounts['phenocounts'].str.contains(phenoCode),:]
    #inPhenoCounts['phenoCountsRows']=inPhenoCounts.loc[:,'phenocounts'].str.split('<>').tolist()

    # len_phenocounts part 2 (update existing sites without the phenotype) -- to do
   	#notInLenPhenoCounts=lenPhenoCounts.loc[~(lenPhenoCounts.index.isin(inLenPhenoCounts.index.values)),:]
    # len_phenocounts part 3 (new sites)
    sampleInput2['new_phenocounts']=''
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']>=95)),'new_phenocounts']="95@" + phenoCode + "@" + phenoName + "@0@1"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']<95) & (sampleInput2['len_max']>=95)),'new_phenocounts']="95@" + phenoCode + "@" + phenoName + "@1@0"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']>=99)),'new_phenocounts']=sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']>=99)),'new_phenocounts'] + "<>" + "99@" + phenoCode + "@" + phenoName + "@0@1"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']<99) & (sampleInput2['len_max']>=99)),'new_phenocounts']=sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']<99) & (sampleInput2['len_max']>=99)),'new_phenocounts'] + "<>" + "99@" + phenoCode + "@" + phenoName + "@1@0"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']>=99.9)),'new_phenocounts']=sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']>=99)),'new_phenocounts'] + "<>" + "99.9@" + phenoCode + "@" + phenoName + "@0@1"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']<99.9) & (sampleInput2['len_max']>=99.9)),'new_phenocounts']=sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']<99) & (sampleInput2['len_max']>=99.9)),'new_phenocounts'] + "<>" + "99.9@" + phenoCode + "@" + phenoName + "@1@0"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']>=100)),'new_phenocounts']=sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']>=100)),'new_phenocounts'] + "<>" + "100@" + phenoCode + "@" + phenoName + "@0@1"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']<100) & (sampleInput2['len_max']>=100)),'new_phenocounts']=sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_min']<100) & (sampleInput2['len_max']>=100)),'new_phenocounts'] + "<>" + "100@" + phenoCode + "@" + phenoName + "@1@0"
    sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_max']>=95)),'len_phenocounts']=sampleInput2.loc[((sampleInput2['len_phenocounts'].isna()) & (sampleInput2['len_max']>=95)),'new_phenocounts']


    # then, repeat for lps phenocounts

    # pull out TRData
    TRData2=sampleInput2.loc[:,TRVDataColNames].sort_values(by=['TRID'],ascending=True)

    ############ deal with GenoData
    sampleInput2=sampleInput2.loc[sampleInput2['_merge']!='both',:].reset_index(drop=True)
    stagingGenoData=sampleInput2.loc[:,GenoDataColNames]
    GenoData=pandas.concat([GenoData,stagingGenoData],axis=0,ignore_index=True).sort_values(by=['SampleID','TRID'],ascending=True)

    TRData2.to_csv(TRVDataFile,sep='\t',header=False,index=False,na_rep="\\N")
    GenoData.to_csv(GenoDataFile,sep='\t',header=False,index=False,na_rep="\\N")
    return

if __name__ == "__main__":
    main(sys.argv[1:])
