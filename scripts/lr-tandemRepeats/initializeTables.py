###### this script is a record of how the lr_TRVData.txt and lr_tr_GenoData.txt files were initially created. 
# it is meant to be run by initializeTables.sh, which collects and formats the external files


import numpy
import pandas
import pybedtools
TRVDataFile = 'lr_TRVData.txt'
GenoDataFile = 'lr_tr_GenoData.txt'
maxSize=50000000

# load genomic annotations
geneLocations=pybedtools.BedTool('GRCh37.75.genes.bed')
genePaddedLocations=pybedtools.BedTool('GRCh37.75.genes2kbBuffer.bed')
exonLocations=pybedtools.BedTool('GRCh37.75.exons.gtf')
exonPaddedLocations=pybedtools.BedTool('GRCh37.75.exons2kbBuffer.gtf')
cdsLocations=pybedtools.BedTool('GRCh37.75.CDSs.gtf')
tssLocations=pybedtools.BedTool('GRCh37.75.TSSs2kbBuffer.gtf')
canonicalTranscriptLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts.bed')
canonicalTranscriptPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts2kbBuffer.bed')
canonicalExonLocations=pybedtools.BedTool('GRCh37.75.canonical.exons.gtf')
canonicalExonPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.exons2kbBuffer.gtf')
canonicalCDSLocations=pybedtools.BedTool('GRCh37.75.canonical.CDSs.gtf')
canonicalTSSLocations=pybedtools.BedTool('GRCh37.75.canonical.TSSs2kbBuffer.gtf')
enhancerLocations=pybedtools.BedTool('EncodeEnhancerSites.bed')
promoterLocations=pybedtools.BedTool('EncodePromoterSites.bed')
ctcfOnlyLocations=pybedtools.BedTool('EncodeCTCFOnlySites.bed')
gwasGeneData=pandas.read_csv('gwas_gene.txt',sep='\t',low_memory=False,quoting=3)
gwasGenesUnique=gwasGeneData.drop_duplicates(subset='geneName',keep='first')
gwasGenesUnique.loc[:,'gene_data']=gwasGenesUnique.apply(lambda x: '||'.join(gwasGeneData.loc[gwasGeneData['geneName']==x.geneName,'gene_data']) , axis=1).values
gwasGeneData=gwasGenesUnique
omimGeneData=pandas.read_csv('omim_data.txt',sep='\t',low_memory=False,quoting=3)
omimGeneData=omimGeneData.astype({'inheritance':str})
gnomadConstraintGeneData=pandas.read_csv('gnomad_constraint_data.txt',sep='\t',low_memory=False,quoting=3,na_values="\\N")
gnomadConstraintGeneData=gnomadConstraintGeneData.astype({'misZ':str,'pLI':str})

GenoDataColNames=['TRID','SampleID','allele1','allele2','phased','reads_allele1','reads_allele2','allele1_len','allele2_len','allele1_lps_motif','allele1_lps_len','allele2_lps_motif','allele2_lps_len',
'allele1_len_percentile','allele2_len_percentile','allele1_lps_percentile','allele2_lps_percentile']
TRVDataColNames=['chr','start','end','TRID_text','TRID','sampleCount', 'hprcLPSQuantilesData','hprcLenQuantilesData',
'genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded',
'isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped','gwasGeneData','omimGeneData','omim_inheritance','gnomadConstraintGeneData','gnomad_constraint_max_pli','gnomad_constraint_max_misz','lps_phenocounts','len_phenocounts']

# load allele frequency annotations
def simplest_motif(motif):
    i = (motif+motif).find(motif, 1, -1)
    if i != -1:
        motif = motif[:i]
    motifs = []
    b = len(motif)
    for i in range(b):
        c = motif[i:]+motif[:i]
        motifs.append(c)
    motifs = [x.upper() for x in motifs]
    return(min(motifs))
hprcLPSQuantiles=pandas.read_csv('HPRC_100_LongestPureSegmentQuantiles.txt.gz',sep='\t',low_memory=False)
hprcLPSQuantiles=hprcLPSQuantiles.rename(columns={'TRID':'TRID_text'})
hprcLPSQuantiles['longestPureSegmentMotif']=hprcLPSQuantiles.loc[:,'longestPureSegmentMotif'].apply(simplest_motif)
hprcLPSQuantiles=hprcLPSQuantiles.sort_values(by=['TRID_text','N_motif','longestPureSegmentMotif'],ascending=[True,False,True]).drop_duplicates(subset=['TRID_text','longestPureSegmentMotif'],keep='first').reset_index(drop=True)
hprcLPSQuantiles['hprcLPSQuantilesData']=hprcLPSQuantiles.iloc[:,1:].astype(str).agg('@'.join,axis=1)
hprcLPSQuantiles2=hprcLPSQuantiles.loc[:,['TRID_text','hprcLPSQuantilesData']].groupby('TRID_text').agg('<>'.join).reset_index(drop=False)
hprcLenQuantiles=pandas.read_csv('HPRC_100_alleleLengthQuantiles.txt.gz',sep='\t',low_memory=False)
hprcLenQuantiles=hprcLenQuantiles.rename(columns={'TRID':'TRID_text'})
hprcLenQuantiles['hprcLenQuantilesData']=hprcLenQuantiles.iloc[:,1:].astype(str).agg('@'.join,axis=1)

trCatalog=pandas.read_csv('variation_clusters_and_isolated_TRs_v1.GRCh37.TRGT.bed',sep='\t',low_memory=False,header=None,names=['chr','start','end','INFO'])
trCatalog['TRID_text']=trCatalog.loc[:,'INFO'].str.split(';',expand=True).iloc[:,0].str[3:]
trCatalog['TRID']=trCatalog.index.values

trCatalog=trCatalog.loc[:,['chr','start','end','TRID_text','TRID']]
trCatalog['sampleCount']=0
trCatalog=trCatalog.merge(hprcLPSQuantiles2,how='left',on='TRID_text')
trCatalog=trCatalog.merge(hprcLenQuantiles,how='left',on='TRID_text')

stagingData=pandas.DataFrame(index=range(0,len(trCatalog)),columns=TRVDataColNames)
stagingData=stagingData.astype({'omim_inheritance':'Int64'})
stagingData.loc[:,['chr','start','end','TRID_text','TRID','sampleCount', 'hprcLPSQuantilesData','hprcLenQuantilesData']]=trCatalog.loc[:,['chr','start','end','TRID_text','TRID','sampleCount', 'hprcLPSQuantilesData','hprcLenQuantilesData']]
stagingData.loc[:,['genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped']]=['FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE']

## add annotation data
stagingDataBT=pybedtools.BedTool.from_dataframe(stagingData)
intersectionCounts=stagingDataBT.intersect(geneLocations,c=True).intersect(genePaddedLocations,c=True).intersect(tssLocations,c=True).intersect(exonLocations,c=True).intersect(cdsLocations,c=True).intersect(exonPaddedLocations,c=True).intersect(canonicalTranscriptLocations,c=True).intersect(canonicalTranscriptPaddedLocations,c=True).intersect(canonicalTSSLocations,c=True).intersect(canonicalExonLocations,c=True).intersect(canonicalCDSLocations,c=True).intersect(canonicalExonPaddedLocations,c=True).to_dataframe(header=None,low_memory=False,index_col=False,names=TRVDataColNames + ['genes','genesPadded','tss','exons','cds','exonsPadded','canonicalTranscripts','canonicalTranscriptsPadded','canonicalTSS','canonicalExons','canonicalCDS','canonicalExonsPadded'])
stagingData.loc[intersectionCounts.index[intersectionCounts['tss']>0],'isNearTSS']="TRUE"
stagingData.loc[intersectionCounts.index[intersectionCounts['exons']>0],'overlapsExon']="TRUE"
stagingData.loc[intersectionCounts.index[intersectionCounts['cds']>0],'overlapsCDS']="TRUE"
stagingData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0)],'isIntronic']="TRUE"
stagingData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0) & (intersectionCounts['exonsPadded']>0)],'isNearExon']="TRUE"
stagingData.loc[intersectionCounts.index[intersectionCounts['canonicalTSS']>0],'isNearCanonicalTSS']="TRUE"
stagingData.loc[intersectionCounts.index[intersectionCounts['canonicalExons']>0],'overlapsCanonicalExon']="TRUE"
stagingData.loc[intersectionCounts.index[intersectionCounts['canonicalCDS']>0],'overlapsCanonicalCDS']="TRUE"
stagingData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0)],'isCanonicalIntronic']="TRUE"
stagingData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0) & (intersectionCounts['canonicalExonsPadded']>0)],'isNearCanonicalExon']="TRUE"
## get list of overlapping genes
# get the closest gene for each TRID 
genesIntersectingSVs=stagingDataBT.intersect(geneLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
# get the subset of rows which have duplicate TRIDs 
multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
# get the list of TRID-geneName combinations
TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
# get the list of TRIDs that I will group by
TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
def f(chr, start, end):
	return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chr']==chr) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID.values)]
if len(TRIDsWithMultipleGenes)>0:
	stagingData.loc[index1,'genesOverlapped']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
# deal with the uniquely matched TRID-gene pairs
uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingData.TRID)]
index2=stagingData.index[stagingData.TRID.isin(uniqueMatches.TRID)]
stagingData.loc[index2,'genesOverlapped']=uniqueMatches.loc[index1,'geneName'].values
## get list of gwas results for overlapped genes
TRIDGeneNameCombinations2=TRIDGeneNameCombinations.merge(gwasGeneData,how='left',on='geneName')
TRIDGeneNameCombinations2=TRIDGeneNameCombinations2.loc[TRIDGeneNameCombinations2['gene_data'].notnull(),:]
TRIDsWithMultipleGenes=TRIDGeneNameCombinations2.loc[~TRIDGeneNameCombinations2.duplicated(keep='first',subset='TRID'),:]
def f(chr, start, end):
	return '||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chr']==chr) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'gene_data'])
index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
if len(TRIDsWithMultipleGenes)>0:
	stagingData.loc[index1,'gwasGeneData']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(gwasGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingData.TRID)]
index2=stagingData.index[stagingData.TRID.isin(uniqueMatches2.TRID)]
stagingData.loc[index2,'gwasGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
# replace entries with more than 10 gwas associations with 'True'
stagingData.loc[stagingData.gwasGeneData.str.count('\|\|')>=10,'gwasGeneData']="TRUE"
## get list of omim results for overlapped genes
TRIDGeneNameCombinations2=TRIDGeneNameCombinations.merge(omimGeneData,how='left',on='geneName')
TRIDGeneNameCombinations2=TRIDGeneNameCombinations2.loc[TRIDGeneNameCombinations2['gene_data'].notnull(),:]
TRIDsWithMultipleGenes=TRIDGeneNameCombinations2.loc[~TRIDGeneNameCombinations2.duplicated(keep='first',subset='TRID'),:]
def f(chr, start, end):
	return '||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chr']==chr) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'gene_data'])
index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
if len(TRIDsWithMultipleGenes)>0:
	stagingData.loc[index1,'omimGeneData']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingData.TRID)]
index2=stagingData.index[stagingData.TRID.isin(uniqueMatches2.TRID)]
stagingData.loc[index2,'omimGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
# replace entries with more than 50 overlapping omim genes with 'True'
stagingData.loc[stagingData.omimGeneData.str.count('\|\|')>=50,'omimGeneData']="TRUE"
## get omim inheritance results for overlapped genes
def f(chr, start, end):
	tmp='||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chr']==chr) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'inheritance'])
	if '5' in tmp:
		return 5
	elif (('1' in tmp) and ('2' in tmp)):
		return 5
	elif (('3' in tmp) or ('6' in tmp) or ('7' in tmp) or ('8' in tmp)):
		return 3
	elif '4' in tmp:
		return 4
	else:
		return int(max([int(s) for s in tmp.split('||')]))
index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
if len(TRIDsWithMultipleGenes)>0:
	stagingData.loc[index1,'omim_inheritance']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['inheritance'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingData.TRID)]
index2=stagingData.index[stagingData.TRID.isin(uniqueMatches2.TRID)]
stagingData.loc[index2,'omim_inheritance']=uniqueMatches2.loc[index1,'inheritance'].astype('int').values
## get list of gnomad constraint results for overlapped genes
TRIDGeneNameCombinations2=TRIDGeneNameCombinations.merge(gnomadConstraintGeneData,how='left',on='geneName')
TRIDGeneNameCombinations2=TRIDGeneNameCombinations2.loc[TRIDGeneNameCombinations2['gene_data'].notnull(),:]
TRIDsWithMultipleGenes=TRIDGeneNameCombinations2.loc[~TRIDGeneNameCombinations2.duplicated(keep='first',subset='TRID'),:]
def f(chr, start, end):
	return '||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chr']==chr) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'gene_data'])
index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
if len(TRIDsWithMultipleGenes)>0:
	stagingData.loc[index1,'gnomadConstraintGeneData']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingData.TRID)]
index2=stagingData.index[stagingData.TRID.isin(uniqueMatches2.TRID)]
stagingData.loc[index2,'gnomadConstraintGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
# replace entries with more than 50 overlapping genes with 'True'
stagingData.loc[stagingData.gnomadConstraintGeneData.str.count('\|\|')>=50,'gnomadConstraintGeneData']="TRUE"
## get gnomad max pLI and misZ results for overlapped genes
def f1(chr, start, end):
	tmp='||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chr']==chr) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'pLI'])
	return max([float(s) for s in tmp.split('||')])
def f2(chr, start, end):
	tmp='||'.join(TRIDGeneNameCombinations2.loc[(TRIDGeneNameCombinations2['chr']==chr) & (TRIDGeneNameCombinations2['start']==start) & (TRIDGeneNameCombinations2['end']==end),'misZ'])
	return max([float(s) for s in tmp.split('||')])
index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
if len(TRIDsWithMultipleGenes)>0:
	stagingData.loc[index1,'gnomad_constraint_max_pli']=TRIDsWithMultipleGenes.apply(lambda x: f1(x.chr,x.start,x.end), axis=1).values
	stagingData.loc[index1,'gnomad_constraint_max_misz']=TRIDsWithMultipleGenes.apply(lambda x: f2(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['pLI'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.TRID.isin(stagingData.TRID)]
index2=stagingData.index[stagingData.TRID.isin(uniqueMatches2.TRID)]
stagingData.loc[index2,'gnomad_constraint_max_pli']=uniqueMatches2.loc[index1,'pLI'].values
stagingData.loc[index2,'gnomad_constraint_max_misz']=uniqueMatches2.loc[index1,'misZ'].values
## get list of overlapping genes (padded)
# get the closest gene for each TRID 
genesIntersectingSVs=stagingDataBT.intersect(genePaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
if len(genesIntersectingSVs)>0:
	# get the subset of rows which have duplicate TRIDs 
	multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	# get the list of TRID-geneName combinations
	TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
	# get the list of TRIDs that I will group by
	TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
	def f(chr, start, end):
		return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chr']==chr) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
	index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
	if len(TRIDsWithMultipleGenes)>0:
		stagingData.loc[index1,'genesOverlappedPadded']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched TRID-gene pairs
	uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingData.TRID)]
	index2=stagingData.index[stagingData.TRID.isin(uniqueMatches.TRID)]
	stagingData.loc[index2,'genesOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
## get list of overlapping canonical transcripts
# get the closest gene for each TRID 
genesIntersectingSVs=stagingDataBT.intersect(canonicalTranscriptLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
if len(genesIntersectingSVs)>0:
	# get the subset of rows which have duplicate TRIDs 
	multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	# get the list of TRID-geneName combinations
	TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
	# get the list of TRIDs that I will group by
	TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
	def f(chr, start, end):
		return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chr']==chr) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
	index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
	if len(TRIDsWithMultipleGenes)>0:
		stagingData.loc[index1,'canonicalTranscriptsOverlapped']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched TRID-gene pairs
	uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingData.TRID)]
	index2=stagingData.index[stagingData.TRID.isin(uniqueMatches.TRID)]
	stagingData.loc[index2,'canonicalTranscriptsOverlapped']=uniqueMatches.loc[index1,'geneName'].values
## get list of overlapping canonical transcripts (padded)
# get the closest gene for each TRID 
genesIntersectingSVs=stagingDataBT.intersect(canonicalTranscriptPaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
if len(genesIntersectingSVs)>0:
	# get the subset of rows which have duplicate TRIDs 
	multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	# get the list of TRID-geneName combinations
	TRIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','geneName']),:]
	# get the list of TRIDs that I will group by
	TRIDsWithMultipleGenes=TRIDGeneNameCombinations.loc[~TRIDGeneNameCombinations.duplicated(keep='first',subset='TRID'),:]
	def f(chr, start, end):
		return ','.join(TRIDGeneNameCombinations.loc[(TRIDGeneNameCombinations['chr']==chr) & (TRIDGeneNameCombinations['start']==start) & (TRIDGeneNameCombinations['end']==end),'geneName'])
	index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleGenes.TRID)]
	if len(TRIDsWithMultipleGenes)>0:
		stagingData.loc[index1,'canonicalTranscriptsOverlappedPadded']=TRIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched TRID-gene pairs
	uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingData.TRID)]
	index2=stagingData.index[stagingData.TRID.isin(uniqueMatches.TRID)]
	stagingData.loc[index2,'canonicalTranscriptsOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
## get list of overlapping encode enhancers
# get the closest enhancer for each TRID 
enhancersIntersectingSVs=stagingDataBT.intersect(enhancerLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRVDataColNames + ['enhancerChr','enhancerStart','enhancerEnd','enhancerID'])
if len(enhancersIntersectingSVs)>0:
	# get the subset of rows which have duplicate TRIDs 
	multipleEntries=enhancersIntersectingSVs.loc[enhancersIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	# get the list of TRID-enhancer combinations
	TRIDEnhancerCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','enhancerID']),:]
	# get the list of TRIDs that I will group by
	TRIDsWithMultipleEnhancers=TRIDEnhancerCombinations.loc[~TRIDEnhancerCombinations.duplicated(keep='first',subset='TRID'),:]
	def f(chr, start, end):
		return ','.join(TRIDEnhancerCombinations.loc[(TRIDEnhancerCombinations['chr']==chr) & (TRIDEnhancerCombinations['start']==start) & (TRIDEnhancerCombinations['end']==end),'enhancerID'])
	index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleEnhancers.TRID)]
	if len(TRIDsWithMultipleEnhancers)>0:
		stagingData.loc[index1,'encodeEnhancersOverlapped']=TRIDsWithMultipleEnhancers.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched TRID-enhancer pairs
	uniqueMatches=enhancersIntersectingSVs.loc[~enhancersIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingData.TRID)]
	index2=stagingData.index[stagingData.TRID.isin(uniqueMatches.TRID)]
	stagingData.loc[index2,'encodeEnhancersOverlapped']=uniqueMatches.loc[index1,'enhancerID'].values
## get list of overlapping encode promoters
# get the closest enhancer for each TRID 
promotersIntersectingSVs=stagingDataBT.intersect(promoterLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRVDataColNames + ['promoterChr','promoterStart','promoterEnd','promoterID'])
if len(promotersIntersectingSVs)>0:
	# get the subset of rows which have duplicate TRIDs 
	multipleEntries=promotersIntersectingSVs.loc[promotersIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	# get the list of TRID-promoter combinations
	TRIDPromoterCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','promoterID']),:]
	# get the list of TRIDs that I will group by
	TRIDsWithMultiplePromoters=TRIDPromoterCombinations.loc[~TRIDPromoterCombinations.duplicated(keep='first',subset='TRID'),:]
	def f(chr, start, end):
		return ','.join(TRIDPromoterCombinations.loc[(TRIDPromoterCombinations['chr']==chr) & (TRIDPromoterCombinations['start']==start) & (TRIDPromoterCombinations['end']==end),'promoterID'])
	index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultiplePromoters.TRID)]
	if len(TRIDsWithMultiplePromoters)>0:
		stagingData.loc[index1,'encodePromotersOverlapped']=TRIDsWithMultiplePromoters.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched TRID-promoter pairs
	uniqueMatches=promotersIntersectingSVs.loc[~promotersIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingData.TRID)]
	index2=stagingData.index[stagingData.TRID.isin(uniqueMatches.TRID)]
	stagingData.loc[index2,'encodePromotersOverlapped']=uniqueMatches.loc[index1,'promoterID'].values
## get list of overlapping encode ctcfOnly elements
# get the closest enhancer for each TRID 
ctcfOnlyIntersectingSVs=stagingDataBT.intersect(ctcfOnlyLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=TRVDataColNames + ['ctcfOnlyChr','ctcfOnlyStart','ctcfOnlyEnd','ctcfOnlyID'])
if len(ctcfOnlyIntersectingSVs)>0:
	# get the subset of rows which have duplicate TRIDs 
	multipleEntries=ctcfOnlyIntersectingSVs.loc[ctcfOnlyIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	# get the list of TRID-ctcfOnly combinations
	TRIDCtcfOnlyCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['TRID','ctcfOnlyID']),:]
	# get the list of TRIDs that I will group by
	TRIDsWithMultipleCTCFOnlySites=TRIDCtcfOnlyCombinations.loc[~TRIDCtcfOnlyCombinations.duplicated(keep='first',subset='TRID'),:]
	def f(chr, start, end):
		return ','.join(TRIDCtcfOnlyCombinations.loc[(TRIDCtcfOnlyCombinations['chr']==chr) & (TRIDCtcfOnlyCombinations['start']==start) & (TRIDCtcfOnlyCombinations['end']==end),'ctcfOnlyID'])
	index1=stagingData.index[stagingData.TRID.isin(TRIDsWithMultipleCTCFOnlySites.TRID)]
	if len(TRIDsWithMultipleCTCFOnlySites)>0:
		stagingData.loc[index1,'encodeCTCFSitesOverlapped']=TRIDsWithMultipleCTCFOnlySites.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched TRID-gene pairs
	uniqueMatches=ctcfOnlyIntersectingSVs.loc[~ctcfOnlyIntersectingSVs.duplicated(keep=False,subset='TRID'),:]
	index1=uniqueMatches.index[uniqueMatches.TRID.isin(stagingData.TRID)]
	index2=stagingData.index[stagingData.TRID.isin(uniqueMatches.TRID)]
	stagingData.loc[index2,'encodeCTCFSitesOverlapped']=uniqueMatches.loc[index1,'ctcfOnlyID'].values
## combine staging with the rest of SVData
TRData=TRData.sort_values(by=['TRID'],ascending=True)
TRData.to_csv(TRVDataFile,sep='\t',header=False,index=False,na_rep="\\N")

# initialize empty GenoData table 
GenoData=pandas.DataFrame({'TRID':numpy.nan,'SampleID':numpy.nan,'allele1':numpy.nan,'allele2':numpy.nan,'phased':numpy.nan,'reads_allele1':numpy.nan,'reads_allele2':numpy.nan,
'allele1_len':numpy.nan,'allele2_len':numpy.nan,'allele1_lps_motif':numpy.nan,'allele1_lps_len':numpy.nan,'allele2_lps_motif':numpy.nan,'allele2_lps_len':numpy.nan,
'allele1_len_percentile':numpy.nan,'allele2_len_percentile':numpy.nan,'allele1_lps_percentile':numpy.nan,'allele2_lps_percentile':numpy.nan},index=[0])
GenoData.to_csv(GenoDataFile,sep='\t',header=False,index=False,na_rep="\\N")
