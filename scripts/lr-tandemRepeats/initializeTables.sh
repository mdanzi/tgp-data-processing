#!/bin/bash

cd $TGP_HOME

# download the updateable annotation tables
aws s3 cp s3://tgp-annotation/omim/omim.tsv . --only-show-errors
aws s3 cp s3://tgp-annotation/gwas/gwas.tsv . --only-show-errors
aws s3 cp s3://tgp-annotation/gnomad_constraint/gnomad_constraint.txt . --only-show-errors

# need to establish zuchnerlab AWS credentials
aws s3 cp s3://zuchnerlab/RepeatExpansions/TRGT/HPRC/HPRC_100_LongestPureSegmentQuantiles.txt.gz . --only-show-errors --profile zuchnerlab
aws s3 cp s3://zuchnerlab/RepeatExpansions/TRGT/HPRC/HPRC_100_alleleLengthQuantiles.txt.gz . --only-show-errors --profile zuchnerlab
aws s3 cp s3://tgp-data-analysis/resources/variation_clusters_and_isolated_TRs_v1.GRCh37.TRGT.bed . --only-show-errors

# transform updateable annotation tables into their aggregate formats
# omim
echo -e "geneName\tgene_data\tinheritance" > tmp_header
cut -f 3 omim.tsv > tmp1
cut -f 1,2,3,4,5,6,7 omim.tsv | sed 's/\t/<>/g' > tmp2
cut -f 7 omim.tsv > tmp3
paste tmp1 tmp2 tmp3 | cat tmp_header - > omim_data.txt
rm tmp1 tmp2 tmp_header
# gwas
echo -e "geneName\tgene_data" > tmp_header
cut -f 8 gwas.tsv > tmp1
cut -f 3,4,5,6,7,8,9 gwas.tsv | sed 's/\t/<>/g' > tmp2
paste tmp1 tmp2 | cat tmp_header - > gwas_gene.txt
rm tmp1 tmp2 tmp_header
# gnomad constraint (canonical transcripts only)
echo -e "geneName\tgene_data\tmisZ\tpLI" > tmp_header
# filter down to only canonical transcripts and keep first if there are multiple
awk -F'\t' -v OFS='\t' '{if($3=="true"){print $0}}' gnomad_constraint.txt | sort -k1,1 -u > gnomad_constraint_filt.txt 
cut -f 1 gnomad_constraint_filt.txt > tmp1
awk -F'\t' -v OFS='\t' '{print $1,$22,$20,$4,$5,$9,$10,$14,$15,$21}' gnomad_constraint_filt.txt | sed 's/\t/<>/g' > tmp2
cut -f 20,22 gnomad_constraint_filt.txt > tmp3
paste tmp1 tmp2 tmp3 | cat tmp_header - > gnomad_constraint_data.txt
rm tmp1 tmp2 tmp_header

# initialze lr_TRVData.txt and lr_tr_GenoData.txt tables 
source activate py37
python $TGP_HOME/scripts/lr-tandemRepeats/initializeTables.py

# convert chromosome names back into numbers
sed -i 's/^X/23/' lr_TRVData.txt
sed -i 's/^Y/24/' lr_TRVData.txt
sed -i 's/^MT/25/' lr_TRVData.txt

# upload the database tables
aws s3 cp lr_TRVData.txt s3://tgp-annotation/lr_tr_variantindex/ --only-show-errors
aws s3 cp lr_tr_GenoData.txt s3://tgp-annotation/lr_tr_genomic_data/ --only-show-errors

