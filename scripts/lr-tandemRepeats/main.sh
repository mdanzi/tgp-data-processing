parseLRTRs() {

    local inputFileS3=$1

    cd $TGP_HOME

    aws s3 cp $inputFileS3 . --only-show-errors

    inputFile=$(basename $inputFileS3)

    # download the database tables
    aws s3 cp s3://tgp-annotation/lr_tr_variantindex/lr_TRVData.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/lr_tr_genomic_data/lr_tr_GenoData.txt . --only-show-errors
    aws s3 cp s3://tgp-data-analysis/resources/HPRC_100_alleleLengthQuantiles.txt.gz . --only-show-errors
    aws s3 cp s3://tgp-data-analysis/resources/HPRC_100_LongestPureSegmentQuantiles.txt.gz . --only-show-errors

    COUNT=0

    source activate py37
    pip install regex # need to update docker eventually to include this

    while IFS=$'\t' read -r fileFullName diagnosisName diagnosisCodeText; do
        COUNT=$((COUNT+1))
        fileName=$(sed 's#^.*/##g' <<< ${fileFullName})
        BASE=$(sed 's/\..*$//g' <<< ${fileName})
        if s3_path_exists $fileFullName && ! grep -qwF $BASE lr_tr_GenoData.txt; then
            aws s3 cp ${fileFullName} . --only-show-errors
            tabix ${fileName}
            if [[ -f ${BASE}.trgt.hiphase.vcf.gz ]]; then # trgt-specific parsing
                # convert to tab-delimited format and evaluate phasing
                bcftools query -f '%INFO/TRID\t[%TGT]\t[%SD]\n' ${fileName} | awk -F'\t' -v OFS='\t' -v sampleID=${BASE} '{if($2 ~ "\\|"){print $0,"1",sampleID}else{print $0,"0",sampleID}}' | grep -vwF '.' | tr '|' ',' | tr '/' ',' > ${BASE}.TR.parsed.txt
            elif [[ -f ${BASE}.longTR.vcf.gz ]]; then # LongTR-specific parsing
                # convert to tab-delimited format and evaluate phasing
                bcftools query -f '%ID\t[%TGT]\t[%PDP]\n' ${fileName} | awk -F'\t' -v OFS='\t' -v sampleID=${BASE} '{if($2 ~ "\\|"){print $0,"1",sampleID}else{print $0,"0",sampleID}}' | grep -vwF '.' | tr '|' ',' | tr '/' ',' > ${BASE}.TR.parsed.txt
            fi 
            python $REPO/scripts/lr-tandemRepeats/parseLRTRs.py --sample=${BASE}.TR.parsed.txt --phenoCode="${diagnosisCodeText}" --phenoName="${diagnosisName}"
            # every other sample, upload the data tables just in case of a crash later
            if [[ $((COUNT % 2)) -eq 0 ]]; then
                aws s3 cp lr_TRVData.txt s3://tgp-annotation/lr_tr_variantindex/ --only-show-errors
                aws s3 cp lr_tr_GenoData.txt s3://tgp-annotation/lr_tr_genomic_data/ --only-show-errors
            fi
            rm ${fileName}
            rm ${BASE}.TR.parsed.txt
        fi
    done < $inputFile

    # upload the database tables
    aws s3 cp lr_TRVData.txt s3://tgp-annotation/lr_tr_variantindex/ --only-show-errors
    aws s3 cp lr_tr_GenoData.txt s3://tgp-annotation/lr_tr_genomic_data/ --only-show-errors

}
