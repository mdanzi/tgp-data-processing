import pandas
import numpy as np 
import sys
import getopt
def main(argv):
    sampleID = ''
    try:
        opts, args = getopt.getopt(argv,"h",["input=","help"])
    except getopt.GetoptError:
        print('mergeRNASeqResults.py --input=<sampleID>')
        sys.exit(0)
    for opt, arg in opts:
        if opt in ('--input'):
            sampleID=arg
        elif opt in ('-h','--help'):
            print('mergeRNASeqResults.py --input=<sampleID>')
            sys.exit(0)
        else:
            print('mergeRNASeqResults.py --input=<sampleID>')
            sys.exit(0)
    # load gene expression data
    expr=pandas.read_csv(sampleID + '_quant.sf',sep='\t',low_memory=False)
    expr=expr.loc[:,['Name','TPM']].rename(columns={'Name':'TranscriptID'})
    # load transcriptToGene conversion table
    conversionTable=pandas.read_csv('transcriptToGeneConversionTable.txt',sep='\t',low_memory=False,header=None,names=['GeneID','TranscriptID'])
    # create gene-level expression table
    expr2=expr.merge(conversionTable,how='inner',on='TranscriptID')
    expr3=expr2.groupby('GeneID')['TPM'].sum().reset_index()
    # get table of transcript-level TPMs for each gene
    expr2['TranscriptTPMs']=expr2.loc[:,['TranscriptID','TPM']].apply(lambda row: '@'.join(row.astype(str)), axis=1)
    expr4=expr2.groupby('GeneID')['TranscriptTPMs'].apply(lambda x: '<>'.join(x))
    expr5=expr3.merge(expr4.to_frame().reset_index(),how='inner',on='GeneID')
    expr5['SampleGenomicID']=sampleID
    expr5=expr5.loc[:,['SampleGenomicID','GeneID','TPM','TranscriptTPMs']]

    # load splicing results
    a3ss=pandas.read_csv(sampleID + '_A3SS.parsed.txt',sep='\t',low_memory=False)
    a5ss=pandas.read_csv(sampleID + '_A5SS.parsed.txt',sep='\t',low_memory=False)
    mxe=pandas.read_csv(sampleID + '_MXE.parsed.txt',sep='\t',low_memory=False)
    ri=pandas.read_csv(sampleID + '_RI.parsed.txt',sep='\t',low_memory=False)
    se=pandas.read_csv(sampleID + '_SE.parsed.txt',sep='\t',low_memory=False)

    # merge on splicing results
    merged=expr5.merge(a3ss,how='left',on='GeneID')
    merged=merged.merge(a5ss,how='left',on='GeneID')
    merged=merged.merge(mxe,how='left',on='GeneID')
    merged=merged.merge(ri,how='left',on='GeneID')
    merged=merged.merge(se,how='left',on='GeneID')
    merged.to_csv(sampleID + '_RNASeq_parsed.txt',sep='\t',header=False,index=False,na_rep="\\N")

if __name__ == "__main__":
    main(sys.argv[1:])
