RNASeqImport() {
    
    local inputFileS3=$1

    aws s3 cp $inputFileS3 . --only-show-errors

    inputFile=$(basename $inputFileS3)

    # download the database tables
    aws s3 cp s3://tgp-annotation/rna_genomic_data/rna_GenoData.txt . --only-show-errors

    while IFS=$'\t' read -r fileFullName; do
        fileName=$(sed 's#^.*/##g' <<< ${fileFullName})
        BASE=$(sed 's/\..*$//g' <<< ${fileName})
        if s3_path_exists $fileFullName && ! grep -qwF $BASE rna_GenoData.txt; then
            aws s3 cp ${fileFullName} - --only-show-errors | tail -n +2 >> rna_GenoData.txt
        fi
    done < $inputFile

    # sort -k1n,1n -k2,2 rna_GenoData.txt > tmp; mv tmp rna_GenoData.txt

    # upload the database table
    aws s3 cp rna_GenoData.txt s3://tgp-annotation/rna_genomic_data/ --only-show-errors


}
