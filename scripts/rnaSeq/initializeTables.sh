#!/bin/bash

cd $TGP_HOME

# download the GTEX data 
wget https://storage.googleapis.com/adult-gtex/bulk-gex/v8/rna-seq/tpms-by-tissue/gene_tpm_2017-06-05_v8_cells_cultured_fibroblasts.gct.gz
wget https://storage.googleapis.com/adult-gtex/bulk-gex/v8/rna-seq/tpms-by-tissue/gene_tpm_2017-06-05_v8_cells_ebv-transformed_lymphocytes.gct.gz
wget https://storage.googleapis.com/adult-gtex/bulk-gex/v8/rna-seq/tpms-by-tissue/gene_tpm_2017-06-05_v8_muscle_skeletal.gct.gz
wget https://storage.googleapis.com/adult-gtex/bulk-gex/v8/rna-seq/tpms-by-tissue/gene_tpm_2017-06-05_v8_whole_blood.gct.gz

# calculate distributions in GTEX data and merge
python $TGP_HOME/tgp-data-processing/scripts/rnaSeq/initializeTables.py

# upload initialized table
aws s3 cp rna_VariantIndex.txt s3://tgp-annotation/rna_variantindex/ --only-show-errors
