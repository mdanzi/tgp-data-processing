import pandas
import numpy as np 
import scipy.stats
import sys
import getopt
def main(argv):
    inFileName = ''
    outFileName = ''
    try:
        opts, args = getopt.getopt(argv,"h",["input=","output=","help"])
    except getopt.GetoptError:
        print('parseSplicing_SE.py --input=<SplicingFile> --output=<ParsedSplicingFile>')
        sys.exit(0)
    for opt, arg in opts:
        if opt in ('--input'):
            inFileName=arg
        elif opt in ('--output'):
            outFileName=arg
        elif opt in ('-h','--help'):
            print('parseSplicing_SE.py --input=<SplicingFile>--output=<ParsedSplicingFile>')
            sys.exit(0)
        else:
            print('parseSplicing_SE.py --input=<SplicingFile>--output=<ParsedSplicingFile>')
            sys.exit(0)
    dataIn=pandas.read_csv(inFileName,sep='\t',low_memory=False)
    # re-compute FDR as rmats sometimes produces NaNs
    dataIn['FDR']=scipy.stats.false_discovery_control(dataIn.loc[:,'PValue'].values,method='bh')
    # create compressed representation column
    dataIn['condensedView']=dataIn.loc[:,['exonStart_0base', 'exonEnd', 'upstreamES', 'upstreamEE', 'downstreamES', 'downstreamEE', 'IJC_SAMPLE_1', 'SJC_SAMPLE_1', 'IJC_SAMPLE_2', 'SJC_SAMPLE_2', 'IncFormLen', 'SkipFormLen', 'PValue', 'FDR', 'IncLevel1', 'IncLevel2', 'IncLevelDifference']].apply(lambda row: '@'.join(row.astype(str)), axis=1)
    # for each gene, combine the compressed representation column values
    tmp1=dataIn.groupby('GeneID')['condensedView'].apply(lambda x: '<>'.join(x))
    # repeat using only significant results
    tmp2=dataIn.loc[dataIn['FDR']<0.05,:].groupby('GeneID')['condensedView'].apply(lambda x: '<>'.join(x))
    # merge gene-level representations
    merged=tmp1.to_frame().reset_index().rename(columns={'condensedView':'SE_data'}).merge(tmp2.to_frame().rename(columns={'condensedView':'SE_sig_data'}),how='left',on='GeneID')
    merged.to_csv(outFileName,sep='\t',index=False)

if __name__ == "__main__":
    main(sys.argv[1:])
