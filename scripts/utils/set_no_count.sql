/* Intended to be used as a startup script to suppress the row count message
 * (eg. "1 rows affected") when using sqlcmd.  */
SET NOCOUNT ON
