# Install version 1.6 of jq instead of 1.5
wget -O /tmp/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
chmod +x /tmp/jq
sudo mv /tmp/jq /usr/local/bin/.

# Accept Microsoft terms without being prompted
# Reference: learn.microsoft.com/en-us/sql/linux/sample-unattended-install-redhat
sudo ACCEPT_EULA=Y yum -y update

# Install bash 5 instead of default 4.2
# Reference: computingforgeeks.com/how-to-install-bash-5-on-centos-linux/
sudo yum -y groupinstall "Development Tools"

current_dir=$(pwd)
cd /tmp
curl -O http://ftp.gnu.org/gnu/bash/bash-5.1.16.tar.gz
tar xzvf bash-5.1.16.tar.gz
cd bash-5.1.16
./configure
make
sudo make install
cd $current_dir

