#!/usr/bin/env bash

get_fastq_read() {
    local readnum=$1
    jq --raw-output ".fastq[] | .uri${readnum}"
}

get_sample_id() {
    jq --raw-output ".sampleId"
}

get_sample_genomic_id() {
    jq --raw-output ".sampleGenomicId"
}

get_sample_run_id() {
    jq --raw-output ".sampleRunId"
}

# Outputs a concatenated stream of all fastq files for a given read number
s3_read_concat() {
    local sampleRunJson=$1 readnum=$2
    local s3reads
    readarray -t s3reads <<< $(get_fastq_read $readnum <<< $sampleRunJson)
    for s3file in "${s3reads[@]}"; do
        aws s3 cp --only-show-errors "$s3file" -
    done
}

# Returns the compression extension of the first fastq file or nothing if no
# compression is used. A dot (.) is prepended to the compression extension for
# convenience.
fastq_compression() {
    local sampleRunJson=$1 first_file extension
    first_file=$(jq --raw-output 'first(.fastq[] | flatten[])' <<< "$sampleRunJson")
    extension=${first_file##*.}
    if [[ $extension = "bz2" || $extension = "gz" ]]; then
        echo ".${extension}"
    fi
}

bwa_read_stream() {
    local sampleRunJson=$1 readnum=$2
    local s3_reads
    readarray -t s3_reads <<< $(get_fastq_read $readnum <<< $sampleRunJson)
    # BWA does not accept bz2 format. Conditionally unzip based on file
    # extension (of first file) 
    local catcmd
    if [[ $(fastq_compression "$sampleRunJson") = ".bz2" ]]; then
        catcmd="bzcat"
    else
        catcmd="cat"
    fi
    s3_read_concat "$sampleRunJson" $readnum | $catcmd 
}
