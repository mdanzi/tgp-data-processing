#!/usr/bin/env bash

errorReport() {
    local lastCmd=$BASH_COMMAND lastCode=$?
    echo "Failed on command: [$lastCmd], exit code: [$lastCode]" >&2
}

s3_cp_to_processing() {
  local src=$1 runid=$2 
  aws s3 cp $src "s3://tgp-sample-processing/$runid/$src" --only-show-errors
}

s3_cp_from_processing() {
  local src=$1 runid=$2 
  aws s3 cp "s3://tgp-sample-processing/$runid/$src" $src --only-show-errors
}

# Test whether directory/object exists. NB: Can't use head-object to test for
# directory existence
s3_path_exists() {
  aws s3 ls "$1" > /dev/null 2>&1
}

s3_stream_output() {
  for s3file in "$@"; do
    aws s3 cp --only-show-errors "$s3file" -
  done
}

get_s3_path() {
  echo "s3://tgp-sample-processing/$1/$2"
}

get_num_cores() {
  grep -c "^processor" /proc/cpuinfo
}

# Initial launch volumes do not get tagged; Tag them with the instance id, like
# the ebs-autoscale volumes are tagged 
# Cromwell implementation (not recommended): 
# https://github.com/geertvandeweyer/cromwell/blob/79666c9ae73ef282ca7f64dfc91e0ec74f19529f/supportedBackends/aws/src/main/scala/cromwell/backend/impl/aws/AwsBatchJob.scala#L163
tag_launch_volumes() {
  local instance_id instance_name volume_ids
  instance_id=$(get_instance_id)
  instance_name=$(get_instance_tag_name $instance_id)
  volume_ids=$(get_launch_volume_ids $instance_id)
  for volume_id in $volume_ids; do
    aws ec2 create-tags --region us-east-1 --resources $volume_id \
      --tags Key=source-instance,Value=$instance_id Key=Name,Value=$instance_name
  done
  echo "[TGP-INFO]: Tagged initial launch volumes of instance $instance_id" >&2
}

# Launch template has two volumes mounted initially: xvda & xvdba.
get_launch_volume_ids() {
  local instance_id=$1
  get_volume_id_for_instance_mount $instance_id xvda
  get_volume_id_for_instance_mount $instance_id xvdba
}

get_volume_id_for_instance_mount() {
  local instance_id=$1 mount_point=$2
  aws ec2 describe-volumes \
    --filters Name=attachment.device,Values=/dev/$mount_point \
    Name=attachment.instance-id,Values=$instance_id \
    --query 'Volumes[0].VolumeId' --output text --region us-east-1
}

get_instance_tag_name() {
  local instance_id=$1
  aws ec2 describe-tags --output text  --region us-east-1 \
    --filters "Name=resource-id,Values=$instance_id" "Name=key,Values=Name" \
    --query 'Tags[].Value'
}

# Function extracted from ebs-autoscale script, including ip address
# Reference: https://github.com/awslabs/amazon-ebs-autoscale/blob/a55e7cc8913dd89e42b4668e4e8883209db26e9c/shared/utils.sh#L31
# Also see this technique: https://repost.aws/knowledge-center/batch-instance-id-ip-address
get_instance_id() {
  curl -s http://169.254.169.254/latest/meta-data/instance-id
}
