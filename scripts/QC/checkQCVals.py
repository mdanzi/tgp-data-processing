import numpy
import pandas
import sys, getopt, os
import csv

def main(argv):
    QCValsFile = 'QCVals.txt'
    sampleFileName = ''
    try:
        opts, args = getopt.getopt(argv,"h",["input=","help"])
    except getopt.GetoptError:
        print('checkQCVals.py --input=<SampleFile>')
        sys.exit(0)
    for opt, arg in opts:
        if opt in ('--input'):
            sampleFileName=arg
        elif opt in ('-h','--help'):
            print('checkQCVals.py --input=<SampleFile>')
            sys.exit(0)
        else:
            print('checkQCVals.py --input=<SampleFile>')
            sys.exit(0)
    ## load the data
    colNames=['SampleID','ExperimentTypeText', 'ReadNumber', 'InconsistentReadNumber', 'Read1Quality', 'Read2Quality', 'ReadQualityDiff', 
    'AlignmentRate', 'ReadPairingRate', 'DuplicationRate', 'ReferenceCoverage', 'ExonEnrichment', 
    'TSTVRatio', 'SNPCount', 'INDELCount', 'AvgDepth', 'AvgQual', 'AvgGQ', 'HetHomRatio', 'GenderMismatch']
    QCVals=pandas.read_csv(QCValsFile,sep='\t',low_memory=False,index_col=0)
    sampleFile=pandas.read_csv(sampleFileName,sep='\t',low_memory=False,header=None,names=colNames)
    experimentType=sampleFile.iloc[0,1]
    
    exceeded_thresholds = {}
    if ((experimentType=="WES") | (experimentType=="WGS") | (experimentType=="HiFi") | (experimentType=="ONT")):
        if ((experimentType=="WES") | (experimentType=="WGS")):
            if sampleFile.iloc[0,5] < (threshold := QCVals.loc[experimentType,'Read2Quality_median']-(5*QCVals.loc[experimentType,'Read2Quality_mad'])):
                exceeded_thresholds['Read2QualityQC'] = threshold
            if sampleFile.iloc[0,8] < (threshold := QCVals.loc[experimentType,'ReadPairingRate_median']-(5*QCVals.loc[experimentType,'ReadPairingRate_mad'])):
                exceeded_thresholds['ReadPairingRateQC'] = threshold
            if sampleFile.iloc[0,9] > (threshold := QCVals.loc[experimentType,'DuplicationRate_median']+(3*QCVals.loc[experimentType,'DuplicationRate_mad'])):
                exceeded_thresholds['DuplicationRateQC'] = threshold
        if sampleFile.iloc[0,7] < (threshold := QCVals.loc[experimentType,'AlignmentRate_median']-(5*QCVals.loc[experimentType,'AlignmentRate_mad'])):
            exceeded_thresholds['AlignmentRateQC'] = threshold
        if sampleFile.iloc[0,12] < (threshold := QCVals.loc[experimentType,'TSTVRatio_median']-(10*QCVals.loc[experimentType,'TSTVRatio_mad'])):
            exceeded_thresholds['TSTVRatioQC'] = threshold
        if sampleFile.iloc[0,16] < (threshold := QCVals.loc[experimentType,'AvgQual_median']-(1*QCVals.loc[experimentType,'AvgQual_mad'])):
            exceeded_thresholds['AvgQualQC'] = threshold
        if sampleFile.iloc[0,18] < (threshold := QCVals.loc[experimentType,'HetHomRatio_median']-(5*QCVals.loc[experimentType,'HetHomRatio_mad'])):
            exceeded_thresholds['HetHomRatioQC'] = threshold
        if experimentType=="WES":
            if sampleFile.iloc[0,4] < (threshold := QCVals.loc[experimentType,'Read1Quality_median']-(5*QCVals.loc[experimentType,'Read1Quality_mad'])):
                exceeded_thresholds['Read1QualityQC'] = threshold
            if sampleFile.iloc[0,6] > (threshold := QCVals.loc[experimentType,'ReadQualityDiff_median']+(5*QCVals.loc[experimentType,'ReadQualityDiff_mad'])):
                exceeded_thresholds['ReadQualityDiffQC'] = threshold
            if sampleFile.iloc[0,10] < (threshold := QCVals.loc[experimentType,'ReferenceCoverage_median']-(2*QCVals.loc[experimentType,'ReferenceCoverage_mad'])):
                exceeded_thresholds['ReferenceCoverageQC'] = threshold
            if sampleFile.iloc[0,10] > (threshold := QCVals.loc[experimentType,'ReferenceCoverage_median']+(2*QCVals.loc[experimentType,'ReferenceCoverage_mad'])):
                exceeded_thresholds['ReferenceCoverageQC'] = threshold
            if sampleFile.iloc[0,11] < (threshold := QCVals.loc[experimentType,'ExonEnrichment_median']-(2*QCVals.loc[experimentType,'ExonEnrichment_mad'])):
                exceeded_thresholds['ExonEnrichmentQC'] = threshold
            if sampleFile.iloc[0,13] < (threshold := QCVals.loc[experimentType,'SNPCount_median']-(2*QCVals.loc[experimentType,'SNPCount_mad'])):
                exceeded_thresholds['SNPCountQC'] = threshold
            if sampleFile.iloc[0,14] < (threshold := QCVals.loc[experimentType,'INDELCount_median']-(2*QCVals.loc[experimentType,'INDELCount_mad'])):
                exceeded_thresholds['INDELCountQC'] = threshold
            if sampleFile.iloc[0,15] < (threshold := QCVals.loc[experimentType,'AvgDepth_median']-(1*QCVals.loc[experimentType,'AvgDepth_mad'])):
                exceeded_thresholds['AvgDepthQC'] = threshold
            if sampleFile.iloc[0,17] < (threshold := QCVals.loc[experimentType,'AvgGQ_median']-(3*QCVals.loc[experimentType,'AvgGQ_mad'])):
                exceeded_thresholds['AvgGQQC'] = threshold
        elif experimentType=="WGS":
            if sampleFile.iloc[0,4] < (threshold := QCVals.loc[experimentType,'Read1Quality_median']-(2.5*QCVals.loc[experimentType,'Read1Quality_mad'])):
                exceeded_thresholds['Read1QualityQC'] = threshold
            if sampleFile.iloc[0,6] > (threshold := QCVals.loc[experimentType,'ReadQualityDiff_median']+(4*QCVals.loc[experimentType,'ReadQualityDiff_mad'])):
                exceeded_thresholds['ReadQualityDiffQC'] = threshold
            if sampleFile.iloc[0,10] < (threshold := QCVals.loc[experimentType,'ReferenceCoverage_median']-(5*QCVals.loc[experimentType,'ReferenceCoverage_mad'])):
                exceeded_thresholds['ReferenceCoverageQC'] = threshold
            if sampleFile.iloc[0,11] > (threshold := QCVals.loc[experimentType,'ExonEnrichment_median']+(10*QCVals.loc[experimentType,'ExonEnrichment_mad'])):
                exceeded_thresholds['ExonEnrichmentQC'] = threshold
            if sampleFile.iloc[0,13] < (threshold := QCVals.loc[experimentType,'SNPCount_median']-(5*QCVals.loc[experimentType,'SNPCount_mad'])):
                exceeded_thresholds['SNPCountQC'] = threshold
            if sampleFile.iloc[0,14] < (threshold := QCVals.loc[experimentType,'INDELCount_median']-(5*QCVals.loc[experimentType,'INDELCount_mad'])):
                exceeded_thresholds['INDELCountQC'] = threshold
            if sampleFile.iloc[0,15] < (threshold := QCVals.loc[experimentType,'AvgDepth_median']-(2*QCVals.loc[experimentType,'AvgDepth_mad'])):
                exceeded_thresholds['AvgDepthQC'] = threshold
            if sampleFile.iloc[0,17] < (threshold := QCVals.loc[experimentType,'AvgGQ_median']-(6*QCVals.loc[experimentType,'AvgGQ_mad'])):
                exceeded_thresholds['AvgGQQC'] = threshold
        elif experimentType=="WGS":
            if sampleFile.iloc[0,4] < (threshold := QCVals.loc[experimentType,'Read1Quality_median']-(2.5*QCVals.loc[experimentType,'Read1Quality_mad'])):
                exceeded_thresholds['Read1QualityQC'] = threshold
            if sampleFile.iloc[0,6] > (threshold := QCVals.loc[experimentType,'ReadQualityDiff_median']+(4*QCVals.loc[experimentType,'ReadQualityDiff_mad'])):
                exceeded_thresholds['ReadQualityDiffQC'] = threshold
            if sampleFile.iloc[0,10] < (threshold := QCVals.loc[experimentType,'ReferenceCoverage_median']-(5*QCVals.loc[experimentType,'ReferenceCoverage_mad'])):
                exceeded_thresholds['ReferenceCoverageQC'] = threshold
            if sampleFile.iloc[0,11] > (threshold := QCVals.loc[experimentType,'ExonEnrichment_median']+(10*QCVals.loc[experimentType,'ExonEnrichment_mad'])):
                exceeded_thresholds['ExonEnrichmentQC'] = threshold
            if sampleFile.iloc[0,13] < (threshold := QCVals.loc[experimentType,'SNPCount_median']-(5*QCVals.loc[experimentType,'SNPCount_mad'])):
                exceeded_thresholds['SNPCountQC'] = threshold
            if sampleFile.iloc[0,14] < (threshold := QCVals.loc[experimentType,'INDELCount_median']-(5*QCVals.loc[experimentType,'INDELCount_mad'])):
                exceeded_thresholds['INDELCountQC'] = threshold
            if sampleFile.iloc[0,15] < (threshold := QCVals.loc[experimentType,'AvgDepth_median']-(2*QCVals.loc[experimentType,'AvgDepth_mad'])):
                exceeded_thresholds['AvgDepthQC'] = threshold
            if sampleFile.iloc[0,17] < (threshold := QCVals.loc[experimentType,'AvgGQ_median']-(6*QCVals.loc[experimentType,'AvgGQ_mad'])):
                exceeded_thresholds['AvgGQQC'] = threshold
        elif ((experimentType=="HiFi") | (experimentType=="ONT"))
            if sampleFile.iloc[0,10] < (threshold := QCVals.loc[experimentType,'ReferenceCoverage_median']-(5*QCVals.loc[experimentType,'ReferenceCoverage_mad'])):
                exceeded_thresholds['ReferenceCoverageQC'] = threshold
            if sampleFile.iloc[0,11] > (threshold := QCVals.loc[experimentType,'ExonEnrichment_median']+(10*QCVals.loc[experimentType,'ExonEnrichment_mad'])):
                exceeded_thresholds['ExonEnrichmentQC'] = threshold
            if sampleFile.iloc[0,13] < (threshold := QCVals.loc[experimentType,'SNPCount_median']-(5*QCVals.loc[experimentType,'SNPCount_mad'])):
                exceeded_thresholds['SNPCountQC'] = threshold
            if sampleFile.iloc[0,14] < (threshold := QCVals.loc[experimentType,'INDELCount_median']-(5*QCVals.loc[experimentType,'INDELCount_mad'])):
                exceeded_thresholds['INDELCountQC'] = threshold
            if sampleFile.iloc[0,15] < (threshold := QCVals.loc[experimentType,'AvgDepth_median']-(2*QCVals.loc[experimentType,'AvgDepth_mad'])):
                exceeded_thresholds['AvgDepthQC'] = threshold
            if sampleFile.iloc[0,17] < (threshold := QCVals.loc[experimentType,'AvgGQ_median']-(6*QCVals.loc[experimentType,'AvgGQ_mad'])):
                exceeded_thresholds['AvgGQQC'] = threshold
    else: # panels
        if sampleFile.iloc[0,5] < (threshold := QCVals.loc[experimentType,'Read2Quality_median']-(5*QCVals.loc[experimentType,'Read2Quality_mad'])):
            exceeded_thresholds['Read2QualityQC'] = threshold
        if sampleFile.iloc[0,7] < (threshold := QCVals.loc[experimentType,'AlignmentRate_median']-(5*QCVals.loc[experimentType,'AlignmentRate_mad'])):
            exceeded_thresholds['AlignmentRateQC'] = threshold
        if sampleFile.iloc[0,8] < (threshold := QCVals.loc[experimentType,'ReadPairingRate_median']-(5*QCVals.loc[experimentType,'ReadPairingRate_mad'])):
            exceeded_thresholds['ReadPairingRateQC'] = threshold
        if sampleFile.iloc[0,12] < (threshold := QCVals.loc[experimentType,'TSTVRatio_median']-(10*QCVals.loc[experimentType,'TSTVRatio_mad'])):
            exceeded_thresholds['TSTVRatioQC'] = threshold
        if sampleFile.iloc[0,16] < (threshold := QCVals.loc[experimentType,'AvgQual_median']-(1*QCVals.loc[experimentType,'AvgQual_mad'])):
            exceeded_thresholds['AvgQualQC'] = threshold
        if sampleFile.iloc[0,18] < (threshold := QCVals.loc[experimentType,'HetHomRatio_median']-(5*QCVals.loc[experimentType,'HetHomRatio_mad'])):
            exceeded_thresholds['HetHomRatioQC'] = threshold
        if sampleFile.iloc[0,4] < (threshold := QCVals.loc[experimentType,'Read1Quality_median']-(5*QCVals.loc[experimentType,'Read1Quality_mad'])):
            exceeded_thresholds['Read1QualityQC'] = threshold
        if sampleFile.iloc[0,6] > (threshold := QCVals.loc[experimentType,'ReadQualityDiff_median']+(5*QCVals.loc[experimentType,'ReadQualityDiff_mad'])):
            exceeded_thresholds['ReadQualityDiffQC'] = threshold
        if sampleFile.iloc[0,10] < (threshold := QCVals.loc[experimentType,'ReferenceCoverage_median']-(2*QCVals.loc[experimentType,'ReferenceCoverage_mad'])):
            exceeded_thresholds['ReferenceCoverageQC'] = threshold
        if sampleFile.iloc[0,10] > (threshold := QCVals.loc[experimentType,'ReferenceCoverage_median']+(2*QCVals.loc[experimentType,'ReferenceCoverage_mad'])):
            exceeded_thresholds['ReferenceCoverageQC'] = threshold
        if sampleFile.iloc[0,11] < (threshold := QCVals.loc[experimentType,'ExonEnrichment_median']-(2*QCVals.loc[experimentType,'ExonEnrichment_mad'])):
            exceeded_thresholds['ExonEnrichmentQC'] = threshold
        if sampleFile.iloc[0,13] < (threshold := QCVals.loc[experimentType,'SNPCount_median']-(2*QCVals.loc[experimentType,'SNPCount_mad'])):
            exceeded_thresholds['SNPCountQC'] = threshold
        if sampleFile.iloc[0,14] < (threshold := QCVals.loc[experimentType,'INDELCount_median']-(2*QCVals.loc[experimentType,'INDELCount_mad'])):
            exceeded_thresholds['INDELCountQC'] = threshold
        if sampleFile.iloc[0,15] < (threshold := QCVals.loc[experimentType,'AvgDepth_median']-(1*QCVals.loc[experimentType,'AvgDepth_mad'])):
            exceeded_thresholds['AvgDepthQC'] = threshold
        if sampleFile.iloc[0,17] < (threshold := QCVals.loc[experimentType,'AvgGQ_median']-(3*QCVals.loc[experimentType,'AvgGQ_mad'])):
            exceeded_thresholds['AvgGQQC'] = threshold
    
    # Return CSV values if thereare any exceeded thresholds
    if exceeded_thresholds:
        output = ', '.join([f"{key} = {value}" for key, value in exceeded_thresholds.items()])
        print(output)
        sys.exit(1)

if __name__ == "__main__":
    main(sys.argv[1:])
