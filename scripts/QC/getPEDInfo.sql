SELECT s1.[SampleGenomicId]
      ,s1.[PatientGenomicId]
      ,s1.[MotherGenomicId]
      ,s1.[FatherGenomicId]
      ,s1.[FamilyGenomicId]
      ,s1.[SexType]
      ,s1.[PeddySites]
      ,s1.[FileType]
      ,s1.[LatestCohortRunIsDone]
      ,s1.[SampleRunIsDone]
      ,s2.[SampleGenomicId] as MotherSampleGenomicId
      ,s2.[PatientGenomicId] as MotherPatientGenomicId
      ,s3.[SampleGenomicId] as FatherSampleGenomicId
      ,s3.[PatientGenomicId] as FatherPatientGenomicId
  FROM [dbo].[SampleSummary] as s1
  LEFT OUTER JOIN [dbo].[SampleSummary] as s2
  ON  s1.MotherId = s2.PatientId
  LEFT OUTER JOIN [dbo].[SampleSummary] as s3
  ON  s1.FatherId = s3.PatientId
  WHERE s1.[CreatedDateTime] > '10/1/2018' and s1.[FileType] != 'VCF' and s1.[ExperimentTypeText] != 'MicroVCF' and s1.[ExperimentTypeText] != 'Targeted Panel' and s1.[LatestCohortRunIsDone] = 1 and s1.[SampleRunIsDone] = 1 and s1.[isPrimarySample] = 1 and (s2.[isPrimarySample] = 1 or s2.[isPrimarySample] is null) and (s3.[isPrimarySample] = 1 or s3.[isPrimarySample] is null)
  ORDER BY s1.[SampleGenomicId] desc
