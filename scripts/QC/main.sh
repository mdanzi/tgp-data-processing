QCModule() {

    set -euo pipefail

    /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -Q "select @@version" > /dev/null 2>&1 || true

    cd $TGP_HOME
    source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports s3_path_exists


    aws s3 cp s3://tgp-data-analysis/qc_cohorts.txt cohorts.txt --only-show-errors


    # establish variables
    numCPUs=$(cat /proc/cpuinfo | grep -m 1 "cpu cores" | awk -F': ' '{print $2}')
    numCPUs=$((numCPUs * 2))


    ## perform the relatedness calculations
    # get the necessary information for relatedness checking
    /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -i $REPO/scripts/QC/getPEDInfo.sql -o metastoreResponse.txt

    # groom the metastoreResponse.txt file a little bit
    tail -n +3 metastoreResponse.txt | head -n -2 > tmp; mv tmp metastoreResponse.txt 
    awk -v OFS='\t' '{print $5,$1,$13,$11,(1+$6),0}' metastoreResponse.txt | sed 's/NULL/0/g' > combined.ped
    awk '{print $7}' metastoreResponse.txt > peddyFilesList.txt
    awk '{print $1}' metastoreResponse.txt > sampleIDsList.txt
    rm metastoreResponse.txt
    # download and concatenate the peddy files
    set +eu
    while read FILE; do
        fileName=$(sed 's#^.*/##g' <<< ${FILE})
        EXISTS=$(aws s3 ls ${FILE} | grep ${fileName} | wc -l || true)
        if [[ ${EXISTS} -ne 0 ]]; then
            aws s3 cp ${FILE} . --only-show-errors
        fi
    done < peddyFilesList.txt
    set -eu
    while read SAMPLE; do
        EXISTS=$(ls ${SAMPLE}.peddySites.vcf | wc -l || true)
        if [[ ${EXISTS} -ne 0 ]]; then
            bgzip ${SAMPLE}.peddySites.vcf
            tabix ${SAMPLE}.peddySites.vcf.gz
        fi
    done < sampleIDsList.txt
    # merge the peddy files together in batches of 500 to avoid bcftools too-many-open-files error
    ls *.peddySites.vcf.gz | split -l 500 - individual_vcfs
    for i in individual_vcfs*; do 
        bcftools merge $(cat $i | tr '\n' ' ') > $i.vcf
        bgzip $i.vcf
        tabix $i.vcf.gz
    done
    bcftools merge -o combined.vcf individual_vcfs*.vcf.gz
    bgzip combined.vcf
    tabix combined.vcf.gz
    # run peddy
    conda activate py2
    python -m peddy -p ${numCPUs} --prefix combined combined.vcf.gz combined.ped
    conda deactivate

    # download the current QCVals.txt file
    aws s3 cp s3://tgp-public-assets/QCVals.txt . --only-show-errors

    while read cohortRunID; do
        # get the set of samples to be checked
        sed "s/<cohortRunID>/${cohortRunID}/" $REPO/scripts/QC/getSamplesOfInterest.sql > getSamplesOfInterest.sql
        /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -i getSamplesOfInterest.sql -o metastoreResponse.txt < /dev/null
        tail -n +3 metastoreResponse.txt | head -n -2 | awk -v OFS='\t' '{print $1,$2,$3}' > samplesOfInterest.txt
        rm metastoreResponse.txt
        # extract the subset of relatedness checking comparisons that we care about
        cut -f 1 samplesOfInterest.txt > sampleIDsOfInterest.txt
        EXISTS=$(grep -wFf sampleIDsOfInterest.txt combined.ped_check.csv | awk -F',' -v OFS='\t' '{if ($9>99){print $0}}' | wc -l || true)
        if [[ ${EXISTS} -ne 0 ]]; then 
            grep -wFf sampleIDsOfInterest.txt combined.ped_check.csv | awk -F',' -v OFS='\t' '{if ($9>99){print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15}}' > SamplesOfInterest.relatednessScores.txt
        else
            echo "" > SamplesOfInterest.relatednessScores.txt
        fi
        while read SAMPLEID SAMPLERUNID SEX; do
            set +eu
            aws s3 cp s3://tgp-sample-processing/${SAMPLERUNID,,}/${SAMPLEID}.fastqLevel.qc . --only-show-errors
            aws s3 cp s3://tgp-sample-processing/${SAMPLERUNID,,}/${SAMPLEID}.hg19.bamLevel.qc . --only-show-errors
            vcfQCPath="s3://tgp-sample-processing/${cohortRunID,,}"
            if s3_path_exists s3://tgp-sample-processing/${SAMPLERUNID,,}/${SAMPLEID}.vcfLevel.byChr.qc; then
                vcfQCPath="s3://tgp-sample-processing/${SAMPLERUNID,,}"
            fi
            aws s3 cp ${vcfQCPath}/${SAMPLEID}.vcfLevel.byChr.qc . --only-show-errors
            set -eu
            READNUMBER1="NULL"
            INCONSISTENTREADNUMBER="NULL"
            READQUALITY1="NULL"
            READQUALITY2="NULL"
            READQUALITYDIFF="NULL"
            ALIGNMENTRATE="NULL"
            READPAIRINGRATE="NULL"
            DUPLICATIONRATE="NULL"
            REFERENCECOVERAGE="NULL"
            EXONENRICHMENT="NULL"
            TSTVRATIO="NULL"
            SNPCOUNT="NULL"
            INDELCOUNT="NULL"
            AVGDEPTH="NULL"
            AVGQUAL="NULL"
            AVGGQ="NULL"
            HETHOMRATIO="NULL"
            GENDERMISMATCH="NULL"
            EXISTS=$(ls ${SAMPLEID}.fastqLevel.qc | wc -l || true)
            if [[ ${EXISTS} -eq 1 ]]; then
                # add the QC values to the SampleQuality table
                READNUMBER1=$(awk '{print $3}' ${SAMPLEID}.fastqLevel.qc | tail -n +2 | head -n 1)
                READNUMBER2=$(awk '{print $3}' ${SAMPLEID}.fastqLevel.qc | tail -n +3 | head -n 1)
                INCONSISTENTREADNUMBER=1
                if [[ ${READNUMBER1} -eq ${READNUMBER2} ]]; then
                    INCONSISTENTREADNUMBER=0
                fi
                READQUALITY1=$(awk '{print $5}' ${SAMPLEID}.fastqLevel.qc | tail -n +2 | head -n 1)
                READQUALITY2=$(awk '{print $5}' ${SAMPLEID}.fastqLevel.qc | tail -n +3 | head -n 1)
                READQUALITYDIFF=$(( READQUALITY1 - READQUALITY2 ))
            fi
            EXISTS=$(ls ${SAMPLEID}.hg19.bamLevel.qc | wc -l || true)
            if [[ ${EXISTS} -eq 1 ]]; then
                ALIGNMENTRATE=$(awk '{print $2}' ${SAMPLEID}.hg19.bamLevel.qc | tail -n +2 | head -n 1)
                READPAIRINGRATE=$(awk '{print $3}' ${SAMPLEID}.hg19.bamLevel.qc | tail -n +2 | head -n 1)
                DUPLICATIONRATE=$(awk '{print $4}' ${SAMPLEID}.hg19.bamLevel.qc | tail -n +2 | head -n 1)
                REFERENCECOVERAGE=$(awk '{print $6}' ${SAMPLEID}.hg19.bamLevel.qc | tail -n +2 | head -n 1)
                EXONENRICHMENT=$(awk '{print $7}' ${SAMPLEID}.hg19.bamLevel.qc | tail -n +2 | head -n 1)
                # if ReadPairingRate or DuplicationRate are not floating point numbers, set them back to NULL
                if [[ ! "$READPAIRINGRATE" =~ ^[0-9]+(\.[0-9]+)?$ ]]; then
                    READPAIRINGRATE="NULL"
                fi
                if [[ ! "$DUPLICATIONRATE" =~ ^[0-9]+(\.[0-9]+)?$ ]]; then
                    DUPLICATIONRATE="NULL"
                fi
            fi
            EXISTS=$(ls ${SAMPLEID}.vcfLevel.byChr.qc | wc -l || true)
            if [[ ${EXISTS} -eq 1 ]]; then
                TSTVRATIO=$(awk '{print $4}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                SNPCOUNT=$(awk '{print $5}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                INDELCOUNT=$(awk '{print $6}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                AVGDEPTH=$(awk '{print $7}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                AVGQUAL=$(awk '{print $8}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                AVGGQ=$(awk '{print $10}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                HETHOMRATIO=$(awk '{print $9}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                PREDICTEDGENDER=$(awk '{print $11}' ${SAMPLEID}.vcfLevel.byChr.qc | tail -n +2 | head -n 1)
                if [[ ${PREDICTEDGENDER} == "M" ]]; then
                    PREDICTEDGENDER="0"
                elif [[ ${PREDICTEDGENDER} == "F" ]]; then
                    PREDICTEDGENDER="1"
                fi
                GENDERMISMATCH=1
                if [[ ${PREDICTEDGENDER} == ${SEX} ]]; then
                    GENDERMISMATCH=0
                fi
            fi
            /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -Q "INSERT INTO SampleQuality (SampleGenomicID, ReadNumber, InconsistentReadNumber, Read1Quality, Read2Quality, ReadQualityDiff, AlignmentRate, ReadPairingRate, DuplicationRate, ReferenceCoverage, ExonEnrichment, TSTVRatio, SNPCount, INDELCount, AvgDepth, AvgQual, AvgGQ, HetHomRatio, GenderMismatch) VALUES (${SAMPLEID}, ${READNUMBER1}, ${INCONSISTENTREADNUMBER}, ${READQUALITY1}, ${READQUALITY2}, ${READQUALITYDIFF}, ${ALIGNMENTRATE}, ${READPAIRINGRATE}, ${DUPLICATIONRATE}, ${REFERENCECOVERAGE}, ${EXONENRICHMENT}, ${TSTVRATIO}, ${SNPCOUNT}, ${INDELCOUNT}, ${AVGDEPTH}, ${AVGQUAL}, ${AVGGQ}, ${HETHOMRATIO}, ${GENDERMISMATCH})" < /dev/null
            # check the QC values and set the 'QC Review' flag if needed:
            set +eu
            /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -Q "SELECT q.SampleGenomicId, s.ExperimentTypeText, q.ReadNumber, q.InconsistentReadNumber, q.Read1Quality, q.Read2Quality, q.ReadQualityDiff, q.AlignmentRate, q.ReadPairingRate, q.DuplicationRate, q.ReferenceCoverage, q.ExonEnrichment, q.TSTVRatio, q.SNPCount, q.INDELCount, q.AvgDepth, q.AvgQual, q.AvgGQ, q.HetHomRatio, q.GenderMismatch from SampleQuality as q inner join SampleSummary as s on q.SampleGenomicId=s.SampleGenomicId where q.SampleGenomicId=${SAMPLEID}" -o ${SAMPLEID}_QCVals.txt < /dev/null
            sed '3q;d' ${SAMPLEID}_QCVals.txt | sed 's/^[[:space:]]*//' | sed 's/ \+/\t/g' > tmp; mv tmp ${SAMPLEID}_QCVals.txt
            
            # checkQCVals.py will exit with err (1) and output the failed metrics in SQL SET syntax if any metrics are exceeded
            if ! python $REPO/scripts/QC/checkQCVals.py --input=${SAMPLEID}_QCVals.txt > failed_metrics.txt; then
                failed_metrics=$(<failed_metrics.txt)
                /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -Q "UPDATE SampleQuality SET ${failed_metrics} WHERE SampleGenomicId = ${SAMPLEID}" < /dev/null
            fi

            rm ${SAMPLEID}_QCVals.txt
            set -eu
            # get the rows of the relatedness table that involve this sample
            EXISTS=$(grep -wF "${SAMPLEID}" SamplesOfInterest.relatednessScores.txt | wc -l || true)
            if [[ ${EXISTS} -ne 0 ]]; then 
                grep -wF "${SAMPLEID}" SamplesOfInterest.relatednessScores.txt > thisSample.relatednessScores.txt
                # get rows of the table that involve its family members
                set +e
                cut -f 1 samplesOfInterest.txt | grep -vwF "${SAMPLEID}" > familyMembers.txt
                EXISTS=$(grep -wFf familyMembers.txt thisSample.relatednessScores.txt | wc -l || true)
                if [[ ${EXISTS} -ne 0 ]]; then
                    grep -wFf familyMembers.txt thisSample.relatednessScores.txt > thisSample.familyRelatednessScores.txt
                    while read sampleA sampleB ibs1 hetsA ibs2 rel hetsB ibs0 totalN pedParent pedRel predParent parentalError dupError relDiff keepVal; do
                        /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -Q "INSERT INTO SampleRelatedness (SampleGenomicId, SampleGenomicIdB, IBS1, HetsA, IBS2, RelScore, HetsB, IBS0, TotalN, PedigreeParent, PedigreeRelatedness, PredictedParent, ParentErrorPrediction, SampleDuplicationPrediction, RelScoreDifference) VALUES (${sampleA}, ${sampleB}, ${ibs1}, ${hetsA}, ${ibs2}, ${rel}, ${hetsB}, ${ibs0}, ${totalN}, '${pedParent}', ${pedRel}, '${predParent}', '${parentalError}', '${dupError}', ${relDiff})" < /dev/null
                    done < thisSample.familyRelatednessScores.txt
                fi
                EXISTS=$(grep -vwFf familyMembers.txt thisSample.relatednessScores.txt | awk -F'\t' -v OFS='\t' '{if ($6>0.05){print $0}}' | wc -l || true)
                if [[ ${EXISTS} -ne 0 ]]; then
                    grep -vwFf familyMembers.txt thisSample.relatednessScores.txt | awk -F'\t' -v OFS='\t' '{if ($6>0.05){print $0}}' > thisSample.nonFamilyRelatednessScores.txt
                    while read sampleA sampleB ibs1 hetsA ibs2 rel hetsB ibs0 totalN pedParent pedRel predParent parentalError dupError relDiff keepVal; do
                        /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -Q "INSERT INTO SampleRelatedness (SampleGenomicId, SampleGenomicIdB, IBS1, HetsA, IBS2, RelScore, HetsB, IBS0, TotalN, PedigreeParent, PedigreeRelatedness, PredictedParent, ParentErrorPrediction, SampleDuplicationPrediction, RelScoreDifference) VALUES (${sampleA}, ${sampleB}, ${ibs1}, ${hetsA}, ${ibs2}, ${rel}, ${hetsB}, ${ibs0}, ${totalN}, '${pedParent}', ${pedRel}, '${predParent}', '${parentalError}', '${dupError}', ${relDiff})" < /dev/null
                    done < thisSample.nonFamilyRelatednessScores.txt
                fi
                set -e
            fi
        done < samplesOfInterest.txt
    done < cohorts.txt

    ## Clean up
    # get current set of raw QC data
    /opt/mssql-tools/bin/sqlcmd -S $METASTORE_SERVER_URL,1433 -U $METASTORE_USERNAME -P $METASTORE_PASSWORD -d $METASTORE_DATABASE -Q "SELECT q.SampleGenomicId, s.ExperimentTypeText, q.ReadNumber, q.InconsistentReadNumber, q.Read1Quality, q.Read2Quality, q.ReadQualityDiff, q.AlignmentRate, q.ReadPairingRate, q.DuplicationRate, q.ReferenceCoverage, q.ExonEnrichment, q.TSTVRatio, q.SNPCount, q.INDELCount, q.AvgDepth, q.AvgQual, q.AvgGQ, q.HetHomRatio, q.GenderMismatch from SampleQuality as q inner join SampleSummary as s on q.SampleGenomicId=s.SampleGenomicId" -o metastoreResponse.txt
    tail -n +3 metastoreResponse.txt | head -n -2 | sed 's/Targeted Panel/TargetedPanel/g' | sed "s/ \+/\t/g" | sed "s/^\t//" | sed 's/TargetedPanel/Targeted Panel/g' > raw_QCVals.txt

    # update the median and MAD values in QCVals.txt
    python $REPO/scripts/QC/updateQCVals.py

    # upload to S3
    aws s3 cp QCVals.txt s3://tgp-public-assets/ --only-show-errors --acl public-read

}
