SELECT s.[SampleGenomicId], s.[LatestSampleRunIdText], s.[SexType]
  FROM [dbo].[SampleSummary] as s
  WHERE s.[LatestCohortRunIdText] = '<cohortRunID>'
  ORDER BY s.[SampleGenomicId] desc
