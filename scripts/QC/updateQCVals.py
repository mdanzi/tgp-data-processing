QCValsFile = 'QCVals.txt'
rawFile = 'raw_QCVals.txt'
import numpy
import pandas
## load the data
colNames=['ExperimentTypeText', 'ReadNumber', 'InconsistentReadNumber', 'Read1Quality', 'Read2Quality', 'ReadQualityDiff', 
'AlignmentRate', 'ReadPairingRate', 'DuplicationRate', 'ReferenceCoverage', 'ExonEnrichment', 
'TSTVRatio', 'SNPCount', 'INDELCount', 'AvgDepth', 'AvgQual', 'AvgGQ', 'HetHomRatio', 'GenderMismatch']
QCVals=pandas.read_csv(QCValsFile,sep='\t',low_memory=False,index_col=0)
rawVals=pandas.read_csv(rawFile,sep='\t',low_memory=False,index_col=0,header=None,names=colNames)

def meanAbsDev(df):
    return (df - df.mean()).abs().mean()

## recalculate QCVals for WES
experimentType='WES'
QCVals.loc[experimentType,'ReadNumber_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'].median()
QCVals.loc[experimentType,'ReadNumber_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'])
QCVals.loc[experimentType,'Read1Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'].median()
QCVals.loc[experimentType,'Read1Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'])
QCVals.loc[experimentType,'Read2Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'].median()
QCVals.loc[experimentType,'Read2Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'])
QCVals.loc[experimentType,'ReadQualityDiff_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'].median()
QCVals.loc[experimentType,'ReadQualityDiff_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'])
QCVals.loc[experimentType,'AlignmentRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'].median()
QCVals.loc[experimentType,'AlignmentRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'])
QCVals.loc[experimentType,'ReadPairingRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'].median()
QCVals.loc[experimentType,'ReadPairingRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'])
QCVals.loc[experimentType,'DuplicationRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'DuplicationRate'].median()
QCVals.loc[experimentType,'DuplicationRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'DuplicationRate'])
QCVals.loc[experimentType,'ReferenceCoverage_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'].median()
QCVals.loc[experimentType,'ReferenceCoverage_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'])
QCVals.loc[experimentType,'ExonEnrichment_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'].median()
QCVals.loc[experimentType,'ExonEnrichment_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'])
QCVals.loc[experimentType,'TSTVRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'].median()
QCVals.loc[experimentType,'TSTVRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'])
QCVals.loc[experimentType,'SNPCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'].median()
QCVals.loc[experimentType,'SNPCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'])
QCVals.loc[experimentType,'INDELCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'].median()
QCVals.loc[experimentType,'INDELCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'])
QCVals.loc[experimentType,'AvgDepth_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'].median()
QCVals.loc[experimentType,'AvgDepth_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'])
QCVals.loc[experimentType,'AvgQual_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'].median()
QCVals.loc[experimentType,'AvgQual_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'])
QCVals.loc[experimentType,'AvgGQ_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'].median()
QCVals.loc[experimentType,'AvgGQ_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'])
QCVals.loc[experimentType,'HetHomRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'].median()
QCVals.loc[experimentType,'HetHomRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'])
## recalculate QCVals for WGS
experimentType='WGS'
QCVals.loc[experimentType,'ReadNumber_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'].median()
QCVals.loc[experimentType,'ReadNumber_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'])
QCVals.loc[experimentType,'Read1Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'].median()
QCVals.loc[experimentType,'Read1Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'])
QCVals.loc[experimentType,'Read2Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'].median()
QCVals.loc[experimentType,'Read2Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'])
QCVals.loc[experimentType,'ReadQualityDiff_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'].median()
QCVals.loc[experimentType,'ReadQualityDiff_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'])
QCVals.loc[experimentType,'AlignmentRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'].median()
QCVals.loc[experimentType,'AlignmentRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'])
QCVals.loc[experimentType,'ReadPairingRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'].median()
QCVals.loc[experimentType,'ReadPairingRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'])
QCVals.loc[experimentType,'DuplicationRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'DuplicationRate'].median()
QCVals.loc[experimentType,'DuplicationRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'DuplicationRate'])
QCVals.loc[experimentType,'ReferenceCoverage_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'].median()
QCVals.loc[experimentType,'ReferenceCoverage_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'])
QCVals.loc[experimentType,'ExonEnrichment_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'].median()
QCVals.loc[experimentType,'ExonEnrichment_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'])
QCVals.loc[experimentType,'TSTVRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'].median()
QCVals.loc[experimentType,'TSTVRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'])
QCVals.loc[experimentType,'SNPCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'].median()
QCVals.loc[experimentType,'SNPCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'])
QCVals.loc[experimentType,'INDELCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'].median()
QCVals.loc[experimentType,'INDELCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'])
QCVals.loc[experimentType,'AvgDepth_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'].median()
QCVals.loc[experimentType,'AvgDepth_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'])
QCVals.loc[experimentType,'AvgQual_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'].median()
QCVals.loc[experimentType,'AvgQual_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'])
QCVals.loc[experimentType,'AvgGQ_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'].median()
QCVals.loc[experimentType,'AvgGQ_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'])
QCVals.loc[experimentType,'HetHomRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'].median()
QCVals.loc[experimentType,'HetHomRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'])
## recalculate QCVals for Panels
experimentType='Targeted Panel'
QCVals.loc[experimentType,'ReadNumber_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'].median()
QCVals.loc[experimentType,'ReadNumber_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'])
QCVals.loc[experimentType,'Read1Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'].median()
QCVals.loc[experimentType,'Read1Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'])
QCVals.loc[experimentType,'Read2Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'].median()
QCVals.loc[experimentType,'Read2Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'])
QCVals.loc[experimentType,'ReadQualityDiff_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'].median()
QCVals.loc[experimentType,'ReadQualityDiff_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'])
QCVals.loc[experimentType,'AlignmentRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'].median()
QCVals.loc[experimentType,'AlignmentRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'])
QCVals.loc[experimentType,'ReadPairingRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'].median()
QCVals.loc[experimentType,'ReadPairingRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'])
QCVals.loc[experimentType,'ReferenceCoverage_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'].median()
QCVals.loc[experimentType,'ReferenceCoverage_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'])
QCVals.loc[experimentType,'ExonEnrichment_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'].median()
QCVals.loc[experimentType,'ExonEnrichment_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'])
QCVals.loc[experimentType,'TSTVRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'].median()
QCVals.loc[experimentType,'TSTVRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'])
QCVals.loc[experimentType,'SNPCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'].median()
QCVals.loc[experimentType,'SNPCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'])
QCVals.loc[experimentType,'INDELCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'].median()
QCVals.loc[experimentType,'INDELCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'])
QCVals.loc[experimentType,'AvgDepth_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'].median()
QCVals.loc[experimentType,'AvgDepth_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'])
QCVals.loc[experimentType,'AvgQual_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'].median()
QCVals.loc[experimentType,'AvgQual_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'])
QCVals.loc[experimentType,'AvgGQ_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'].median()
QCVals.loc[experimentType,'AvgGQ_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'])
QCVals.loc[experimentType,'HetHomRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'].median()
QCVals.loc[experimentType,'HetHomRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'])
## recalculate QCVals for HiFi
experimentType='HiFi'
QCVals.loc[experimentType,'ReadNumber_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'].median()
QCVals.loc[experimentType,'ReadNumber_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'])
QCVals.loc[experimentType,'Read1Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'].median()
QCVals.loc[experimentType,'Read1Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'])
QCVals.loc[experimentType,'Read2Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'].median()
QCVals.loc[experimentType,'Read2Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'])
QCVals.loc[experimentType,'ReadQualityDiff_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'].median()
QCVals.loc[experimentType,'ReadQualityDiff_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'])
QCVals.loc[experimentType,'AlignmentRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'].median()
QCVals.loc[experimentType,'AlignmentRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'])
QCVals.loc[experimentType,'ReadPairingRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'].median()
QCVals.loc[experimentType,'ReadPairingRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'])
QCVals.loc[experimentType,'ReferenceCoverage_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'].median()
QCVals.loc[experimentType,'ReferenceCoverage_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'])
QCVals.loc[experimentType,'ExonEnrichment_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'].median()
QCVals.loc[experimentType,'ExonEnrichment_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'])
QCVals.loc[experimentType,'TSTVRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'].median()
QCVals.loc[experimentType,'TSTVRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'])
QCVals.loc[experimentType,'SNPCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'].median()
QCVals.loc[experimentType,'SNPCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'])
QCVals.loc[experimentType,'INDELCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'].median()
QCVals.loc[experimentType,'INDELCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'])
QCVals.loc[experimentType,'AvgDepth_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'].median()
QCVals.loc[experimentType,'AvgDepth_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'])
QCVals.loc[experimentType,'AvgQual_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'].median()
QCVals.loc[experimentType,'AvgQual_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'])
QCVals.loc[experimentType,'AvgGQ_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'].median()
QCVals.loc[experimentType,'AvgGQ_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'])
QCVals.loc[experimentType,'HetHomRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'].median()
QCVals.loc[experimentType,'HetHomRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'])
## recalculate QCVals for ONT
experimentType='ONT'
QCVals.loc[experimentType,'ReadNumber_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'].median()
QCVals.loc[experimentType,'ReadNumber_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadNumber'])
QCVals.loc[experimentType,'Read1Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'].median()
QCVals.loc[experimentType,'Read1Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read1Quality'])
QCVals.loc[experimentType,'Read2Quality_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'].median()
QCVals.loc[experimentType,'Read2Quality_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'Read2Quality'])
QCVals.loc[experimentType,'ReadQualityDiff_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'].median()
QCVals.loc[experimentType,'ReadQualityDiff_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadQualityDiff'])
QCVals.loc[experimentType,'AlignmentRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'].median()
QCVals.loc[experimentType,'AlignmentRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AlignmentRate'])
QCVals.loc[experimentType,'ReadPairingRate_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'].median()
QCVals.loc[experimentType,'ReadPairingRate_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReadPairingRate'])
QCVals.loc[experimentType,'ReferenceCoverage_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'].median()
QCVals.loc[experimentType,'ReferenceCoverage_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ReferenceCoverage'])
QCVals.loc[experimentType,'ExonEnrichment_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'].median()
QCVals.loc[experimentType,'ExonEnrichment_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'ExonEnrichment'])
QCVals.loc[experimentType,'TSTVRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'].median()
QCVals.loc[experimentType,'TSTVRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'TSTVRatio'])
QCVals.loc[experimentType,'SNPCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'].median()
QCVals.loc[experimentType,'SNPCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'SNPCount'])
QCVals.loc[experimentType,'INDELCount_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'].median()
QCVals.loc[experimentType,'INDELCount_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'INDELCount'])
QCVals.loc[experimentType,'AvgDepth_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'].median()
QCVals.loc[experimentType,'AvgDepth_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgDepth'])
QCVals.loc[experimentType,'AvgQual_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'].median()
QCVals.loc[experimentType,'AvgQual_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgQual'])
QCVals.loc[experimentType,'AvgGQ_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'].median()
QCVals.loc[experimentType,'AvgGQ_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'AvgGQ'])
QCVals.loc[experimentType,'HetHomRatio_median']=rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'].median()
QCVals.loc[experimentType,'HetHomRatio_mad']=meanAbsDev(rawVals.loc[rawVals['ExperimentTypeText']==experimentType,'HetHomRatio'])
QCVals.to_csv(QCValsFile,sep='\t',header=True,index=True,index_label="ExperimentType")

