###### this script is a record of how the lr_SVData.txt and lr_sv_GenoData.txt files were initially created. 
# the omim, gwas, and gnomad_constraint tables were downloaded and groomed prior to this script being run, as in main.sh
# pre-processing was done to the CoLoRSdb SV call set, which is not recorded fully here
# colorsdb file originates from the v1.0 release here: https://zenodo.org/records/11511513
# those SV calls were lifted over from GRCh38 to GRCh37 for use here


import numpy
import pandas
import pybedtools
SVDataFile = 'lr_SVData.txt'
GenoDataFile = 'lr_sv_GenoData.txt'
maxSize=50000000
# load annotations
geneLocations=pybedtools.BedTool('GRCh37.75.genes.bed')
genePaddedLocations=pybedtools.BedTool('GRCh37.75.genes2kbBuffer.bed')
exonLocations=pybedtools.BedTool('GRCh37.75.exons.gtf')
exonPaddedLocations=pybedtools.BedTool('GRCh37.75.exons2kbBuffer.gtf')
cdsLocations=pybedtools.BedTool('GRCh37.75.CDSs.gtf')
tssLocations=pybedtools.BedTool('GRCh37.75.TSSs2kbBuffer.gtf')
canonicalTranscriptLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts.bed')
canonicalTranscriptPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts2kbBuffer.bed')
canonicalExonLocations=pybedtools.BedTool('GRCh37.75.canonical.exons.gtf')
canonicalExonPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.exons2kbBuffer.gtf')
canonicalCDSLocations=pybedtools.BedTool('GRCh37.75.canonical.CDSs.gtf')
canonicalTSSLocations=pybedtools.BedTool('GRCh37.75.canonical.TSSs2kbBuffer.gtf')
enhancerLocations=pybedtools.BedTool('EncodeEnhancerSites.bed')
promoterLocations=pybedtools.BedTool('EncodePromoterSites.bed')
ctcfOnlyLocations=pybedtools.BedTool('EncodeCTCFOnlySites.bed')
gwasGeneData=pandas.read_csv('gwas_gene.txt',sep='\t',low_memory=False,quoting=3)
gwasGenesUnique=gwasGeneData.drop_duplicates(subset='geneName',keep='first')
gwasGenesUnique.loc[:,'gene_data']=gwasGenesUnique.apply(lambda x: '||'.join(gwasGeneData.loc[gwasGeneData['geneName']==x.geneName,'gene_data']) , axis=1).values
gwasGeneData=gwasGenesUnique
omimGeneData=pandas.read_csv('omim_data.txt',sep='\t',low_memory=False,quoting=3)
omimGeneData=omimGeneData.astype({'inheritance':str})
gnomadConstraintGeneData=pandas.read_csv('gnomad_constraint_data.txt',sep='\t',low_memory=False,quoting=3,na_values="\\N")
gnomadConstraintGeneData=gnomadConstraintGeneData.astype({'misZ':str,'pLI':str})

SVDataColNames=['chr','start','end','SVID','SVType','SVSize','sampleCount','homozygousSampleCount','AN','AC','AF','AC_Het','AC_HomAlt','HWE','ExcHet',
'genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded',
'isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped','gwasGeneData','omimGeneData','omim_inheritance','gnomadConstraintGeneData','gnomad_constraint_max_pli','gnomad_constraint_max_misz','phenocounts']

colorsdb=pandas.read_csv('CoLoRSdb.GRCh37.txt',sep='\t',low_memory=False,header=None,names=['chr','start','end','SVType','SVSize','AN','AC','AF','AC_Het','AC_HomAlt','HWE','ExcHet'])
colorsdb['SVID']=colorsdb.loc[:,'chr'].astype(str) + '_' + colorsdb.loc[:,'start'].astype(str) + '_' + colorsdb.loc[:,'end'].astype(str) + '_' + colorsdb.loc[:,'SVType'].astype(str) + '_' + colorsdb.loc[:,'SVSize'].astype(str)
stagingSVData=pandas.DataFrame(index=range(0,len(colorsdb)),columns=SVDataColNames)
stagingSVData=stagingSVData.astype({'AN':'Int64','AC':'Int64','omim_inheritance':'Int64'})
stagingSVData.loc[:,['chr','start','end','SVID','SVType','SVSize','AN','AC','AF','AC_Het','AC_HomAlt','HWE','ExcHet']]=colorsdb.loc[:,['chr','start','end','SVID','SVType','SVSize','AN','AC','AF','AC_Het','AC_HomAlt','HWE','ExcHet']]
stagingSVData.loc[:,['sampleCount','homozygousSampleCount','genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped']]=[0,0,'FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE']
# drop duplicates from stagingSVData
stagingSVData=stagingSVData.drop_duplicates(subset='SVID').reset_index(drop=True)
## add annotation data
stagingSVDataBT=pybedtools.BedTool.from_dataframe(stagingSVData)
intersectionCounts=stagingSVDataBT.intersect(geneLocations,c=True).intersect(genePaddedLocations,c=True).intersect(tssLocations,c=True).intersect(exonLocations,c=True).intersect(cdsLocations,c=True).intersect(exonPaddedLocations,c=True).intersect(canonicalTranscriptLocations,c=True).intersect(canonicalTranscriptPaddedLocations,c=True).intersect(canonicalTSSLocations,c=True).intersect(canonicalExonLocations,c=True).intersect(canonicalCDSLocations,c=True).intersect(canonicalExonPaddedLocations,c=True).to_dataframe(header=None,low_memory=False,index_col=False,names=SVDataColNames + ['genes','genesPadded','tss','exons','cds','exonsPadded','canonicalTranscripts','canonicalTranscriptsPadded','canonicalTSS','canonicalExons','canonicalCDS','canonicalExonsPadded'])
stagingSVData.loc[intersectionCounts.index[intersectionCounts['tss']>0],'isNearTSS']="TRUE"
stagingSVData.loc[intersectionCounts.index[intersectionCounts['exons']>0],'overlapsExon']="TRUE"
stagingSVData.loc[intersectionCounts.index[intersectionCounts['cds']>0],'overlapsCDS']="TRUE"
stagingSVData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0)],'isIntronic']="TRUE"
stagingSVData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0) & (intersectionCounts['exonsPadded']>0)],'isNearExon']="TRUE"
stagingSVData.loc[intersectionCounts.index[intersectionCounts['canonicalTSS']>0],'isNearCanonicalTSS']="TRUE"
stagingSVData.loc[intersectionCounts.index[intersectionCounts['canonicalExons']>0],'overlapsCanonicalExon']="TRUE"
stagingSVData.loc[intersectionCounts.index[intersectionCounts['canonicalCDS']>0],'overlapsCanonicalCDS']="TRUE"
stagingSVData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0)],'isCanonicalIntronic']="TRUE"
stagingSVData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0) & (intersectionCounts['canonicalExonsPadded']>0)],'isNearCanonicalExon']="TRUE"
## get list of overlapping genes
# get the closest gene for each SVID 
genesIntersectingSVs=stagingSVDataBT.intersect(geneLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
# get the subset of rows which have duplicate SVIDs 
multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
# get the list of SVID-geneName combinations
SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
# get the list of SVIDs that I will group by
SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
def f(chr, start, end):
	return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID.values)]
if len(SVIDsWithMultipleGenes)>0:
	stagingSVData.loc[index1,'genesOverlapped']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
# deal with the uniquely matched SVID-gene pairs
uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
stagingSVData.loc[index2,'genesOverlapped']=uniqueMatches.loc[index1,'geneName'].values
## get list of gwas results for overlapped genes
SVIDGeneNameCombinations2=SVIDGeneNameCombinations.merge(gwasGeneData,how='left',on='geneName')
SVIDGeneNameCombinations2=SVIDGeneNameCombinations2.loc[SVIDGeneNameCombinations2['gene_data'].notnull(),:]
SVIDsWithMultipleGenes=SVIDGeneNameCombinations2.loc[~SVIDGeneNameCombinations2.duplicated(keep='first',subset='SVID'),:]
def f(chr, start, end):
	return '||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'gene_data'])
index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
if len(SVIDsWithMultipleGenes)>0:
	stagingSVData.loc[index1,'gwasGeneData']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(gwasGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
stagingSVData.loc[index2,'gwasGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
# replace entries with more than 10 gwas associations with 'True'
stagingSVData.loc[stagingSVData.gwasGeneData.str.count('\|\|')>=10,'gwasGeneData']="TRUE"
## get list of omim results for overlapped genes
SVIDGeneNameCombinations2=SVIDGeneNameCombinations.merge(omimGeneData,how='left',on='geneName')
SVIDGeneNameCombinations2=SVIDGeneNameCombinations2.loc[SVIDGeneNameCombinations2['gene_data'].notnull(),:]
SVIDsWithMultipleGenes=SVIDGeneNameCombinations2.loc[~SVIDGeneNameCombinations2.duplicated(keep='first',subset='SVID'),:]
def f(chr, start, end):
	return '||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'gene_data'])
index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
if len(SVIDsWithMultipleGenes)>0:
	stagingSVData.loc[index1,'omimGeneData']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
stagingSVData.loc[index2,'omimGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
# replace entries with more than 50 overlapping omim genes with 'True'
stagingSVData.loc[stagingSVData.omimGeneData.str.count('\|\|')>=50,'omimGeneData']="TRUE"
## get omim inheritance results for overlapped genes
def f(chr, start, end):
	tmp='||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'inheritance'])
	if '5' in tmp:
		return 5
	elif (('1' in tmp) and ('2' in tmp)):
		return 5
	elif (('3' in tmp) or ('6' in tmp) or ('7' in tmp) or ('8' in tmp)):
		return 3
	elif '4' in tmp:
		return 4
	else:
		return int(max([int(s) for s in tmp.split('||')]))
index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
if len(SVIDsWithMultipleGenes)>0:
	stagingSVData.loc[index1,'omim_inheritance']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['inheritance'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
stagingSVData.loc[index2,'omim_inheritance']=uniqueMatches2.loc[index1,'inheritance'].astype('int').values
## get list of gnomad constraint results for overlapped genes
SVIDGeneNameCombinations2=SVIDGeneNameCombinations.merge(gnomadConstraintGeneData,how='left',on='geneName')
SVIDGeneNameCombinations2=SVIDGeneNameCombinations2.loc[SVIDGeneNameCombinations2['gene_data'].notnull(),:]
SVIDsWithMultipleGenes=SVIDGeneNameCombinations2.loc[~SVIDGeneNameCombinations2.duplicated(keep='first',subset='SVID'),:]
def f(chr, start, end):
	return '||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'gene_data'])
index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
if len(SVIDsWithMultipleGenes)>0:
	stagingSVData.loc[index1,'gnomadConstraintGeneData']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
stagingSVData.loc[index2,'gnomadConstraintGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
# replace entries with more than 50 overlapping genes with 'True'
stagingSVData.loc[stagingSVData.gnomadConstraintGeneData.str.count('\|\|')>=50,'gnomadConstraintGeneData']="TRUE"
## get gnomad max pLI and misZ results for overlapped genes
def f1(chr, start, end):
	tmp='||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'pLI'])
	return max([float(s) for s in tmp.split('||')])
def f2(chr, start, end):
	tmp='||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'misZ'])
	return max([float(s) for s in tmp.split('||')])
index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
if len(SVIDsWithMultipleGenes)>0:
	stagingSVData.loc[index1,'gnomad_constraint_max_pli']=SVIDsWithMultipleGenes.apply(lambda x: f1(x.chr,x.start,x.end), axis=1).values
	stagingSVData.loc[index1,'gnomad_constraint_max_misz']=SVIDsWithMultipleGenes.apply(lambda x: f2(x.chr,x.start,x.end), axis=1).values
uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['pLI'].notnull(),:]
index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
stagingSVData.loc[index2,'gnomad_constraint_max_pli']=uniqueMatches2.loc[index1,'pLI'].values
stagingSVData.loc[index2,'gnomad_constraint_max_misz']=uniqueMatches2.loc[index1,'misZ'].values
## get list of overlapping genes (padded)
# get the closest gene for each SVID 
genesIntersectingSVs=stagingSVDataBT.intersect(genePaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
if len(genesIntersectingSVs)>0:
	# get the subset of rows which have duplicate SVIDs 
	multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	# get the list of SVID-geneName combinations
	SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
	# get the list of SVIDs that I will group by
	SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
	def f(chr, start, end):
		return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
	index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
	if len(SVIDsWithMultipleGenes)>0:
		stagingSVData.loc[index1,'genesOverlappedPadded']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched SVID-gene pairs
	uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
	index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
	stagingSVData.loc[index2,'genesOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
## get list of overlapping canonical transcripts
# get the closest gene for each SVID 
genesIntersectingSVs=stagingSVDataBT.intersect(canonicalTranscriptLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
if len(genesIntersectingSVs)>0:
	# get the subset of rows which have duplicate SVIDs 
	multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	# get the list of SVID-geneName combinations
	SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
	# get the list of SVIDs that I will group by
	SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
	def f(chr, start, end):
		return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
	index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
	if len(SVIDsWithMultipleGenes)>0:
		stagingSVData.loc[index1,'canonicalTranscriptsOverlapped']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched SVID-gene pairs
	uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
	index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
	stagingSVData.loc[index2,'canonicalTranscriptsOverlapped']=uniqueMatches.loc[index1,'geneName'].values
## get list of overlapping canonical transcripts (padded)
# get the closest gene for each SVID 
genesIntersectingSVs=stagingSVDataBT.intersect(canonicalTranscriptPaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
if len(genesIntersectingSVs)>0:
	# get the subset of rows which have duplicate SVIDs 
	multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	# get the list of SVID-geneName combinations
	SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
	# get the list of SVIDs that I will group by
	SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
	def f(chr, start, end):
		return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
	index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
	if len(SVIDsWithMultipleGenes)>0:
		stagingSVData.loc[index1,'canonicalTranscriptsOverlappedPadded']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched SVID-gene pairs
	uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
	index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
	stagingSVData.loc[index2,'canonicalTranscriptsOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
## get list of overlapping encode enhancers
# get the closest enhancer for each SVID 
enhancersIntersectingSVs=stagingSVDataBT.intersect(enhancerLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['enhancerChr','enhancerStart','enhancerEnd','enhancerID'])
if len(enhancersIntersectingSVs)>0:
	# get the subset of rows which have duplicate SVIDs 
	multipleEntries=enhancersIntersectingSVs.loc[enhancersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	# get the list of SVID-enhancer combinations
	SVIDEnhancerCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','enhancerID']),:]
	# get the list of SVIDs that I will group by
	SVIDsWithMultipleEnhancers=SVIDEnhancerCombinations.loc[~SVIDEnhancerCombinations.duplicated(keep='first',subset='SVID'),:]
	# remove SVs over 50MB in size
	SVIDEnhancerCombinations=SVIDEnhancerCombinations.loc[SVIDEnhancerCombinations.SVSize<maxSize,:]
	SVIDsWithMultipleEnhancers=SVIDsWithMultipleEnhancers.loc[SVIDsWithMultipleEnhancers.SVSize<maxSize,:]
	def f(chr, start, end):
		return ','.join(SVIDEnhancerCombinations.loc[(SVIDEnhancerCombinations['chr']==chr) & (SVIDEnhancerCombinations['start']==start) & (SVIDEnhancerCombinations['end']==end),'enhancerID'])
	index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleEnhancers.SVID)]
	if len(SVIDsWithMultipleEnhancers)>0:
		stagingSVData.loc[index1,'encodeEnhancersOverlapped']=SVIDsWithMultipleEnhancers.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched SVID-enhancer pairs
	uniqueMatches=enhancersIntersectingSVs.loc[~enhancersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
	index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
	stagingSVData.loc[index2,'encodeEnhancersOverlapped']=uniqueMatches.loc[index1,'enhancerID'].values
## get list of overlapping encode promoters
# get the closest enhancer for each SVID 
promotersIntersectingSVs=stagingSVDataBT.intersect(promoterLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['promoterChr','promoterStart','promoterEnd','promoterID'])
if len(promotersIntersectingSVs)>0:
	# get the subset of rows which have duplicate SVIDs 
	multipleEntries=promotersIntersectingSVs.loc[promotersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	# get the list of SVID-promoter combinations
	SVIDPromoterCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','promoterID']),:]
	# get the list of SVIDs that I will group by
	SVIDsWithMultiplePromoters=SVIDPromoterCombinations.loc[~SVIDPromoterCombinations.duplicated(keep='first',subset='SVID'),:]
	# remove SVs over 50MB in size
	SVIDPromoterCombinations=SVIDPromoterCombinations.loc[SVIDPromoterCombinations.SVSize<maxSize,:]
	SVIDsWithMultiplePromoters=SVIDsWithMultiplePromoters.loc[SVIDsWithMultiplePromoters.SVSize<maxSize,:]
	def f(chr, start, end):
		return ','.join(SVIDPromoterCombinations.loc[(SVIDPromoterCombinations['chr']==chr) & (SVIDPromoterCombinations['start']==start) & (SVIDPromoterCombinations['end']==end),'promoterID'])
	index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultiplePromoters.SVID)]
	if len(SVIDsWithMultiplePromoters)>0:
		stagingSVData.loc[index1,'encodePromotersOverlapped']=SVIDsWithMultiplePromoters.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched SVID-promoter pairs
	uniqueMatches=promotersIntersectingSVs.loc[~promotersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
	index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
	stagingSVData.loc[index2,'encodePromotersOverlapped']=uniqueMatches.loc[index1,'promoterID'].values
## get list of overlapping encode ctcfOnly elements
# get the closest enhancer for each SVID 
ctcfOnlyIntersectingSVs=stagingSVDataBT.intersect(ctcfOnlyLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['ctcfOnlyChr','ctcfOnlyStart','ctcfOnlyEnd','ctcfOnlyID'])
if len(ctcfOnlyIntersectingSVs)>0:
	# get the subset of rows which have duplicate SVIDs 
	multipleEntries=ctcfOnlyIntersectingSVs.loc[ctcfOnlyIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	# get the list of SVID-ctcfOnly combinations
	SVIDCtcfOnlyCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','ctcfOnlyID']),:]
	# get the list of SVIDs that I will group by
	SVIDsWithMultipleCTCFOnlySites=SVIDCtcfOnlyCombinations.loc[~SVIDCtcfOnlyCombinations.duplicated(keep='first',subset='SVID'),:]
	# remove SVs over 50MB in size
	SVIDCtcfOnlyCombinations=SVIDCtcfOnlyCombinations.loc[SVIDCtcfOnlyCombinations.SVSize<maxSize,:]
	SVIDsWithMultipleCTCFOnlySites=SVIDsWithMultipleCTCFOnlySites.loc[SVIDsWithMultipleCTCFOnlySites.SVSize<maxSize,:]
	def f(chr, start, end):
		return ','.join(SVIDCtcfOnlyCombinations.loc[(SVIDCtcfOnlyCombinations['chr']==chr) & (SVIDCtcfOnlyCombinations['start']==start) & (SVIDCtcfOnlyCombinations['end']==end),'ctcfOnlyID'])
	index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleCTCFOnlySites.SVID)]
	if len(SVIDsWithMultipleCTCFOnlySites)>0:
		stagingSVData.loc[index1,'encodeCTCFSitesOverlapped']=SVIDsWithMultipleCTCFOnlySites.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
	# deal with the uniquely matched SVID-gene pairs
	uniqueMatches=ctcfOnlyIntersectingSVs.loc[~ctcfOnlyIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
	index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
	index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
	stagingSVData.loc[index2,'encodeCTCFSitesOverlapped']=uniqueMatches.loc[index1,'ctcfOnlyID'].values
## combine staging with the rest of SVData
SVData=pandas.concat([SVData,stagingSVData],axis=0,ignore_index=True)
SVData=SVData.sort_values(by=['chr','start'])
SVData.to_csv(SVDataFile,sep='\t',header=False,index=False,na_rep="\\N")

# initialize empty GenoData table 
GenoDataColNames=['SVID','SampleID','Genotype','phaseGroup','ref_reads','alt_reads','isImprecise','Filter']
GenoData=pandas.DataFrame({'SVID':numpy.nan,'SampleID':numpy.nan,'Genotype':numpy.nan,'phaseGroup':numpy.nan,'ref_reads':numpy.nan,'alt_reads':numpy.nan,'isImprecise':numpy.nan,'Filter':numpy.nan},index=[0])
GenoData.to_csv(GenoDataFile,sep='\t',header=False,index=False,na_rep="\\N")
