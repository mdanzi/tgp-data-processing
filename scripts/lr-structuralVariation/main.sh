parseLRSVs() {

    local inputFileS3=$1

    cd /working
    #cd $TGP_HOME

    aws s3 cp $inputFileS3 . --only-show-errors

    inputFile=$(basename $inputFileS3)

    # download the database tables
    aws s3 cp s3://tgp-annotation/lr_svariantindex/lr_SVData.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/lr_sv_genomic_data/lr_sv_GenoData.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/omim/omim.tsv . --only-show-errors
    aws s3 cp s3://tgp-annotation/gwas/gwas.tsv . --only-show-errors
    aws s3 cp s3://tgp-annotation/gnomad_constraint/gnomad_constraint.txt . --only-show-errors
    aws s3 cp s3://tgp-data-analysis/resources/dfam38_full.0.h5.gz /working/RepeatMasker/Libraries/famdb/ --only-show-errors
    gunzip /working/RepeatMasker/Libraries/famdb/dfam38_full.0.h5.gz

    # convert chromosome numbers back into names
    sed -i 's/^23/X/' lr_SVData.txt
    sed -i 's/^24/Y/' lr_SVData.txt
    sed -i 's/^25/MT/' lr_SVData.txt

    # transform updateable annotation tables into their aggregate formats
    # omim
    echo -e "geneName\tgene_data\tinheritance" > tmp_header
    cut -f 3 omim.tsv > tmp1
    cut -f 1,2,3,4,5,6,7 omim.tsv | sed 's/\t/<>/g' > tmp2
    cut -f 7 omim.tsv > tmp3
    paste tmp1 tmp2 tmp3 | cat tmp_header - > omim_data.txt
    rm tmp1 tmp2 tmp_header
    # gwas
    echo -e "geneName\tgene_data" > tmp_header
    cut -f 8 gwas.tsv > tmp1
    cut -f 3,4,5,6,7,8,9 gwas.tsv | sed 's/\t/<>/g' > tmp2
    paste tmp1 tmp2 | cat tmp_header - > gwas_gene.txt
    rm tmp1 tmp2 tmp_header
    # gnomad constraint (canonical transcripts only)
    echo -e "geneName\tgene_data\tmisZ\tpLI" > tmp_header
    # filter down to only canonical transcripts and keep first if there are multiple
    awk -F'\t' -v OFS='\t' '{if($3=="true"){print $0}}' gnomad_constraint.txt | sort -k1,1 -u > gnomad_constraint_filt.txt 
    cut -f 1 gnomad_constraint_filt.txt > tmp1
    awk -F'\t' -v OFS='\t' '{print $1,$22,$20,$4,$5,$9,$10,$14,$15,$21}' gnomad_constraint_filt.txt | sed 's/\t/<>/g' > tmp2
    cut -f 20,22 gnomad_constraint_filt.txt > tmp3
    paste tmp1 tmp2 tmp3 | cat tmp_header - > gnomad_constraint_data.txt
    rm tmp1 tmp2 tmp_header

    mkdir -p toUpload
    COUNT=0

    while IFS=$'\t' read -r fileFullName diagnosisName diagnosisCodeText; do
        COUNT=$((COUNT+1))
        fileName=$(sed 's#^.*/##g' <<< ${fileFullName})
        BASE=$(sed 's/\..*$//g' <<< ${fileName})
        if s3_path_exists $fileFullName && ! grep -qwF $BASE lr_sv_GenoData.txt; then
            aws s3 cp ${fileFullName} . --only-show-errors
            tabix ${fileName}
            if [[ "${fileName}" == "${BASE}.pbsv.hiphase.vcf.gz" ]]; then # pbsv-specific parsing
                # convert to tab-delimited format, remove TR_overlap results, and split AD into DR and DV
                bcftools query -f '%CHROM\t%POS\t%INFO/END\t%FIRST_ALT\t%FILTER\t%INFO/SVTYPE\t%INFO/SVLEN\t%INFO/IMPRECISE\t[%GT]\t[%AD]\t[%PF]\n' ${fileName} | grep -v 'TR_OVERLAP' | grep -vF './.' | grep -vF '0/0' | grep -vF '0|0' | tr ',' '\t' | awk -F'\t' -v OFS='\t' -v sampleID=${BASE} '{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,sampleID}' > ${BASE}.SV.parsed.txt
            elif [[ "${fileName}" == "${BASE}.Sniffles.vcf.gz" ]]; then # Sniffles-specific parsing
                bcftools query -f '%CHROM\t%POS\t%INFO/END\t%FIRST_ALT\t%FILTER\t%INFO/SVTYPE\t%INFO/SVLEN\t%INFO/IMPRECISE\t[%GT]\t[%DR]\t[%DV]\n' ${fileName} | grep -vF './.' | grep -vF '0/0' | grep -vF '0|0' | awk -F'\t' -v OFS='\t' -v sampleID=${BASE} '{print $0,sampleID}' > ${BASE}.SV.parsed.txt
            else
                echo "Error: Unknown file format for ${fileName}. Expected either .pbsv.hiphase.vcf.gz or .Sniffles.vcf.gz" >&2
                exit 1
            fi 
            # remove SVs from undesired chromosomes
            grep -v '^GL' ${BASE}.SV.parsed.txt | grep -v '^hs37' > tmp.txt; mv tmp.txt ${BASE}.SV.parsed.txt
            # classify inserted sequences with RepMasker and tack that information on 
            awk -F'\t' -v OFS='\t' '{if(($6=="INS") && ($4!="<INS>")){print ">"$1"_"$2"_"$3"_"$6"_"$7"\n"$4}}' ${BASE}.SV.parsed.txt > ${BASE}.INS.fa
            /working/RepeatMasker/RepeatMasker -species human ${BASE}.INS.fa
            tail -n +4 ${BASE}.INS.fa.out | awk -v OFS='\t' '{print $5,$10,$11}' > ${BASE}.INS.fa.out.txt
            # annotate novel variants and update database tables
            python $REPO/scripts/lr-structuralVariation/parseLRSVs.py --sample=${BASE}.SV.parsed.txt --sampleInsertions=${BASE}.INS.fa.out.txt --phenoCode="${diagnosisCodeText}" --phenoName="${diagnosisName}"
            # every other sample, upload the data tables just in case of a crash later
            if [[ $((COUNT % 2)) -eq 0 ]]; then
                sed 's/^X/23/' lr_SVData.txt | sed 's/^Y/24/' | sed 's/^MT/25/' > toUpload/lr_SVData.txt
                aws s3 cp toUpload/lr_SVData.txt s3://tgp-annotation/lr_svariantindex/ --only-show-errors
                aws s3 cp lr_sv_GenoData.txt s3://tgp-annotation/lr_sv_genomic_data/ --only-show-errors
            fi
            rm ${fileName}
            rm ${BASE}.SV.parsed.txt
        fi
    done < $inputFile

    # convert chromosome names back into numbers
    sed -i 's/^X/23/' lr_SVData.txt
    sed -i 's/^Y/24/' lr_SVData.txt
    sed -i 's/^MT/25/' lr_SVData.txt

    # upload the database tables
    aws s3 cp lr_SVData.txt s3://tgp-annotation/lr_svariantindex/ --only-show-errors
    aws s3 cp lr_sv_GenoData.txt s3://tgp-annotation/lr_sv_genomic_data/ --only-show-errors

}
