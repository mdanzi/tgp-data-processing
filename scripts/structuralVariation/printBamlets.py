
import sys, getopt, os

def main ( argv ):
	vcfFile = ''
	bamFile = ''
	outDir = '.'
	try:
		opts, args = getopt.getopt(argv,"h",["vcf=","bam=","out=","help"])
	except getopt.GetoptError:
		print('printBamlet.py --vcf=<VcfFile> --bam=<BamFile> --out=<OutputDir>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--vcf'):
			vcfFile=arg
		elif opt in ('--bam'):
			bamFile=arg
		elif opt in ('--out'):
			outDir=arg
			try: 
				os.makedirs(outDir)
			except OSError:
				if not os.path.isdir(outDir):
					raise
		elif opt in ('-h','--help'):
			print('printBamlet.py --vcf=<VcfFile> --bam=<BamFile> --out=<OutputDir>')
			sys.exit()
		else:
			print('printBamlet.py --vcf=<VcfFile> --bam=<BamFile> --out=<OutputDir>')
			sys.exit()
	import vcf
	import vcf.utils
	import pysam
	vcfIn=vcf.Reader(open(vcfFile,'r'))
	bamIn=pysam.AlignmentFile(bamFile,'rb')
	for record in vcfIn:
		if (not record.FILTER) or (record.FILTER[0]=="Unknown"):
			if record.INFO['SVTYPE']!='BND':
				if (record.INFO['END']-record.POS)<50000:
					start=record.POS-2000
					if start<1:
						start=1
					stop=record.INFO['END']+2000
					bamletOut=pysam.AlignmentFile(outDir + "/chr" + record.CHROM + "-" + str(record.POS) + "-" + str(record.INFO['END']) + ".bam",'wb',template=bamIn)
					for read in bamIn.fetch(record.CHROM,start,stop):
						bamletOut.write(read)
					bamletOut.close()
			else:
				start=record.POS-2000
				if start<1:
					start=1
				stop=record.POS+2001
				bamletOut=pysam.AlignmentFile(outDir + "/chr" + record.CHROM + "-" + str(record.POS) + "-" + str(record.POS+1) + ".bam",'wb',template=bamIn)
				for read in bamIn.fetch(record.CHROM,start,stop):
					bamletOut.write(read)
				bamletOut.close()
	return

if __name__ == "__main__":
	main(sys.argv[1:])