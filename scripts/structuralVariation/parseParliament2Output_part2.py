import sys, getopt, os

def main ( argv ):
	sampleFile = ''
	phenoCode = ''
	phenoName = ''
	try:
		opts, args = getopt.getopt(argv,"h",["sample=","phenoCode=","phenoName=","help"])
	except getopt.GetoptError:
		print('parseParliament2Outupt_part2.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--sample'):
			sampleFile=arg
		elif opt in ('--phenoCode'):
			phenoCode=arg
		elif opt in ('--phenoName'):
			phenoName=arg
		elif opt in ('-h','--help'):
			print('parseParliament2Outupt_part2.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
			sys.exit()
		else:
			print('parseParliament2Outupt_part2.py --sample=<SampleFile> --phenoCode=<phenoCode> --phenoName=<phenoName>')
			sys.exit()
	import numpy
	import pandas
	import pybedtools
	SVDataFile = 'SVData.txt'
	GenoDataFile = 'GenoData.txt'
	maxSize=5000000
	# load annotations
	geneLocations=pybedtools.BedTool('GRCh37.75.genes.bed')
	genePaddedLocations=pybedtools.BedTool('GRCh37.75.genes2kbBuffer.bed')
	exonLocations=pybedtools.BedTool('GRCh37.75.exons.gtf')
	exonPaddedLocations=pybedtools.BedTool('GRCh37.75.exons2kbBuffer.gtf')
	cdsLocations=pybedtools.BedTool('GRCh37.75.CDSs.gtf')
	tssLocations=pybedtools.BedTool('GRCh37.75.TSSs2kbBuffer.gtf')
	canonicalTranscriptLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts.bed')
	canonicalTranscriptPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.transcripts2kbBuffer.bed')
	canonicalExonLocations=pybedtools.BedTool('GRCh37.75.canonical.exons.gtf')
	canonicalExonPaddedLocations=pybedtools.BedTool('GRCh37.75.canonical.exons2kbBuffer.gtf')
	canonicalCDSLocations=pybedtools.BedTool('GRCh37.75.canonical.CDSs.gtf')
	canonicalTSSLocations=pybedtools.BedTool('GRCh37.75.canonical.TSSs2kbBuffer.gtf')
	enhancerLocations=pybedtools.BedTool('EncodeEnhancerSites.bed')
	promoterLocations=pybedtools.BedTool('EncodePromoterSites.bed')
	ctcfOnlyLocations=pybedtools.BedTool('EncodeCTCFOnlySites.bed')
	gwasGeneData=pandas.read_csv('gwas_gene.txt',sep='\t',low_memory=False,quoting=3)
	gwasGenesUnique=gwasGeneData.drop_duplicates(subset='geneName',keep='first')
	gwasGenesUnique.loc[:,'gene_data']=gwasGenesUnique.apply(lambda x: '||'.join(gwasGeneData.loc[gwasGeneData['geneName']==x.geneName,'gene_data']) , axis=1).values
	gwasGeneData=gwasGenesUnique
	omimGeneData=pandas.read_csv('omim_data.txt',sep='\t',low_memory=False,quoting=3)
	omimGeneData=omimGeneData.astype({'inheritance':str})
	gnomadConstraintGeneData=pandas.read_csv('gnomad_constraint_data.txt',sep='\t',low_memory=False,quoting=3,na_values="\\N")
	gnomadConstraintGeneData=gnomadConstraintGeneData.astype({'misZ':str,'pLI':str})
	# load the data
	sampleInput=pybedtools.BedTool(sampleFile)
	SVData=pybedtools.BedTool(SVDataFile)
	GenoDataColNames=['SVID','SampleID','Genotype','thisSampleStart','thisSampleEnd','breakdancer','breakseq','cnvnator','delly','lumpy','manta','bnd_origin_chr','bnd_origin_pos','bnd_origin_gene', 'bnd_origin_overlapsExon']
	GenoData=pandas.read_csv(GenoDataFile,sep='\t',header=None,low_memory=False,names=GenoDataColNames,na_values="\\N",dtype={'SVID':str,'SampleID':'Int64','Genotype':int,'thisSampleStart':'Int64','thisSampleEnd':'Int64','breakdancer':int,'breakseq':int,'cnvnator':int,'delly':int,'lumpy':int,'manta':int,'bnd_origin_chr':'Int64','bnd_origin_pos':'Int64','bnd_origin_gene':str,'bnd_origin_overlapsExon':str})
	SVDataColNames=['SVDataChr','SVDataStart','SVDataEnd','SVDataSVID','SVDataSVType','SVDataSVSize','SVDataSampleCount','SVDataHomozygousSampleCount','Filter','Evidence','Protein_Coding_LOF','Protein_Coding_DUP_LOF','Protein_coding_Copy_Gain',
	'Protein_coding_DUP_Partial','Protein_Coding_MSV_Exon_OVR','Protein_Coding_Intronic','Protein_Coding_INV_Span','Protein_Coding_UTR','Protein_Coding_Nearest_TSS','Protein_Coding_Intergenic','Protein_Coding_Promoter','AN','AC','AF',
	'FREQ_HOMREF','FREQ_HET','FREQ_HOMALT','genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','isNearCanonicalTSS',
	'overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped','gwasGeneData','omimGeneData','omim_inheritance','gnomadConstraintGeneData','gnomad_constraint_max_pli','gnomad_constraint_max_misz','phenocounts']
	matchingPairs=sampleInput.intersect(SVData,wo=True,f=0.95,F=0.95,sorted=True).to_dataframe(names=['sampleChr','sampleStart','sampleEnd','sampleSVID','sampleName','sampleSVType','sampleSVSize','sampleGenotype','breakdancer','breakseq','cnvnator','delly','lumpy','manta','bnd_origin_chr','bnd_origin_pos'] + SVDataColNames + ['bpOverlap'],index_col=False,low_memory=False,na_values="\\N")
	matchingPairs2=matchingPairs.loc[matchingPairs.sampleSVType==matchingPairs.SVDataSVType,:]
	SVDataColNames=['chr','start','end','SVID','SVType','SVSize','sampleCount','homozygousSampleCount','Filter','Evidence','Protein_Coding_LOF','Protein_Coding_DUP_LOF','Protein_coding_Copy_Gain','Protein_coding_DUP_Partial',
	'Protein_Coding_MSV_Exon_OVR','Protein_Coding_Intronic','Protein_Coding_INV_Span','Protein_Coding_UTR','Protein_Coding_Nearest_TSS','Protein_Coding_Intergenic','Protein_Coding_Promoter','AN','AC','AF',
	'FREQ_HOMREF','FREQ_HET','FREQ_HOMALT','genesOverlapped','genesOverlappedPadded','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded',
	'isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped','gwasGeneData','omimGeneData','omim_inheritance','gnomadConstraintGeneData','gnomad_constraint_max_pli','gnomad_constraint_max_misz','phenocounts']
	sampleInput=pandas.read_csv(sampleFile,sep='\t',header=None,low_memory=False,names=['chr','start','end','SVID','SampleID','SVType','SVSize','Genotype','breakdancer','breakseq','cnvnator','delly','lumpy','manta','bnd_origin_chr','bnd_origin_pos'],dtype={'chr':str,'start':'Int64','end':'Int64','SVID':str,'SampleID':'Int64','SVType':str,'SVSize':float,'Genotype':int,'breakdancer':int,'breakseq':int,'cnvnator':int,'delly':int,'lumpy':int,'manta':int,'bnd_origin_chr':str,'bnd_origin_pos':'Int64'})
	variantsNotInSVData=sampleInput.loc[~sampleInput.SVID.isin(matchingPairs2.sampleSVID),:]
	# remove any duplicates
	variantsNotInSVData=variantsNotInSVData.drop_duplicates(subset='SVID',keep='first')
	matchesWithDuplicateSVDataSVIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='SVDataSVID',keep=False),:]
	duplicateSVDataSVIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='SVDataSVID',keep='first'),'SVDataSVID'].unique()
	blackListIndices=[]
	for k in range(0,len(duplicateSVDataSVIDs)):
		tmp=matchesWithDuplicateSVDataSVIDs.loc[matchesWithDuplicateSVDataSVIDs.SVDataSVID==duplicateSVDataSVIDs[k],:]
		# check if any match perfectly
		perfectMatch=-1
		for l in range(0,len(tmp)):
			if ((tmp.iloc[l].sampleSVID==tmp.iloc[l].SVDataSVID) or ((tmp.iloc[l].sampleStart==tmp.iloc[l].SVDataStart) & (tmp.iloc[l].sampleEnd==tmp.iloc[l].SVDataEnd))):
				perfectMatch=l
				break
		if (perfectMatch==-1):
			blackListIndices.append(tmp.index[tmp['sampleSVSize'] != numpy.min(tmp.sampleSVSize)])
			# manage possible ties for smallest SVSize
			tmp2=tmp.loc[tmp['sampleSVSize'] == numpy.min(tmp.sampleSVSize),:]
			if (len(tmp2)>1):
				blackListIndices.append(tmp2.iloc[1:].index)
		else:
			blackListIndices.append(tmp.index[tmp['sampleSVID'] != tmp.iloc[perfectMatch].sampleSVID])
	if (len(blackListIndices)>0):
		matchingPairs2=matchingPairs2.drop(numpy.concatenate(blackListIndices))
	blackListIndices=[]
	matchesWithDuplicateSampleSVIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='sampleSVID',keep=False),:]
	duplicateSampleSVIDs=matchingPairs2.loc[matchingPairs2.duplicated(subset='sampleSVID',keep='first'),'sampleSVID'].unique()
	# go through sites where one SV from this sample matched multiple SVs in SVData
	for k in range(0,len(duplicateSampleSVIDs)):
		tmp=matchesWithDuplicateSampleSVIDs.loc[matchesWithDuplicateSampleSVIDs.sampleSVID==duplicateSampleSVIDs[k],:]
		# check if any match perfectly
		perfectMatch=-1
		for l in range(0,len(tmp)):
			if ((tmp.iloc[l].sampleStart==tmp.iloc[l].SVDataStart) & (tmp.iloc[l].sampleEnd==tmp.iloc[l].SVDataEnd)):
				perfectMatch=l
				break
		if (perfectMatch==-1):
			# check if any of the SVData entries matching this sample SV are from gnomAD
			gnomADSites=tmp.loc[tmp['AN'].notnull(),:]
			if (len(gnomADSites)==1):
				blackListIndices.append(tmp.index[tmp['AN'].isnull()])
			elif (len(gnomADSites)>1):
				# pick the site with the best match, add the rest to the blacklist
				sum=1000000000
				idx=-1
				for l in range(0,len(gnomADSites)):
					tmpSum=abs(gnomADSites.iloc[l].sampleStart-gnomADSites.iloc[l].SVDataStart) + abs(gnomADSites.iloc[l].sampleEnd-gnomADSites.iloc[l].SVDataEnd)
					if (tmpSum<sum):
						sum=tmpSum
						idx=l
				blackListIndices.append(tmp.index[tmp.index!=gnomADSites.index[idx]])
			else:
				blackListIndices.append(tmp.index[tmp['SVDataSVSize'] != numpy.min(tmp.SVDataSVSize)])
				# manage possible ties for smallest SVSize
				tmp2=tmp.loc[tmp['SVDataSVSize'] == numpy.min(tmp.SVDataSVSize),:]
				if (len(tmp2)>1):
					blackListIndices.append(tmp2.iloc[1:].index)
		else:
			blackListIndices.append(tmp.index[tmp['SVDataSVID'] != tmp.iloc[perfectMatch].SVDataSVID])
	if (len(blackListIndices)>0):
		matchingPairs2=matchingPairs2.drop(numpy.concatenate(blackListIndices))
	# update SVID, start/end coordinates, and sampleCount values in SVData for non-gnomAD sites
	SVData=pandas.read_csv('SVData.txt',sep='\t',header=None,low_memory=False,index_col=False,names=SVDataColNames,na_values="\\N",dtype={'AN':'Int64','AC':'Int64','omim_inheritance':'Int64'})
	tmp1=sampleInput.loc[sampleInput.SVID.isin(matchingPairs2.sampleSVID)]
	tmp1=tmp1.assign(sort_cat=pandas.Categorical(tmp1['SVID'],categories=matchingPairs2.sampleSVID,ordered=True))
	i=tmp1.sort_values('sort_cat').index
	tmp2=SVData.loc[SVData.SVID.isin(matchingPairs2.SVDataSVID)]
	tmp2=tmp2.assign(sort_cat=pandas.Categorical(tmp2['SVID'],categories=matchingPairs2.SVDataSVID,ordered=True))
	j=tmp2.sort_values('sort_cat').index
	sampleInput.loc[i,'SVID']=matchingPairs2.SVDataSVID.values
	SVData.loc[j,'sampleCount']+=1
	SVData.loc[j,'homozygousSampleCount']+=sampleInput.loc[i,'Genotype'].values.astype(int)-1
	# only change the following values for the non-gnomAD sites
	j=tmp2.loc[tmp2['AN'].isnull(),:].sort_values('sort_cat').index
	if (len(j)>0):
		SVData.loc[j,'start']=numpy.round((((SVData.loc[j,'sampleCount']-1).values*matchingPairs2.loc[matchingPairs2.AN.isnull(),'SVDataStart'].values)+matchingPairs2.loc[matchingPairs2.AN.isnull(),'sampleStart'].values)/SVData.loc[j,'sampleCount'].values).astype('int')
		SVData.loc[j,'end']=numpy.round((((SVData.loc[j,'sampleCount']-1).values*matchingPairs2.loc[matchingPairs2.AN.isnull(),'SVDataEnd'].values)+matchingPairs2.loc[matchingPairs2.AN.isnull(),'sampleEnd'].values)/SVData.loc[j,'sampleCount'].values).astype('int')
		SVData.loc[j,'SVSize']=(((SVData.loc[j,'sampleCount']-1).values*SVData.loc[j,'SVSize'].values)+matchingPairs2.loc[matchingPairs2.AN.isnull(),'sampleSVSize'].values)/SVData.loc[j,'sampleCount'].values
	# remove any rows from sampleInput that are in neither variantsNotInSVData nor matchingPairs2
	sampleInput=sampleInput.loc[((sampleInput['SVID'].isin(matchingPairs2.SVDataSVID.values)) | (sampleInput['SVID'].isin(variantsNotInSVData.SVID.values))),:]
	# remove any duplicates from sampleInput
	sampleInput=sampleInput.drop_duplicates(subset='SVID',keep='first')
	# deal with variantsNotInSVData (pre-fill annotation values as FALSE)
	# need to add all the other columns from new annotations
	if len(variantsNotInSVData)>0:
		stagingSVData=pandas.DataFrame(index=range(0,len(variantsNotInSVData)),columns=SVData.columns.values)
		stagingSVData=stagingSVData.astype({'AN':'Int64','AC':'Int64','omim_inheritance':'Int64'})
		stagingSVData.loc[:,['chr','start','end','SVID','SVType', 'SVSize']]=variantsNotInSVData[['chr','start','end','SVID','SVType', 'SVSize']].values
		stagingSVData.loc[:,['sampleCount','isNearTSS','overlapsExon','overlapsCDS','isIntronic','isNearExon','isNearCanonicalTSS','overlapsCanonicalExon','overlapsCanonicalCDS','isCanonicalIntronic','isNearCanonicalExon']]=[1,'FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE']
		stagingSVData.loc[:,['genesOverlapped','genesOverlappedPadded','canonicalTranscriptsOverlapped','canonicalTranscriptsOverlappedPadded','encodeEnhancersOverlapped','encodePromotersOverlapped','encodeCTCFSitesOverlapped']]=[numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan,numpy.nan]
		stagingSVData.loc[:,'homozygousSampleCount']=variantsNotInSVData.Genotype.values.astype(int)-1
		# check for duplicate SVIDs
		if len(stagingSVData.loc[stagingSVData.SVID.isin(SVData.SVID),'SVID'])>0:
			for k in stagingSVData.index[stagingSVData.SVID.isin(SVData.SVID)]:
				thisSVID=stagingSVData.loc[k,'SVID']
				val=len(SVData.loc[SVData['SVID'].str.contains(thisSVID),:])+1
				sampleInput.loc[sampleInput['SVID']==thisSVID,'SVID']=thisSVID + "-" + str(val)
				stagingSVData.loc[k,'SVID']=thisSVID + "-" + str(val)
		## add annotation data
		stagingSVDataBT=pybedtools.BedTool.from_dataframe(stagingSVData)
		intersectionCounts=stagingSVDataBT.intersect(geneLocations,c=True).intersect(genePaddedLocations,c=True).intersect(tssLocations,c=True).intersect(exonLocations,c=True).intersect(cdsLocations,c=True).intersect(exonPaddedLocations,c=True).intersect(canonicalTranscriptLocations,c=True).intersect(canonicalTranscriptPaddedLocations,c=True).intersect(canonicalTSSLocations,c=True).intersect(canonicalExonLocations,c=True).intersect(canonicalCDSLocations,c=True).intersect(canonicalExonPaddedLocations,c=True).to_dataframe(header=None,low_memory=False,index_col=False,names=SVDataColNames + ['genes','genesPadded','tss','exons','cds','exonsPadded','canonicalTranscripts','canonicalTranscriptsPadded','canonicalTSS','canonicalExons','canonicalCDS','canonicalExonsPadded'])
		stagingSVData.loc[intersectionCounts.index[intersectionCounts['tss']>0],'isNearTSS']="TRUE"
		stagingSVData.loc[intersectionCounts.index[intersectionCounts['exons']>0],'overlapsExon']="TRUE"
		stagingSVData.loc[intersectionCounts.index[intersectionCounts['cds']>0],'overlapsCDS']="TRUE"
		stagingSVData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0)],'isIntronic']="TRUE"
		stagingSVData.loc[intersectionCounts.index[(intersectionCounts['genes']>0) & (intersectionCounts['exons']==0) & (intersectionCounts['exonsPadded']>0)],'isNearExon']="TRUE"
		stagingSVData.loc[intersectionCounts.index[intersectionCounts['canonicalTSS']>0],'isNearCanonicalTSS']="TRUE"
		stagingSVData.loc[intersectionCounts.index[intersectionCounts['canonicalExons']>0],'overlapsCanonicalExon']="TRUE"
		stagingSVData.loc[intersectionCounts.index[intersectionCounts['canonicalCDS']>0],'overlapsCanonicalCDS']="TRUE"
		stagingSVData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0)],'isCanonicalIntronic']="TRUE"
		stagingSVData.loc[intersectionCounts.index[(intersectionCounts['canonicalTranscripts']>0) & (intersectionCounts['canonicalExons']==0) & (intersectionCounts['canonicalExonsPadded']>0)],'isNearCanonicalExon']="TRUE"
		## get list of overlapping genes
		# get the closest gene for each SVID 
		genesIntersectingSVs=stagingSVDataBT.intersect(geneLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		if len(genesIntersectingSVs)>0:
			# get the subset of rows which have duplicate SVIDs 
			multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			# get the list of SVID-geneName combinations
			SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'genesOverlapped']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			# deal with the uniquely matched SVID-gene pairs
			uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
			stagingSVData.loc[index2,'genesOverlapped']=uniqueMatches.loc[index1,'geneName'].values
			## get list of gwas results for overlapped genes
			SVIDGeneNameCombinations2=SVIDGeneNameCombinations.merge(gwasGeneData,how='left',on='geneName')
			SVIDGeneNameCombinations2=SVIDGeneNameCombinations2.loc[SVIDGeneNameCombinations2['gene_data'].notnull(),:]
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations2.loc[~SVIDGeneNameCombinations2.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return '||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'gene_data'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'gwasGeneData']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			uniqueMatches2=uniqueMatches.merge(gwasGeneData,how='left',on='geneName')
			uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
			index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
			stagingSVData.loc[index2,'gwasGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
			# replace entries with more than 10 gwas associations with 'True'
			stagingSVData.loc[stagingSVData.gwasGeneData.str.count('\|\|')>=10,'gwasGeneData']="TRUE"
			## get list of omim results for overlapped genes
			SVIDGeneNameCombinations2=SVIDGeneNameCombinations.merge(omimGeneData,how='left',on='geneName')
			SVIDGeneNameCombinations2=SVIDGeneNameCombinations2.loc[SVIDGeneNameCombinations2['gene_data'].notnull(),:]
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations2.loc[~SVIDGeneNameCombinations2.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return '||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'gene_data'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'omimGeneData']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
			uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
			index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
			stagingSVData.loc[index2,'omimGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
			# replace entries with more than 50 overlapping omim genes with 'True'
			stagingSVData.loc[stagingSVData.omimGeneData.str.count('\|\|')>=50,'omimGeneData']="TRUE"
			## get omim inheritance results for overlapped genes
			def f(chr, start, end):
				tmp='||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'inheritance'])
				if '5' in tmp:
					return 5
				elif (('1' in tmp) and ('2' in tmp)):
					return 5
				elif (('3' in tmp) or ('6' in tmp) or ('7' in tmp) or ('8' in tmp)):
					return 3
				elif '4' in tmp:
					return 4
				else:
					return int(max([int(s) for s in tmp.split('||')]))
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'omim_inheritance']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			uniqueMatches2=uniqueMatches.merge(omimGeneData,how='left',on='geneName')
			uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['inheritance'].notnull(),:]
			index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
			stagingSVData.loc[index2,'omim_inheritance']=uniqueMatches2.loc[index1,'inheritance'].astype('int').values
			## get list of gnomad constraint results for overlapped genes
			SVIDGeneNameCombinations2=SVIDGeneNameCombinations.merge(gnomadConstraintGeneData,how='left',on='geneName')
			SVIDGeneNameCombinations2=SVIDGeneNameCombinations2.loc[SVIDGeneNameCombinations2['gene_data'].notnull(),:]
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations2.loc[~SVIDGeneNameCombinations2.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return '||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'gene_data'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'gnomadConstraintGeneData']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
			uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['gene_data'].notnull(),:]
			index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
			stagingSVData.loc[index2,'gnomadConstraintGeneData']=uniqueMatches2.loc[index1,'gene_data'].values
			# replace entries with more than 50 overlapping genes with 'True'
			stagingSVData.loc[stagingSVData.gnomadConstraintGeneData.str.count('\|\|')>=50,'gnomadConstraintGeneData']="TRUE"
			## get gnomad max pLI and misZ results for overlapped genes
			def f1(chr, start, end):
				tmp='||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'pLI'])
				return max([float(s) for s in tmp.split('||')])
			def f2(chr, start, end):
				tmp='||'.join(SVIDGeneNameCombinations2.loc[(SVIDGeneNameCombinations2['chr']==chr) & (SVIDGeneNameCombinations2['start']==start) & (SVIDGeneNameCombinations2['end']==end),'misZ'])
				return max([float(s) for s in tmp.split('||')])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'gnomad_constraint_max_pli']=SVIDsWithMultipleGenes.apply(lambda x: f1(x.chr,x.start,x.end), axis=1).values
				stagingSVData.loc[index1,'gnomad_constraint_max_misz']=SVIDsWithMultipleGenes.apply(lambda x: f2(x.chr,x.start,x.end), axis=1).values
			uniqueMatches2=uniqueMatches.merge(gnomadConstraintGeneData,how='left',on='geneName')
			uniqueMatches2=uniqueMatches2.loc[uniqueMatches2['pLI'].notnull(),:]
			index1=uniqueMatches2.index[uniqueMatches2.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches2.SVID)]
			stagingSVData.loc[index2,'gnomad_constraint_max_pli']=uniqueMatches2.loc[index1,'pLI'].values
			stagingSVData.loc[index2,'gnomad_constraint_max_misz']=uniqueMatches2.loc[index1,'misZ'].values
		## get list of overlapping genes (padded)
		# get the closest gene for each SVID 
		genesIntersectingSVs=stagingSVDataBT.intersect(genePaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		if len(genesIntersectingSVs)>0:
			# get the subset of rows which have duplicate SVIDs 
			multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			# get the list of SVID-geneName combinations
			SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'genesOverlappedPadded']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			# deal with the uniquely matched SVID-gene pairs
			uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
			stagingSVData.loc[index2,'genesOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping canonical transcripts
		# get the closest gene for each SVID 
		genesIntersectingSVs=stagingSVDataBT.intersect(canonicalTranscriptLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		if len(genesIntersectingSVs)>0:
			# get the subset of rows which have duplicate SVIDs 
			multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			# get the list of SVID-geneName combinations
			SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'canonicalTranscriptsOverlapped']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			# deal with the uniquely matched SVID-gene pairs
			uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
			stagingSVData.loc[index2,'canonicalTranscriptsOverlapped']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping canonical transcripts (padded)
		# get the closest gene for each SVID 
		genesIntersectingSVs=stagingSVDataBT.intersect(canonicalTranscriptPaddedLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['geneChr','geneStart','geneEnd','geneName'])
		if len(genesIntersectingSVs)>0:
			# get the subset of rows which have duplicate SVIDs 
			multipleEntries=genesIntersectingSVs.loc[genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			# get the list of SVID-geneName combinations
			SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			if len(SVIDsWithMultipleGenes)>0:
				stagingSVData.loc[index1,'canonicalTranscriptsOverlappedPadded']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			# deal with the uniquely matched SVID-gene pairs
			uniqueMatches=genesIntersectingSVs.loc[~genesIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
			stagingSVData.loc[index2,'canonicalTranscriptsOverlappedPadded']=uniqueMatches.loc[index1,'geneName'].values
		## get list of overlapping encode enhancers
		# get the closest enhancer for each SVID 
		enhancersIntersectingSVs=stagingSVDataBT.intersect(enhancerLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['enhancerChr','enhancerStart','enhancerEnd','enhancerID'])
		if len(enhancersIntersectingSVs)>0:
			# get the subset of rows which have duplicate SVIDs 
			multipleEntries=enhancersIntersectingSVs.loc[enhancersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			# get the list of SVID-enhancer combinations
			SVIDEnhancerCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','enhancerID']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultipleEnhancers=SVIDEnhancerCombinations.loc[~SVIDEnhancerCombinations.duplicated(keep='first',subset='SVID'),:]
			# remove SVs over 1MB in size
			SVIDEnhancerCombinations=SVIDEnhancerCombinations.loc[SVIDEnhancerCombinations.SVSize<maxSize,:]
			SVIDsWithMultipleEnhancers=SVIDsWithMultipleEnhancers.loc[SVIDsWithMultipleEnhancers.SVSize<maxSize,:]
			def f(chr, start, end):
				return ','.join(SVIDEnhancerCombinations.loc[(SVIDEnhancerCombinations['chr']==chr) & (SVIDEnhancerCombinations['start']==start) & (SVIDEnhancerCombinations['end']==end),'enhancerID'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleEnhancers.SVID)]
			if len(SVIDsWithMultipleEnhancers)>0:
				stagingSVData.loc[index1,'encodeEnhancersOverlapped']=SVIDsWithMultipleEnhancers.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			# deal with the uniquely matched SVID-enhancer pairs
			uniqueMatches=enhancersIntersectingSVs.loc[~enhancersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
			stagingSVData.loc[index2,'encodeEnhancersOverlapped']=uniqueMatches.loc[index1,'enhancerID'].values
		## get list of overlapping encode promoters
		# get the closest enhancer for each SVID 
		promotersIntersectingSVs=stagingSVDataBT.intersect(promoterLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['promoterChr','promoterStart','promoterEnd','promoterID'])
		if len(promotersIntersectingSVs)>0:
			# get the subset of rows which have duplicate SVIDs 
			multipleEntries=promotersIntersectingSVs.loc[promotersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			# get the list of SVID-promoter combinations
			SVIDPromoterCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','promoterID']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultiplePromoters=SVIDPromoterCombinations.loc[~SVIDPromoterCombinations.duplicated(keep='first',subset='SVID'),:]
			# remove SVs over 1MB in size
			SVIDPromoterCombinations=SVIDPromoterCombinations.loc[SVIDPromoterCombinations.SVSize<maxSize,:]
			SVIDsWithMultiplePromoters=SVIDsWithMultiplePromoters.loc[SVIDsWithMultiplePromoters.SVSize<maxSize,:]
			def f(chr, start, end):
				return ','.join(SVIDPromoterCombinations.loc[(SVIDPromoterCombinations['chr']==chr) & (SVIDPromoterCombinations['start']==start) & (SVIDPromoterCombinations['end']==end),'promoterID'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultiplePromoters.SVID)]
			if len(SVIDsWithMultiplePromoters)>0:
				stagingSVData.loc[index1,'encodePromotersOverlapped']=SVIDsWithMultiplePromoters.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			# deal with the uniquely matched SVID-promoter pairs
			uniqueMatches=promotersIntersectingSVs.loc[~promotersIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
			stagingSVData.loc[index2,'encodePromotersOverlapped']=uniqueMatches.loc[index1,'promoterID'].values
		## get list of overlapping encode ctcfOnly elements
		# get the closest enhancer for each SVID 
		ctcfOnlyIntersectingSVs=stagingSVDataBT.intersect(ctcfOnlyLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=SVDataColNames + ['ctcfOnlyChr','ctcfOnlyStart','ctcfOnlyEnd','ctcfOnlyID'])
		if len(ctcfOnlyIntersectingSVs)>0:
			# get the subset of rows which have duplicate SVIDs 
			multipleEntries=ctcfOnlyIntersectingSVs.loc[ctcfOnlyIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			# get the list of SVID-ctcfOnly combinations
			SVIDCtcfOnlyCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','ctcfOnlyID']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultipleCTCFOnlySites=SVIDCtcfOnlyCombinations.loc[~SVIDCtcfOnlyCombinations.duplicated(keep='first',subset='SVID'),:]
			# remove SVs over 1MB in size
			SVIDCtcfOnlyCombinations=SVIDCtcfOnlyCombinations.loc[SVIDCtcfOnlyCombinations.SVSize<maxSize,:]
			SVIDsWithMultipleCTCFOnlySites=SVIDsWithMultipleCTCFOnlySites.loc[SVIDsWithMultipleCTCFOnlySites.SVSize<maxSize,:]
			def f(chr, start, end):
				return ','.join(SVIDCtcfOnlyCombinations.loc[(SVIDCtcfOnlyCombinations['chr']==chr) & (SVIDCtcfOnlyCombinations['start']==start) & (SVIDCtcfOnlyCombinations['end']==end),'ctcfOnlyID'])
			index1=stagingSVData.index[stagingSVData.SVID.isin(SVIDsWithMultipleCTCFOnlySites.SVID)]
			if len(SVIDsWithMultipleCTCFOnlySites)>0:
				stagingSVData.loc[index1,'encodeCTCFSitesOverlapped']=SVIDsWithMultipleCTCFOnlySites.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
			# deal with the uniquely matched SVID-gene pairs
			uniqueMatches=ctcfOnlyIntersectingSVs.loc[~ctcfOnlyIntersectingSVs.duplicated(keep=False,subset='SVID'),:]
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingSVData.SVID)]
			index2=stagingSVData.index[stagingSVData.SVID.isin(uniqueMatches.SVID)]
			stagingSVData.loc[index2,'encodeCTCFSitesOverlapped']=uniqueMatches.loc[index1,'ctcfOnlyID'].values
		## combine staging with the rest of SVData
		SVData=pandas.concat([SVData,stagingSVData],axis=0,ignore_index=True)
	SVData=SVData.sort_values(by=['chr','start'])
	## increment values in phenocounts column
	# phenocounts part 1 (new sites)
	phenoCounts=SVData.loc[SVData['SVID'].isin(sampleInput.loc[:,'SVID'].values),['SVID','phenocounts']]
	noPhenoCounts=phenoCounts.loc[phenoCounts['phenocounts'].isna(),:]
	noPhenoCounts2=noPhenoCounts.merge(sampleInput.loc[:,['SVID','Genotype']],how='left',on='SVID')
	noPhenoCounts2['isHet']=0
	noPhenoCounts2['isHom']=0
	noPhenoCounts2.loc[noPhenoCounts2['Genotype']==1,'isHet']=1
	noPhenoCounts2.loc[noPhenoCounts2['Genotype']==2,'isHom']=1
	noPhenoCounts2['phenocounts']=phenoCode + "@" + phenoName + "@" + noPhenoCounts2.loc[:,'isHet'].astype(str) + "@" + noPhenoCounts2.loc[:,'isHom'].astype(str)
	SVData=SVData.merge(noPhenoCounts2.loc[:,['SVID','phenocounts']],how='left',on='SVID')
	SVData.loc[SVData['phenocounts_y'].isna(),'phenocounts_y']=SVData.loc[SVData['phenocounts_y'].isna(),'phenocounts_x']
	SVData=SVData.drop(columns='phenocounts_x')
	SVData=SVData.rename(columns={'phenocounts_y':'phenocounts'})
	# phenocounts part 2 (new phenotype for existing sites)
	inPhenoCounts=phenoCounts.loc[(~(phenoCounts['phenocounts'].isna()) & (phenoCounts['phenocounts'].str.contains(phenoCode))),:]
	notInPhenoCounts=phenoCounts.loc[~((phenoCounts['phenocounts'].isna()) | (phenoCounts.index.isin(inPhenoCounts.index.values))),:]
	notInPhenoCounts2=notInPhenoCounts.merge(sampleInput.loc[:,['SVID','Genotype']],how='left',on='SVID')
	notInPhenoCounts2['isHet']=0
	notInPhenoCounts2['isHom']=0
	notInPhenoCounts2.loc[notInPhenoCounts2['Genotype']==1,'isHet']=1
	notInPhenoCounts2.loc[notInPhenoCounts2['Genotype']==2,'isHom']=1
	notInPhenoCounts2['phenocounts']=notInPhenoCounts2.loc[:,'phenocounts'] + '<>' + phenoCode + "@" + phenoName + "@" + notInPhenoCounts2.loc[:,'isHet'].astype(str) + "@" + notInPhenoCounts2.loc[:,'isHom'].astype(str)
	SVData=SVData.merge(notInPhenoCounts2.loc[:,['SVID','phenocounts']],how='left',on='SVID')
	SVData.loc[SVData['phenocounts_y'].isna(),'phenocounts_y']=SVData.loc[SVData['phenocounts_y'].isna(),'phenocounts_x']
	SVData=SVData.drop(columns='phenocounts_x')
	SVData=SVData.rename(columns={'phenocounts_y':'phenocounts'})
	# phenocounts part 3 (update phenotype in existing sites)
	inPhenoCounts=inPhenoCounts.reset_index(drop=True)
	inPhenoCounts['phenoCountsRows']=inPhenoCounts.loc[:,'phenocounts'].str.split('<>').tolist()
	inPhenoCounts2=inPhenoCounts.loc[:,['SVID','phenoCountsRows']].explode('phenoCountsRows').reset_index(drop=True)
	if len(inPhenoCounts2)>0:
		inPhenoCounts2[['phenoCode','phenoName','het','hom']]=inPhenoCounts2.loc[:,'phenoCountsRows'].str.split('@',expand=True)
		hets=sampleInput.loc[sampleInput['Genotype']==1,:]
		homs=sampleInput.loc[sampleInput['Genotype']==2,:]
		inPhenoCounts2.loc[((inPhenoCounts2['SVID'].isin(hets['SVID'].values)) & (inPhenoCounts2['phenoCode']==phenoCode)),'het']=inPhenoCounts2.loc[((inPhenoCounts2['SVID'].isin(hets['SVID'].values)) & (inPhenoCounts2['phenoCode']==phenoCode)),'het'].values.astype(int) + 1
		inPhenoCounts2.loc[((inPhenoCounts2['SVID'].isin(homs['SVID'].values)) & (inPhenoCounts2['phenoCode']==phenoCode)),'hom']=inPhenoCounts2.loc[((inPhenoCounts2['SVID'].isin(homs['SVID'].values)) & (inPhenoCounts2['phenoCode']==phenoCode)),'hom'].values.astype(int) + 1
		inPhenoCounts2['phenocounts']=inPhenoCounts2.loc[:,['phenoCode','phenoName','het','hom']].astype(str).agg('@'.join,axis=1)
		inPhenoCounts3=inPhenoCounts2.loc[:,['SVID','phenocounts']].groupby('SVID').agg('<>'.join).reset_index(drop=False)
		SVData=SVData.merge(inPhenoCounts3.loc[:,['SVID','phenocounts']],how='left',on='SVID')
		SVData.loc[SVData['phenocounts_y'].isna(),'phenocounts_y']=SVData.loc[SVData['phenocounts_y'].isna(),'phenocounts_x']
		SVData=SVData.drop(columns='phenocounts_x')
		SVData=SVData.rename(columns={'phenocounts_y':'phenocounts'})
	############ deal with GenoData
	stagingGenoData=pandas.DataFrame(index=range(0,len(sampleInput)),columns=GenoDataColNames)
	stagingGenoData=stagingGenoData.astype({'bnd_origin_chr':'Int64','bnd_origin_pos':'Int64'})
	stagingGenoData.loc[:,['SVID','SampleID','Genotype','thisSampleStart','thisSampleEnd','breakdancer','breakseq','cnvnator','delly','lumpy','manta','bnd_origin_chr','bnd_origin_pos']]=sampleInput[['SVID','SampleID','Genotype','start','end','breakdancer','breakseq','cnvnator','delly','lumpy','manta','bnd_origin_chr','bnd_origin_pos']].values
	stagingGenoData.loc[:,['bnd_origin_gene', 'bnd_origin_overlapsExon']]=["FALSE","FALSE"]
	bndSites=stagingGenoData.loc[stagingGenoData['bnd_origin_chr'].notnull(),['bnd_origin_chr','bnd_origin_pos','SVID']]
	if len(bndSites)>0:
		bndSites['bnd_origin_start']=bndSites.loc[:,'bnd_origin_pos'].astype(int)-1
		bndSites=bndSites.rename(columns={'bnd_origin_chr':'chr','bnd_origin_start':'start','bnd_origin_pos':'end'})
		bndSites=bndSites[['chr','start','end','SVID']]
		bndSitesBT=pybedtools.BedTool.from_dataframe(bndSites)
		intersectionCounts=bndSitesBT.intersect(exonLocations,c=True).to_dataframe(header=None,low_memory=False,index_col=False,names=['chr','start','end','SVID','exons'])
		stagingGenoData.loc[stagingGenoData['SVID'].isin(intersectionCounts.loc[intersectionCounts['exons']>0,'SVID'].values),'bnd_origin_overlapsExon']="TRUE"
		## get list of overlapping genes for translocations
		# get the closest gene for each SVID 
		genesIntersectingBNDs=bndSitesBT.intersect(geneLocations,wo=True).to_dataframe(index_col=False,low_memory=False,names=['chr','start','end','SVID','geneChr','geneStart','geneEnd','geneName'])
		# get the subset of rows which have duplicate SVIDs 
		multipleEntries=genesIntersectingBNDs.loc[genesIntersectingBNDs.duplicated(keep=False,subset='SVID'),:]
		if len(multipleEntries)>0:
			# get the list of SVID-geneName combinations
			SVIDGeneNameCombinations=multipleEntries.loc[~multipleEntries.duplicated(keep='first',subset=['SVID','geneName']),:]
			# get the list of SVIDs that I will group by
			SVIDsWithMultipleGenes=SVIDGeneNameCombinations.loc[~SVIDGeneNameCombinations.duplicated(keep='first',subset='SVID'),:]
			def f(chr, start, end):
				return ','.join(SVIDGeneNameCombinations.loc[(SVIDGeneNameCombinations['chr']==chr) & (SVIDGeneNameCombinations['start']==start) & (SVIDGeneNameCombinations['end']==end),'geneName'])
			index1=stagingGenoData.index[stagingGenoData.SVID.isin(SVIDsWithMultipleGenes.SVID)]
			stagingGenoData.loc[index1,'bnd_origin_gene']=SVIDsWithMultipleGenes.apply(lambda x: f(x.chr,x.start,x.end), axis=1).values
		# deal with the uniquely matched SVID-gene pairs
		uniqueMatches=genesIntersectingBNDs.loc[~genesIntersectingBNDs.duplicated(keep=False,subset='SVID'),:]
		if len(uniqueMatches)>0:
			index1=uniqueMatches.index[uniqueMatches.SVID.isin(stagingGenoData.SVID)]
			index2=stagingGenoData.index[stagingGenoData.SVID.isin(uniqueMatches.SVID)]
			stagingGenoData.loc[index2,'bnd_origin_gene']=uniqueMatches.loc[index1,'geneName'].values
	GenoData=pandas.concat([GenoData,stagingGenoData])
	SVData.to_csv(SVDataFile,sep='\t',header=False,index=False,na_rep="\\N")
	GenoData.to_csv(GenoDataFile,sep='\t',header=False,index=False,na_rep="\\N")
	return

if __name__ == "__main__":
	main(sys.argv[1:])
