parseSVs() {

    local inputFileS3=$1

    cd $TGP_HOME

    aws s3 cp $inputFileS3 . --only-show-errors

    inputFile=$(basename $inputFileS3)

    # download the database tables
    aws s3 cp s3://tgp-annotation/svariantindex/SVData.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/sv_genomic_data/GenoData.txt . --only-show-errors
    aws s3 cp s3://tgp-annotation/omim/omim.tsv . --only-show-errors
    aws s3 cp s3://tgp-annotation/gwas/gwas.tsv . --only-show-errors
    aws s3 cp s3://tgp-annotation/gnomad_constraint/gnomad_constraint.txt . --only-show-errors

    # sort and convert chromosome numbers back into names
    sed -i 's/^23/X/' SVData.txt
    sed -i 's/^24/Y/' SVData.txt
    sed -i 's/^25/MT/' SVData.txt

    # transform updateable annotation tables into their aggregate formats
    # omim
    echo -e "geneName\tgene_data\tinheritance" > tmp_header
    cut -f 3 omim.tsv > tmp1
    cut -f 1,2,3,4,5,6,7 omim.tsv | sed 's/\t/<>/g' > tmp2
    cut -f 7 omim.tsv > tmp3
    paste tmp1 tmp2 tmp3 | cat tmp_header - > omim_data.txt
    rm tmp1 tmp2 tmp_header
    # gwas
    echo -e "geneName\tgene_data" > tmp_header
    cut -f 8 gwas.tsv > tmp1
    cut -f 3,4,5,6,7,8,9 gwas.tsv | sed 's/\t/<>/g' > tmp2
    paste tmp1 tmp2 | cat tmp_header - > gwas_gene.txt
    rm tmp1 tmp2 tmp_header
    # gnomad constraint (canonical transcripts only)
    echo -e "geneName\tgene_data\tmisZ\tpLI" > tmp_header
    # filter down to only canonical transcripts and keep first if there are multiple
    awk -F'\t' -v OFS='\t' '{if($3=="true"){print $0}}' gnomad_constraint.txt | sort -k1,1 -u > gnomad_constraint_filt.txt 
    cut -f 1 gnomad_constraint_filt.txt > tmp1
    awk -F'\t' -v OFS='\t' '{print $1,$22,$20,$4,$5,$9,$10,$14,$15,$21}' gnomad_constraint_filt.txt | sed 's/\t/<>/g' > tmp2
    cut -f 20,22 gnomad_constraint_filt.txt > tmp3
    paste tmp1 tmp2 tmp3 | cat tmp_header - > gnomad_constraint_data.txt
    rm tmp1 tmp2 tmp_header

    mkdir -p toUpload
    COUNT=0

    source activate py37

    while IFS=$'\t' read -r fileFullName diagnosisName diagnosisCodeText; do
        COUNT=$((COUNT+1))
        fileName=$(sed 's#^.*/##g' <<< ${fileFullName})
        BASE=$(sed 's/\.combined\.genotyped\.vcf//g' <<< ${fileName})
        if s3_path_exists $fileFullName && ! grep -qwF $BASE GenoData.txt; then
            aws s3 cp ${fileFullName} . --only-show-errors
            gatk-4.2.6.1/gatk VariantsToTable -V ${BASE}.combined.genotyped.vcf -O ${BASE}.gatkParsed.txt -F CHROM -F POS -F END -F FILTER -F SVTYPE -F CALLERS -F HET -F HOM-VAR -F AVGLEN -F CHR2 --show-filtered
            python $REPO/scripts/structuralVariation/parseParliament2Output_part1.py --sampleID=${BASE}
            NUMSVS=$(cat ${BASE}.ParsedSVs.txt | wc -l || true)
            if [[ ${NUMSVS} -gt 0 ]]; then
                sort -k1,1 -k2n,2n -k3n,3n ${BASE}.ParsedSVs.txt > tmp.txt ; mv tmp.txt ${BASE}.ParsedSVs.txt
                python $REPO/scripts/structuralVariation/parseParliament2Output_part2.py --sample=${BASE}.ParsedSVs.txt --phenoCode=${diagnosisCodeText} --phenoName="${diagnosisName}"
            fi
            # every other sample, upload the data tables just in case of a crash later
            if [[ $((COUNT % 2)) -eq 0 ]]; then
                sed 's/^X/23/' SVData.txt | sed 's/^Y/24/' | sed 's/^MT/25/' > toUpload/SVData.txt
                aws s3 cp toUpload/SVData.txt s3://tgp-annotation/svariantindex/ --only-show-errors
                aws s3 cp GenoData.txt s3://tgp-annotation/sv_genomic_data/ --only-show-errors
            fi
            rm ${fileName}
            rm ${BASE}.ParsedSVs.txt
        fi
    done < $inputFile

    # convert chromosome names back into numbers
    sed -i 's/^X/23/' SVData.txt
    sed -i 's/^Y/24/' SVData.txt
    sed -i 's/^MT/25/' SVData.txt

    # upload the database tables
    aws s3 cp SVData.txt s3://tgp-annotation/svariantindex/ --only-show-errors
    aws s3 cp GenoData.txt s3://tgp-annotation/sv_genomic_data/ --only-show-errors

}
