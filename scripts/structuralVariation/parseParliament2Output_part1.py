import sys, getopt, os
def main ( argv ):
	sampleID = ''
	try:
		opts, args = getopt.getopt(argv,"h",["sampleID=","help"])
	except getopt.GetoptError:
		print('parseParliament2Outupt_part1.py --sampleID=<sampleID>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('--sampleID'):
			sampleID=arg
		elif opt in ('-h','--help'):
			print('parseParliament2Outupt_part1.py --sampleID=<sampleID>')
			sys.exit()
		else:
			print('parseParliament2Outupt_part1.py --sampleID=<sampleID>')
			sys.exit()
	import pandas
	import numpy as np
	input=pandas.read_csv(sampleID + '.gatkParsed.txt',sep='\t',low_memory=False)
	# rename some of the columns
	input=input.rename(columns={'CHROM':'chr','POS':'start','SVTYPE':'SVType'})
	# remove filtered variants
	input=input.loc[((input['FILTER']=='PASS') | (input['FILTER']=='Unknown')),:].reset_index(drop=True)
	# remove variants where the start and end position are the same
	input=input.loc[((input['SVType']=='BND') | (input['start']!=input['END'])),:].reset_index(drop=True)
	# prepare columns
	input['stop']=input.loc[:,'END']
	input.loc[input['SVType']=='BND','stop']=input.loc[input['SVType']=='BND','start']
	input['SVID']=input.loc[:,['chr','start','stop','SVType']].astype(str).agg('-'.join,axis=1)
	input['SampleID']=sampleID
	input['SVSize']=input.loc[:,'stop']-input.loc[:,'start']
	input.loc[input['SVType']=='BND','SVSize']=input.loc[input['SVType']=='BND','AVGLEN']
	input.loc[input['SVType']=='INS','SVSize']=input.loc[input['SVType']=='INS','AVGLEN']
	input['Genotype']=0
	input.loc[input['HET']==1,'Genotype']=1
	input.loc[input['HOM-VAR']==1,'Genotype']=2
	input['breakdancer']=0
	input['breakseq']=0
	input['cnvnator']=0
	input['delly']=0
	input['lumpy']=0
	input['manta']=0
	input.loc[input['CALLERS'].str.contains('BREAKDANCER'),'breakdancer']=1
	input.loc[input['CALLERS'].str.contains('BREAKSEQ'),'breakseq']=1
	input.loc[input['CALLERS'].str.contains('CNVNATOR'),'cnvnator']=1
	input.loc[input['CALLERS'].str.contains('DELLY'),'delly']=1
	input.loc[input['CALLERS'].str.contains('LUMPY'),'lumpy']=1
	input.loc[input['CALLERS'].str.contains('MANTA'),'manta']=1
	input['bnd_origin_chr']="NULL"
	input['bnd_origin_pos']="NULL"
	# deal with translocation-specific considerations
	input.loc[input['SVType']=='BND','bnd_origin_chr']=input.loc[input['SVType']=='BND','CHR2'].astype(str)
	input.loc[input['bnd_origin_chr']=='X','bnd_origin_chr']='23'
	input.loc[input['bnd_origin_chr']=='Y','bnd_origin_chr']='24'
	input.loc[input['bnd_origin_chr']=='MT','bnd_origin_chr']='25'
	input.loc[input['SVType']=='BND','bnd_origin_pos']=input.loc[input['SVType']=='BND','END'].astype(str)
	# remove duplicate entries, and sort rows and columns
	input=input.drop_duplicates(subset=['SVID'])
	input=input.loc[:,['chr','start','stop','SVID','SampleID','SVType','SVSize','Genotype','breakdancer','breakseq','cnvnator','delly','lumpy','manta','bnd_origin_chr','bnd_origin_pos']]
	input=input.sort_values(by=['chr','start'])
	# print the parsed sample data to file 
	input.to_csv(sampleID + ".ParsedSVs.txt",sep='\t',header=False,index=False)
	return

if __name__ == "__main__":
	main(sys.argv[1:])
