score_variants() {

    local inputFileS3=$1

    cd $TGP_HOME

    aws s3 cp ${inputFileS3} . --only-show-errors
    fileName=$(sed 's#^.*/##g' <<< ${inputFileS3})
    S3LOC=$(sed "s/${fileName}//" <<< ${inputFileS3})
    BASE=$(sed 's/\.txt//' <<< ${fileName})

    # process variants with annovar
    echo "Starting Step 1: Get coding changes with Annovar"
    awk -F'\t' -v OFS='\t' '{print $1,$2,".",$3,$4,".","PASS",".","GT","0/1"}' ${BASE}.txt | sed 's/^23/X/' | sed 's/^24/Y/' | sed 's/^25/MT/' | cat $REPO/scripts/variantPathogenicity/header.vcf - > ${BASE}.vcf
    sed 's/^23/X/' ${BASE}.txt | sed 's/^24/Y/' | sed 's/^25/MT/' > ${BASE}.chrConverted.txt
    annovar/convert2annovar.pl -format vcf4 ${BASE}.vcf > ${BASE}.avinput
    annovar/annotate_variation.pl -dbtype wgEncodeGencodeBasicV33lift37 -buildver hg19 --exonicsplicing ${BASE}.avinput annovar/humandb/
    # if there are no scorable variants, end early
    SCORABLEVARIANTS=$(cat ${BASE}.avinput.exonic_variant_function | wc -l || true)
    if [[ ${SCORABLEVARIANTS} -eq 0 ]]; then touch ${BASE}_ensemblePredictions.txt; aws s3 cp ${BASE}_ensemblePredictions.txt ${S3LOC} --only-show-errors; exit 0; fi
    annovar/coding_change.pl ${BASE}.avinput.exonic_variant_function annovar/humandb/hg19_wgEncodeGencodeBasicV33lift37.txt annovar/humandb/hg19_wgEncodeGencodeBasicV33lift37Mrna.fa --includesnp --onlyAltering --alltranscript > ${BASE}.coding_changes.txt

    # select transcript
    echo "Starting Step 2: Select transcript"
    source activate maverick
    python $REPO/scripts/variantPathogenicity/groomAnnovarOutput.py --inputBase=${BASE}
    # if there are no scorable variants, end early
    SCORABLEVARIANTS=$(cat ${BASE}.groomed.txt | wc -l || true)
    if [[ ${SCORABLEVARIANTS} -lt 2 ]]; then touch ${BASE}_ensemblePredictions.txt; aws s3 cp ${BASE}_ensemblePredictions.txt ${S3LOC} --only-show-errors; exit 0; fi

    # add on annotations
    echo "Starting Step 3: Merge on annotations"
    python $REPO/scripts/variantPathogenicity/annotateVariants.py --inputBase=${BASE}

    # run variants through each of the models
    echo "Starting Step 4: Score variants with the 8 models"
    python $REPO/scripts/variantPathogenicity/runModels.py --input=${BASE}

    # output results
    # convert chromosome names back to numbers
    sed 's/^X/23/' ${BASE}_ensemblePredictions.txt | sed 's/^Y/24/' | sed 's/^MT/25/' | tail -n +2 > tmp; mv tmp ${BASE}_ensemblePredictions.txt
    aws s3 cp ${BASE}_ensemblePredictions.txt ${S3LOC} --only-show-errors

    echo "Done"
}
