version 1.0

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/JointVariantCalling.wdl" as JVC
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

workflow cohortLevelWorkflow_vcfInput {

  meta {
    description: "Processes a VCF file into the database as directly as possible. File can contain multiple individuals."
  }

  input {
    File inputVCF
    Array[String] sampleGenomicIDs
    Array[String] sampleGUIDs
    String cohortRunID

    File all_calling_intervals

    # Runtime attribute overrides
    RuntimeAttr? runtime_attr_override_vcfInputQC
    RuntimeAttr? runtime_attr_override_parseVCF
        
  }

    call JVC.vcfInputQC as vcfInputQC {
        input:
            inputVCF = inputVCF,
            sampleGenomicIDs = sampleGenomicIDs,
            sampleGUIDs = sampleGUIDs,
            cohortRunID = cohortRunID,
            callingIntervals = all_calling_intervals,
            runtime_attr_override = runtime_attr_override_vcfInputQC
    }

    call JVC.parseVCFInput as parseVCF {
        input:
            combinedVCF = vcfInputQC.combinedVCFName,
            sampleGenomicIDs = sampleGenomicIDs,
            cohortRunID = cohortRunID,
            runtime_attr_override = runtime_attr_override_parseVCF
        }

  }