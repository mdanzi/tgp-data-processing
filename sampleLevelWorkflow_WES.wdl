version 1.0

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/Align.wdl" as Align
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/MergeBams.wdl" as MergeBams
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/FastqLevelQC.wdl" as FastqLevelQC
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/GATK4Prep.wdl" as GATK4Prep
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/GATK4HapCaller.wdl" as Calling
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/CallTRs.wdl" as TRs
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/BamToFastq.wdl" as BamToFastq
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"


workflow sampleLevelWorkflow_WES {

  meta {
    description: "Aligns a WES, performs QC, calls SNVs, Indels, and Repeat Expansions."
  }

  input {
    Array[File?] R1Fastqs
    Array[File?] R2Fastqs
    File? inputBam
    String sampleGenomicID
    String sampleRunID
    String sampleGUID

    File scattered_calling_intervals_list
    Array[File] scattered_calling_intervals = read_lines(scattered_calling_intervals_list)

    String gatk_path
    String java_opt

    String? experimentType = "WES" # differentiate if this is an exome or a targeted panel

    # Runtime attribute overrides
    RuntimeAttr? runtime_attr_override_bamToFastq
    RuntimeAttr? runtime_attr_override_align
    RuntimeAttr? runtime_attr_override_mergeBams
    RuntimeAttr? runtime_attr_override_fastqLevelQC
    RuntimeAttr? runtime_attr_override_gatk4Prep
    RuntimeAttr? runtime_attr_override_baseRecalibrator
    RuntimeAttr? runtime_attr_override_gatherBqsrReports
    RuntimeAttr? runtime_attr_override_applyBQSR
    RuntimeAttr? runtime_attr_override_gatherBamFiles
    RuntimeAttr? runtime_attr_override_haplotypeCaller
    RuntimeAttr? runtime_attr_override_gatherVCFs
    RuntimeAttr? runtime_attr_override_cleanUpOutputs
    RuntimeAttr? runtime_attr_override_runPeddy
    RuntimeAttr? runtime_attr_override_customGenotypeCalls
    RuntimeAttr? runtime_attr_override_callTRsWES        
  }

  parameter_meta {
    R1Fastqs: "File or array of files with the R1 fastq reads, can optionally be compressed with gzip or bzip2."
    R2Fastqs: "File or array of files with the R2 fastq reads, can optionally be compressed with gzip or bzip2."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  if ((! defined(R1Fastqs)) && (! defined(inputBam))) {call BamToFastq.StopWorkflow as no_input_provided {input: reason = "Neither fastq nor bam inputs provided"}}
  if ((! defined(R2Fastqs)) && (! defined(inputBam))) {call BamToFastq.StopWorkflow as no_input_provided2 {input: reason = "Neither fastq nor bam inputs provided"}}

  if (defined(inputBam)) {
    File inputBam2 = select_first([inputBam])
    call BamToFastq.bamToFastq as bamToFastq {
      input: 
          bam = inputBam2, 
          sampleGenomicID = sampleGenomicID, 
          sampleRunID = sampleRunID, 
          runtime_attr_override = runtime_attr_override_bamToFastq
        }
    call Align.align_presharded as alignFromBam {
      input:
        R1Fastq = bamToFastq.R1Fastq,
        R2Fastq = bamToFastq.R2Fastq,
        sampleGenomicID = sampleGenomicID,
        sampleRunID = sampleRunID,
        runtime_attr_override = runtime_attr_override_align
    }
    call FastqLevelQC.fastqLevelQC as fastqLevelQCFromBam {
      input:
        R1Fastqs = [bamToFastq.R1Fastq],
        R2Fastqs = [bamToFastq.R2Fastq],
        sampleGenomicID = sampleGenomicID,
        sampleRunID = sampleRunID,
        alignOutputStr = mergeBams.sorted_bam,
        runtime_attr_override = runtime_attr_override_fastqLevelQC  
    }
  }

  if (!defined(inputBam)) {
    Array[File] R1FastqsForAnalysis = select_all(R1Fastqs)
    Array[File] R2FastqsForAnalysis = select_all(R2Fastqs)

    # Perform alignment in parallel over sharded fastq pairs
    scatter (idx in range(length(R1FastqsForAnalysis))) {
      call Align.align_presharded as align {
        input:
          R1Fastq = R1FastqsForAnalysis[idx],
          R2Fastq = R2FastqsForAnalysis[idx],
          sampleGenomicID = sampleGenomicID,
          sampleRunID = sampleRunID,
          runtime_attr_override = runtime_attr_override_align
        }
    }
    
    # Merge the aligned bam files
    call MergeBams.mergeBams as mergeBams {
      input:
        input_bams = align.sorted_bam,
        sampleGenomicID = sampleGenomicID,
        sampleRunID = sampleRunID,
        runtime_attr_override = runtime_attr_override_mergeBams
    }

    call FastqLevelQC.fastqLevelQC as fastqLevelQC {
      input:
        R1Fastqs = R1FastqsForAnalysis,
        R2Fastqs = R2FastqsForAnalysis,
        sampleGenomicID = sampleGenomicID,
        sampleRunID = sampleRunID,
        alignOutputStr = mergeBams.sorted_bam,
        runtime_attr_override = runtime_attr_override_fastqLevelQC  
    }
  }

  call GATK4Prep.gatk4Prep as gatk4Prep {
    input:
      input_bam = select_first([mergeBams.sorted_bam,alignFromBam.sorted_bam]),
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      experimentType = experimentType,
      runtime_attr_override = runtime_attr_override_gatk4Prep
  }

  # Perform Base Quality Score Recalibration (BQSR) on the sorted BAM in parallel
  scatter (subgroup in scattered_calling_intervals) {
      # Generate the recalibration model by interval
      call Calling.baseRecalibrator as baseRecalibrator {
          input:
              input_bam = gatk4Prep.output_bam,
              input_bam_index = gatk4Prep.output_bam_bai,
              recalibration_report_filename = sampleGenomicID + ".hg19" + ".recal_data.csv",
              sequence_group_interval_file = subgroup,
              sampleRunID = sampleRunID,
              gatk_path = gatk_path,
              java_opt = java_opt,
              runtime_attr_override = runtime_attr_override_baseRecalibrator
      }
  }
  
  # Merge the recalibration reports resulting from by-interval recalibration
  call Calling.gatherBqsrReports as gatherBqsrReports {
      input:
          input_bqsr_reports = baseRecalibrator.recalibration_report,
          output_report_filename = sampleGenomicID + ".hg19.recal_data.csv",
          sampleRunID = sampleRunID,
          gatk_path = gatk_path,
          java_opt = java_opt,
          runtime_attr_override = runtime_attr_override_gatherBqsrReports
  }
  scatter (subgroup in scattered_calling_intervals) {
      # Apply the recalibration model by interval
      call Calling.applyBQSR  as applyBQSR {
          input:
              input_bam = gatk4Prep.output_bam,
              input_bam_index = gatk4Prep.output_bam + ".bai",
              output_bam_basename = sampleGenomicID + ".hg19.aligned.duplicates_marked.recalibrated",
              recalibration_report = gatherBqsrReports.output_bqsr_report,
              sequence_group_interval_file = subgroup,
              sampleRunID = sampleRunID,
              gatk_path = gatk_path,
              java_opt = java_opt,
              runtime_attr_override = runtime_attr_override_applyBQSR
      }
  }
  # Merge the recalibrated BAM files resulting from by-interval recalibration
  call Calling.gatherBamFiles as gatherBamFiles {
      input:
          input_bams = applyBQSR.recalibrated_bam,
          output_bam_basename = sampleGenomicID + ".hg19",
          sampleRunID = sampleRunID,
          gatk_path = gatk_path,
          java_opt = java_opt,
          runtime_attr_override = runtime_attr_override_gatherBamFiles
  }
  # Haplotype call scatter-gather 
  scatter (subInterval in scattered_calling_intervals) {
      call Calling.haplotypeCaller as haplotypeCaller {
          input: 
              input_bam=gatherBamFiles.output_bam,
              input_bam_index=gatherBamFiles.output_bam_index,
              interval_list=subInterval,
              gvcf_basename=sampleGenomicID + ".hg19",
              sampleRunID = sampleRunID,
              gatk_path=gatk_path,
              java_opt="-Xmx8g",
              runtime_attr_override = runtime_attr_override_haplotypeCaller
      }
  }
  call Calling.gatherVCFs as gatherVCFs {
    input:
        input_vcfs = haplotypeCaller.output_gvcf,
        input_vcfs_indexes = haplotypeCaller.output_gvcf_index,
        output_vcf_name = sampleGenomicID + ".hg19.g.vcf",
        sampleRunID = sampleRunID,
        gatk_path=gatk_path,
        runtime_attr_override = runtime_attr_override_gatherVCFs
  }
  call Calling.cleanUpOutputs as cleanUpOutputs {
    input:
        input_vcf=gatherVCFs.output_vcf, 
        input_vcf_idx=gatherVCFs.output_vcf_index,
        sampleGenomicID = sampleGenomicID,
        sampleRunID=sampleRunID,
        runtime_attr_override = runtime_attr_override_cleanUpOutputs
  }
  call Calling.runPeddy as runPeddy {
      input:
          input_bam=gatherBamFiles.output_bam,
          input_bam_bai=gatherBamFiles.output_bam_index,
          sampleGenomicID = sampleGenomicID,
          sampleRunID=sampleRunID,
          gatk_path=gatk_path,
          runtime_attr_override = runtime_attr_override_runPeddy
  }

  call Calling.customGenotypeCalls as customGenotypeCalls {
      input:
        input_bam=gatherBamFiles.output_bam,
        input_bam_bai=gatherBamFiles.output_bam_index,
        sampleGenomicID = sampleGenomicID,
        sampleRunID=sampleRunID,
        gatk_path=gatk_path,
        runtime_attr_override = runtime_attr_override_customGenotypeCalls
    }

  call TRs.callTRsWES as callTRsWES {
   input:
      input_bam = gatk4Prep.output_bam,
      input_bam_bai = gatk4Prep.output_bam_bai,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      sampleGUID = sampleGUID,
      runtime_attr_override = runtime_attr_override_callTRsWES
  }
}
