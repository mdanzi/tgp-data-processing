version 1.0

workflow testWorkflow {

  meta {
    description: "Aligns a sample."
  }

  input {
    File[Array] R1Fastqs
    File[Array] R2Fastqs
    String sampleGenomicID
    String sampleRunID

    
  }

  parameter_meta {
    R1Fastqs: "File or array of files with the R1 fastq reads, can optionally be compressed with gzip or bzip2."
    R2Fastqs: "File or array of files with the R2 fastq reads, can optionally be compressed with gzip or bzip2."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  call align {
    input:
      R1Fastqs = R1Fastqs,
      R2Fastqs = R2Fastqs,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID
    }

}

task align {
  input {
    Array[String] R1Fastqs
    Array[String] R2Fastqs
    String sampleGenomicID
    String sampleRunID
    Int? preemptible_tries
}

  meta {
    description: "Aligns a set of fastq file pairs to the GRCh37 reference genome."
  }
  parameter_meta {
    R1Fastqs: "File or array of files with the R1 fastq reads, can optionally be compressed with gzip or bzip2."
    R2Fastqs: "File or array of files with the R2 fastq reads, can optionally be compressed with gzip or bzip2."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }
  command <<<
    set -eo pipefail
    su - ec2-user
    cd /home/ec2-user/ 
    git -C aws-pipeline pull
    source aws-pipeline/utils/utils.sh  # imports errorReport
    source aws-pipeline/utils/parse_run_data.sh  # imports get_sample_id, etc
    trap "errorReport sample ~{sampleRunID}" ERR
    
    # re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
    firstFile=~{R1Fastqs[0]}
    extension=${firstFile##*.}
    fastqExtension=""
    if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=$(echo ".${extension}"); fi

    read1="~{sampleGenomicID}_read1.fq${fastqExtension}"
    read2="~{sampleGenomicID}_read2.fq${fastqExtension}"

    # re-implementation of the s3_read_concat function -- can be moved later if that gets updated to cromwell-friendly version
    # Outputs a concatenated stream of all files listed in the input array
    s3_read_concat_cromwell() {
        local s3Reads=$1
        for s3file in ~{sep=" " s3reads}; do
            aws s3 cp --only-show-errors "$s3file" -
        done
    }
    s3_read_concat_cromwell "~{R1Fastqs}" > ${read1} &
    s3_read_concat_cromwell "~{R2Fastqs}" > ${read2}
    wait 

    # establish variables
    numCPUs=$(cat /proc/cpuinfo | grep -m 1 "cpu cores" | awk -F': ' '{print $2}')
    numCPUs=$((numCPUs * 2))

    # note, the Read Group is written to match the style and defaults used by Picard AddOrReplaceReadGroups
    readGroup="@RG\\tID:1\\tSM:~{sampleGenomicID}\\tPL:illumina\\tLB:lib1\\tPU:unit1"

    # align reads
    bwa mem -M -t ${numCPUs} -R "${readGroup}" ref/hs37d5.fa ${read1} ${read2} | sambamba view -f bam -l 0 -S -t ${numCPUs} /dev/stdin | sambamba sort -t ${numCPUs} -o ~{sampleGenomicID}.sorted.bam /dev/stdin

    # upload output to S3
    aws s3 cp ~{sampleGenomicID}.sorted.bam s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors


  >>>
  runtime {
    memory: "30 GB"
    cpu: "16"
    disks: "local-disk 600 HDD"
    docker: "public.ecr.aws/s5z5a3q9/tgp:latest"
  }
  output {
    String sorted_bam = "s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.sorted.bam"
  }
}
