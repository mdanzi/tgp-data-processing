This repository contains the code for GENESIS' data processing pipelines.

We have two pipeline parts: sample-level and cohort-level. 
The sample-level pipeline aligns the sample and calls variants. It also produces Fastq-level and Bam-level quality checking files.
The cohort-level pipeline performs joint calling (if necessary) and VCF-level quality checking.

Pipelines are run by Cromwell using AWS Batch.
