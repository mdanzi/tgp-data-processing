version 1.0

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/Align.wdl" as Align
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/Utility/ONTUtils.wdl" as ONT
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/Utility/Utils.wdl" as Utils
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/VariantCalling/CallVariantsONT.wdl" as VAR
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/Utility/Finalize.wdl" as FF
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/QC/SampleLevelAlignedMetrics.wdl" as COV
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

workflow sampleLevelWorkflow_ONT {

    meta {
        description: "A workflow that performs single sample variant calling on Oxford Nanopore reads from one or more flow cells. The workflow merges multiple flowcells into a single BAM prior to variant calling."
    }
    parameter_meta {
        Fastqs:       "File or array of files with the fastq reads, can optionally be compressed with gzip or bzip2."
        sampleGenomicID: "Internal ID to be used for this sample."
        sampleRunID: "Internal ID to be used for this run of this workflow."

        ref_map_file:       "table indicating reference sequence and auxillary file locations"
        s3_out_root_dir:   "s3 bucket to store the reads, variants, and metrics files"

        call_svs:               "whether to call SVs"
        fast_less_sensitive_sv: "to trade less sensitive SV calling for faster speed"

        call_small_variants: "whether to call small variants"

        run_dv_pepper_analysis:  "to turn on DV-Pepper analysis or not (non-trivial increase in cost and runtime)"
        ref_scatter_interval_list_locator: "A file holding paths to interval_list files; needed only when running DV-Pepper"
        ref_scatter_interval_list_ids:     "A file that gives short IDs to the interval_list files; needed only when running DV-Pepper"
    }

    input {
        Array[File] Fastqs

        File? bed_to_compute_coverage

        File ref_fasta="s3://tgp-data-analysis/ref/hs37d5.fa"
        File ref_fasta_fai="s3://tgp-data-analysis/ref/hs37d5.fa.fai"
        File ref_fasta_dict="s3://tgp-data-analysis/ref/hs37d5.dict"

        String sampleGenomicID
        String sampleRunID

        String s3_out_root_dir="s3://tgp-sample-processing/~{sampleRunID}"

        Boolean call_svs = true
        Boolean? fast_less_sensitive_sv = true
        File sniffles_tandem_repeat_bed = "s3://tgp-data-analysis/resources/human_hs37d5.trf.bed"

        Boolean call_small_variants = true

        Boolean? run_dv_pepper_analysis = true
        Int? dvp_threads = 32
        Int? dvp_memory = 128
        File? ref_scatter_interval_list_locator = "s3://tgp-data-analysis/resources/lr-scattered_calling_intervals_files.txt"
        File? ref_scatter_interval_list_ids = "s3://tgp-data-analysis/resources/lr-scattered_calling_intervals_ids.txt"
    }

    String outdir = s3_out_root_dir

    # align reads
    call Align.Minimap2 as align {
        input:
            reads             = Fastqs,
            sampleGenomicID   = sampleGenomicID,
            sampleRunID       = sampleRunID,
            ref_fasta         = ref_fasta,
            ref_fasta_fai     = ref_fasta_fai,
            ref_dict          = ref_dict,
            map_preset        = 'map-ont'
        }

    File bam = align.aligned_bam
    File bai = align.aligned_bai

    call COV.SampleLevelAlignedMetrics as coverage {
        input:
            aligned_bam = bam,
            aligned_bai = bai,
            ref_fasta   = ref_fasta,
            bed_to_compute_coverage = bed_to_compute_coverage
    }

    String dir = outdir + "/alignments"

    call FF.FinalizeToFile as FinalizeBam { input: outdir = dir, file = bam, name = "~{sampleGenomicID}.bam" }
    call FF.FinalizeToFile as FinalizeBai { input: outdir = dir, file = bai, name = "~{sampleGenomicID}.bam.bai" }
    call FF.FinalizeToFile as FinalizePbi { input: outdir = dir, file = pbi, name = "~{sampleGenomicID}.bam.pbi" }

    if (defined(bed_to_compute_coverage)) { call FF.FinalizeToFile as FinalizeRegionalCoverage { input: outdir = dir, file = select_first([coverage.bed_cov_summary]) } }

    ####################################################################################################
    if (call_svs || call_small_variants) {

        # verify arguments are provided
        if (call_svs) {
            if (! defined(fast_less_sensitive_sv)) {call Utils.StopWorkflow as fast_less_sensitive_sv_not_provided {input: reason = "Calling SVs without specifying arg fast_less_sensitive_sv"}}
        }
        if (call_small_variants) {
            if (! defined(call_small_vars_on_mitochondria)) {call Utils.StopWorkflow as call_small_vars_on_mitochondria_not_provided {input: reason = "Unprovided arg call_small_vars_on_mitochondria"}}
            if (! defined(run_dv_pepper_analysis)) {call Utils.StopWorkflow as run_dv_pepper_analysis_not_provided {input: reason = "Unprovided arg run_dv_pepper_analysis"}}
            if (! defined(dvp_threads)) {call Utils.StopWorkflow as dvp_threads_not_provided {input: reason = "Unprovided arg dvp_threads"}}
            if (! defined(ref_scatter_interval_list_locator)) {call Utils.StopWorkflow as ref_scatter_interval_list_locator_not_provided {input: reason = "Unprovided arg ref_scatter_interval_list_locator"}}
            if (! defined(ref_scatter_interval_list_ids)) {call Utils.StopWorkflow as ref_scatter_interval_list_ids_not_provided {input: reason = "Unprovided arg ref_scatter_interval_list_ids"}}
        }

        call VAR.CallVariants {
            input:
                bam               = bam,
                bai               = bai,
                sample_id         = sampleGenomicID,
                ref_fasta         = ref_fasta,
                ref_fasta_fai     = ref_fasta_fai,
                ref_dict          = ref_dict,
                tandem_repeat_bed = sniffles_tandem_repeat_bed,

                prefix = sampleGenomicID,

                call_svs = call_svs,
                fast_less_sensitive_sv = select_first([fast_less_sensitive_sv]),

                call_small_variants = call_small_variants,
                run_dv_pepper_analysis = select_first([run_dv_pepper_analysis]),
                dvp_threads = select_first([dvp_threads]),
                dvp_memory = select_first([dvp_memory]),
                ref_scatter_interval_list_locator = select_first([ref_scatter_interval_list_locator]),
                ref_scatter_interval_list_ids = select_first([ref_scatter_interval_list_ids]),

                call_trs = call_trs
        }

        String svdir = outdir + "/variants/sv"
        String smalldir = outdir + "/variants/small"

        if (call_svs) {
            call FF.FinalizeToFile as FinalizePBSV { input: outdir = svdir, file = select_first([CallVariants.pbsv_vcf]) }
            call FF.FinalizeToFile as FinalizePBSVtbi { input: outdir = svdir, file = select_first([CallVariants.pbsv_tbi]) }

            call FF.FinalizeToFile as FinalizeSniffles { input: outdir = svdir, file = select_first([CallVariants.sniffles_vcf]) }
            call FF.FinalizeToFile as FinalizeSnifflesTbi { input: outdir = svdir, file = select_first([CallVariants.sniffles_tbi]) }
        }

        if (call_small_variants) {
            if (select_first([run_dv_pepper_analysis])) {
                call FF.FinalizeToFile as FinalizeDVPepperVcf { input: outdir = smalldir, file = select_first([CallVariants.dvp_vcf])}
                call FF.FinalizeToFile as FinalizeDVPepperTbi { input: outdir = smalldir, file = select_first([CallVariants.dvp_tbi])}
                call FF.FinalizeToFile as FinalizeDVPepperGVcf { input: outdir = smalldir, file = select_first([CallVariants.dvp_g_vcf])}
                call FF.FinalizeToFile as FinalizeDVPepperGTbi { input: outdir = smalldir, file = select_first([CallVariants.dvp_g_tbi])}
                call FF.FinalizeToFile as FinalizeDVPEPPERPhasedVcf { input: outdir = smalldir, file = select_first([CallVariants.dvp_phased_vcf]), name = "~{sampleGenomicID}.deepvariant_pepper.phased.vcf.gz" }
                call FF.FinalizeToFile as FinalizeDVPEPPERPhasedTbi { input: outdir = smalldir, file = select_first([CallVariants.dvp_phased_tbi]), name = "~{sampleGenomicID}.deepvariant_pepper.phased.vcf.gz.tbi" }
            }
        }
    }

    output {
        File aligned_bam = FinalizeBam.s3_path
        File aligned_bai = FinalizeBai.s3_path

        Float aligned_num_reads = coverage.aligned_num_reads
        Float aligned_num_bases = coverage.aligned_num_bases
        Float aligned_frac_bases = coverage.aligned_frac_bases
        Float aligned_est_fold_cov = coverage.aligned_est_fold_cov

        Float aligned_read_length_mean = coverage.aligned_read_length_mean
        Float aligned_read_length_median = coverage.aligned_read_length_median
        Float aligned_read_length_stdev = coverage.aligned_read_length_stdev
        Float aligned_read_length_N50 = coverage.aligned_read_length_N50

        Float average_identity = coverage.average_identity
        Float median_identity = coverage.median_identity

        File? bed_cov_summary = FinalizeRegionalCoverage.s3_path
        ########################################
        File? pbsv_vcf = FinalizePBSV.s3_path
        File? pbsv_tbi = FinalizePBSVtbi.s3_path

        File? sniffles_vcf = FinalizeSniffles.s3_path
        File? sniffles_tbi = FinalizeSnifflesTbi.s3_path

        File? dvp_vcf = FinalizeDVPepperVcf.s3_path
        File? dvp_tbi = FinalizeDVPepperTbi.s3_path
        File? dvp_g_vcf = FinalizeDVPepperGVcf.s3_path
        File? dvp_g_tbi = FinalizeDVPepperGTbi.s3_path
        File? dvp_phased_vcf = FinalizeDVPEPPERPhasedVcf.s3_path
        File? dvp_phased_tbi = FinalizeDVPEPPERPhasedTbi.s3_path
    }
}