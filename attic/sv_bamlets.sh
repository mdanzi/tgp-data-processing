#!/usr/bin/env bash
set -xeuo pipefail

sampleGenomicID=$1
sampleRunID=$2
sampleGUID=$3
allSampleRunIDs=$4

source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport & s3_path_exists
trap "errorReport" ERR


s3_sv_vcf=$(get_s3_path $sampleRunID $sampleGenomicID.combined.genotyped.vcf)
s3_bam=$(get_s3_path $sampleRunID $sampleGenomicID.hg19.sorted.dedup.bam)
if ! s3_path_exists $s3_sv_vcf || ! s3_path_exists $s3_bam; then
  echo "[TGP Warning]: Exiting without creating SV bamlets because $s3_sv_vcf and $s3_bam were not found" >&2
  exit 0
fi

# download all SV vcfs
for runID in ${allSampleRunIDs//,/ }; do
  if aws s3 ls s3://tgp-sample-processing/${runID}/ | grep ".combined.genotyped.vcf"; then
    aws s3 cp s3://tgp-sample-processing/${runID}/ . --recursive --exclude "*" --include "*.combined.genotyped.vcf" --only-show-errors
  fi
done

aws s3 rm s3://tgp-sample-assets/${sampleGUID}/SVs/ --recursive --only-show-errors
aws s3 cp ${s3_bam} . --only-show-errors
aws s3 cp ${s3_bam}.bai . --only-show-errors
set +u
source activate py37
set -u
for VCF in *.combined.genotyped.vcf; do
    python $TGP_HOME/tgp-data-processing/scripts/structuralVariation/printBamlets.py --vcf=${VCF} --bam=${sampleGenomicID}.hg19.sorted.dedup.bam --out=${sampleGUID}/SVs/
done
set +u
conda deactivate
set -u            
cd ${sampleGUID}/SVs/
for FILE in *.bam; do samtools index ${FILE}; done
cd ../../
aws s3 cp ${sampleGUID}/SVs/ s3://tgp-sample-assets/${sampleGUID}/SVs/ --recursive --only-show-errors

