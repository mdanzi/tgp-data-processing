#!/usr/bin/env bash
set -xeuo pipefail

cohortRunID=$1
sampleGenomicID=$2
sampleRunID=$3
sampleGUID=$4

# cd /home/ec2-user
# source .bashrc
# git clone https://gitlab.com/mdanzi/tgp-data-processing.git
source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
trap "errorReport" ERR

combined_vcf=${cohortRunID}.vcf.gz
s3_combined_vcf=$(get_s3_path $cohortRunID $combined_vcf)

aws s3 cp $s3_combined_vcf . --only-show-errors
gunzip $combined_vcf

vcf-subset -c ${sampleGenomicID} ${cohortRunID}.vcf > ${sampleGenomicID}.vcf
echo -e "fileName\tsampleName\tChr\tTSTV\tnumSNPs\tnumINDELs\tavgDepth\tavgQuality\tHeterozygousHomozygousRatio\tmeanGQ\tGender" > ${sampleGenomicID}.vcfLevel.byChr.qc
TSTV=$(cat ${sampleGenomicID}.vcf | vcf-tstv | awk '{print $1}')
vcf-stats ${sampleGenomicID}.vcf > tmpStats.txt 
SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
DEPTH=$(vcftools --vcf ${sampleGenomicID}.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
QUAL=$(vcftools --vcf ${sampleGenomicID}.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
if [ "${SNPs}" == "" ]; then SNPs="0"; fi
if [ "${INDELs}" == "" ]; then INDELs="0"; fi
if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
GQ=$(vcftools --vcf ${sampleGenomicID}.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')

## new, peddy-based sex check
echo -e "${sampleGenomicID}\t${sampleGenomicID}\t0\t0\t0\t0" > ${sampleGenomicID}.ped
bgzip -c ${sampleGenomicID}.vcf > ${sampleGenomicID}.vcf.gz
tabix ${sampleGenomicID}.vcf.gz
set +u
source activate py2
set -u
python -m peddy --prefix ${sampleGenomicID} ${sampleGenomicID}.vcf.gz ${sampleGenomicID}.ped
set +u
conda deactivate
set -u
SEX=$(sed -n '2p' ${sampleGenomicID}.sex_check.csv | cut -f 7 -d, )
if [ ${SEX} == "male" ]; then
    SEX='M'
elif [ ${SEX} == "female" ]; then
    SEX='F'
else 
    SEX='U'
fi 

echo -e "${cohortRunID}.vcf\t${sampleGenomicID}\tGenome\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
for j in {1..22} X Y; do 
    vcftools --vcf ${sampleGenomicID}.vcf --chr ${j} --stdout --recode > thisChr.vcf 
    numVariants=$(grep -v ^'#' thisChr.vcf | wc -l)
    if [ "${numVariants}" -gt "0" ]; then 
        TSTV=$(cat thisChr.vcf | vcf-tstv | awk '{print $1}')
        vcf-stats thisChr.vcf > tmpStats.txt 
        SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
        INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
        DEPTH=$(vcftools --vcf thisChr.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
        QUAL=$(vcftools --vcf thisChr.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
        HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
        HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
        HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
        if [ "${SNPs}" == "" ]; then SNPs="0"; fi
        if [ "${INDELs}" == "" ]; then INDELs="0"; fi
        if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
        if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
        if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
        HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
        GQ=$(vcftools --vcf thisChr.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')
        echo -e "${cohortRunID}.vcf\t${sampleGenomicID}\t${j}\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
    fi
done 
##ancestry clustering
aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.peddySites.vcf . --only-show-errors
vcftools --vcf ${sampleGenomicID}.peddySites.vcf --012 --out ${sampleGenomicID}
set +u
source activate py2
set -u
python $TGP_HOME/tgp-data-processing/scripts/vcfLevelQC/samplePCA.py -i ${sampleGenomicID}
set +u
conda deactivate
set -u

# upload results 
aws s3 cp ${sampleGenomicID}.AncestryClustering.png s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
aws s3 cp ${sampleGenomicID}.vcfLevel.byChr.qc s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
