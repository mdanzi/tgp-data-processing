subset_bam_to_chr_m() {
  local sample_genomic_id=$1 sample_run_id=$2 input_bam=$3

  s3_cp_from_processing $input_bam $sample_run_id
  s3_cp_from_processing $input_bam.bai $sample_run_id

  $GATK PrintReads \
    -R $TGP_HOME/ref/hs37d5.fa \
    -L MT \
    --read-filter MateOnSameContigOrNoMappedMateReadFilter \
    --read-filter MateUnmappedAndUnmappedReadFilter \
    -I $input_bam \
    --read-index $input_bam.bai \
    -O ${sample_genomic_id}.bam
}

revert_sam() {
  local input_bam=$1 sample_genomic_id=$2 sample_run_id=$3

  $GATK --java-options "-Xmx1000m" RevertSam \
    --INPUT $input_bam \
    --OUTPUT_BY_READGROUP false \
    --OUTPUT ${sample_genomic_id}.bam \
    --VALIDATION_STRINGENCY LENIENT \
    --ATTRIBUTE_TO_CLEAR FT \
    --ATTRIBUTE_TO_CLEAR CO \
    --SORT_ORDER queryname \
    --RESTORE_ORIGINAL_QUALITIES false
}

coverage_at_every_base() {
  local bam_regular=$1 bam_regular_idx=$2 
  local bam_shifted=$3 bam_shifted_idx=$4
  local sample_genomic_id=$5 sample_run_id=$6

  $GATK CollectHsMetrics \
    -I $bam_regular \
    -R $TGP_HOME/ref/mitochondria/hs37d5.MT.fa \
    --PER_BASE_COVERAGE non_control_region.tsv \
    -O non_control_region.metrics \
    -TI $TGP_HOME/ref/mitochondria/non_control_region.MT.interval_list \
    -BI $TGP_HOME/ref/mitochondria/non_control_region.MT.interval_list \
    -covMax 20000 \
    --SAMPLE_SIZE 1
  
  $GATK CollectHsMetrics \
    -I $bam_shifted \
    -R $TGP_HOME/ref/mitochondria/hs37d5.MT.shifted_by_8000_bases.fa \
    --PER_BASE_COVERAGE control_region_shifted.tsv \
    -O control_region_shifted.metrics \
    -TI $TGP_HOME/ref/mitochondria/control_region_shifted.MT.interval_list \
    -BI $TGP_HOME/ref/mitochondria/control_region_shifted.MT.interval_list \
    -covMax 20000 \
    --SAMPLE_SIZE 1

  set +u
  conda activate gatk
  set -u
  
  R --vanilla <<CODE
    shift_back = function(x) {
      if (x < 8570) { 
        return(x + 8000)  
      } else {
        return (x - 8569)
      }
    }

    control_region_shifted = read.table("control_region_shifted.tsv", header=T) 
    shifted_back = sapply(control_region_shifted[,"pos"], shift_back)
    control_region_shifted[,"pos"] = shifted_back

    beginning = subset(control_region_shifted, control_region_shifted[,'pos']<8000)
    end = subset(control_region_shifted, control_region_shifted[,'pos']>8000)  

    non_control_region = read.table("non_control_region.tsv", header=T)
    combined_table = rbind(beginning, non_control_region, end)
    write.table(combined_table, "per_base_coverage.tsv", row.names=F, col.names=T, quote=F, sep="\t")  
CODE

  coverage_tsv=${sample_genomic_id}_MT_per_base_coverage.tsv
  mv per_base_coverage.tsv $coverage_tsv 
  s3_cp_to_processing $coverage_tsv $sample_run_id
}

split_multi_allelic_sites() {
  local input_vcf=$1 sample_genomic_id=$2 sample_run_id=$3
  local output_vcf=${sample_genomic_id}.MT.final.split.vcf

  $GATK LeftAlignAndTrimVariants \
    -R $TGP_HOME/ref/hs37d5.fa \
    -V $input_vcf \
    -O $output_vcf \
    --split-multi-allelics \
    --dont-trim-alleles \
    --keep-original-ac
  
  s3_cp_to_processing $output_vcf $sample_run_id
  s3_cp_to_processing $output_vcf.idx $sample_run_id  
}
