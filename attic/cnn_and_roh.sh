#!/usr/bin/env bash
set -xeuo pipefail

# String sampleGenomicID
# String cohortRunID
# String sampleRunID
# String sampleGUID
# File callingIntervals
# String gatk_path
# Boolean? singleton = false
# RuntimeAttr? runtime_attr_override

TGP_HOME=/home/ec2-user

cohortRunID=$1
sampleGenomicID=$2
sampleRunID=$3
sampleGUID=$4
callingIntervals=$5
gatk_path=$6
singleton=$7

run_roh() {
  local vcf_gz=$1
  local sample_genomic_id=$2
  local sample_id=$3
  local run_id=$4
  local roh_regions=${sample_genomic_id}.RohRegions.txt
  local roh_txt=${sample_genomic_id}.RegionsOfHomozygosity.txt
  local roh_bed=${sample_genomic_id}.RegionsOfHomozygosity.bed
  bcftools annotate -c CHROM,POS,REF,ALT,AF1KG -h 1000GP-AFs/AFs.tab.gz.hdr -a 1000GP-AFs/AFs.tab.gz "${vcf_gz}" |
    bcftools roh --AF-tag AF1KG -M 100 -m genetic-map/genetic_map_chr{CHROM}_combined_b37.txt -o roh.txt
  if grep '^RG' roh.txt > $roh_regions; then
    # filter the output to only include ROHs at least 5kb in size
    echo -e "chr\tstart\tend\tlength\tnumberOfMarkers\tquality" > $roh_txt
    awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,$6,$7,$8}}' $roh_regions >> $roh_txt
    # create bed file for IGV visualization
    echo "#gffTags" > $roh_bed
    awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,"Length="$6";NumberOfMarkers="$7";Quality="$8}}' $roh_regions >> $roh_bed
    # upload the roh's to s3
    aws s3 cp $roh_txt s3://tgp-sample-processing/${run_id}/ --only-show-errors
    aws s3 cp $roh_bed s3://tgp-sample-assets/${sample_id}/ROHs/ --only-show-errors
  fi
}

#cd /home/ec2-user
# source .bashrc
# git clone https://gitlab.com/mdanzi/tgp-data-processing.git
source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
trap "errorReport cohort ${cohortRunID}" ERR

aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.hg19.g.vcf.gz . --only-show-errors
aws s3 cp s3://tgp-sample-processing/${sampleRunID}/${sampleGenomicID}.hg19.g.vcf.idx . --only-show-errors
gunzip ${sampleGenomicID}.hg19.g.vcf.gz


# prepare 1000Genomes data on allele frequencies and genetic mappings for ROH mapping
aws s3 cp s3://tgp-data-analysis/StructuralVariation/genetic-map.tgz . --only-show-errors
aws s3 cp s3://tgp-data-analysis/StructuralVariation/1000GP-AFs.tgz . --only-show-errors
tar -zxvf 1000GP-AFs.tgz
tar -zxvf genetic-map.tgz

${gatk_path} --java-options "-Xmx5g" GenotypeGVCFs -V ${sampleGenomicID}.hg19.g.vcf -R $TGP_HOME/ref/hs37d5.fa -O ${sampleGenomicID}.hg19.genotyped.vcf --allow-old-rms-mapping-quality-annotation-data --use-new-qual-calculator --only-output-calls-starting-in-intervals -L ${callingIntervals} --merge-input-intervals
bcftools filter -i "(FORMAT/DP)>=6" ${sampleGenomicID}.hg19.genotyped.vcf > ${sampleGenomicID}.hg19.genotyped.filtered.vcf
if [[ ${singleton} -eq 1 ]]; then
  bgzip -c ${sampleGenomicID}.hg19.genotyped.filtered.vcf > ${cohortRunID}.vcf.gz
  tabix ${cohortRunID}.vcf.gz
  aws s3 cp ${cohortRunID}.vcf.gz s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
  aws s3 cp ${cohortRunID}.vcf.gz.tbi s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
fi
set +u
source activate gatk
set -u
${gatk_path} CNNScoreVariants -V ${sampleGenomicID}.hg19.genotyped.filtered.vcf -R $TGP_HOME/ref/hs37d5.fa -O ${sampleGenomicID}.CNN.vcf
${gatk_path} FilterVariantTranches -V ${sampleGenomicID}.CNN.vcf -O ${sampleGenomicID}.CNN.filtered.vcf --resource $TGP_HOME/ref/hapmap_3.3.b37.vcf --resource $TGP_HOME/ref/Mills_and_1000G_gold_standard.indels.b37.vcf --info-key CNN_1D --snp-tranche 99.95 --snp-tranche 99.9 --snp-tranche 99.5 --snp-tranche 99 --snp-tranche 98 --snp-tranche 95 --indel-tranche 99.4 --indel-tranche 99.2 --indel-tranche 99 --indel-tranche 98 --indel-tranche 95
set +u
conda deactivate
set -u
# upload the CNN-filtered VCF to the cohortRun folder on S3
aws s3 cp ${sampleGenomicID}.CNN.filtered.vcf s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
## Runs of Homozygosity mapping
# remove the CNN-failed variants
grep -wv 'CNN_1D_SNP_Tranche_99.95_100.00' ${sampleGenomicID}.CNN.filtered.vcf | grep -wv 'CNN_1D_INDEL_Tranche_99.40_100.00' > ${sampleGenomicID}.filtered.vcf
bgzip ${sampleGenomicID}.filtered.vcf
tabix ${sampleGenomicID}.filtered.vcf.gz

# run bcftools roh on the sample
run_roh ${sampleGenomicID}.filtered.vcf.gz $sampleGenomicID $sampleGUID $sampleRunID
