#!/usr/bin/env bash
set -xeuo pipefail

source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport & s3_path_exists
trap "errorReport" ERR

GATK_HOME=/home/ec2-user/gatk-4.2.6.1/gatk
WORKSPACE=genomicsdb

cohortRunID=$1
all_sample_genomics_ids=$2
all_sample_run_ids=$3
map_file=$4
interval=$5

rm -rf $WORKSPACE

aws s3 cp s3://tgp-sample-processing/$cohortRunID/$map_file . --only-show-errors

sampleGenomicIDsArray=(${all_sample_genomics_ids//,/ })
sampleRunIDsArray=(${all_sample_run_ids//,/ })

sample_count=${#sampleGenomicIDsArray[@]}
for ((idx=0; idx < $sample_count; idx++)); do
  gvcf="s3://tgp-sample-processing/${sampleRunIDsArray[${idx}]}/${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz"
  aws s3 cp $gvcf . --only-show-errors
  if s3_path_exists ${gvcf}.tbi; then
    aws s3 cp ${gvcf}.tbi . --only-show-errors
  else
    gunzip ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz
    bgzip ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf
    tabix ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz
  fi
done

# We've seen some GenomicsDB performance regressions related to intervals, so we're going to pretend we only have a single interval
# using the --merge-input-intervals arg
# There's no data in between since we didn't run HaplotypeCaller over those loci so we're not wasting any compute

# The memory setting here is very important and must be several GiB lower
# than the total memory allocated to the VM because this tool uses
# a significant amount of non-heap memory for native libraries.
# Also, testing has shown that the multithreaded reader initialization
# does not scale well beyond 5 threads, so don't increase beyond that.
$GATK_HOME --java-options "-Xms8000m -Xmx25000m" \
  GenomicsDBImport \
  --genomicsdb-workspace-path $WORKSPACE \
  -L ${interval} \
  --sample-name-map ${map_file} \
  --reader-threads 5 \
  --merge-input-intervals 

tar -cf $WORKSPACE.tar $WORKSPACE 
