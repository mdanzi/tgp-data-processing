#!/usr/bin/env bash
set -xeuo pipefail

source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport & s3_path_exists
trap "errorReport" ERR

GATK_HOME=/home/ec2-user/gatk-4.2.6.1/gatk

outfile=$1
cohort_run_id=$2
shift
shift # remaining args are *.vcf.gz files


# --ignore-safety-checks makes a big performance difference so we include it in our invocation.
# This argument disables expensive checks that the file headers contain the same set of
# genotyped samples and that files are in order by position of first record.
$GATK_HOME --java-options "-Xms6000m -Xmx6500m" \
  GatherVcfsCloud \
  --ignore-safety-checks \
  --gather-type BLOCK \
  $(printf -- "--input %s " "$@") \
  --output $outfile

# output should be bgzipped
mv $outfile tmp.gz
tabix tmp.gz

bcftools filter -i "MAX(FORMAT/DP)>=6" -o $outfile -O b tmp.gz 
tabix $outfile
s3_cp_to_processing $outfile $cohort_run_id
s3_cp_to_processing $outfile.tbi $cohort_run_id

