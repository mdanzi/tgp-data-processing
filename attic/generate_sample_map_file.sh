#!/usr/bin/env bash
set -xeuo pipefail

source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport & s3_path_exists
trap "errorReport" ERR

cohortRunID=$1
allSampleGenomicIDs=$2
# sampleGenomicIDsArray=(${allSampleGenomicIDs//,/ })

for sampleGenomicID in ${allSampleGenomicIDs//,/ }; do
  printf "%s\t%s.hg19.g.vcf.gz\n" $sampleGenomicID $sampleGenomicID >> sample_map_file.txt
done

aws s3 cp sample_map_file.txt s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
