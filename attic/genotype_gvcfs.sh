#!/usr/bin/env bash
set -xeuo pipefail

source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport & s3_path_exists
trap "errorReport" ERR

GATK_HOME=/home/ec2-user/gatk-4.2.6.1/gatk

workspace_tar=$1
interval=$2
outfile=$3

tar -xf ${workspace_tar}
WORKSPACE=$(basename ${workspace_tar} .tar)

$GATK_HOME --java-options "-Xms8000m -Xmx25000m" \
  GenotypeGVCFs \
  -R $TGP_HOME/ref/hs37d5.fa \
  -O ${outfile} \
  --only-output-calls-starting-in-intervals \
  --use-new-qual-calculator \
  -V gendb://$WORKSPACE \
  -L ${interval} \
  --allow-old-rms-mapping-quality-annotation-data \
  --merge-input-intervals

#mv ~{output_vcf_filename} /tmp/scratch/
#mv ~{output_vcf_filename}.tbi /tmp/scratch/
