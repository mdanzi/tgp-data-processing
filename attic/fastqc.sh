#!/usr/bin/env bash
set -xeuo pipefail

source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh
trap "errorReport" ERR

sample_genomic_id=$1
sample_run_id=$2
read1_csv=$3
read2_csv=$4
output="${sample_genomic_id}.fastqLevel.qc"

read1_array=(${read1_csv//,/ })
read2_array=(${read2_csv//,/ })

# re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
firstFile=${read1_array[0]}
extension=${firstFile##*.}
fastqExtension=""
if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=$(echo ".${extension}"); fi

# make output file for this file 
echo -e "fileName\tsampleName\tnumberOfReads\tsizeInGB\tQ30" > $output

# QC read1
read1="${sample_genomic_id}_read1.fq${fastqExtension}"
mkfifo $read1
s3_stream_output "${read1_array[@]}" > $read1 &
$TGP_HOME/FastQC/fastqc ${read1} 
unzip ${sample_genomic_id}_read1_fastqc.zip
numReads=$(grep "Total Sequences" ${sample_genomic_id}_read1_fastqc/fastqc_data.txt | awk '{print $3}')
fileSize=$(du -h ${read1} | awk '{print $1}')
numQ30=$(grep -A42 -m1 "Per sequence quality" ${sample_genomic_id}_read1_fastqc/fastqc_data.txt | sed -n '/^29/{:a;n;p;/^>>END/!ba}' | head -n -1 | awk '{n1+=$2} END {print n1}')
percentQ30=$(expr 100 \* ${numQ30} / ${numReads} )
echo -e "${read1}\t${sample_genomic_id}\t${numReads}\t${fileSize}\t${percentQ30}" >> $output

# QC read2
read2="${sample_genomic_id}_read2.fq${fastqExtension}"
mkfifo $read2
s3_stream_output "${read2_array[@]}" > $read2 &
$TGP_HOME/FastQC/fastqc ${read2} 
unzip ${sample_genomic_id}_read2_fastqc.zip
numReads=$(grep "Total Sequences" ${sample_genomic_id}_read2_fastqc/fastqc_data.txt | awk '{print $3}')
fileSize=$(du -h ${read2} | awk '{print $1}')
numQ30=$(grep -A42 -m1 "Per sequence quality" ${sample_genomic_id}_read2_fastqc/fastqc_data.txt | sed -n '/^29/{:a;n;p;/^>>END/!ba}' | head -n -1 | awk '{n1+=$2} END {print n1}')
percentQ30=$(expr 100 \* ${numQ30} / ${numReads} )
echo -e "${read2}\t${sample_genomic_id}\t${numReads}\t${fileSize}\t${percentQ30}" >> $output

# upload output to S3
s3_cp_to_processing $output $sample_run_id
