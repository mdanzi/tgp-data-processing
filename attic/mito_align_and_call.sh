align_and_mark_duplicates() {
  local input_bam=$1 ref_prefix=$2 sample_genomic_id=$3 sample_run_id=$4

  $GATK SamToFastq \
    --INPUT $input_bam \
    --FASTQ /dev/stdout \
    --INTERLEAVE true \
    -NON_PF true | \
  
  bwa mem -K 100000000 -p -v 3 -t 2 -Y $ref_prefix.fa /dev/stdin - 2> >(tee ${sample_genomic_id}.bwa.stderr.log >&2) | \

  $GATK MergeBamAlignment \
    --VALIDATION_STRINGENCY SILENT \
    --EXPECTED_ORIENTATIONS FR \
    --ATTRIBUTES_TO_RETAIN X0 \
    --ATTRIBUTES_TO_REMOVE NM \ 
    --ATTRIBUTES_TO_REMOVE MD \
    --ALIGNED_BAM /dev/stdin \
    --UNMAPPED_BAM $input_bam \
    --OUTPUT mba.bam \
    --REFERENCE_SEQUENCE $ref_prefix.fa \
    --PAIRED_RUN true \
    --SORT_ORDER unsorted \
    --IS_BISULFITE_SEQUENCE false \
    --ALIGNED_READS_ONLY false \
    --CLIP_ADAPTERS false \
    --MAX_RECORDS_IN_RAM 2000000 \
    --ADD_MATE_CIGAR true \
    --MAX_INSERTIONS_OR_DELETIONS -1 \
    --PRIMARY_ALIGNMENT_STRATEGY MostDistant  

  $GATK MarkDuplicates \
    --INPUT mba.bam \ 
    --OUTPUT md.bam \
    --METRICS_FILE ${sample_genomic_id}.metrics \
    --VALIDATION_STRINGENCY SILENT \
    --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
    --ASSUME_SORT_ORDER queryname \
    --CLEAR_DT false \
    --ADD_PG_TAG_TO_READS false

  $GATK SortSam \
    --INPUT md.bam \
    --OUTPUT ${sample_genomic_id}.sorted.bam \
    --SORT_ORDER coordinate \
    --CREATE_INDEX true \
    --MAX_RECORDS_IN_RAM 300000
}

get_contamination() {
  local input_vcf=$1 sample_genomic_id=$2 sample_run_id=$3

  java -jar haplocheckCLI/haplocheckCLI.jar $(dirname $input_vcf)

  # process output
} 

collect_wgs_metrics() {
  local input_bam=$1 input_bam_index=$2 sample_genomic_id=$3 sample_run_id=$4
  
  $GATK CollectWgsMetrics \
    --INPUT $input_bam \
    --REFERENCE_SEQUENCE $TGP_HOME/ref/mitochondria/hs37d5.MT.fa \ 
    --OUTPUT metrics.txt \
    --USE_FAST_ALGORITHM true \  
    --INCLUDE_BQ_HISTOGRAM true \
    --THEORETICAL_SENSITIVITY_OUTPUT theoretical_sensitivity.txt

  Rscript process_metrics.R
}

lift_over_and_combine_vcfs() {
  local shifted_vcf=$1 vcf=$2 sample_genomic_id=$3 sample_run_id=$4
  
  $GATK LiftoverVcf \
    -I $shifted_vcf \
    -O ${sample_genomic_id}.shifted_back.vcf \
    -R $TGP_HOME/ref/mitochondria/hs37d5.MT.fa \
    --CHAIN $TGP_HOME/ref/mitochondria/ShiftBack.chain \
    --REJECT ${sample_genomic_id}.rejected.vcf

  $GATK MergeVcfs 
    -I ${sample_genomic_id}.shifted_back.vcf
    -I $vcf 
    -O ${sample_genomic_id}.merged.vcf  
}

m2() {
  local ref_prefix=$1 input_bam=$2 input_bai=$3 
  local sample_genomic_id=$4 sample_run_id=$5  
  
  $GATK Mutect2 \
    -R $ref_prefix.fa  
    -I $input_bam \
    --read-filter MateOnSameContigOrNoMappedMateReadFilter \
    --read-filter MateUnmappedAndUnmappedReadFilter \ 
    -O ${sample_genomic_id}.raw.vcf.gz \
    --annotation StrandBiasBySample \
    --mitochondria-mode \ 
    --max-reads-per-alignment-start 75 \
    --max-mnp-distance 0
}

filter() {
  local raw_vcf=$1 raw_vcf_index=$2 raw_vcf_stats=$3  
  local sample_genomic_id=$4 sample_run_id=$5
  
  $GATK FilterMutectCalls -V $raw_vcf  
    -R $TGP_HOME/ref/mitochondria/hs37d5.MT.fa  
    -O ${sample_genomic_id}.filtered.vcf  
    --stats $raw_vcf_stats  
    --mitochondria-mode

  $GATK VariantFiltration  
    -V ${sample_genomic_id}.filtered.vcf  
    -O ${sample_genomic_id}.final.vcf
    --apply-allele-specific-filters  
    --mask $TGP_HOME/ref/mitochondria/blacklist_sites.hs37d5.MT.bed
    --mask-name blacklisted_site
} 

merge_stats() {
  local shifted_stats=$1 non_shifted_stats=$2 sample_genomic_id=$3 sample_run_id=$4

  $GATK MergeMutectStats \ 
    --stats $shifted_stats \
    --stats $non_shifted_stats \
    -O ${sample_genomic_id}.combined.stats  
}

split_multi_allelic_sites() {
  local filtered_vcf=$1 filtered_vcf_index=$2 sample_genomic_id=$3 sample_run_id=$4

  $GATK LeftAlignAndTrimVariants \
    -R $TGP_HOME/ref/mitochondria/hs37d5.MT.fa \
    -V $filtered_vcf \
    -O ${sample_genomic_id}.split.vcf \
    --split-multi-allelics \
    --dont-trim-alleles \
    --keep-original-ac

  $GATK SelectVariants  
    -V ${sample_genomic_id}.split.vcf  
    -O ${sample_genomic_id}.split_pass_only.vcf  
    --exclude-filtered
}
