set -xeuo pipefail

source $TGP_HOME/tgp-data-processing/scripts/utils/utils.sh  # imports errorReport & s3_path_exists
trap "errorReport" ERR

cohortRunID=$1
allSampleGenomicIDs=$2
allSampleRunIDs=$3

combinedVCF="${cohortRunID}.vcf.gz"
aws s3 cp s3://tgp-sample-processing/${cohortRunID}/${combinedVCF} . --only-show-errors
gunzip ${combinedVCF}

sampleGenomicIDsArray=(${allSampleGenomicIDs//,/ })
sampleRunIDsArray=(${allSampleRunIDs//,/ })
firstSampleGenomicID=${sampleGenomicIDsArray[0]}


# parse the vcf into a text file
timeCount=$(date +%s)
timeCount2=$(echo "scale=0; ${timeCount}/3600" | bc)
set +u
source activate py2
set -u
python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${firstSampleGenomicID}" -s $allSampleGenomicIDs < ${cohortRunID}.vcf > ${cohortRunID}.parsed.txt

# max_idx=$((${#sampleGenomicIDsArray[@]}-1))
# idx_list=$(seq 0 $max_idx)
# for idx in {0..~{length(sampleGenomicIDs)-1}}; do 
sample_count=${#sampleGenomicIDsArray[@]}
for ((idx=0; idx < $sample_count; idx++)); do
    sampleID=${sampleGenomicIDsArray[${idx}]}
    runID=${sampleRunIDsArray[${idx}]}
    # separate the parsed text file out by sample
    grep -w "^${sampleID}" ${cohortRunID}.parsed.txt > ${sampleID}.parsed.txt
    # download the CNN-filtered vcf file
    aws s3 cp s3://tgp-sample-processing/${cohortRunID}/${sampleID}.CNN.filtered.vcf . --only-show-errors
    # parse the CNN-filtered vcf into a text file
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.CNN.filtered.vcf > ${sampleID}.CNN.parsed.txt
    # set the filter column values to use the CNN filter status
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnCNNFilters.py --input=${sampleID}.parsed.txt --cnn=${sampleID}.CNN.parsed.txt --output=${sampleID}.parsed.2.txt
    # swap the mitochondrial variant calls
    if s3_path_exists s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf; then
      aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf . --only-show-errors
      aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf.idx . --only-show-errors
      python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.MT.final.split.vcf > ${sampleID}.MT.parsed.txt
      awk -F'\t' -v OFS='\t' '{if($2<25){print $0}}' ${sampleID}.parsed.2.txt | cat - ${sampleID}.MT.parsed.txt > ${sampleID}.parsed.3.txt
    else
      cp ${sampleID}.parsed.2.txt ${sampleID}.parsed.3.txt
    fi
    # add in custom variants
    if s3_path_exists s3://tgp-sample-processing/${runID}/${sampleID}.customSites.vcf; then
      aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.customSites.vcf . --only-show-errors
      python $TGP_HOME/tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.customSites.vcf > ${sampleID}.customSites.parsed.txt
      python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnCustomGenotypeCalls.py --input=${sampleID}.parsed.3.txt --customSites=${sampleID}.customSites.parsed.txt --output=${sampleID}.parsed.4.txt
    else
      cp ${sampleID}.parsed.3.txt ${sampleID}.parsed.4.txt
    fi
    # download the ROH files to this workspace
    if s3_path_exists s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt; then
        aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt . --only-show-errors
        # add the ROH info as another column
        tail -n +2 ${sampleID}.RegionsOfHomozygosity.txt | cut -f 1,2,3,4 > ${sampleID}.RegionsOfHomozygosity.2.txt
        numFields=$(grep -v '^#' ${cohortRunID}.vcf | awk -F'\t' '{print NF; exit}' || true)
        numFields=$((numFields + 4))
        bedtools intersect -wo -a ${cohortRunID}.vcf -b ${sampleID}.RegionsOfHomozygosity.2.txt | cut -f 1,2,${numFields} | sort -u -k1n,1n -k2n,2n > variantsInROHs.txt
    else
        touch variantsInROHs.txt
    fi
    python $TGP_HOME/tgp-data-processing/scripts/vcfParser/mergeOnROH.py --input=${sampleID}.parsed.4.txt --roh=variantsInROHs.txt --output=${sampleID}.parsed.txt
    # upload the results to S3
    aws s3 cp ${sampleID}.parsed.txt s3://tgp-sample-processing/${cohortRunID}/ --only-show-errors
done


