version 1.0

workflow testWorkflow {

  call test {
    }

}

task test {
  command <<<
    set -eo pipefail
    su - ec2-user
    echo $(pwd)
    cd /home/ec2-user/
    echo $(pwd)
    samtools --help
    git -C aws-pipeline pull
    echo "Completed Everything"
    >>>
  runtime {
    memory: "3 GB"
    cpu: "2"
    disks: "local-disk 200 HDD"
    docker: "public.ecr.aws/s5z5a3q9/tgp:latest"
  }
}
