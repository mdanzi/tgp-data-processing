version 1.0

struct RuntimeAttr {
    Float? mem_gb
    Int? cpu_cores
    Int? max_retries
    String? docker
    String? queueArn
}