version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task splitFastqs {
  input {
    Array[String] R1Fastqs
    Array[String] R2Fastqs
    String sampleGenomicID
    String sampleRunID
    Int? numCPUs=2
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Aligns a set of fastq file pairs to the GRCh37 reference genome."
  }

  parameter_meta {
    R1Fastqs: "File or array of files with the R1 fastq reads, can optionally be compressed with gzip or bzip2."
    R2Fastqs: "File or array of files with the R2 fastq reads, can optionally be compressed with gzip or bzip2."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    # download seqkit
    wget https://github.com/shenwei356/seqkit/releases/download/v2.4.0/seqkit_linux_amd64.tar.gz -O - | tar -zxf -

    # re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
    firstFile=~{R1Fastqs[0]}
    extension=${firstFile##*.}
    fastqExtension=""
    if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=$(echo ".${extension}"); fi

    read1="~{sampleGenomicID}_read1.fq${fastqExtension}"
    read2="~{sampleGenomicID}_read2.fq${fastqExtension}"

    # re-implementation of the s3_read_concat function -- can be moved later if that gets updated to cromwell-friendly version
    # Outputs a concatenated stream of all files listed in the input array
    s3_read_concat_cromwell() {
        for s3file in $1; do
            aws s3 cp --only-show-errors "$s3file" -
        done
    }
    s3_read_concat_cromwell "~{sep=' ' R1Fastqs}" > ${read1} &
    s3_read_concat_cromwell "~{sep=' ' R2Fastqs}" > ${read2}
    wait 


    # split fastqs
    ./seqkit split2 -1 ${read1} -2 ${read2} -p 16 -O /tmp/scratch -e .gz -j ~{numCPUs}
    


  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          numCPUs,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    Array[File] R1_shards = glob("~{sampleGenomicID}_read1.part*")
    Array[File] R2_shards = glob("~{sampleGenomicID}_read2.part*")
  }
}