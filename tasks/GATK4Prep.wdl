version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task gatk4Prep {
  input {
    String input_bam
    String sampleGenomicID
    String sampleRunID
    String? experimentType = "Unknown"
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Dedups Bam file and performs Bam-level quality checking."
  }

  parameter_meta {
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    # download data
    aws s3 cp ~{input_bam} . --only-show-errors

    # establish variables
    MinCov="6"
    RefExons="exome_calling_regions.bed"

    # dedup the BAM file
    if [ "~{experimentType}" != "Panel" ]; then
      { # try to mark duplicates
          gatk-4.2.6.1/gatk --java-options -Xmx3G MarkDuplicates -I ~{sampleGenomicID}.sorted.bam -O ~{sampleGenomicID}.hg19.sorted.dedup.bam -M ~{sampleGenomicID}.metrics.txt
      } || { # catch exception (this deals with the 'value put into pairInfoMap more than once' error)
          samtools view -b -f 0x2 ~{sampleGenomicID}.sorted.bam > ~{sampleGenomicID}.sorted.bam.tmp
          mv ~{sampleGenomicID}.sorted.bam.tmp ~{sampleGenomicID}.sorted.bam
          gatk-4.2.6.1/gatk --java-options -Xmx3G MarkDuplicates -I ~{sampleGenomicID}.sorted.bam -O ~{sampleGenomicID}.hg19.sorted.dedup.bam -M ~{sampleGenomicID}.metrics.txt
      }
    fi
    samtools index ~{sampleGenomicID}.hg19.sorted.dedup.bam

    # BAM-level QC
    DedupMetrics="~{sampleGenomicID}.metrics.txt"
    samtools flagstat ~{sampleGenomicID}.hg19.sorted.dedup.bam > ~{sampleGenomicID}.hg19.sorted.dedup.bam.flagstat 
    echo -e "sampleName\tpercentReadsAligned\tpercentProperlyPaired\tpercentDuplicates\testimatedLibrarySize\tpercentReferenceCovered\tpercentReadsOnExons" > ~{sampleGenomicID}.hg19.bamLevel.qc
    bedtools genomecov -ibam ~{sampleGenomicID}.hg19.sorted.dedup.bam > ~{sampleGenomicID}.hg19.coverage.txt
    PercentReadsAligned=$(grep "mapped" ~{sampleGenomicID}.hg19.sorted.dedup.bam.flagstat | sed '1q;d' | awk -F'(' '{print $2}' | sed 's/%.*//') 
    PercentReadsPaired=$(grep "properly paired" ~{sampleGenomicID}.hg19.sorted.dedup.bam.flagstat | sed '1q;d' | awk -F'(' '{print $2}' | sed 's/%.*//') 
    PercentDups=$(grep -v "#" ${DedupMetrics} | sed '3q;d' | awk -F'\t' '{print 100*$9}')
    EstLibSize=$(grep -v "#" ${DedupMetrics} | sed '3q;d' | awk -F'\t' '{print $10}')
    PercentRefCov=$((grep "genome" ~{sampleGenomicID}.hg19.coverage.txt || :) | head -n ${MinCov} | awk -F'\t' '{n1+=$5} END {print 1-n1}')
    PercentRefCov=$(awk -F'\t' '{print 100*$1}' <<< ${PercentRefCov})
    ReadsOnExons=$(bedtools intersect -u -bed -abam ~{sampleGenomicID}.hg19.sorted.dedup.bam -b ${RefExons} | wc -l) 
    TotalReads=$(grep "mapped" ~{sampleGenomicID}.hg19.sorted.dedup.bam.flagstat | sed '1q;d' | awk -F' ' '{print $1}') 
    PercentReadsOnExons=$(expr 100 \* ${ReadsOnExons} / ${TotalReads} )
    echo -e "~{sampleGenomicID}\t${PercentReadsAligned}\t${PercentReadsPaired}\t${PercentDups}\t${EstLibSize}\t${PercentRefCov}\t${PercentReadsOnExons}" >> ~{sampleGenomicID}.hg19.bamLevel.qc 

    # upload output to S3
    aws s3 cp ~{sampleGenomicID}.hg19.bamLevel.qc s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
    aws s3 cp ~{sampleGenomicID}.hg19.sorted.dedup.bam s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
    aws s3 cp ~{sampleGenomicID}.hg19.sorted.dedup.bam.bai s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors

    mv ~{sampleGenomicID}.hg19.sorted.dedup.bam /tmp/scratch/
    mv ~{sampleGenomicID}.hg19.sorted.dedup.bam.bai /tmp/scratch/

  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    String bam_qc = "s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.bamLevel.qc"
    String sorted_dedup_bam = "s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.sorted.dedup.bam"
    String sorted_dedup_bam_bai = "s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.sorted.dedup.bam.bai"
    String output_bam = "~{sampleGenomicID}.hg19.sorted.dedup.bam"
    String output_bam_bai = "~{sampleGenomicID}.hg19.sorted.dedup.bam.bai"
  }
}