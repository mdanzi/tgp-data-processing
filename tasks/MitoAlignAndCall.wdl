version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

workflow AlignAndCall {
  meta {
    description: "Takes in unmapped bam and outputs VCF of SNP/Indel calls on the mitochondria."
  }

  input {
    File unmapped_bam
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    Boolean compress_output_vcf = true
    RuntimeAttr? runtime_attr_override_AlignToMt
    RuntimeAttr? runtime_attr_override_AlignToShiftedMt
    RuntimeAttr? runtime_attr_override_CollectWgsMetrics
    RuntimeAttr? runtime_attr_override_CallMt
    RuntimeAttr? runtime_attr_override_CallShiftedMt
    RuntimeAttr? runtime_attr_override_LiftoverAndCombineVcfs
    RuntimeAttr? runtime_attr_override_MergeStats
    RuntimeAttr? runtime_attr_override_InitialFilter
    RuntimeAttr? runtime_attr_override_SplitMultiAllelicsAndRemoveNonPassSites
    RuntimeAttr? runtime_attr_override_GetContamination
    RuntimeAttr? runtime_attr_override_FilterContamination
  }

  parameter_meta {
    unmapped_bam: "Unmapped and subset bam, optionally with original alignment (OA) tag"
  }

  call AlignAndMarkDuplicates as AlignToMt {
    input:
      input_bam = unmapped_bam,
      ref_prefix = "ref/mitochondria/hs37d5.MT",
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_AlignToMt
  }

  call AlignAndMarkDuplicates as AlignToShiftedMt {
    input:
      input_bam = unmapped_bam,
      ref_prefix = "ref/mitochondria/hs37d5.MT.shifted_by_8000_bases",
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_AlignToShiftedMt
  }

  call CollectWgsMetrics {
    input:
      input_bam = AlignToMt.mt_aligned_bam,
      input_bam_index = AlignToMt.mt_aligned_bai,
      coverage_cap = 100000,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_CollectWgsMetrics
  }

  Int? M2_mem = if CollectWgsMetrics.mean_coverage > 25000 then 14 else 7

  call M2 as CallMt {
    input:
      ref_prefix = "ref/mitochondria/hs37d5.MT",
      input_bam = AlignToMt.mt_aligned_bam,
      input_bai = AlignToMt.mt_aligned_bai,
      compress = compress_output_vcf,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_CallMt,
      # Everything is called except the control region.
      m2_extra_args = " -L MT:576-16024 "
  }

  call M2 as CallShiftedMt {
    input:
      ref_prefix = "ref/mitochondria/hs37d5.MT.shifted_by_8000_bases",
      input_bam = AlignToShiftedMt.mt_aligned_bam,
      input_bai = AlignToShiftedMt.mt_aligned_bai,
      compress = compress_output_vcf,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_CallShiftedMt,
      # Only the control region is called.
      m2_extra_args = " -L MT:8025-9144 "
  }

  call LiftoverAndCombineVcfs {
    input:
      shifted_vcf = CallShiftedMt.raw_vcf,
      vcf = CallMt.raw_vcf,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_LiftoverAndCombineVcfs
  }

  call MergeStats {
    input:
      shifted_stats = CallShiftedMt.stats,
      non_shifted_stats = CallMt.stats,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_MergeStats
  }

  call Filter as InitialFilter {
    input:
      raw_vcf = LiftoverAndCombineVcfs.merged_vcf,
      raw_vcf_index = LiftoverAndCombineVcfs.merged_vcf_index,
      raw_vcf_stats = MergeStats.stats,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      compress = compress_output_vcf,
      max_alt_allele_count = 4,
      vaf_filter_threshold = 0,
      run_contamination = false,
      runtime_attr_override = runtime_attr_override_InitialFilter
  }

 
  call SplitMultiAllelicsAndRemoveNonPassSites {
    input:
      filtered_vcf = InitialFilter.filtered_vcf,
      filtered_vcf_index = InitialFilter.filtered_vcf_idx,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_SplitMultiAllelicsAndRemoveNonPassSites
  }

  call GetContamination {
    input:
      input_vcf = SplitMultiAllelicsAndRemoveNonPassSites.vcf_for_haplochecker,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      runtime_attr_override = runtime_attr_override_GetContamination
  }

  call Filter as FilterContamination {
    input:
      raw_vcf = InitialFilter.filtered_vcf,
      raw_vcf_index = InitialFilter.filtered_vcf_idx,
      raw_vcf_stats = MergeStats.stats,
      run_contamination = true,
      hasContamination = GetContamination.hasContamination,
      contamination_major = GetContamination.major_level,
      contamination_minor = GetContamination.minor_level,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      compress = compress_output_vcf,
      max_alt_allele_count = 4,
      runtime_attr_override = runtime_attr_override_FilterContamination
 }

  output {
    File mt_aligned_bam = AlignToMt.mt_aligned_bam
    File mt_aligned_bai = AlignToMt.mt_aligned_bai
    File mt_aligned_shifted_bam = AlignToShiftedMt.mt_aligned_bam
    File mt_aligned_shifted_bai = AlignToShiftedMt.mt_aligned_bai
    File out_vcf = FilterContamination.filtered_vcf
    File out_vcf_index = FilterContamination.filtered_vcf_idx
    File input_vcf_for_haplochecker = SplitMultiAllelicsAndRemoveNonPassSites.vcf_for_haplochecker
    File duplicate_metrics = AlignToMt.duplicate_metrics
    File coverage_metrics = CollectWgsMetrics.metrics
    File theoretical_sensitivity_metrics = CollectWgsMetrics.theoretical_sensitivity
    File contamination_metrics = GetContamination.contamination_file
    Int mean_coverage = CollectWgsMetrics.mean_coverage
    Float median_coverage = CollectWgsMetrics.median_coverage
    String major_haplogroup = GetContamination.major_hg
    Float contamination = FilterContamination.contamination
  }
}

task AlignAndMarkDuplicates {
  input {
    File input_bam
    String bwa_commandline = "bwa mem -K 100000000 -p -v 3 -t 2 -Y $bash_ref_fasta"
    String ref_prefix
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
    String? read_name_regex
  }

  String basename = basename(input_bam, ".bam")
  String metrics_filename = basename + ".metrics"
  String output_bam_basename = sampleGenomicID + ".realigned"

  meta {
    description: "Aligns with BWA and MergeBamAlignment, then Marks Duplicates. Outputs a coordinate sorted bam."
  }
  parameter_meta {
    input_bam: "Unmapped bam"
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    # do this before setting euo pipefail
    bwa_version=$(bwa 2>&1 | grep -e '^Version' | sed 's/Version: //')
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    # set the bash variable needed for the command-line
    bash_ref_fasta="~{ref_prefix}.fa"
    ~{gatk_path} --java-options "-Xms5000m" SamToFastq \
      --INPUT ~{input_bam} \
      --FASTQ /dev/stdout \
      --INTERLEAVE true \
      -NON_PF true | \
    ~{bwa_commandline} /dev/stdin - 2> >(tee ~{output_bam_basename}.bwa.stderr.log >&2) | \
    ~{gatk_path} --java-options "-Xms3000m" MergeBamAlignment \
      --VALIDATION_STRINGENCY SILENT \
      --EXPECTED_ORIENTATIONS FR \
      --ATTRIBUTES_TO_RETAIN X0 \
      --ATTRIBUTES_TO_REMOVE NM \
      --ATTRIBUTES_TO_REMOVE MD \
      --ALIGNED_BAM /dev/stdin \
      --UNMAPPED_BAM ~{input_bam} \
      --OUTPUT mba.bam \
      --REFERENCE_SEQUENCE ~{ref_prefix}.fa \
      --PAIRED_RUN true \
      --SORT_ORDER "unsorted" \
      --IS_BISULFITE_SEQUENCE false \
      --ALIGNED_READS_ONLY false \
      --CLIP_ADAPTERS false \
      --MAX_RECORDS_IN_RAM 2000000 \
      --ADD_MATE_CIGAR true \
      --MAX_INSERTIONS_OR_DELETIONS -1 \
      --PRIMARY_ALIGNMENT_STRATEGY MostDistant \
      --PROGRAM_RECORD_ID "bwamem" \
      --PROGRAM_GROUP_VERSION "${bwa_version}" \
      --PROGRAM_GROUP_COMMAND_LINE "~{bwa_commandline}" \
      --PROGRAM_GROUP_NAME "bwamem" \
      --UNMAPPED_READ_STRATEGY COPY_TO_TAG \
      --ALIGNER_PROPER_PAIR_FLAGS true \
      --UNMAP_CONTAMINANT_READS true \
      --ADD_PG_TAG_TO_READS false

    ~{gatk_path} --java-options "-Xms4000m" MarkDuplicates \
      --INPUT mba.bam \
      --OUTPUT md.bam \
      --METRICS_FILE ~{metrics_filename} \
      --VALIDATION_STRINGENCY SILENT \
      ~{"--READ_NAME_REGEX " + read_name_regex} \
      --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
      --ASSUME_SORT_ORDER "queryname" \
      --CLEAR_DT "false" \
      --ADD_PG_TAG_TO_READS false

    ~{gatk_path} --java-options "-Xms4000m" SortSam \
      --INPUT md.bam \
      --OUTPUT ~{output_bam_basename}.bam \
      --SORT_ORDER "coordinate" \
      --CREATE_INDEX true \
      --MAX_RECORDS_IN_RAM 300000

    mv ~{output_bam_basename}.bam /tmp/scratch/
    mv ~{output_bam_basename}.bai /tmp/scratch/
    mv ~{output_bam_basename}.bwa.stderr.log /tmp/scratch/
    mv ~{metrics_filename} /tmp/scratch/
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             6,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File mt_aligned_bam = "~{output_bam_basename}.bam"
    File mt_aligned_bai = "~{output_bam_basename}.bai"
    File bwa_stderr_log = "~{output_bam_basename}.bwa.stderr.log"
    File duplicate_metrics = "~{metrics_filename}"
  }
}

task GetContamination {
  input {
    File input_vcf
    String sampleGenomicID
    String sampleRunID
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Uses new Haplochecker to estimate levels of contamination in mitochondria"
  }
  parameter_meta {
    input_vcf: "Filtered and split multi-allelic sites VCF for mitochondria"
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    PARENT_DIR="$(dirname "~{input_vcf}")"
    java -jar haplocheckCLI/haplocheckCLI.jar "${PARENT_DIR}"
  
    sed 's/\"//g' output > output-noquotes
  
    grep "SampleID" output-noquotes > headers
    FORMAT_ERROR="Bad contamination file format"
    if [ `awk '{print $2}' headers` != "Contamination" ]; then
      echo $FORMAT_ERROR; exit 1
    fi
    if [ `awk '{print $6}' headers` != "HgMajor" ]; then
      echo $FORMAT_ERROR; exit 1
    fi
    if [ `awk '{print $8}' headers` != "HgMinor" ]; then
      echo $FORMAT_ERROR; exit 1
    fi
    if [ `awk '{print $14}' headers` != "MeanHetLevelMajor" ]; then
      echo $FORMAT_ERROR; exit 1
    fi
    if [ `awk '{print $15}' headers` != "MeanHetLevelMinor" ]; then
      echo $FORMAT_ERROR; exit 1
    fi
  
    grep -v "SampleID" output-noquotes > output-data
    awk -F "\t" '{print $2}' output-data > /tmp/scratch/contamination.txt
    awk -F "\t" '{print $6}' output-data > /tmp/scratch/major_hg.txt
    awk -F "\t" '{print $8}' output-data > /tmp/scratch/minor_hg.txt
    awk -F "\t" '{print $14}' output-data > /tmp/scratch/mean_het_major.txt
    awk -F "\t" '{print $15}' output-data > /tmp/scratch/mean_het_minor.txt
    mv output-noquotes /tmp/scratch/
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             3,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File contamination_file = "output-noquotes"
    String hasContamination = read_string("contamination.txt") 
    String major_hg = read_string("major_hg.txt")
    String minor_hg = read_string("minor_hg.txt")
    Float major_level = read_float("mean_het_major.txt")
    Float minor_level = read_float("mean_het_minor.txt")
  }
}

task CollectWgsMetrics {
  input {
    File input_bam
    File input_bam_index
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
    Int? read_length
    Int? coverage_cap
  }

  Int read_length_for_optimization = select_first([read_length, 151])

  meta {
    description: "Collect coverage metrics"
  }
  parameter_meta {
    read_length: "Read length used for optimization only. If this is too small CollectWgsMetrics might fail. Default is 151."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path} --java-options "-Xms2000m" CollectWgsMetrics \
      --INPUT ~{input_bam} \
      --VALIDATION_STRINGENCY SILENT \
      --REFERENCE_SEQUENCE ref/mitochondria/hs37d5.MT.fa \
      --OUTPUT metrics.txt \
      --USE_FAST_ALGORITHM true \
      --READ_LENGTH ~{read_length_for_optimization} \
      ~{"--COVERAGE_CAP " + coverage_cap} \
      --INCLUDE_BQ_HISTOGRAM true \
      --THEORETICAL_SENSITIVITY_OUTPUT theoretical_sensitivity.txt

    set +u 
    source activate gatk
    set -u
    R --vanilla <<CODE
      df = read.table("metrics.txt",skip=6,header=TRUE,stringsAsFactors=FALSE,sep='\t',nrows=1)
      write.table(floor(df[,"MEAN_COVERAGE"]), "mean_coverage.txt", quote=F, col.names=F, row.names=F)
      write.table(df[,"MEDIAN_COVERAGE"], "median_coverage.txt", quote=F, col.names=F, row.names=F)
    CODE
    mv metrics.txt /tmp/scratch/
    mv theoretical_sensitivity.txt /tmp/scratch/
    mv mean_coverage.txt /tmp/scratch/
    mv median_coverage.txt /tmp/scratch/
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             3,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File metrics = "metrics.txt"
    File theoretical_sensitivity = "theoretical_sensitivity.txt"
    Int mean_coverage = read_int("mean_coverage.txt")
    Float median_coverage = read_float("median_coverage.txt")
  }
}

task LiftoverAndCombineVcfs {
  input {
    File shifted_vcf
    File vcf
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Lifts over shifted vcf of control region and combines it with the rest of the chrM calls."
  }
  parameter_meta {
    shifted_vcf: "VCF of control region on shifted reference"
    vcf: "VCF of the rest of chrM on original reference"
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path} LiftoverVcf \
      -I ~{shifted_vcf} \
      -O ~{sampleGenomicID}.shifted_back.vcf \
      -R ref/mitochondria/hs37d5.MT.fa \
      --CHAIN ref/mitochondria/ShiftBack.chain \
      --REJECT /tmp/scratch/~{sampleGenomicID}.rejected.vcf

    ~{gatk_path} MergeVcfs \
      -I ~{sampleGenomicID}.shifted_back.vcf \
      -I ~{vcf} \
      -O ~{sampleGenomicID}.merged.vcf
    
    mv ~{sampleGenomicID}.merged.vcf /tmp/scratch/
    mv ~{sampleGenomicID}.merged.vcf.idx /tmp/scratch/
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             3,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output{
        # rejected_vcf should always be empty
        File rejected_vcf = "~{sampleGenomicID}.rejected.vcf"
        File merged_vcf = "~{sampleGenomicID}.merged.vcf"
        File merged_vcf_index = "~{sampleGenomicID}.merged.vcf.idx"
    }
}

task M2 {
  input {
    String ref_prefix
    File input_bam
    File input_bai
    Int max_reads_per_alignment_start = 75
    Boolean compress
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    String m2_extra_args
    RuntimeAttr? runtime_attr_override

  }

  String output_vcf = "~{sampleGenomicID}.raw" + if compress then ".vcf.gz" else ".vcf"
  String output_vcf_index = output_vcf + if compress then ".tbi" else ".idx"

  meta {
    description: "Mutect2 for calling Snps and Indels"
  }
  parameter_meta {
    input_bam: "Aligned Bam"
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path} --java-options "-Xmx3000m" Mutect2 \
        -R ~{ref_prefix}.fa \
        -I ~{input_bam} \
        --read-filter MateOnSameContigOrNoMappedMateReadFilter \
        --read-filter MateUnmappedAndUnmappedReadFilter \
        -O ~{output_vcf} \
        ~{m2_extra_args} \
        --annotation StrandBiasBySample \
        --mitochondria-mode \
        --max-reads-per-alignment-start ~{max_reads_per_alignment_start} \
        --max-mnp-distance 0

    mv ~{output_vcf} /tmp/scratch/
    mv ~{output_vcf_index} /tmp/scratch/
    mv ~{output_vcf}.stats /tmp/scratch/
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
      File raw_vcf = "~{output_vcf}"
      File raw_vcf_idx = "~{output_vcf_index}"
      File stats = "~{output_vcf}.stats"
  }
}

task Filter {
  input {
    File raw_vcf
    File raw_vcf_index
    File raw_vcf_stats
    Boolean compress
    Float? vaf_cutoff

    Int max_alt_allele_count
    Float? vaf_filter_threshold
    Float? f_score_beta

    Boolean run_contamination
    String? hasContamination
    Float? contamination_major
    Float? contamination_minor
    Float? verifyBamID

    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override

  }

  String output_vcf = sampleGenomicID + if compress then ".vcf.gz" else ".vcf"
  String output_vcf_index = output_vcf + if compress then ".tbi" else ".idx"
  Float hc_contamination = if run_contamination && hasContamination == "YES" then (if contamination_major == 0.0 then contamination_minor else 1.0 - contamination_major) else 0.0
  Float max_contamination = if defined(verifyBamID) && verifyBamID > hc_contamination then verifyBamID else hc_contamination

  meta {
    description: "Mutect2 Filtering for calling Snps and Indels"
  }
  parameter_meta {
      vaf_filter_threshold: "Hard cutoff for minimum allele fraction. All sites with VAF less than this cutoff will be filtered."
      f_score_beta: "F-Score beta balances the filtering strategy between recall and precision. The relative weight of recall to precision."
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path}  --java-options "-Xmx2500m" FilterMutectCalls -V ~{raw_vcf} \
        -R ref/mitochondria/hs37d5.MT.fa \
        -O filtered.vcf \
        --stats ~{raw_vcf_stats} \
        --max-alt-allele-count ~{max_alt_allele_count} \
        --mitochondria-mode \
        ~{"--min-allele-fraction " + vaf_filter_threshold} \
        ~{"--f-score-beta " + f_score_beta} \
        ~{"--contamination-estimate " + max_contamination}

    ~{gatk_path} VariantFiltration -V filtered.vcf \
        -O /tmp/scratch/~{output_vcf} \
        --apply-allele-specific-filters \
        --mask ref/mitochondria/blacklist_sites.hs37d5.MT.bed \
        --mask-name "blacklisted_site"

  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
      File filtered_vcf = "~{output_vcf}"
      File filtered_vcf_idx = "~{output_vcf_index}"
      Float contamination = "~{hc_contamination}"
  }
}

task MergeStats {
  input {
    File shifted_stats
    File non_shifted_stats
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path} MergeMutectStats --stats ~{shifted_stats} --stats ~{non_shifted_stats} -O /tmp/scratch/raw.combined.stats
  >>>
  output {
    File stats = "raw.combined.stats"
  }
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
}

task SplitMultiAllelicsAndRemoveNonPassSites {
  input {
    File filtered_vcf
    File filtered_vcf_index
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path}  LeftAlignAndTrimVariants \
      -R ref/mitochondria/hs37d5.MT.fa \
      -V ~{filtered_vcf} \
      -O split.vcf \
      --split-multi-allelics \
      --dont-trim-alleles \
      --keep-original-ac

    ~{gatk_path}  SelectVariants \
        -V split.vcf \
        -O /tmp/scratch/splitAndPassOnly.vcf \
        --exclude-filtered
  
  >>>
  output {
    File vcf_for_haplochecker = "splitAndPassOnly.vcf"
  }
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
}
