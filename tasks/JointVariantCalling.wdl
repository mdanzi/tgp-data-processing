version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task generateSampleMapFile {
  input{
    # Command parameters
    Array[String] sample_names
    String cohortRunID 
    String outfile 
    RuntimeAttr? runtime_attr_override

  }
    command <<<
    set -oe pipefail
    
    python << CODE
    sample_names = ['~{sep="','" sample_names}']

    with open("sample_map_file.txt", "w") as fi:
      for i in range(len(sample_names)):
        fi.write(sample_names[i] + "\t" + sample_names[i] + ".hg19.g.vcf.gz\n") 

    CODE
    mv sample_map_file.txt ~{outfile}
    aws s3 cp ~{outfile} s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
    >>>

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             7,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
    output {
        File sample_map = outfile
    }
}

task calculateCNNAndROH {
    input {
        String sampleGenomicID
        String cohortRunID
        String sampleRunID
        String sampleGUID
        File callingIntervals
        String gatk_path
        Boolean? singleton = false
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport cohort ~{cohortRunID}" ERR

        aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.g.vcf.gz . --only-show-errors
        aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.g.vcf.idx . --only-show-errors
        gunzip ~{sampleGenomicID}.hg19.g.vcf.gz


        # prepare 1000Genomes data on allele frequencies and genetic mappings for ROH mapping
        aws s3 cp s3://tgp-data-analysis/StructuralVariation/genetic-map.tgz . --only-show-errors
        aws s3 cp s3://tgp-data-analysis/StructuralVariation/1000GP-AFs.tgz . --only-show-errors
        tar -zxvf 1000GP-AFs.tgz
        tar -zxvf genetic-map.tgz

        ~{gatk_path} --java-options "-Xmx5g" GenotypeGVCFs -V ~{sampleGenomicID}.hg19.g.vcf -R ref/hs37d5.fa -O ~{sampleGenomicID}.hg19.genotyped.vcf --allow-old-rms-mapping-quality-annotation-data --use-new-qual-calculator --only-output-calls-starting-in-intervals -L ~{callingIntervals} --merge-input-intervals
        bcftools filter -i "(FORMAT/DP)>=6" ~{sampleGenomicID}.hg19.genotyped.vcf > ~{sampleGenomicID}.hg19.genotyped.filtered.vcf
        if (~{singleton}); then
          bgzip -c ~{sampleGenomicID}.hg19.genotyped.filtered.vcf > ~{cohortRunID}.vcf.gz
          tabix ~{cohortRunID}.vcf.gz
          aws s3 cp ~{cohortRunID}.vcf.gz s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
          aws s3 cp ~{cohortRunID}.vcf.gz.tbi s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
        fi
        set +u
        source activate gatk
        set -u
        ~{gatk_path} CNNScoreVariants -V ~{sampleGenomicID}.hg19.genotyped.filtered.vcf -R ref/hs37d5.fa -O ~{sampleGenomicID}.CNN.vcf
        ~{gatk_path} FilterVariantTranches -V ~{sampleGenomicID}.CNN.vcf -O ~{sampleGenomicID}.CNN.filtered.vcf --resource ref/hapmap_3.3.b37.vcf --resource ref/Mills_and_1000G_gold_standard.indels.b37.vcf --info-key CNN_1D --snp-tranche 99.95 --snp-tranche 99.9 --snp-tranche 99.5 --snp-tranche 99 --snp-tranche 98 --snp-tranche 95 --indel-tranche 99.4 --indel-tranche 99.2 --indel-tranche 99 --indel-tranche 98 --indel-tranche 95
        set +u
        conda deactivate
        set -u
        # upload the CNN-filtered VCF to the cohortRun folder on S3
        aws s3 cp ~{sampleGenomicID}.CNN.filtered.vcf s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
        ## Runs of Homozygosity mapping
        # remove the CNN-failed variants
        grep -wv 'CNN_1D_SNP_Tranche_99.95_100.00' ~{sampleGenomicID}.CNN.filtered.vcf | grep -wv 'CNN_1D_INDEL_Tranche_99.40_100.00' > ~{sampleGenomicID}.filtered.vcf
        bgzip ~{sampleGenomicID}.filtered.vcf
        tabix ~{sampleGenomicID}.filtered.vcf.gz
        # run bcftools roh on the sample
        bcftools annotate -c CHROM,POS,REF,ALT,AF1KG -h 1000GP-AFs/AFs.tab.gz.hdr -a 1000GP-AFs/AFs.tab.gz ~{sampleGenomicID}.filtered.vcf.gz | bcftools roh --AF-tag AF1KG -M 100 -m genetic-map/genetic_map_chr{CHROM}_combined_b37.txt -o roh.txt
        grep '^RG' roh.txt > ~{sampleGenomicID}.RohRegions.txt
        # filter the output to only include ROHs at least 5kb in size
        echo -e "chr\tstart\tend\tlength\tnumberOfMarkers\tquality" > header.txt
        awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,$6,$7,$8}}' ~{sampleGenomicID}.RohRegions.txt | cat header.txt - > ~{sampleGenomicID}.RegionsOfHomozygosity.txt
        # create bed file for IGV visualization
        echo "#gffTags" > header.txt
        awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,"Length="$6";NumberOfMarkers="$7";Quality="$8}}' ~{sampleGenomicID}.RohRegions.txt | cat header.txt - > ~{sampleGenomicID}.RegionsOfHomozygosity.bed
        # upload the roh's to s3
        aws s3 cp ~{sampleGenomicID}.RegionsOfHomozygosity.txt s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
        aws s3 cp ~{sampleGenomicID}.RegionsOfHomozygosity.bed s3://tgp-sample-assets/~{sampleGUID}/ROHs/ --only-show-errors

    >>>

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             7,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }

    output {
        String CNNVCFName = "~{sampleGenomicID}.CNN.filtered.vcf"
    }

}

task importGVCFs {

  input {
    Array[String] sampleGenomicIDs
    Array[String] sampleRunIDs
    String cohortRunID
    File sample_name_map
    File interval
    String workspace_dir_name
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport cohort ~{cohortRunID}" ERR

    rm -rf ~{workspace_dir_name}

    sampleGenomicIDsArray=(~{sep=" " sampleGenomicIDs})
    sampleRunIDsArray=(~{sep=" " sampleRunIDs})

    for idx in {0..~{length(sampleGenomicIDs)-1}}; do 
      aws s3 cp s3://tgp-sample-processing/${sampleRunIDsArray[${idx}]}/${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz . --only-show-errors
      EXISTS=$(aws s3 ls s3://tgp-sample-processing/${sampleRunIDsArray[${idx}]}/${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz.tbi | grep 'g.vcf.gz.tbi' | wc -l || true)
            if [[ ${EXISTS} -ne 0 ]]; then
              aws s3 cp s3://tgp-sample-processing/${sampleRunIDsArray[${idx}]}/${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz.tbi . --only-show-errors
            else
              gunzip ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz
              bgzip ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf
              tabix ${sampleGenomicIDsArray[${idx}]}.hg19.g.vcf.gz
            fi
    done

    # We've seen some GenomicsDB performance regressions related to intervals, so we're going to pretend we only have a single interval
    # using the --merge-input-intervals arg
    # There's no data in between since we didn't run HaplotypeCaller over those loci so we're not wasting any compute

    # The memory setting here is very important and must be several GiB lower
    # than the total memory allocated to the VM because this tool uses
    # a significant amount of non-heap memory for native libraries.
    # Also, testing has shown that the multithreaded reader initialization
    # does not scale well beyond 5 threads, so don't increase beyond that.
    ~{gatk_path} --java-options "-Xms8000m -Xmx25000m" \
      GenomicsDBImport \
      --genomicsdb-workspace-path ~{workspace_dir_name} \
      -L ~{interval} \
      --sample-name-map ~{sample_name_map} \
      --reader-threads 5 \
      --merge-input-intervals 

    tar -cf /tmp/scratch/~{workspace_dir_name}.tar ~{workspace_dir_name} 
  >>>

  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          4,
      mem_gb:             30,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }

  output {
    File output_genomicsdb = "~{workspace_dir_name}.tar"
  }
}

task genotypeGVCFs {

  input {
    File workspace_tar
    File interval
    String output_vcf_filename
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  parameter_meta {
    interval: {
      localization_optional: true
    }
  }

  command <<<
    set -euo pipefail
    cd /home/ec2-user/

    tar -xf ~{workspace_tar}
    WORKSPACE=$(basename ~{workspace_tar} .tar)

    ~{gatk_path} --java-options "-Xms8000m -Xmx25000m" \
      GenotypeGVCFs \
      -R ref/hs37d5.fa \
      -O ~{output_vcf_filename} \
      --only-output-calls-starting-in-intervals \
      --use-new-qual-calculator \
      -V gendb://$WORKSPACE \
      -L ~{interval} \
      --allow-old-rms-mapping-quality-annotation-data \
      --merge-input-intervals

    mv ~{output_vcf_filename} /tmp/scratch/
    mv ~{output_vcf_filename}.tbi /tmp/scratch/
  >>>

  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             30,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }

  output {
    File output_vcf = "~{output_vcf_filename}"
    File output_vcf_index = "~{output_vcf_filename}.tbi"
  }
}


task gatherVCFs {

  input {
    Array[File] input_vcfs
    String output_vcf_name
    String cohortRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  parameter_meta {
    input_vcfs: {
      localization_optional: true
    }
  }

  command <<<
    set -euo pipefail

    # --ignore-safety-checks makes a big performance difference so we include it in our invocation.
    # This argument disables expensive checks that the file headers contain the same set of
    # genotyped samples and that files are in order by position of first record.
    ~{gatk_path} --java-options "-Xms6000m -Xmx6500m" \
      GatherVcfsCloud \
      --ignore-safety-checks \
      --gather-type BLOCK \
      --input ~{sep=" --input " input_vcfs} \
      --output ~{output_vcf_name}

    # output should be bgzipped
    mv ~{output_vcf_name} tmp.gz
    tabix tmp.gz

    bcftools filter -i "MAX(FORMAT/DP)>=6" -o ~{output_vcf_name} -O b tmp.gz 
    tabix ~{output_vcf_name}
    aws s3 cp ~{output_vcf_name} s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
    aws s3 cp ~{output_vcf_name}.tbi s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors

  >>>

  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             7,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }

  output {
    String combinedVCFName = "~{output_vcf_name}"
  }
}

task vcfLevelQC {
      input {
        String sampleGenomicID
        String combinedVCF
        String sampleRunID
        String cohortRunID
        String gatk_path
        Array[String] CNNVCFs # this input is not used, it is only present to ensure this task gets run after calculateCNNAndROH
        RuntimeAttr? runtime_attr_override
      }

      command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport cohort ~{cohortRunID}" ERR

        aws s3 cp s3://tgp-sample-processing/~{cohortRunID}/~{combinedVCF} . --only-show-errors
        gunzip ~{combinedVCF}

        vcf-subset -c ~{sampleGenomicID} ~{cohortRunID}.vcf > ~{sampleGenomicID}.vcf
        echo -e "fileName\tsampleName\tChr\tTSTV\tnumSNPs\tnumINDELs\tavgDepth\tavgQuality\tHeterozygousHomozygousRatio\tmeanGQ\tGender" > ~{sampleGenomicID}.vcfLevel.byChr.qc
        TSTV=$(cat ~{sampleGenomicID}.vcf | vcf-tstv | awk '{print $1}')
        vcf-stats ~{sampleGenomicID}.vcf > tmpStats.txt 
        SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
        INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
        DEPTH=$(vcftools --vcf ~{sampleGenomicID}.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
        QUAL=$(vcftools --vcf ~{sampleGenomicID}.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
        HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
        HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
        HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
        if [ "${SNPs}" == "" ]; then SNPs="0"; fi
        if [ "${INDELs}" == "" ]; then INDELs="0"; fi
        if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
        if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
        if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
        HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
        GQ=$(vcftools --vcf ~{sampleGenomicID}.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')

        ## new, peddy-based sex check
        echo -e "~{sampleGenomicID}\t~{sampleGenomicID}\t0\t0\t0\t0" > ~{sampleGenomicID}.ped
        bgzip -c ~{sampleGenomicID}.vcf > ~{sampleGenomicID}.vcf.gz
        tabix ~{sampleGenomicID}.vcf.gz
        set +u
        source activate py2
        set -u
        python -m peddy --prefix ~{sampleGenomicID} ~{sampleGenomicID}.vcf.gz ~{sampleGenomicID}.ped
        set +u
        conda deactivate
        set -u
        SEX=$(sed -n '2p' ~{sampleGenomicID}.sex_check.csv | cut -f 7 -d, )
        if [ ${SEX} == "male" ]; then
            SEX='M'
        elif [ ${SEX} == "female" ]; then
            SEX='F'
        else 
            SEX='U'
        fi 
        
        echo -e "~{cohortRunID}.vcf\t~{sampleGenomicID}\tGenome\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ~{sampleGenomicID}.vcfLevel.byChr.qc 
        for j in {1..22} X Y; do 
            vcftools --vcf ~{sampleGenomicID}.vcf --chr ${j} --stdout --recode > thisChr.vcf 
            numVariants=$(grep -v ^'#' thisChr.vcf | wc -l)
            if [ "${numVariants}" -gt "0" ]; then 
                TSTV=$(cat thisChr.vcf | vcf-tstv | awk '{print $1}')
                vcf-stats thisChr.vcf > tmpStats.txt 
                SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
                INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
                DEPTH=$(vcftools --vcf thisChr.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
                QUAL=$(vcftools --vcf thisChr.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
                HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
                HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
                HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
                if [ "${SNPs}" == "" ]; then SNPs="0"; fi
                if [ "${INDELs}" == "" ]; then INDELs="0"; fi
                if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
                if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
                if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
                HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
                GQ=$(vcftools --vcf thisChr.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')
                echo -e "~{cohortRunID}.vcf\t~{sampleGenomicID}\t${j}\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ~{sampleGenomicID}.vcfLevel.byChr.qc 
            fi
        done 
        ##ancestry clustering
        aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.peddySites.vcf . --only-show-errors
        vcftools --vcf ~{sampleGenomicID}.peddySites.vcf --012 --out ~{sampleGenomicID}
        set +u
        source activate py2
        set -u
        python tgp-data-processing/scripts/vcfLevelQC/samplePCA.py -i ~{sampleGenomicID}
        set +u
        conda deactivate
        set -u
        
        # upload results 
        aws s3 cp ~{sampleGenomicID}.AncestryClustering.png s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
        aws s3 cp ~{sampleGenomicID}.vcfLevel.byChr.qc s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
        
      >>>
 
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             7,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }

  }

task createSVBamlets {
      input {
        String sampleGenomicID
        Array[String] sampleGenomicIDs
        String sampleRunID
        Array[String] sampleRunIDs
        String sampleGUID
        String cohortRunID
        RuntimeAttr? runtime_attr_override
      }

      command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport cohort ~{cohortRunID}" ERR

        # download all SV vcfs
        for runID in ~{sep=" " sampleRunIDs}; do
            EXISTS=$(aws s3 ls s3://tgp-sample-processing/${runID}/ | grep '.combined.genotyped.vcf' | wc -l || true)
            if [[ ${EXISTS} -ne 0 ]]; then
                aws s3 cp s3://tgp-sample-processing/${runID}/ . --recursive --exclude "*" --include "*.combined.genotyped.vcf" --only-show-errors
            fi
        done
        
        # print bamlets for this sample using variants from all SV vcfs
        EXISTS=$(aws s3 ls s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.combined.genotyped.vcf | grep ~{sampleGenomicID} | wc -l || true)
        EXISTS2=$(aws s3 ls s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.sorted.dedup.bam | grep ~{sampleGenomicID} | wc -l || true)
        if [[ ${EXISTS} -ne 0 ]]; then if [[ ${EXISTS2} -ne 0 ]]; then
            aws s3 rm s3://tgp-sample-assets/~{sampleGUID}/SVs/ --recursive --only-show-errors
            aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.sorted.dedup.bam . --only-show-errors
            aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.hg19.sorted.dedup.bam.bai . --only-show-errors
            set +u
            source activate py37
            set -u
            for VCF in *.combined.genotyped.vcf; do
                python tgp-data-processing/scripts/structuralVariation/printBamlets.py --vcf=${VCF} --bam=~{sampleGenomicID}.hg19.sorted.dedup.bam --out=~{sampleGUID}/SVs/
            done
            set +u
            conda deactivate
            set -u            
            cd ~{sampleGUID}/SVs/
            for FILE in *.bam; do samtools index ${FILE}; done
            cd ../../
            aws s3 cp ~{sampleGUID}/SVs/ s3://tgp-sample-assets/~{sampleGUID}/SVs/ --recursive --only-show-errors
        fi; fi
      >>>

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             7,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
}


task parseVCF {
      input {
        String combinedVCF
        Array[String] CNNVCFs
        Array[String] sampleGenomicIDs
        Array[String] sampleRunIDs
        String cohortRunID
        String firstSampleGenomicID = "~{select_first(sampleGenomicIDs)}"
        RuntimeAttr? runtime_attr_override
      }

      command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport cohort ~{cohortRunID}" ERR

        aws s3 cp s3://tgp-sample-processing/~{cohortRunID}/~{combinedVCF} . --only-show-errors
        gunzip ~{combinedVCF}

        # parse the vcf into a text file
        timeCount=$(date +%s)
        timeCount2=$(echo "scale=0; ${timeCount}/3600" | bc)
        set +u
        source activate py2
        set -u
        python tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}~{firstSampleGenomicID}" -s ~{sep="," sampleGenomicIDs} < ~{cohortRunID}.vcf > ~{cohortRunID}.parsed.txt

        sampleGenomicIDsArray=(~{sep=" " sampleGenomicIDs})
        sampleRunIDsArray=(~{sep=" " sampleRunIDs})

        for idx in {0..~{length(sampleGenomicIDs)-1}}; do 
            sampleID=${sampleGenomicIDsArray[${idx}]}
            runID=${sampleRunIDsArray[${idx}]}
            # separate the parsed text file out by sample
            grep -w "^${sampleID}" ~{cohortRunID}.parsed.txt > ${sampleID}.parsed.txt
            # download the CNN-filtered vcf file
            aws s3 cp s3://tgp-sample-processing/~{cohortRunID}/${sampleID}.CNN.filtered.vcf . --only-show-errors
            # parse the CNN-filtered vcf into a text file
            python tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.CNN.filtered.vcf > ${sampleID}.CNN.parsed.txt
            # set the filter column values to use the CNN filter status
            python tgp-data-processing/scripts/vcfParser/mergeOnCNNFilters.py --input=${sampleID}.parsed.txt --cnn=${sampleID}.CNN.parsed.txt --output=${sampleID}.parsed.2.txt
            # swap the mitochondrial variant calls
            EXISTS=$(aws s3 ls s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf | grep ${sampleID} | wc -l || true)
            if [[ ${EXISTS} -ne 0 ]]; then
              aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf . --only-show-errors
              aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.MT.final.split.vcf.idx . --only-show-errors
              python tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.MT.final.split.vcf > ${sampleID}.MT.parsed.txt
              awk -F'\t' -v OFS='\t' '{if($2<25){print $0}}' ${sampleID}.parsed.2.txt | cat - ${sampleID}.MT.parsed.txt > ${sampleID}.parsed.3.txt
            else
              cp ${sampleID}.parsed.2.txt ${sampleID}.parsed.3.txt
            fi
            # add in custom variants
            EXISTS=$(aws s3 ls s3://tgp-sample-processing/${runID}/${sampleID}.customSites.vcf | grep ${sampleID} | wc -l || true)
            if [[ ${EXISTS} -ne 0 ]]; then
              aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.customSites.vcf . --only-show-errors
              python tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}${sampleID}" -s ${sampleID} < ${sampleID}.customSites.vcf > ${sampleID}.customSites.parsed.txt
              python tgp-data-processing/scripts/vcfParser/mergeOnCustomGenotypeCalls.py --input=${sampleID}.parsed.3.txt --customSites=${sampleID}.customSites.parsed.txt --output=${sampleID}.parsed.4.txt
            else
              cp ${sampleID}.parsed.3.txt ${sampleID}.parsed.4.txt
            fi
            # download the ROH files to this workspace
            EXISTS=$(aws s3 ls s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt | grep ${sampleID} | wc -l || true)
            if [[ ${EXISTS} -ne 0 ]]; then
                aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt . --only-show-errors
                # add the ROH info as another column
                tail -n +2 ${sampleID}.RegionsOfHomozygosity.txt | cut -f 1,2,3,4 > ${sampleID}.RegionsOfHomozygosity.2.txt
                numFields=$(grep -v '^#' ~{cohortRunID}.vcf | awk -F'\t' '{print NF; exit}' || true)
                numFields=$((numFields + 4))
                bedtools intersect -wo -a ~{cohortRunID}.vcf -b ${sampleID}.RegionsOfHomozygosity.2.txt | cut -f 1,2,${numFields} | sort -u -k1n,1n -k2n,2n > variantsInROHs.txt
            else
                touch variantsInROHs.txt
            fi
            python tgp-data-processing/scripts/vcfParser/mergeOnROH.py --input=${sampleID}.parsed.4.txt --roh=variantsInROHs.txt --output=${sampleID}.parsed.txt
            # upload the results to S3
            aws s3 cp ${sampleID}.parsed.txt s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
        done
        
        >>>

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             7,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
}


task parseVCFInput {
      input {
        String combinedVCF
        Array[String] sampleGenomicIDs
        String cohortRunID
        String firstSampleGenomicID = "~{select_first(sampleGenomicIDs)}"
        RuntimeAttr? runtime_attr_override
      }

      command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport cohort ~{cohortRunID}" ERR

        aws s3 cp s3://tgp-sample-processing/~{cohortRunID}/~{combinedVCF} . --only-show-errors
        gunzip ~{combinedVCF}

        # parse the vcf into a text file
        timeCount=$(date +%s)
        timeCount2=$(echo "scale=0; ${timeCount}/3600" | bc)
        set +u
        source activate py2
        set -u
        python tgp-data-processing/scripts/vcfParser/vcfFileParser.py -r "${timeCount2}~{firstSampleGenomicID}" -s ~{sep="," sampleGenomicIDs} < ~{cohortRunID}.vcf > ~{cohortRunID}.parsed.txt

        sampleGenomicIDsArray=(~{sep=" " sampleGenomicIDs})

        for idx in {0..~{length(sampleGenomicIDs)-1}}; do 
            sampleID=${sampleGenomicIDsArray[${idx}]}
            # separate the parsed text file out by sample
            grep -w "^${sampleID}" ~{cohortRunID}.parsed.txt > ${sampleID}.parsed.txt
            # skip setting the filter column values to use the CNN filter status
            python tgp-data-processing/scripts/vcfParser/mergeOnCNNFilters.py --input=${sampleID}.parsed.txt --output=${sampleID}.parsed.2.txt
            # skip swapping in the mitochondrial variant calls
            cp ${sampleID}.parsed.2.txt ${sampleID}.parsed.3.txt
            # skip adding in custom variants
            cp ${sampleID}.parsed.3.txt ${sampleID}.parsed.4.txt
            # download the ROH files to this workspace
            EXISTS=$(aws s3 ls s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt | grep ${sampleID} | wc -l || true)
            if [[ ${EXISTS} -ne 0 ]]; then
                aws s3 cp s3://tgp-sample-processing/${runID}/${sampleID}.RegionsOfHomozygosity.txt . --only-show-errors
                # add the ROH info as another column
                tail -n +2 ${sampleID}.RegionsOfHomozygosity.txt | cut -f 1,2,3,4 > ${sampleID}.RegionsOfHomozygosity.2.txt
                numFields=$(grep -v '^#' ~{cohortRunID}.vcf | awk -F'\t' '{print NF; exit}' || true)
                numFields=$((numFields + 4))
                bedtools intersect -wo -a ~{cohortRunID}.vcf -b ${sampleID}.RegionsOfHomozygosity.2.txt | cut -f 1,2,${numFields} | sort -u -k1n,1n -k2n,2n > variantsInROHs.txt
            else
                touch variantsInROHs.txt
            fi
            python tgp-data-processing/scripts/vcfParser/mergeOnROH.py --input=${sampleID}.parsed.4.txt --roh=variantsInROHs.txt --output=${sampleID}.parsed.txt
            # upload the results to S3
            aws s3 cp ${sampleID}.parsed.txt s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
        done
        
        >>>

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             7,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
}

task vcfInputQC {
    input {
        File inputVCF
        Array[String] sampleGenomicIDs
        String cohortRunID
        Array[String] sampleGUIDs
        File callingIntervals
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport cohort ~{cohortRunID}" ERR

        inputVCFFilename=$(sed 's#^.*/##' <<< ~{inputVCF})
        extension=${inputVCFFilename##*.} 
        if [[ $extension = "gz" || $extension = "bgz" ]]; then 
          gunzip -c ~{inputVCF} > input.vcf
        else
          cp ~{inputVCF} input.vcf
        fi

        # convert chromosome names to grch37 style:
        sed -i 's/##contig=<ID=chrM/##contig=<ID=MT/g' input.vcf
        sed -i 's/^chrM/MT/g' input.vcf
        sed -i 's/##contig=<ID=chr/##contig=<ID=/g' input.vcf
        sed -i 's/^chr//g' input.vcf

        bgzip input.vcf 
        tabix input.vcf.gz

        (bcftools filter -i "(FORMAT/DP)>=6" input.vcf.gz > input.vcf) || (bcftools filter -i "DP>=6" input.vcf.gz > input.vcf)
        rm input.vcf.gz
        rm input.vcf.gz.tbi

        # reheader the vcf so that it uses the sampleGenomicIDs
        echo ~{sep='\n' sampleGenomicIDs} > sampleIDs.txt
        bcftools reheader -s sampleIDs.txt input.vcf -o ~{cohortRunID}.vcf

        # upload the variant calls
        bgzip -c ~{cohortRunID}.vcf > ~{cohortRunID}.vcf.gz
        tabix ~{cohortRunID}.vcf.gz
        aws s3 cp ~{cohortRunID}.vcf.gz s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
        aws s3 cp ~{cohortRunID}.vcf.gz.tbi s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors

        # prepare 1000Genomes data on allele frequencies and genetic mappings for ROH mapping
        aws s3 cp s3://tgp-data-analysis/StructuralVariation/genetic-map.tgz . --only-show-errors
        aws s3 cp s3://tgp-data-analysis/StructuralVariation/1000GP-AFs.tgz . --only-show-errors
        tar -zxvf 1000GP-AFs.tgz
        tar -zxvf genetic-map.tgz

        # QC
        # turn off errexit since this part is error prone
        set +eo pipefail
        trap "" ERR
        sampleGenomicIDsArray=(~{sep=" " sampleGenomicIDs})
        sampleGUIDsArray=(~{sep=" " sampleGUIDs})

        for idx in {0..~{length(sampleGenomicIDs)-1}}; do   
          sampleGenomicID=${sampleGenomicIDsArray[${idx}]}
          sampleGUID=${sampleGUIDsArray[${idx}]}
          vcf-subset -c ${sampleGenomicID} ~{cohortRunID}.vcf > ${sampleGenomicID}.vcf
          echo -e "fileName\tsampleName\tChr\tTSTV\tnumSNPs\tnumINDELs\tavgDepth\tavgQuality\tHeterozygousHomozygousRatio\tmeanGQ\tGender" > ${sampleGenomicID}.vcfLevel.byChr.qc
          TSTV=$(cat ${sampleGenomicID}.vcf | vcf-tstv | awk '{print $1}')
          vcf-stats ${sampleGenomicID}.vcf > tmpStats.txt 
          SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
          INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
          DEPTH=$(vcftools --vcf ${sampleGenomicID}.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
          QUAL=$(vcftools --vcf ${sampleGenomicID}.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
          HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
          HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
          HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
          if [ "${SNPs}" == "" ]; then SNPs="0"; fi
          if [ "${INDELs}" == "" ]; then INDELs="0"; fi
          if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
          if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
          if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
          HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
          GQ=$(vcftools --vcf ${sampleGenomicID}.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')

          ## peddy-based sex check
          echo -e "${sampleGenomicID}\t${sampleGenomicID}\t0\t0\t0\t0" > ${sampleGenomicID}.ped
          bgzip -c ${sampleGenomicID}.vcf > ${sampleGenomicID}.vcf.gz
          tabix ${sampleGenomicID}.vcf.gz
          set +u
          source activate py2
          set -u
          python -m peddy --prefix ${sampleGenomicID} ${sampleGenomicID}.vcf.gz ${sampleGenomicID}.ped
          set +u
          conda deactivate
          set -u
          SEX=$(sed -n '2p' ${sampleGenomicID}.sex_check.csv | cut -f 7 -d, )
          if [ ${SEX} == "male" ]; then
              SEX='M'
          elif [ ${SEX} == "female" ]; then
              SEX='F'
          else 
              SEX='U'
          fi 
          
          echo -e "~{cohortRunID}.vcf\t${sampleGenomicID}\tGenome\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
          for j in {1..22} X Y; do 
              vcftools --vcf ${sampleGenomicID}.vcf --chr ${j} --stdout --recode > thisChr.vcf 
              numVariants=$(grep -v ^'#' thisChr.vcf | wc -l)
              if [ "${numVariants}" -gt "0" ]; then 
                  TSTV=$(cat thisChr.vcf | vcf-tstv | awk '{print $1}')
                  vcf-stats thisChr.vcf > tmpStats.txt 
                  SNPs=$((grep 'snp_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
                  INDELs=$((grep 'indel_count' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//') 
                  DEPTH=$(vcftools --vcf thisChr.vcf --depth --stdout | sed '2q;d' | awk '{print $3}')
                  QUAL=$(vcftools --vcf thisChr.vcf --site-quality --stdout | tail -n +2 | awk '{total += $3} END {if (NR>0) {print total/NR} else {print "NA"}}')
                  HET_RA=$((grep 'het_RA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
                  HET_AA=$((grep 'het_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
                  HOM_AA=$((grep 'hom_AA' tmpStats.txt || :) | sed '1q;d' | awk '{print $3}' | sed 's/,$//')
                  if [ "${SNPs}" == "" ]; then SNPs="0"; fi
                  if [ "${INDELs}" == "" ]; then INDELs="0"; fi
                  if [ "${HET_RA}" == "" ]; then HET_RA="0"; fi
                  if [ "${HET_AA}" == "" ]; then HET_AA="0"; fi
                  if [ "${HOM_AA}" == "" ]; then HOM_AA="0"; fi
                  HETHOMRATIO=$(awk -v het1="${HET_RA}" -v het2="${HET_AA}" -v hom="${HOM_AA}" 'BEGIN {if (hom>0) {print (het1+het2)/hom} else {print "NA"}}')
                  GQ=$(vcftools --vcf thisChr.vcf --extract-FORMAT-info GQ --stdout | tail -n +2 | awk '{sum+=$3} END {if (NR>0) {print sum/NR} else {print "NA"}}')
                  echo -e "~{cohortRunID}.vcf\t${sampleGenomicID}\t${j}\t${TSTV}\t${SNPs}\t${INDELs}\t${DEPTH}\t${QUAL}\t${HETHOMRATIO}\t${GQ}\t${SEX}" >> ${sampleGenomicID}.vcfLevel.byChr.qc 
              fi
          done 
          
          # upload QC results 
          aws s3 cp ${sampleGenomicID}.vcfLevel.byChr.qc s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors

          ## Runs of Homozygosity mapping
          # run bcftools roh on the sample
          bcftools annotate -c CHROM,POS,REF,ALT,AF1KG -h 1000GP-AFs/AFs.tab.gz.hdr -a 1000GP-AFs/AFs.tab.gz ${sampleGenomicID}.vcf.gz | bcftools roh --AF-tag AF1KG -M 100 -m genetic-map/genetic_map_chr{CHROM}_combined_b37.txt -o roh.txt
          grep '^RG' roh.txt > ${sampleGenomicID}.RohRegions.txt
          # filter the output to only include ROHs at least 5kb in size
          echo -e "chr\tstart\tend\tlength\tnumberOfMarkers\tquality" > header.txt
          awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,$6,$7,$8}}' ${sampleGenomicID}.RohRegions.txt | cat header.txt - > ${sampleGenomicID}.RegionsOfHomozygosity.txt
          # create bed file for IGV visualization
          echo "#gffTags" > header.txt
          awk -F'\t' -v OFS='\t' '{if ($6>5000){print $3,$4,$5,"Length="$6";NumberOfMarkers="$7";Quality="$8}}' ${sampleGenomicID}.RohRegions.txt | cat header.txt - > ${sampleGenomicID}.RegionsOfHomozygosity.bed
          # upload the roh's to s3
          aws s3 cp ${sampleGenomicID}.RegionsOfHomozygosity.txt s3://tgp-sample-processing/~{cohortRunID}/ --only-show-errors
          aws s3 cp ${sampleGenomicID}.RegionsOfHomozygosity.bed s3://tgp-sample-assets/${sampleGUID}/ROHs/ --only-show-errors
        done

    >>>

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             4,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }

    output {
      String combinedVCFName = "~{cohortRunID}.vcf.gz"
    }

}