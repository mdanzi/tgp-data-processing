version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task align_presharded {
  input {
    File R1Fastq
    File R2Fastq
    String sampleGenomicID
    String sampleRunID
    Int? numCPUs=16
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Aligns a set of fastq file pairs to the GRCh37 reference genome."
  }

  parameter_meta {
    R1Fastq: "File with the R1 fastq reads, pre-sharded, compressed with gzip."
    R2Fastq: "File with the R2 fastq reads, pre-sharded, compressed with gzip."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    # note, the Read Group is written to match the style and defaults used by Picard AddOrReplaceReadGroups
    readGroup="@RG\\tID:1\\tSM:~{sampleGenomicID}\\tPL:illumina\\tLB:lib1\\tPU:unit1"

    # align reads
    bwa mem -M -t ~{numCPUs} -R "${readGroup}" ref/hs37d5.fa ~{R1Fastq} ~{R2Fastq} | sambamba view -f bam -l 0 -S -t ~{numCPUs} /dev/stdin | sambamba sort -t ~{numCPUs} -o ~{sampleGenomicID}.sorted.bam /dev/stdin
    mv ~{sampleGenomicID}.sorted.bam /tmp/scratch/
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          numCPUs,
      mem_gb:             30,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/priority-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File sorted_bam = "~{sampleGenomicID}.sorted.bam"
  }
}

task align {
  input {
    Array[String] R1Fastqs
    Array[String] R2Fastqs
    String sampleGenomicID
    String sampleRunID
    Int? numCPUs=16
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Aligns a set of fastq file pairs to the GRCh37 reference genome."
  }

  parameter_meta {
    R1Fastqs: "File or array of files with the R1 fastq reads, can optionally be compressed with gzip or bzip2."
    R2Fastqs: "File or array of files with the R2 fastq reads, can optionally be compressed with gzip or bzip2."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR
    
    # re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
    firstFile=~{R1Fastqs[0]}
    extension=${firstFile##*.}
    fastqExtension=""
    if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=$(echo ".${extension}"); fi

    read1="~{sampleGenomicID}_read1.fq${fastqExtension}"
    read2="~{sampleGenomicID}_read2.fq${fastqExtension}"

    # re-implementation of the s3_read_concat function -- can be moved later if that gets updated to cromwell-friendly version
    # Outputs a concatenated stream of all files listed in the input array
    s3_read_concat_cromwell() {
        for s3file in $1; do
            aws s3 cp --only-show-errors "$s3file" -
        done
    }
    s3_read_concat_cromwell "~{sep=' ' R1Fastqs}" > ${read1} &
    s3_read_concat_cromwell "~{sep=' ' R2Fastqs}" > ${read2}
    wait 

    # note, the Read Group is written to match the style and defaults used by Picard AddOrReplaceReadGroups
    readGroup="@RG\\tID:1\\tSM:~{sampleGenomicID}\\tPL:illumina\\tLB:lib1\\tPU:unit1"

    # align reads
    bwa mem -M -t ~{numCPUs} -R "${readGroup}" ref/hs37d5.fa ${read1} ${read2} | sambamba view -f bam -l 0 -S -t ~{numCPUs} /dev/stdin | sambamba sort -t ~{numCPUs} -o ~{sampleGenomicID}.sorted.bam /dev/stdin

    # upload output to S3
    aws s3 cp ~{sampleGenomicID}.sorted.bam s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors


  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          numCPUs,
      mem_gb:             30,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/priority-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    String sorted_bam = "s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.sorted.bam"
  }
}