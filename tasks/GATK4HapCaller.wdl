version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

# Generate Base Quality Score Recalibration (BQSR) model
task baseRecalibrator {
    input {
        String input_bam
        String input_bam_index
        String recalibration_report_filename
        File sequence_group_interval_file
        String sampleRunID
        String gatk_path
        String java_opt
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR

        aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam} . --only-show-errors
        aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam_index} . --only-show-errors

        ~{gatk_path} --java-options "~{java_opt}" \
            BaseRecalibrator \
            -R ref/hs37d5.fa \
            -I ~{input_bam} \
            --use-original-qualities \
            -O ~{recalibration_report_filename} \
            --known-sites ref/dbsnp_138.b37.vcf \
            --known-sites ref/Mills_and_1000G_gold_standard.indels.b37.vcf \
            --known-sites ref/1000G_phase1.indels.b37.vcf \
            -L ~{sequence_group_interval_file}
        mv ~{recalibration_report_filename} /tmp/scratch/
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             4,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }

    output {
        File recalibration_report = "~{recalibration_report_filename}"
    }
}

# Combine multiple recalibration tables from scattered BaseRecalibrator runs
# Note that when run from GATK 3.x the tool is not a walker and is invoked differently.
task gatherBqsrReports {
    input {
        Array[File] input_bqsr_reports
        String output_report_filename
        String sampleRunID

        String gatk_path
        String java_opt
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR
        ~{gatk_path} --java-options "~{java_opt}" \
            GatherBQSRReports \
            -I ~{sep=' -I ' input_bqsr_reports} \
            -O ~{output_report_filename}
        mv ~{output_report_filename} /tmp/scratch/
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             4,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
    output {
        File output_bqsr_report = "~{output_report_filename}"
    }
}

# Apply Base Quality Score Recalibration (BQSR) model
task applyBQSR {
    input {
        String input_bam
        String input_bam_index
        String output_bam_basename
        File recalibration_report
        File sequence_group_interval_file
        String sampleRunID

        String gatk_path
        String java_opt
        RuntimeAttr? runtime_attr_override
    }

    command <<<    
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR

        aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam} . --only-show-errors
        aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam_index} . --only-show-errors

        ~{gatk_path} --java-options "~{java_opt}" \
            ApplyBQSR \
            -R ref/hs37d5.fa \
            -I ~{input_bam} \
            -O ~{output_bam_basename}.bam \
            -L ~{sequence_group_interval_file} \
            -bqsr ~{recalibration_report} \
            --static-quantized-quals 10 --static-quantized-quals 20 --static-quantized-quals 30 \
            --add-output-sam-program-record \
            --create-output-bam-md5 \
            --use-original-qualities
        mv ~{output_bam_basename}.bam /tmp/scratch/
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             4,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
    output {
        File recalibrated_bam = "~{output_bam_basename}.bam"
    }
}

# Combine multiple recalibrated BAM files from scattered ApplyRecalibration runs
task gatherBamFiles {
    input {
        Array[File] input_bams
        String output_bam_basename
        String sampleRunID
        String gatk_path
        String java_opt
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR
        ~{gatk_path} --java-options "~{java_opt}" \
            GatherBamFiles \
            -I ~{sep=' -I ' input_bams} \
            -O ~{output_bam_basename}.bam
        samtools index ~{output_bam_basename}.bam 
        mv ~{output_bam_basename}.bam /tmp/scratch/
        mv ~{output_bam_basename}.bam.bai /tmp/scratch/
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             4,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
    output {
        File output_bam = "~{output_bam_basename}.bam"
        File output_bam_index = "~{output_bam_basename}.bam.bai"
    }
}

task haplotypeCaller {
    input {
        File input_bam
        File input_bam_index
        File interval_list
        String gvcf_basename
        String sampleRunID
        String gatk_path
        String java_opt
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR
        ~{gatk_path} --java-options "~{java_opt}" HaplotypeCaller \
            -R ref/hs37d5.fa \
            -I ~{input_bam} \
            -ERC GVCF \
            -L ~{interval_list} \
            -ip 100 \
            -O ~{gvcf_basename}.vcf.gz 
        mv ~{gvcf_basename}.vcf.gz /tmp/scratch/
        mv ~{gvcf_basename}.vcf.gz.tbi /tmp/scratch/
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             10,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
    output {
        File output_gvcf = "~{gvcf_basename}.vcf.gz"
        File output_gvcf_index = "~{gvcf_basename}.vcf.gz.tbi"
    }
}

task gatherVCFs {
    input {
        Array[File] input_vcfs
        Array[File] input_vcfs_indexes
        String output_vcf_name
        String sampleRunID
        String gatk_path
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR
        ~{gatk_path} MergeVcfs \
            -I ~{sep=' -I ' input_vcfs} \
            -O ~{output_vcf_name}
        mv ~{output_vcf_name} /tmp/scratch/
        mv ~{output_vcf_name}.idx /tmp/scratch/
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             4,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
    output {
        File output_vcf = "~{output_vcf_name}"
        File output_vcf_index = "~{output_vcf_name}.idx"
    }
}

task cleanUpOutputs {
    input {
        File input_vcf
        File input_vcf_idx
        String sampleGenomicID
        String sampleRunID
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR
        bgzip -c ~{input_vcf} > ~{sampleGenomicID}.hg19.g.vcf.gz
        tabix ~{sampleGenomicID}.hg19.g.vcf.gz
        aws s3 cp ~{sampleGenomicID}.hg19.g.vcf.gz s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
        aws s3 cp ~{sampleGenomicID}.hg19.g.vcf.gz.tbi s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
        aws s3 cp ~{input_vcf_idx} s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             4,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }

}

task runPeddy {
    input {
        File input_bam
        File input_bam_bai
        String sampleGenomicID
        String sampleRunID
        String gatk_path
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR
        ~{gatk_path} HaplotypeCaller \
            -I ~{input_bam} \
            -R ref/hs37d5.fa \
            -O ~{sampleGenomicID}.peddySites.vcf \
            --alleles ref/dbsnp_138.peddySites.vcf \
            -L ref/dbsnp_138.peddySites.vcf
        aws s3 cp ~{sampleGenomicID}.peddySites.vcf s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
        
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          4,
        mem_gb:             2,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
}

task customGenotypeCalls {
    input {
        File input_bam
        File input_bam_bai
        File custom_sites = "s3://tgp-data-analysis/resources/variantsToGenotypeInEverySample.vcf"
        File custom_sites_idx = "s3://tgp-data-analysis/resources/variantsToGenotypeInEverySample.vcf.idx"
        String sampleGenomicID
        String sampleRunID
        String gatk_path
        RuntimeAttr? runtime_attr_override
    }

    command <<<
        cd /home/ec2-user
        source .bashrc
        set -euo pipefail
        git clone https://gitlab.com/mdanzi/tgp-data-processing.git
        source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
        trap "errorReport sample ~{sampleRunID}" ERR
        ~{gatk_path} HaplotypeCaller \
            -I ~{input_bam} \
            -R ref/hs37d5.fa \
            -O ~{sampleGenomicID}.customSites.vcf \
            --alleles ~{custom_sites} \
            -L ~{custom_sites}
        aws s3 cp ~{sampleGenomicID}.customSites.vcf s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
        
    >>>
    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             2,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
}
