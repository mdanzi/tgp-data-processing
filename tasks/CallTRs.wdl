version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task callTRs {
  input {
    String input_bam
    String input_bam_bai
    String sampleGenomicID
    String sampleRunID
    String sampleGUID
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Runs ExpansionHunter and ExpansionHunter Denovo on the bam file to call tandem repeats."
  }

  parameter_meta {
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam} . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam_bai} . --only-show-errors

    ExpansionHunterDenovo profile --reads ~{input_bam} --reference ref/hs37d5.fa --output-prefix ~{sampleGenomicID} --min-unit-len 3 --max-unit-len 8

    set +u
    source activate py37
    set -u

    # groom output data and print bamlet (always include known pathogenic sites)
    mv ~{sampleGenomicID}.str_profile.json ~{sampleGenomicID}.eh-denovo-output.json # this is done for backward compatibility 
    echo -e "~{sampleGenomicID}\tcase\t~{sampleGenomicID}.eh-denovo-output.json" > manifest.txt
    python tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/combine_counts.py --manifest manifest.txt --combinedCounts ~{sampleGenomicID}.json
    python tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/compare_anchored_irrs.py --manifest manifest.txt --inputCounts ~{sampleGenomicID}.json --outputRegions ~{sampleGenomicID}.bed --minCount 5
    grep -wv '^hs37d5' ~{sampleGenomicID}.bed | grep -v '^GL' | cut -f 1,2,3 | cat - tgp-data-processing/scripts/repeatExpansions/knownPathogenicSites.bed | sort -k1,1 -k2n,2n | bedtools slop -i stdin -g ref/hs37d5.fa.fai -b 2000 | bedtools merge -i stdin > ~{sampleGenomicID}.groomed.bed
    python tgp-data-processing/scripts/repeatExpansions/printBamlet.py --bed=~{sampleGenomicID}.groomed.bed --bam=~{input_bam} --name=~{sampleGenomicID}
    samtools sort ~{sampleGenomicID}_repeatRegionsBamlet.bam > ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam
    samtools index ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam

    # upload EHDn output to S3
    aws s3 cp ~{sampleGenomicID}.eh-denovo-output.json s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors

    # upload bamlet to S3
    aws s3 cp ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam s3://tgp-sample-assets/~{sampleGUID}/RepeatExpansions/ --only-show-errors
    aws s3 cp ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam.bai s3://tgp-sample-assets/~{sampleGUID}/RepeatExpansions/ --only-show-errors


    #### Part 2 -- run outlier pipeline on EHDn results ####
    filename="~{sampleGenomicID}.eh-denovo-output.json"
    mamba install -y -c conda-forge r-mustat
    grep -wv '^hs37d5' ~{sampleGenomicID}.bed | grep -v '^GL' | sed 's/^/chr/' > EHDnResults.combinedCounts.filtered.bed
    Rscript tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/reformatEHDenovoOutput.R
    cut -f 1,2,3,4,7 EHDnResults.combinedCounts.filtered.bed | sed '1d' | sort -k1,1 -k2n,2n > ~{sampleGenomicID}.sorted.txt
    python3 tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/selectMotifMatchingOverlaps.py --input=~{sampleGenomicID}.sorted.txt --ref=tgp-data-processing/scripts/repeatExpansions/Reference_files/reference.sorted.txt --output=output.txt
    Rscript tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/combine_case_control.R EHDnResults.combinedCounts.filtered.reformatted.bed tgp-data-processing/scripts/repeatExpansions/Reference_files/1000Genomes.combinedCounts.hg19.filtered.reformatted.bed output.txt
    Rscript tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/CreateFile_for_annotation.R CaseCont_combined.filtered.reformatted.bed
    mv file_for_annotation.txt file_for_annotation.bed
    annotatePeaks.pl file_for_annotation.bed hg19 > AnnotationFILE.txt
    Rscript tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/Annotate_addStats.R CaseCont_combined.filtered.reformatted.bed AnnotationFILE.txt
    cut -f 1,2,3 file_for_annotation.bed | sed '1s/^/#/' | sort -k1,1 -k2n,2n > file_for_intersection.bed
    bedtools intersect -a file_for_intersection.bed -b tgp-data-processing/scripts/repeatExpansions/Reference_files/SimpleRepeats_hg19_loci.bed -wo > intersection_seeds.txt
    Rscript tgp-data-processing/scripts/repeatExpansions/EHDn-v0.6.2_HelperScripts/Filter_hits_individual.R CaseControl_statistics.bed intersection_seeds.txt
    sed -e '2,$s/^chr//' filtered_hits.txt > ~{sampleGenomicID}_outliers.txt
    aws s3 cp ~{sampleGenomicID}_outliers.txt s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors


    #### Part 3 -- Convert EHDn coordinates to EH coordinates ####
    FILELEN=$(cat ~{sampleGenomicID}_outliers.txt | wc -l || true)
    if [[ ${FILELEN} -gt 1 ]]; then
        # remove false positives
        Rscript tgp-data-processing/scripts/repeatExpansions/ehdn-to-eh-master/Remove_falsePositiveTRs.R ~{sampleGenomicID}_outliers.txt ~{sampleGenomicID}
        cut -f 1,2,3,4,25 FinalHits_minusFP_~{sampleGenomicID}.txt > tgp-data-processing/scripts/repeatExpansions/ehdn-to-eh-master/input/input_~{sampleGenomicID}.txt
        python3 tgp-data-processing/scripts/repeatExpansions/ehdn-to-eh-master/create_variant_catalog_auto.py ~{sampleGenomicID}
        # save the outliers that don't get resolved for EH and upload to S3
        tail -n +2 unmatched_regions_~{sampleGenomicID}.txt | cat excluded_~{sampleGenomicID}.txt - > ~{sampleGenomicID}_unresolvedOutliers.txt
        aws s3 cp ~{sampleGenomicID}_unresolvedOutliers.txt s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
        python tgp-data-processing/scripts/repeatExpansions/mergeJSONFiles.py --name=~{sampleGenomicID} --json1=variant_catalog_~{sampleGenomicID}.json --json2=tgp-data-processing/scripts/repeatExpansions/gnomAD_grch37_catalog_withoutOffTargets.json
    else
        # make an empty output file
        echo -e "contig\tstart\tend\tmotif\tcount" > ~{sampleGenomicID}_unresolvedOutliers.txt
        aws s3 cp ~{sampleGenomicID}_unresolvedOutliers.txt s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
        # use the default gnomAD catalog for part 4
        cp tgp-data-processing/scripts/repeatExpansions/gnomAD_grch37_catalog_withoutOffTargets.json variant_catalog_merged_~{sampleGenomicID}.json
    fi

    #### Part 4 -- Run EH and REViewer ####
    # run Expansion Hunter on the outlier and known pathogenic sites
    ExpansionHunter --reads ~{input_bam} --reference ref/hs37d5.fa --variant-catalog variant_catalog_merged_~{sampleGenomicID}.json --output-prefix ~{sampleGenomicID}

    # sort and index the bamlet produced by EH
    samtools sort ~{sampleGenomicID}_realigned.bam > ~{sampleGenomicID}_realigned.sorted.bam
    samtools index ~{sampleGenomicID}_realigned.sorted.bam

    # run REViewer on each site in the VCF file
    grep -v '^#' ~{sampleGenomicID}.vcf | grep -v 'LowDepth' | cut -f 5 -d';' | sed 's/VARID=//' | grep -v '_' > varIDs.txt
    while read VARID; do
        if [ -n "${VARID}" ]; then REViewer --reads ~{sampleGenomicID}_realigned.sorted.bam --vcf ~{sampleGenomicID}.vcf --reference ref/hs37d5.fa --catalog variant_catalog_merged_~{sampleGenomicID}.json --locus ${VARID} --output-prefix ~{sampleGenomicID}; fi
        if [ -f "~{sampleGenomicID}.${VARID}.svg" ]; then aws s3 cp ~{sampleGenomicID}.${VARID}.svg s3://tgp-sample-assets/~{sampleGUID}/RepeatExpansions/ --only-show-errors ; fi
    done < varIDs.txt


    # save the genotype calls to S3 for input to the parser/annotator module 
    mv ~{sampleGenomicID}.vcf ~{sampleGenomicID}_EH.vcf
    aws s3 cp ~{sampleGenomicID}_EH.vcf s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors

  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             8,
      max_retries:        1,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
}

task callTRsWES {
  input {
    String input_bam
    String input_bam_bai
    String sampleGenomicID
    String sampleRunID
    String sampleGUID
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Runs ExpansionHunter on the bam file to call tandem repeats."
  }

  parameter_meta {
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam} . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam_bai} . --only-show-errors
   
    # make an empty output file
    echo -e "contig\tstart\tend\tmotif\tcount" > ~{sampleGenomicID}_unresolvedOutliers.txt
    aws s3 cp ~{sampleGenomicID}_unresolvedOutliers.txt s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors

    # use the default gnomAD catalog for EH
    cp tgp-data-processing/scripts/repeatExpansions/gnomAD_grch37_catalog_withoutOffTargets.json variant_catalog_merged_~{sampleGenomicID}.json

    # print bamlet for known pathogenic sites
    set +u
    source activate py37
    set -u
    sort -k1,1 -k2n,2n tgp-data-processing/scripts/repeatExpansions/knownPathogenicSites.bed | bedtools slop -i stdin -g ref/hs37d5.fa.fai -b 2000 | bedtools merge -i stdin > ~{sampleGenomicID}.groomed.bed
    python tgp-data-processing/scripts/repeatExpansions/printBamlet.py --bed=~{sampleGenomicID}.groomed.bed --bam=~{input_bam} --name=~{sampleGenomicID}
    samtools sort ~{sampleGenomicID}_repeatRegionsBamlet.bam > ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam
    samtools index ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam
    # upload bamlet to S3
    aws s3 cp ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam s3://tgp-sample-assets/~{sampleGUID}/RepeatExpansions/ --only-show-errors
    aws s3 cp ~{sampleGenomicID}_repeatRegionsBamlet.sorted.bam.bai s3://tgp-sample-assets/~{sampleGUID}/RepeatExpansions/ --only-show-errors

    #### Part 4 -- Run EH and REViewer ####
    # run Expansion Hunter on the outlier and known pathogenic sites
    ExpansionHunter --reads ~{input_bam} --reference ref/hs37d5.fa --variant-catalog variant_catalog_merged_~{sampleGenomicID}.json --output-prefix ~{sampleGenomicID}

    # sort and index the bamlet produced by EH
    samtools sort ~{sampleGenomicID}_realigned.bam > ~{sampleGenomicID}_realigned.sorted.bam
    samtools index ~{sampleGenomicID}_realigned.sorted.bam

    # run REViewer on each site in the VCF file
    grep -v '^#' ~{sampleGenomicID}.vcf | grep -v 'LowDepth' | cut -f 5 -d';' | sed 's/VARID=//' | grep -v '_' > varIDs.txt
    while read VARID; do
      if [ -n "${VARID}" ]; then REViewer --reads ~{sampleGenomicID}_realigned.sorted.bam --vcf ~{sampleGenomicID}.vcf --reference ref/hs37d5.fa --catalog variant_catalog_merged_~{sampleGenomicID}.json --locus ${VARID} --output-prefix ~{sampleGenomicID} ; fi
      if [ -f "~{sampleGenomicID}.${VARID}.svg" ]; then aws s3 cp ~{sampleGenomicID}.${VARID}.svg s3://tgp-sample-assets/~{sampleGUID}/RepeatExpansions/ --only-show-errors ; fi
    done < varIDs.txt

    # save the genotype calls to S3 for input to the parser/annotator module 
    mv ~{sampleGenomicID}.vcf ~{sampleGenomicID}_EH.vcf
    aws s3 cp ~{sampleGenomicID}_EH.vcf s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors

  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             8,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
}