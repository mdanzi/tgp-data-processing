version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task mergeBams {
  input {
    Array[File] input_bams
    String sampleGenomicID
    String sampleRunID
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Merges a set of aligned bam files."
  }

  parameter_meta {
    input_bams: "Array of bam files produced by the scattered alignment tasks."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    samtools merge -c -p -o ~{sampleGenomicID}.sorted.bam "~{sep='" "' input_bams}"
    
    # upload output to S3
    aws s3 cp ~{sampleGenomicID}.sorted.bam s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors

  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    String sorted_bam = "s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.sorted.bam"
  }
}