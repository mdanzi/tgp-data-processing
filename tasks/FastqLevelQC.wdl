version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task fastqLevelQC {
  input {
    Array[String] R1Fastqs
    Array[String] R2Fastqs
    String sampleGenomicID
    String sampleRunID
    String? alignOutputStr = "NULL" # can be used to coerce fastqLevelQC to run only after alignment is complete
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Performs quality checking on set of fastq file pairs."
  }
  parameter_meta {
    R1Fastqs: "File or array of files with the R1 fastq reads, can optionally be compressed with gzip or bzip2."
    R2Fastqs: "File or array of files with the R2 fastq reads, can optionally be compressed with gzip or bzip2."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    source tgp-data-processing/scripts/utils/parse_run_data.sh  # imports get_sample_id, etc
    trap "errorReport sample ~{sampleRunID}" ERR
    
    # re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
    firstFile=~{R1Fastqs[0]}
    extension=${firstFile##*.}
    fastqExtension=""
    if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=$(echo ".${extension}"); fi

    read1="~{sampleGenomicID}_read1.fq${fastqExtension}"
    read2="~{sampleGenomicID}_read2.fq${fastqExtension}"

    # re-implementation of the s3_read_concat function -- can be moved later if that gets updated to cromwell-friendly version
    # Outputs a concatenated stream of all files listed in the input array
    s3_read_concat_cromwell() {
        for s3file in $1; do
            aws s3 cp --only-show-errors "$s3file" -
        done
    }
    s3_read_concat_cromwell "~{sep=' ' R1Fastqs}" > ${read1} &
    s3_read_concat_cromwell "~{sep=' ' R2Fastqs}" > ${read2}
    wait 

    # make output file for this file 
    echo -e "fileName\tsampleName\tnumberOfReads\tsizeInGB\tQ30" > ~{sampleGenomicID}.fastqLevel.qc

    # QC read1
    FastQC/fastqc ${read1} 
    unzip ~{sampleGenomicID}_read1_fastqc.zip
    numReads=$(grep "Total Sequences" ~{sampleGenomicID}_read1_fastqc/fastqc_data.txt | awk '{print $3}')
    fileSize=$(du -h ${read1} | awk '{print $1}')
    numQ30=$(grep -A42 -m1 "Per sequence quality" ~{sampleGenomicID}_read1_fastqc/fastqc_data.txt | sed -n '/^29/{:a;n;p;/^>>END/!ba}' | head -n -1 | awk '{n1+=$2} END {print n1}')
    percentQ30=$(expr 100 \* ${numQ30} / ${numReads} )
    echo -e "${read1}\t~{sampleGenomicID}\t${numReads}\t${fileSize}\t${percentQ30}" >> ~{sampleGenomicID}.fastqLevel.qc

    # QC read2
    FastQC/fastqc ${read2} 
    unzip ~{sampleGenomicID}_read2_fastqc.zip
    numReads=$(grep "Total Sequences" ~{sampleGenomicID}_read2_fastqc/fastqc_data.txt | awk '{print $3}')
    fileSize=$(du -h ${read2} | awk '{print $1}')
    numQ30=$(grep -A42 -m1 "Per sequence quality" ~{sampleGenomicID}_read2_fastqc/fastqc_data.txt | sed -n '/^29/{:a;n;p;/^>>END/!ba}' | head -n -1 | awk '{n1+=$2} END {print n1}')
    percentQ30=$(expr 100 \* ${numQ30} / ${numReads} )
    echo -e "${read2}\t~{sampleGenomicID}\t${numReads}\t${fileSize}\t${percentQ30}" >> ~{sampleGenomicID}.fastqLevel.qc

    # upload output to S3
    aws s3 cp ~{sampleGenomicID}.fastqLevel.qc s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors


  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             4,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    String fastq_qc = "s3://tgp-sample-processing/~{sampleRunID}/~{sampleGenomicID}.fastqLevel.qc"
  }
}