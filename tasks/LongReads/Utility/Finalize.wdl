version 1.0

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"
task FinalizeToFile {

    meta{
        description: "Copies the given file to the specified bucket."
    }

    parameter_meta {
        file: "file to finalize"
        keyfile : "[optional] File used to key this finaliation.  Finalization will not take place until the KeyFile exists.  This can be used to force the finaliation to wait until a certain point in a workflow.  NOTE: The latest WDL development spec includes the `after` keyword which will obviate this."
        outdir: "directory to which files should be uploaded"
        name:   "name to set for uploaded file"
    }

    input {
        File file
        String outdir
        String? name

        File? keyfile

        RuntimeAttr? runtime_attr_override
    }



    String s3_output_dir = sub(outdir, "/+$", "")
    String s3_output_file = s3_output_dir + "/" + select_first([name, basename(file)])

    command <<<
        set -euxo pipefail

        /usr/local/aws-cli/v2/current/dist/aws s3 cp "~{file}" "~{s3_output_file}"
    >>>

    output {
        String s3_path = s3_output_file
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             1,
        max_retries:        2,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-finalize:0.1.3"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }
}

task FinalizeToDir {

    meta {
        description: "Copies the given file to the specified bucket."
    }

    parameter_meta {
        files: "files to finalize"
        keyfile : "[optional] File used to key this finaliation.  Finalization will not take place until the KeyFile exists.  This can be used to force the finaliation to wait until a certain point in a workflow.  NOTE: The latest WDL development spec includes the `after` keyword which will obviate this."
        outdir: "directory to which files should be uploaded"
    }

    input {
        Array[File] files
        String outdir

        File? keyfile

        RuntimeAttr? runtime_attr_override
    }

    String s3_output_dir = sub(outdir, "/+$", "")

    command <<<
        set -euxo pipefail

        while read FILE; do
            /usr/local/aws-cli/v2/current/dist/aws s3 cp ${FILE} "~{s3_output_dir}"
        done < $(cat ~{write_lines(files)})
    >>>

    output {
        String s3_dir = s3_output_dir
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             1,
        max_retries:        2,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-finalize:0.1.3"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }
}

task FinalizeTarGzContents {
    meta {
        description : "Copies the contents of the given tar.gz file to the specified bucket."
    }

    parameter_meta {
        tar_gz_file : "Gzipped tar file whose contents we'll copy."
        outdir : "Google cloud path to the destination folder."

        keyfile : "[optional] File used to key this finaliation.  Finalization will not take place until the KeyFile exists.  This can be used to force the finaliation to wait until a certain point in a workflow.  NOTE: The latest WDL development spec includes the `after` keyword which will obviate this."

        runtime_attr_override : "[optional] Additional runtime parameters."
    }

    input {
        File tar_gz_file
        String outdir

        File? keyfile

        RuntimeAttr? runtime_attr_override
    }

    # This idiom ensures that we don't accidentally have double-slashes in our GCS paths
    String s3_output_dir = sub(sub(outdir + "/", "/+", "/"), "s3:/", "s3://")

    command <<<
        set -euxo pipefail

        mkdir tmp
        cd tmp
        tar -zxf ~{tar_gz_file}

        /usr/local/aws-cli/v2/current/dist/aws s3 cp ./* ~{s3_output_dir} --recursive
    >>>

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             2,
        max_retries:        2,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-finalize:0.1.3"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }
}

task WriteCompletionFile {

    meta {
        description : "Write a file to the given directory indicating the run has completed."
    }

    parameter_meta {
        outdir : "Google cloud path to the destination folder."
        keyfile : "[optional] File used to key this finaliation.  Finalization will not take place until the KeyFile exists.  This can be used to force the finaliation to wait until a certain point in a workflow.  NOTE: The latest WDL development spec includes the `after` keyword which will obviate this."
    }

    input {
        String outdir
        File? keyfile
    }

    command <<<
        set -euxo pipefail

        completion_file="COMPLETED_AT_$(date +%Y%m%dT%H%M%S).txt"
        touch $completion_file

        /usr/local/aws-cli/v2/current/dist/aws s3 cp $completion_file ~{outdir}
    >>>

    #########################

    runtime {
        cpu:                    1
        memory:                 1 + " GiB"
        maxRetries:             2
        docker:                 "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-finalize:0.1.3"
    }
}

task WriteNamedFile {

    meta {
        description : "Write a file to the given directory with the given name."
    }

    parameter_meta {
        name : "Name of the file to write."
        outdir : "Google cloud path to the destination folder."
        keyfile : "[optional] File used to key this finaliation.  Finalization will not take place until the KeyFile exists.  This can be used to force the finaliation to wait until a certain point in a workflow.  NOTE: The latest WDL development spec includes the `after` keyword which will obviate this."
    }

    input {
        String name
        String outdir
        File? keyfile
    }

    command <<<
        set -euxo pipefail

        touch "~{name}"

        /usr/local/aws-cli/v2/current/dist/aws s3 cp "~{name}" ~{outdir}
    >>>

    #########################

    runtime {
        cpu:                    1
        memory:                 1 + " GiB"
        maxRetries:             2
        docker:                 "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-finalize:0.1.3"
    }
}

task CompressAndFinalize {

    meta {
        description: "Gzip a file and finalize"
    }

    parameter_meta {
        file : "File to compress and finalize."
        outdir : "Google cloud path to the destination folder."
        name : "[optional] Name of the file to write.  If not specified, the name of the input file will be used."
        runtime_attr_override : "[optional] Additional runtime parameters."
    }

    input {
        File file
        String outdir
        String? name

        RuntimeAttr? runtime_attr_override
    }

    String base = basename(file)
    String out = sub(select_first([name, base]), ".gz$", "") +  ".gz"
    # THIS IS ABSOLUTELY CRITICAL: DON'T CHANGE TYPE TO FILE, AS CROMWELL WILL TRY TO LOCALIZE THIS NON-EXISTENT FILE
    String s3_output_file = sub(outdir, "/+$", "") + "/" + out

    command <<<
        set -euxo pipefail

        gzip -vkc ~{file} > "~{base}.gz"
        /usr/local/aws-cli/v2/current/dist/aws s3 cp "~{base}.gz" "~{s3_output_file}"
    >>>

    output {
        String s3_path = s3_output_file
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          1,
        mem_gb:             4,
        max_retries:        2,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-finalize:0.1.3"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }
}

task FinalizeAndCompress {
    meta {
        description: "Gzip a bunch of files and finalize to the same \'folder\'"
    }

    parameter_meta {
        files : "Files to compress and finalize."
        outdir : "Google cloud path to the destination folder."
        prefix : "[optional] Prefix to add to the output files."
        runtime_attr_override : "[optional] Additional runtime parameters."
    }

    input {
        Array[File] files
        String outdir

        String prefix

        RuntimeAttr? runtime_attr_override
    }

    String s3_output_file = sub(outdir, "/+$", "") + "/" + prefix + "/"

    command <<<
        set -euxo pipefail

        for ff in ~{sep=' ' files};
        do
            base="$(basename -- ${ff})"
            mv "${ff}" "${base}" && gzip -vk "${base}"
        done

        /usr/local/aws-cli/v2/current/dist/aws s3 cp /cromwell_root/ "~{s3_output_file}" --recursive --exclude "*" --include "*.gz"
    >>>

    output {
        String s3_path = s3_output_file
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          2,
        mem_gb:             7,
        max_retries:        2,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-finalize:0.1.3"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }
}
