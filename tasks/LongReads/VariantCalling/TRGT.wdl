version 1.0

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

workflow runTRGT {

  meta {
    description: "Uses TRGT to size TRs in a bam file."
  }

  input {
    File input_bam
    File input_bam_bai
    String basename = basename(input_bam, ".bam")
    File ref_dict
    File ref_fasta
    File ref_fasta_fai
    File repeatCatalog = "s3://tgp-data-analysis/resources/adotto_TRregions_TRGTFormatWithFlankingSeq_v1.0_under1kb_GRCh37.bed"

    #Optional runtime arguments
    RuntimeAttr? runtime_attr_override
  }

  call processWithTRGT {
    input:
      input_bam = input_bam,
      input_bam_bai = input_bam_bai,
      basename = basename,
      ref_fasta = ref_fasta,
      ref_fasta_fai = ref_fasta_fai,
      ref_dict = ref_dict,
      repeatCatalog = repeatCatalog,
      runtime_attr_override = runtime_attr_override
  }

  output {
    File trgt_output_vcf = processWithTRGT.trgt_output_vcf
    File trgt_output_bam = processWithTRGT.trgt_output_bam
  }
}

task processWithTRGT {
  input {
    File input_bam
    File input_bam_bai
    String basename
    File ref_fasta
    File ref_fasta_fai
    File ref_dict
    File repeatCatalog
    Int cpuCores = 4

    RuntimeAttr? runtime_attr_override

  }

  meta {
    description: "Uses TRGT to size TRs in a bam file."
  }

  command <<<
    set -euo pipefail
    trgt genotype --genome ~{ref_fasta} --repeats ~{repeatCatalog} --reads ~{input_bam} --threads ~{cpuCores} --output-prefix ~{basename}_trgt

  >>>

  output {
    File trgt_output_vcf = "~{basename}_trgt.vcf.gz"
    File trgt_output_bam = "~{basename}_trgt.spanning.bam"
  }

  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          cpuCores,
      mem_gb:             31,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-trgt:1.1.0",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/priority-gwfcore4"

  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])

  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
}