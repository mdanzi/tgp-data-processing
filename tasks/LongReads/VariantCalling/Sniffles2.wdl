version 1.0


import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

workflow Sniffles2 {

    meta {
        description: "This workflow calls SV candidates using Sniffles2 population mode."
    }

    parameter_meta {
        # input
        sampleBAMs:      "GCS paths to aligned BAM files from multiple samples"
        sampleBAIs:       "GCS paths to aligned BAM files indices from multiple samples"
        sampleIDs:       "matching sample IDs of the BAMs"
        minsvlen:        "Minimum SV length in bp"
        prefix:          "prefix for output files"
        # output
        single_snf:      "[OUTPUT] .snf output containing SV candidates from a single sample"
        multisample_vcf: "[OUTPUT] Multi-sample vcf output"
    }

    input {
        Array[File] sampleBAMs
        Array[File] sampleBAIs
        Array[String] sampleIDs
        String prefix
        Int minsvlen = 50
        File? tandem_repeat_bed
    }

    scatter (i in range(length(sampleBAMs))) {
        call SampleSV {
            input:
                bam = sampleBAMs[i],
                bai = sampleBAIs[i],
                minsvlen = minsvlen,
                prefix = prefix,
                sample_id = sampleIDs[i],
                tandem_repeat_bed = tandem_repeat_bed
        }
    }

    call MergeCall {
        input:
            snfs = SampleSV.snf,
            prefix = prefix
    }


    output {
         Array[File] single_snf = SampleSV.snf
         File multisample_vcf = MergeCall.vcf
    }
}



task SampleSV {

    meta {
        description: "This task calls SV candidates from a single sample."
    }

    input {
        File bam
        File bai
        Int minsvlen = 50
        String sample_id
        String prefix
        File? tandem_repeat_bed
        Boolean phase_sv = false
        RuntimeAttr? runtime_attr_override
    }

    parameter_meta {
        bam:              "input BAM from which to call SVs"
        bai:              "index accompanying the BAM"
        minsvlen:         "minimum SV length in bp. Default 50"
        sample_id:        "Sample ID"
        prefix:           "prefix for output"
    }

    Int cpus = 8
    String snf_output = "~{prefix}.sniffles.snf"
    String vcf_output = "~{prefix}.sniffles.vcf.gz"
    String tbi_output = "~{prefix}.sniffles.vcf.gz.tbi"

    command <<<
        set -eux

        sniffles -t ~{cpus} \
                 -i ~{bam} \
                 --minsvlen ~{minsvlen} \
                 --sample-id ~{sample_id} \
                 ~{if defined(tandem_repeat_bed) then "--tandem-repeats ~{tandem_repeat_bed}" else ""} \
                 ~{true="--phase" false="" phase_sv} \
                 --vcf ~{vcf_output} \
                 --snf ~{snf_output}
    >>>

    output {
        File snf = "~{snf_output}"
        File vcf = "~{vcf_output}"
        File tbi = "~{tbi_output}"
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          cpus,
        mem_gb:             46,
        max_retries:        2,
        #docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-sniffles2:2.0.7",
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-pb:0.1.40",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/priority-gwfcore4"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
}


task MergeCall {

    meta {
        description: "This tasks performs joined-calling from multiple .snf files and produces a single .vcf"
    }

    input {
        Array[File] snfs
        String prefix
        RuntimeAttr? runtime_attr_override
    }

    parameter_meta {
        snfs: ".snf files"
    }

    command <<<
        set -eux
        sniffles --input ~{sep=" " snfs} \
            --vcf multisample.vcf
        tree
    >>>

    output {
        File vcf = "~{prefix}.vcf"
    }

    Int cpus = 8
                                                                                                                                                                                                                                                                                                                                                                                                                                       #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          cpus,
        mem_gb:             46,
        max_retries:        2,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-sniffles2:2.0.7"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }

}