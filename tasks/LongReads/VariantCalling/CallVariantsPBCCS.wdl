version 1.0 

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/Utility/Utils.wdl"
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/Utility/VariantUtils.wdl"
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/VariantCalling/PBSV.wdl"
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/VariantCalling/Sniffles2.wdl" as Sniffles2
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/VariantCalling/CCSPepper.wdl"
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/LongReads/VariantCalling/TRGT.wdl" as TRGT

workflow CallVariants {

    meta {
        description: "A workflow for calling small and/or structural variants from an aligned CCS BAM file."
    }

    parameter_meta {
        bam: "Aligned CCS BAM file"
        bai: "Index for the aligned CCS BAM file"
        minsvlen: "Minimum SV length in bp (default: 50)"
        prefix: "Prefix for output files"
        sample_id: "Sample ID"
        ref_fasta: "Reference FASTA file"
        ref_fasta_fai: "Index for the reference FASTA file"
        ref_dict: "Dictionary for the reference FASTA file"
        call_svs: "Call structural variants or not"
        fast_less_sensitive_sv: "to trade less sensitive SV calling for faster speed"
        tandem_repeat_bed: "BED file containing TRF finder for better PBSV calls (e.g. http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.trf.bed.gz)"
        call_small_variants: "Call small variants or not"
        run_dv_pepper_analysis: "to turn on DV-Pepper analysis or not (non-trivial increase in cost and runtime)"
        dvp_threads: "number of threads for DV-Pepper"
        dvp_memory: "memory for DV-Pepper"
        ref_scatter_interval_list_locator: "A file holding paths to interval_list files; needed only when running DV-Pepper"
        ref_scatter_interval_list_ids: "A file that gives short IDs to the interval_list files; needed only when running DV-Pepper"
    }

    input {
        File bam
        File bai
        Int minsvlen = 50
        String prefix
        String sample_id

        File ref_fasta
        File ref_fasta_fai
        File ref_dict

        Boolean call_svs = true
        Boolean fast_less_sensitive_sv
        File? tandem_repeat_bed

        Boolean call_small_variants

        Boolean run_dv_pepper_analysis = true
        Int? dvp_threads
        Int? dvp_memory
        File? ref_scatter_interval_list_locator
        File? ref_scatter_interval_list_ids

        Boolean call_trs = true
    }

    ######################################################################
    # Block for small variants handling
    ######################################################################

    if (call_small_variants) {
        # size-balanced scatter
        # todo: phasing isn't done for CCS data yet, waiting for Pepper Team to respond
        if (run_dv_pepper_analysis) {
            File scatter_interval_list_ids = select_first([ref_scatter_interval_list_ids])
            File scatter_interval_list_loc = select_first([ref_scatter_interval_list_locator])
            Array[String] interval_list_ids   = read_lines(scatter_interval_list_ids)
            Array[String] interval_list_files = read_lines(scatter_interval_list_loc)
            Array[Pair[String, String]] ided_interval_list_files = zip(interval_list_ids, interval_list_files)

            scatter (pair in ided_interval_list_files) {
                call Utils.ResilientSubsetBam as size_balanced_scatter {
                    input:
                        bam = bam,
                        bai = bai,
                        interval_list_file = pair.right,
                        interval_id = pair.left,
                        prefix = basename(bam, ".bam")
                }

                call CCSPepper.CCSPepper {
                    input:
                        bam           = size_balanced_scatter.subset_bam,
                        bai           = size_balanced_scatter.subset_bai,
                        ref_fasta     = ref_fasta,
                        ref_fasta_fai = ref_fasta_fai,

                        pepper_threads = select_first([dvp_threads]),
                        pepper_memory  = select_first([dvp_memory]),
                        dv_threads = select_first([dvp_threads]),
                        dv_memory  = select_first([dvp_memory])
                }
            }

            String dvp_prefix = prefix + ".deepvariant_pepper"

            call VariantUtils.MergeAndSortVCFs as MergeDeepVariantGVCFs {
                input:
                    vcfs     = CCSPepper.gVCF,
                    prefix   = dvp_prefix + ".g",
                    ref_fasta_fai = ref_fasta_fai
            }

            # todo: phasing VCF could happen here, i.e. on gathered VCFs as that's going to be less intensive
            call VariantUtils.MergeAndSortVCFs as MergeDeepVariantVCFs {
                input:
                    vcfs     = CCSPepper.VCF,
                    prefix   = dvp_prefix,
                    ref_fasta_fai = ref_fasta_fai
            }

            call Utils.MergeBams {
                input:
                    bams = CCSPepper.hap_tagged_bam,
                    prefix = prefix +  ".MARGIN_PHASED.PEPPER_SNP_MARGIN.haplotagged"
            }

            call CCSPepper.MarginPhase {
                input:
                    bam           = bam,
                    bai           = bai,
                    unphased_vcf  = MergeDeepVariantVCFs.vcf,
                    unphased_vcf_tbi = MergeDeepVariantVCFs.tbi,
                    ref_fasta     = ref_fasta,
                    ref_fasta_fai = ref_fasta_fai,
                    memory        = select_first([dvp_memory, 64])
            }
        }
    }

    ######################################################################
    # Block for SV handling
    ######################################################################
    if (call_svs) {
        if (fast_less_sensitive_sv) {

            call Utils.MakeChrIntervalList {
            input:
                ref_dict = ref_dict,
                filter = ['random', 'chrUn', 'decoy', 'alt', 'HLA', 'EBV']
            }

            scatter (c in MakeChrIntervalList.chrs) {
                String contig_for_sv = c[0]

                call Utils.SubsetBam {
                    input:
                        bam = bam,
                        bai = bai,
                        locus = contig_for_sv
                }

                call PBSV.RunPBSV {
                    input:
                        bam = SubsetBam.subset_bam,
                        bai = SubsetBam.subset_bai,
                        ref_fasta = ref_fasta,
                        ref_fasta_fai = ref_fasta_fai,
                        chr = contig_for_sv,
                        prefix = prefix,
                        tandem_repeat_bed = tandem_repeat_bed,
                        is_ccs = true
                }

            }

            call VariantUtils.MergePerChrCalls as MergePBSVVCFs {
                input:
                    vcfs     = RunPBSV.vcf,
                    ref_dict = ref_dict,
                    prefix   = prefix + ".pbsv"
            }

        }

        if (!fast_less_sensitive_sv) {
            call PBSV.RunPBSV as PBSVslow {
                input:
                    bam = bam,
                    bai = bai,
                    ref_fasta = ref_fasta,
                    ref_fasta_fai = ref_fasta_fai,
                    prefix = prefix,
                    tandem_repeat_bed = tandem_repeat_bed,
                    is_ccs = true
            }

            call VariantUtils.ZipAndIndexVCF as ZipAndIndexPBSV {input: vcf = PBSVslow.vcf }
        }

        call Sniffles2.SampleSV as Sniffles2SV {
            input:
                bam    = bam,
                bai    = bai,
                minsvlen = minsvlen,
                sample_id = sample_id,
                prefix = prefix,
                tandem_repeat_bed = tandem_repeat_bed
        }

    }
    ######################################################################
    # Block for TR handling
    ######################################################################
    if (call_trs) {
        call TRGT.runTRGT {
            input:
                input_bam = bam,
                input_bam_bai = bai,
                ref_fasta = ref_fasta,
                ref_fasta_fai = ref_fasta_fai,
                ref_dict = ref_dict
        }
    }

    output {
        File? sniffles_vcf = Sniffles2SV.vcf
        File? sniffles_tbi = Sniffles2SV.tbi
        File? sniffles_snf = Sniffles2SV.snf
        File? pbsv_vcf = select_first([MergePBSVVCFs.vcf, ZipAndIndexPBSV.vcfgz])
        File? pbsv_tbi = select_first([MergePBSVVCFs.tbi, ZipAndIndexPBSV.tbi])

        File? dvp_g_vcf = MergeDeepVariantGVCFs.vcf
        File? dvp_g_tbi = MergeDeepVariantGVCFs.tbi
        File? dvp_vcf = MergeDeepVariantVCFs.vcf
        File? dvp_tbi = MergeDeepVariantVCFs.tbi
        File? dvp_phased_vcf = MarginPhase.phasedVCF
        File? dvp_phased_tbi = MarginPhase.phasedtbi

        File? trgt_vcf = runTRGT.trgt_output_vcf
        File? trgt_bam = runTRGT.trgt_output_bam
    }
}
