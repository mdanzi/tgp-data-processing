version 1.0

import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"
workflow RunPBSV {

    meta {
        description: "Run PBSV to call SVs from a BAM file."
    }

    parameter_meta {
        bam:               "input BAM from which to call SVs"
        bai:               "index accompanying the BAM"
        is_ccs:            "if input BAM is CCS reads"
        ref_fasta:         "reference to which the BAM was aligned to"
        ref_fasta_fai:     "index accompanying the reference"
        prefix:            "prefix for output"
        zones:             "zones to run in"
        tandem_repeat_bed: "BED file containing TRF finder results (e.g. http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.trf.bed.gz)"
    }

    input {
        File bam
        File bai
        Boolean is_ccs

        File ref_fasta
        File ref_fasta_fai
        String prefix

        String? chr
        String? zones = "None" # defunct, but kept for compatibility
        File? tandem_repeat_bed
    }

    call Discover {
        input:
            bam               = bam,
            bai               = bai,
            ref_fasta         = ref_fasta,
            ref_fasta_fai     = ref_fasta_fai,
            chr               = chr,
            tandem_repeat_bed = tandem_repeat_bed,
            prefix            = prefix,
            zones             = zones
    }

    call Call {
        input:
            svsigs        = [ Discover.svsig ],
            ref_fasta     = ref_fasta,
            ref_fasta_fai = ref_fasta_fai,
            ccs           = is_ccs,
            prefix        = prefix,
            zones         = zones
    }

    output {
        File vcf = Call.vcf
    }
}

task Discover {
    input {
        File bam
        File bai
        File ref_fasta
        File ref_fasta_fai
        File? tandem_repeat_bed
        String? chr
        String prefix
        String? zones
        RuntimeAttr? runtime_attr_override
    }

    parameter_meta {
        bam:               "input BAM from which to call SVs"
        bai:               "index accompanying the BAM"
        ref_fasta:         "reference to which the BAM was aligned to"
        ref_fasta_fai:     "index accompanying the reference"
        tandem_repeat_bed: "BED file containing TRF finder (e.g. http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.trf.bed.gz)"
        chr:               "chr on which to call variants"
        prefix:            "prefix for output"
    }

    Int MINIMAL_DISK = 500
    Boolean is_big_bam = size(bam, "GB") > 100
    Int inflation_factor = if (is_big_bam) then 5 else 2

    String fileoutput = if defined(chr) then "~{prefix}.~{chr}.svsig.gz" else "~{prefix}.svsig.gz"

    command <<<
        set -euxo pipefail

        pbsv discover \
            ~{if defined(tandem_repeat_bed) then "--tandem-repeats ~{tandem_repeat_bed}" else ""} \
            ~{bam} \
            ~{fileoutput}
    >>>

    output {
        File svsig = "~{fileoutput}"
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          if(defined(chr)) then 8 else 32,
        mem_gb:             if(defined(chr)) then 32 else 128,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-sv:0.1.8",
        queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/priority-gwfcore4"

    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
        queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
    }
}

task Call {
    input {
        Array[File] svsigs
        File ref_fasta
        File ref_fasta_fai
        Boolean ccs
        String prefix
        String? zones
        RuntimeAttr? runtime_attr_override
    }

    parameter_meta {
        svsigs:            "per-chromosome *.svsig.gz files"
        ref_fasta:         "reference to which the BAM was aligned to"
        ref_fasta_fai:     "index accompanying the reference"
        ccs:               "use optimizations for CCS data"
        prefix:            "prefix for output"
    }

    command <<<
        set -euxo pipefail

        num_core=$(cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l)

        pbsv call -j $num_core --log-level INFO ~{true='--ccs' false='' ccs} \
            ~{ref_fasta} \
            ~{sep=' ' svsigs} \
            ~{prefix}.pbsv.pre.vcf

        cat ~{prefix}.pbsv.pre.vcf | grep -v -e '##fileDate' > ~{prefix}.pbsv.vcf
    >>>

    output {
        File vcf = "~{prefix}.pbsv.vcf"
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          4,
        mem_gb:             96,
        max_retries:        0,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-sv:0.1.8"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }
}

