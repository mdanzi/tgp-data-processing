version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"
task alignPB {
  input {
    Array[String] Fastqs
    String sampleGenomicID
    String sampleRunID
    Int? numCPUs=16

    File ref_fasta="s3://tgp-data-analysis/ref/hs37d5.fa"
    File ref_fasta_fai="s3://tgp-data-analysis/ref/hs37d5.fa.fai"
    File ref_fasta_dict="s3://tgp-data-analysis/ref/hs37d5.dict"

    String map_preset="CCS"
    Boolean drop_per_base_N_pulse_tags=true
    String median_filter = if map_preset == "SUBREAD" then "--median-filter" else ""
    String extra_options = if drop_per_base_N_pulse_tags then " --strip " else ""

    RuntimeAttr? runtime_attr_override

  }

  meta {
    description: "Aligns a set of fastq files to the GRCh37 reference genome."
  }

  parameter_meta {
    Fastqs: "File or array of files with the fastq reads, can optionally be compressed with gzip or bzip2."
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR
    
    # re-implementation of the fastq_compression function -- can be moved later if that gets updated to cromwell-friendly version
    firstFile=~{Fastqs[0]}
    extension=${firstFile##*.}
    fastqExtension=""
    if [[ $extension = "bz2" || $extension = "gz" ]]; then fastqExtension=$(echo ".${extension}"); fi

    reads="~{sampleGenomicID}.fq${fastqExtension}"

    # re-implementation of the s3_read_concat function -- can be moved later if that gets updated to cromwell-friendly version
    # Outputs a concatenated stream of all files listed in the input array
    s3_read_concat_cromwell() {
        for s3file in $1; do
            /usr/local/aws-cli/v2/current/dist/aws s3 cp --only-show-errors "$s3file" -
        done
    }
    s3_read_concat_cromwell "~{sep=' ' Fastqs}" > ${reads}
    wait 

    # note, the Read Group is written to match the style and defaults used by Picard AddOrReplaceReadGroups
    readGroup="@RG\\tID:1\\tSM:~{sampleGenomicID}\\tPL:PBCCS\\tLB:lib1\\tPU:unit1"

    # align reads
    pbmm2 align ~{ref_fasta} ${reads} ~{sampleGenomicID}.pre.bam --preset ~{map_preset} ~{median_filter} --sample ~{sampleGenomicID} ~{extra_options} --sort --unmapped
    samtools calmd -b --no-PG ~{sampleGenomicID}.pre.bam ~{ref_fasta} > ~{sampleGenomicID}.bam
    # index reads
    samtools index ~{sampleGenomicID}.bam
    pbindex ~{sampleGenomicID}.bam


  >>>

  output {
    File aligned_bam = "~{sampleGenomicID}.bam"
    File aligned_bai = "~{sampleGenomicID}.bam.bai"
    File aligned_pbi = "~{sampleGenomicID}.bam.pbi"
    }

  RuntimeAttr default_attr = object {
      cpu_cores:          numCPUs,
      mem_gb:             96,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-pb:0.1.40",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/priority-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
}


task Minimap2 {
    input {
        Array[File] reads
        File ref_fasta

        String sampleGenomicID
        String sampleRunID

        String RG = "@RG\\tID:1\\tSM:~{sampleGenomicID}\\tPL:ONT\\tPU:unknown\\tDT:2021-01-01T12:00:00.000000-05:00"
        String map_preset
        Array[String] tags_to_preserve = []

        String prefix = sampleGenomicID
        RuntimeAttr? runtime_attr_override
    }
    meta {
        descrpiton: "A wrapper to minimap2 for mapping & aligning (groups of) sequences to a reference"
    }
    parameter_meta {
        reads:            "query sequences to be mapped and aligned"
        ref_fasta:        "reference fasta"
        RG:               "read group information to be supplied to parameter '-R' (note that tabs should be input as '\t')"
        map_preset:       "preset to be used for minimap2 parameter '-x'"
        tags_to_preserve: "sam tags to carry over to aligned bam file"
        prefix:           "[default-valued] prefix for output BAM"
    }

    Boolean do_preserve_tags = if length(tags_to_preserve) != 0 then true else false

    Int cpus = 4
    Int mem = 30

    command <<<
        set -euxo pipefail

        NUM_CPUS=$( cat /proc/cpuinfo | grep '^processor' | tail -n1 | awk '{print $NF+1}' )
        RAM_IN_GB=$( free -g | grep "^Mem" | awk '{print $2}' )
        MEM_FOR_SORT=$( echo "" | awk "{print int(($RAM_IN_GB - 1)/$NUM_CPUS)}" )

        rg_len=$(echo -n '~{RG}' | wc -c | awk '{print $NF}')
        if [[ $rg_len -ne 0 ]] ; then
            MAP_PARAMS="-ayYL --MD --eqx -x ~{map_preset} -R ~{RG} -t ${NUM_CPUS} ~{ref_fasta}"
        else
            MAP_PARAMS="-ayYL --MD --eqx -x ~{map_preset} -t ${NUM_CPUS} ~{ref_fasta}"
        fi

        SORT_PARAMS="-@${NUM_CPUS} -m${MEM_FOR_SORT}G --no-PG -o ~{prefix}.pre.bam"
        FILE="~{reads[0]}"
        FILES="~{sep=' ' reads}"

        # We write to a SAM file before sorting and indexing because rarely, doing everything
        # in a single one-liner leads to a truncated file error and segmentation fault of unknown
        # origin.  Separating these commands requires more resources, but is more reliable overall.

        if [[ "$FILE" =~ \.fastq$ ]] || [[ "$FILE" =~ \.fq$ ]]; then
            cat $FILES | minimap2 $MAP_PARAMS - > tmp.sam
        elif [[ "$FILE" =~ \.fastq.gz$ ]] || [[ "$FILE" =~ \.fq.gz$ ]]; then
            zcat $FILES | minimap2 $MAP_PARAMS - > tmp.sam
        elif [[ "$FILE" =~ \.fasta$ ]] || [[ "$FILE" =~ \.fa$ ]]; then
            cat $FILES | python3 /usr/local/bin/cat_as_fastq.py | minimap2 $MAP_PARAMS - > tmp.sam
        elif [[ "$FILE" =~ \.fasta.gz$ ]] || [[ "$FILE" =~ \.fa.gz$ ]]; then
            zcat $FILES | python3 /usr/local/bin/cat_as_fastq.py | minimap2 $MAP_PARAMS - > tmp.sam
        elif [[ "$FILE" =~ \.bam$ ]]; then

            # samtools fastq takes only 1 file at a time so we need to merge them together:
            for f in "~{sep=' ' reads}" ; do
                if ~{do_preserve_tags} ; then
                    samtools fastq -T  ~{sep=',' tags_to_preserve} "$f"
                else
                    samtools fastq "$f"
                fi
            done > tmp.fastq

            echo "Memory info:"
            cat /proc/meminfo
            echo ""

            if ~{do_preserve_tags} ; then
                minimap2 ${MAP_PARAMS} -y tmp.fastq > tmp.sam
            else
                minimap2 ${MAP_PARAMS} tmp.fastq > tmp.sam
            fi
        else
            echo "Did not understand file format for '$FILE'"
            exit 1
        fi

        samtools sort ${SORT_PARAMS} tmp.sam

        samtools calmd -b --no-PG ~{prefix}.pre.bam ~{ref_fasta} > ~{prefix}.bam
        samtools index -@${NUM_CPUS} ~{prefix}.bam

    >>>

    output {
        File aligned_bam = "~{prefix}.bam"
        File aligned_bai = "~{prefix}.bam.bai"
    }

    #########################
    RuntimeAttr default_attr = object {
        cpu_cores:          cpus,
        mem_gb:             mem,
        max_retries:        2,
        docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-align:0.1.28"
    }
    RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
    runtime {
        cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
        memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
        maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
        docker:                 select_first([runtime_attr.docker,            default_attr.docker])
    }
}
