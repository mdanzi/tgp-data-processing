version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task bamToFastq {
  input {
    File bam
    String sampleGenomicID
    String sampleRunID
    RuntimeAttr? runtime_attr_override
  }


  parameter_meta {
    bam: "Input bam file"
    sampleGenomicID: "Internal ID to be used for this sample."
    sampleRunID: "Internal ID to be used for this run of this workflow."
  }

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    samtools collate -f --output-fmt BAM -r 100000 -o ~{sampleGenomicID}_collated.bam ~{bam}
    samtools fastq -c 4 -1 ~{sampleGenomicID}_R1.fastq.gz -2 ~{sampleGenomicID}_R2.fastq.gz -0 /dev/null -s /dev/null ~{sampleGenomicID}_collated.bam
    aws s3 cp ~{sampleGenomicID}_R1.fastq.gz s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
    aws s3 cp ~{sampleGenomicID}_R2.fastq.gz s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
    mv ~{sampleGenomicID}_R1.fastq.gz /tmp/scratch/
    mv ~{sampleGenomicID}_R2.fastq.gz /tmp/scratch/

  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          2,
      mem_gb:             7,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File R1Fastq = "~{sampleGenomicID}_R1.fastq.gz"
    File R2Fastq = "~{sampleGenomicID}_R2.fastq.gz"
  }
}

task StopWorkflow {

    meta {
        description: "Utility to stop a workflow"
    }

    parameter_meta {
        reason: "reason for stopping"
    }

    input {
        String reason
    }
    command <<<
        echo -e "Workflow explicitly stopped because \n  ~{reason}." && exit 1
    >>>
    runtime {docker: "179757815329.dkr.ecr.us-east-1.amazonaws.com/lr-basic:0.1.2"}
}
