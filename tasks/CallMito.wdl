version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/tasks/MitoAlignAndCall.wdl" as MitoAlignAndCall
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

workflow callMitoVariants {

  meta {
    description: "Takes in an hg19 bam and outputs VCF of SNP/Indel calls on the mitochondria."
    allowNestedInputs: true
  }

  input {
    String input_bam
    String input_bam_bai
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    String contig_name = "MT"
    Boolean compress_output_vcf = false

    RuntimeAttr? runtime_attr_override_SubsetBamToChrM
    RuntimeAttr? runtime_attr_override_RevertSam
    RuntimeAttr? runtime_attr_override_CoverageAtEveryBase
    RuntimeAttr? runtime_attr_override_SplitMultiAllelicSites
    RuntimeAttr? runtime_attr_override_AlignToMt
    RuntimeAttr? runtime_attr_override_AlignToShiftedMt
    RuntimeAttr? runtime_attr_override_CollectWgsMetrics
    RuntimeAttr? runtime_attr_override_CallMt
    RuntimeAttr? runtime_attr_override_CallShiftedMt
    RuntimeAttr? runtime_attr_override_LiftoverAndCombineVcfs
    RuntimeAttr? runtime_attr_override_MergeStats
    RuntimeAttr? runtime_attr_override_InitialFilter
    RuntimeAttr? runtime_attr_override_SplitMultiAllelicsAndRemoveNonPassSites
    RuntimeAttr? runtime_attr_override_GetContamination
    RuntimeAttr? runtime_attr_override_FilterContamination


  }

  parameter_meta {
    input_bam: "Full WGS hg19 bam"
    out_vcf: "Final VCF of mitochondrial SNPs and INDELs"
    vaf_filter_threshold: "Hard threshold for filtering low VAF sites"
    f_score_beta: "F-Score beta balances the filtering strategy between recall and precision. The relative weight of recall to precision."
    contig_name: "Name of mitochondria contig in reference that input_bam is aligned to"
  }

  call SubsetBamToChrM {
    input:
      input_bam = input_bam,
      input_bai = input_bam_bai,
      contig_name = contig_name,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_SubsetBamToChrM
  }

  call RevertSam {
    input:
      input_bam = SubsetBamToChrM.output_bam,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_RevertSam
  }

  call MitoAlignAndCall.AlignAndCall as AlignAndCall {
    input:
      unmapped_bam = RevertSam.unmapped_bam,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override_AlignToMt = runtime_attr_override_AlignToMt,
      runtime_attr_override_AlignToShiftedMt = runtime_attr_override_AlignToShiftedMt,
      runtime_attr_override_CollectWgsMetrics = runtime_attr_override_CollectWgsMetrics,
      runtime_attr_override_CallMt = runtime_attr_override_CallMt,
      runtime_attr_override_CallShiftedMt = runtime_attr_override_CallShiftedMt,
      runtime_attr_override_LiftoverAndCombineVcfs = runtime_attr_override_LiftoverAndCombineVcfs,
      runtime_attr_override_MergeStats = runtime_attr_override_MergeStats,
      runtime_attr_override_InitialFilter = runtime_attr_override_InitialFilter,
      runtime_attr_override_SplitMultiAllelicsAndRemoveNonPassSites = runtime_attr_override_SplitMultiAllelicsAndRemoveNonPassSites,
      runtime_attr_override_GetContamination = runtime_attr_override_GetContamination,
      runtime_attr_override_FilterContamination = runtime_attr_override_FilterContamination
  }

  # This is a temporary task to handle "joint calling" until Mutect2 can produce a GVCF.
  # This proivdes coverage at each base so low coverage sites can be considered ./. rather than 0/0.
  call CoverageAtEveryBase {
    input:
      input_bam_regular_ref = AlignAndCall.mt_aligned_bam,
      input_bam_regular_ref_index = AlignAndCall.mt_aligned_bai,
      input_bam_shifted_ref = AlignAndCall.mt_aligned_shifted_bam,
      input_bam_shifted_ref_index = AlignAndCall.mt_aligned_shifted_bai,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_CoverageAtEveryBase
  }
  
  call SplitMultiAllelicSites {
    input:
      input_vcf = AlignAndCall.out_vcf,
      input_vcf_index = AlignAndCall.out_vcf_index,
      sampleGenomicID = sampleGenomicID,
      sampleRunID = sampleRunID,
      gatk_path = gatk_path,
      runtime_attr_override = runtime_attr_override_SplitMultiAllelicSites
  }

  output {
    File mito_vcf = SplitMultiAllelicSites.split_vcf
    File mito_vcf_index = SplitMultiAllelicSites.split_vcf_index
  }
}

task SubsetBamToChrM {
  input {
    String input_bam
    String input_bai
    String contig_name
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Subsets a whole genome bam to just Mitochondria reads"
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam} . --only-show-errors
    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bai} . --only-show-errors

    ~{gatk_path} PrintReads \
      -R ref/hs37d5.fa \
      -L ~{contig_name} \
      --read-filter MateOnSameContigOrNoMappedMateReadFilter \
      --read-filter MateUnmappedAndUnmappedReadFilter \
      -I ~{input_bam} \
      --read-index ~{input_bai} \
      -O /tmp/scratch/~{sampleGenomicID}.bam
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             7,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File output_bam = "~{sampleGenomicID}.bam"
    File output_bai = "~{sampleGenomicID}.bai"
  }
}

task RevertSam {
  input {
    File input_bam
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Removes alignment information while retaining recalibrated base qualities and original alignment tags"
  }
  parameter_meta {
    input_bam: "aligned bam"
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path} --java-options "-Xmx1000m" RevertSam \
      --INPUT ~{input_bam} \
      --OUTPUT_BY_READGROUP false \
      --OUTPUT /tmp/scratch/~{sampleGenomicID}.bam \
      --VALIDATION_STRINGENCY LENIENT \
      --ATTRIBUTE_TO_CLEAR FT \
      --ATTRIBUTE_TO_CLEAR CO \
      --SORT_ORDER queryname \
      --RESTORE_ORIGINAL_QUALITIES false
  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             2,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File unmapped_bam = "~{sampleGenomicID}.bam"
  }
}

task CoverageAtEveryBase {
  input {
    File input_bam_regular_ref
    File input_bam_regular_ref_index
    File input_bam_shifted_ref
    File input_bam_shifted_ref_index
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override
  }

  meta {
    description: "Remove this hack once there's a GVCF solution."
  }
  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path} CollectHsMetrics \
      -I ~{input_bam_regular_ref} \
      -R ref/mitochondria/hs37d5.MT.fa \
      --PER_BASE_COVERAGE non_control_region.tsv \
      -O non_control_region.metrics \
      -TI ref/mitochondria/non_control_region.MT.interval_list \
      -BI ref/mitochondria/non_control_region.MT.interval_list \
      -covMax 20000 \
      --SAMPLE_SIZE 1

    ~{gatk_path} CollectHsMetrics \
      -I ~{input_bam_shifted_ref} \
      -R ref/mitochondria/hs37d5.MT.shifted_by_8000_bases.fa \
      --PER_BASE_COVERAGE control_region_shifted.tsv \
      -O control_region_shifted.metrics \
      -TI ref/mitochondria/control_region_shifted.MT.interval_list \
      -BI ref/mitochondria/control_region_shifted.MT.interval_list \
      -covMax 20000 \
      --SAMPLE_SIZE 1

    set +u 
    conda activate gatk
    set -u
    R --vanilla <<CODE
      shift_back = function(x) {
        if (x < 8570) {
          return(x + 8000)
        } else {
          return (x - 8569)
        }
      }

      control_region_shifted = read.table("control_region_shifted.tsv", header=T)
      shifted_back = sapply(control_region_shifted[,"pos"], shift_back)
      control_region_shifted[,"pos"] = shifted_back

      beginning = subset(control_region_shifted, control_region_shifted[,'pos']<8000)
      end = subset(control_region_shifted, control_region_shifted[,'pos']>8000)

      non_control_region = read.table("non_control_region.tsv", header=T)
      combined_table = rbind(beginning, non_control_region, end)
      write.table(combined_table, "per_base_coverage.tsv", row.names=F, col.names=T, quote=F, sep="\t")

    CODE
    mv per_base_coverage.tsv /tmp/scratch/~{sampleGenomicID}_MT_per_base_coverage.tsv
    aws s3 cp /tmp/scratch/~{sampleGenomicID}_MT_per_base_coverage.tsv s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors

  >>>
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             2,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
  output {
    File table = "~{sampleGenomicID}_MT_per_base_coverage.tsv"
  }
}

task SplitMultiAllelicSites {
  input {
    File input_vcf
    File input_vcf_index
    String sampleGenomicID
    String sampleRunID
    String gatk_path
    RuntimeAttr? runtime_attr_override

  }

  String output_vcf = sampleGenomicID + ".MT.final.split.vcf"
  String output_vcf_index = output_vcf + ".idx"

  command <<<
    cd /home/ec2-user
    source .bashrc
    set -euo pipefail
    git clone https://gitlab.com/mdanzi/tgp-data-processing.git
    source tgp-data-processing/scripts/utils/utils.sh  # imports errorReport
    trap "errorReport sample ~{sampleRunID}" ERR

    ~{gatk_path} LeftAlignAndTrimVariants \
      -R ref/hs37d5.fa \
      -V ~{input_vcf} \
      -O ~{output_vcf} \
      --split-multi-allelics \
      --dont-trim-alleles \
      --keep-original-ac
    
    aws s3 cp ~{output_vcf} s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
    aws s3 cp ~{output_vcf_index} s3://tgp-sample-processing/~{sampleRunID}/ --only-show-errors
    mv ~{output_vcf} /tmp/scratch/
    mv ~{output_vcf_index} /tmp/scratch/

  >>>
  output {
    File split_vcf = "~{output_vcf}"
    File split_vcf_index = "~{output_vcf}"
  }
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          1,
      mem_gb:             1,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/tgp:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/default-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }
}
