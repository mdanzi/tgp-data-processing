version 1.0
import "https://gitlab.com/mdanzi/tgp-data-processing/-/raw/main/structs.wdl"

task callSVs {
  input {
    String input_bam
    String input_bam_bai
    String sampleGenomicID
    String sampleRunID
    File ref_fasta = "s3://tgp-data-analysis/ref/hs37d5.fa"
    File ref_fasta_idx = "s3://tgp-data-analysis/ref/hs37d5.fa.fai"
    RuntimeAttr? runtime_attr_override

  }

  command {
    # Note that this docker image uses an old version of the aws-cli with slightly different syntax. 
    # I also have not been able to get the errorReport to work on this system. 

    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam} /home/dnanexus/in/ --region us-east-1 --quiet
    aws s3 cp s3://tgp-sample-processing/~{sampleRunID}/~{input_bam_bai} /home/dnanexus/in/  --region us-east-1 --quiet

    cp ~{ref_fasta} /home/dnanexus/in/
    cp ~{ref_fasta_idx} /home/dnanexus/in/

    set +euo pipefail

    /home/dnanexus/parliament2.py \
        --bam ~{input_bam} \
        --bai ~{input_bam_bai} \
        --r hs37d5.fa \
        --fai hs37d5.fa.fai \
        --prefix ~{sampleGenomicID} \
        --filter_short_contigs --breakdancer --breakseq --manta --cnvnator --lumpy \
        --delly_deletion --delly_insertion --delly_inversion --delly_duplication --genotype || true


    set -euo pipefail 
    
    # fix chromosome naming in output vcf
    sed 's/^chr//' /home/dnanexus/out/~{sampleGenomicID}.combined.genotyped.vcf > ~{sampleGenomicID}.combined.genotyped.vcf

    # upload output to S3
    aws s3 cp ~{sampleGenomicID}.combined.genotyped.vcf s3://tgp-sample-processing/~{sampleRunID}/  --region us-east-1 --quiet

  }
  #########################
  RuntimeAttr default_attr = object {
      cpu_cores:          8,
      mem_gb:             32,
      max_retries:        0,
      docker:             "179757815329.dkr.ecr.us-east-1.amazonaws.com/parliament2:latest",
      queueArn:           "arn:aws:batch:us-east-1:179757815329:job-queue/priority-gwfcore4"
  }
  RuntimeAttr runtime_attr = select_first([runtime_attr_override, default_attr])
  runtime {
      cpu:                    select_first([runtime_attr.cpu_cores,         default_attr.cpu_cores])
      memory:                 select_first([runtime_attr.mem_gb,            default_attr.mem_gb]) + " GiB"
      maxRetries:             select_first([runtime_attr.max_retries,       default_attr.max_retries])
      docker:                 select_first([runtime_attr.docker,            default_attr.docker])
      queueArn:               select_first([runtime_attr.queueArn,          default_attr.queueArn])
  }

}
